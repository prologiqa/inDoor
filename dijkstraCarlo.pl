:- module(dijkstraCarlo, [dijkstra/3]).

/*  File:    dijkstraCarlo.pl
 *  Author: Kili�n Imre, 2021-22-24...
 *  taken over from:
 *        - Carlo, created an initial Dijkstra algorithm implementation
 *          (Purpose: learn graph programming with attribute variables
 *           https://copyprogramming.com/howto/dijkstras-shortest-path-algorithm)

    Modified by Kili�n Imre P�cs, Hungary
    - 28-Aug-2021. adaptation to node-edgelist-weight representation
    - 25-Jan-2024. interface between dijkstra and wgraph
*/

dijkstra(Graph, Start, Solution) :-
    setof(FROM, TOL^member(FROM-TOL, Graph), NODES),
    length(NODES, L),
    length(Vars, L),
    aggregate_all(sum(W), (member(FROM-TOL, Graph),
                           member(_Y-W,TOL)),Infinity),
    catch((algo(Graph, Infinity, NODES, Vars, Start, Solution),
           throw(sol(Solution))
          ), sol(Solution), true).


algo(Graph, Infinity, Nodes, Vars, Start, Solution) :-
    pairs_keys_values(PAIRS, Nodes, Vars),
    maplist(init_adjs(PAIRS), Graph),
    maplist(init_dist(Infinity), PAIRS),
    memberchk(Start-SVAR, PAIRS),
    put_attr(SVAR, dist, 0),
    time(main_loop(Vars)),
    maplist(solution(Start), Vars, Solution).

solution(Start, VAR, s(NAME, DIST, [Start|PATH])) :-
    get_attr(VAR, name, NAME),
    get_attr(VAR, dist, DIST),
    rpath(VAR, [], PATH).

rpath(VAR, PATH0, PATH) :-
    get_attr(VAR, name, NAME),
    (   get_attr(VAR, previous, PREVNODE)
    ->  rpath(PREVNODE, [NAME|PATH0], PATH)
    ;   PATH = PATH0
    ).

init_dist(Infinity, NAME-VAR) :-
    put_attr(VAR, name, NAME),
    put_attr(VAR, dist, Infinity).

%PAIRS: list of NODE-VAR pairs (VAR-s are attributed)
%neighbour list is attached adjs:[ADJ-DIST|LIST]
init_adjs(_, _-[]) :- !.
init_adjs(PAIRS, FROM-[TO-W|TOS]) :-
    memberchk(FROM-FVAR,PAIRS), memberchk(TO-TOVAR,PAIRS),
    adj_add(FVAR, TOVAR, W),adj_add(TOVAR, FVAR, W),
    init_adjs(PAIRS,FROM-TOS).

adj_add(X, Y, D) :-
    (   get_attr(X, adjs, L)
    ->  put_attr(X, adjs, [Y-D|L])
    ;   put_attr(X, adjs, [Y-D])
    ).

main_loop([]).
main_loop([Q|Qs]) :- %not yet processed nodes
    smallest_distance(Qs, Q, U, Qn),
    put_attr(U, assigned, true),
    get_attr(U, adjs, TOS),
    update_neighbours(TOS, U),
    main_loop(Qn).

smallest_distance([A|Qs], C, M, [T|Qn]) :-
    get_attr(A, dist, Av),
    get_attr(C, dist, Cv),
    (   Av < Cv
    ->  (N,T) = (A,C)
    ;   (N,T) = (C,A)
    ),
    !, smallest_distance(Qs, N, M, Qn).
smallest_distance([], U, U, []).

update_neighbours([V-Duv|Vs], U) :-
    (   get_attr(V, assigned, true)
    ->  true
    ;   get_attr(U, dist, Du),
        get_attr(V, dist, Dv),
        Alt is Du + Duv,
        (   Alt < Dv
        ->  put_attr(V, dist, Alt),
        put_attr(V, previous, U)
        ;   true
        )
    ),
    update_neighbours(Vs, U).
update_neighbours([], _).



:- begin_tests(dijkstraCarlo).

test(dijkstra,PATHS=[s(a,0,[a]), s(b,1,[a,b]),
                     s(c,2,[a,b,c]), s(d,2,[a,d])]):-
    nl,
    time(dijkstra([a-[b-1,d-2],b-[c-1],c-[d-1],d-[]], a, PATHS)).


:- if(false).
test(2) :-
    open('salesman.pl', read, F),
    readf(F, L),
    close(F),
    nl,
    dijkstra(L, penzance, R),
    maplist(writeln, R).

readf(F, [d(X,Y,D)|R]) :-
    read(F, dist(X,Y,D)), !, readf(F, R).
readf(_, []).
:- endif.

:- end_tests(dijkstraCarlo).






