:- module(planar,[
                 corners/3,
                 createNavi/2,     %creates data structures for navigation
                 pathNavi/7,
                 genNaviGraph/5,
                 checkNaviGraph/0,
                 floors/3, rooms/4, rooms/2,
                 room2Polygon/3, centerRoom/3,
                 sameWallDoors/6, wallsMinMax/7
                 ]).
%
%planar graph operations / operations on the wall-graph
%Created by: Kili�n Imre, 30-July-2021
%
% Graph nodes: door(DOOR,_); room(ROOM,_); stair(FL/ID,_) structures
%              - door(DOOR,XY) XY: geometrically the middle point of
%              DOOR is the given ID of the door
%              - room(ROOM,XY) XY: geometrically the center point
%              of a ROOM (Use the empty variables!)
%              - stair(FL/ID,XY): where FL: floor ID, ID: stair ID
%              XY: geometrical middle point of last stair step
%
% Graph arcs:  rooms are reachable from their doors and vica versa
%              (warning, one door belongs to two rooms!)
%              doors are reachable directly from the doors of the same
%              room... (For long corridors walking along the longer axis
%              is to be considered!)
%
%:- pathNavi(room(a101,_),room(elevator,_),P,W).
%

:- use_module(listExt).

listToSection([TO|_],TO,[TO]):- !.
listToSection([X|T],TO,[X|TT]):-
    listToSection(T,TO,TT).

listSection([FR|T],FR,TO,[FR|TT]):- !,
    listToSection(T,TO,TT).
listSection([_|T],FR,TO,TT):-
    listSection(T,FR,TO,TT).

listXSection([FR|T],FR,TO,[FR|TT]):- !,
    listToSection(T,TO,TT).
listXSection([TO|T],FR,TO,[TO|TT]):- !,
    listToSection(T,FR,TT).
listXSection([_|T],FR,TO,TT):-
    listXSection(T,FR,TO,TT).


undirectedEdge(FROM-TO:W,ALLEDGES):-
    member(FROM-TO:W,ALLEDGES);member(TO-FROM:W,ALLEDGES).


%path(+WGRAPH,-VISITED,-PATH,?SUM)/is nondet.
%path(+WGRAPH,-VISITED.+PATH,?SUM)/is det.
path(_,[],[],0):- ! .
path(ALLEDGES,[EDGE0|VISITED],[EDGE|PATH],SUM):-
    ground([EDGE|PATH]), !,
    once((undirectedEdge(EDGE,ALLEDGES), \+ member(EDGE,[EDGE0|VISITED]))),
    path(ALLEDGES,VISITED,[PATH],SUM).


floors(DATA,BUILDING,FLOORS):-
  setof(FL,DATA:K^L^S^R^room(BUILDING/FL/R,K,L,S),FLOORS).

rooms(DATA,B,FL,ALLROOMS):-
  setof(R,DATA:K^L^S^R^room(B/FL/R,K,L,S),ALLROOMS).

rooms(DATA,ALLROOMS):-
  setof(R,DATA:B^FL^K^L^S^R^room(B/FL/R,K,L,S),ALLROOMS).


corners(DATA,CORNERS,POINTS):-
    maplist(DATA:corner,CORNERS,POINTS).

:-if(false).
sumSideCorners(DATA,SIDES,XS/YS,N):-
    aggregate_all(r(sum(X),sum(Y),count),WALL,
                  (member(side(WALL,CORNER1,_CORNER2),SIDES),
                         DATA:corner(CORNER1,X/Y)),
                 r(XS,YS,N)).
:-endif.

sides2Polygon(DATA,SIDES,POLYGON):-
    findall(X/Y,(member(side(_WALL,CORNER1,_CORNER2),SIDES),
                 DATA:corner(CORNER1,X/Y)),POLYGON).

%called directly from display.pl
room2Polygon(DATA,BFLR,POLYGON):-
  DATA:room(BFLR,_,_,SIDES), sides2Polygon(DATA,SIDES,POLYGON).


centerRoom(DATA,BFLROOM,CENTROID):-
  DATA:room(BFLROOM,_KIND,_LEVEL,SIDES),
  %sumSideCorners(SIDES,XS/YS,N), X is XS/N, Y is YS/N.
  sides2Polygon(DATA,SIDES,POLYGON), centroidPolygon(POLYGON,CENTROID).

:-if(false).
corner2Middle(DATA,C0,C1,MIDDLE):-
  DATA:corner(C0,XY0), DATA:corner(C1,XY1), middleSection(XY0,XY1,MIDDLE).
:-endif.

corner2MiddleDist(DATA,C0,C1,DISTANCE,MIDDLE):-
  DATA:corner(C0,XY0), DATA:corner(C1,XY1),
  orthogonalShift(XY0-XY1,DISTANCE,XY01-XY11),
  middleSection(XY01,XY11,MIDDLE).

:- op(1,fx,@).

:- use_module(wgraphs).
:- use_module(geometry).
%:- use_module(data).
:- use_module(model).
:- use_module(display).
:- use_module(xpceFace).


:- if(fail).
:- dynamic naviGraph/1.

setNaviGraph(GRAPH):-
    retractall(naviGraph(_)), assert(naviGraph(GRAPH)).
:- else.
%nb_getval(naviGraph,GR).
naviGraph(GRAPH):- nb_getval(naviGraph,GRAPH).

setNaviGraph(GRAPH):-
    nb_setval(naviGraph,GRAPH).
:- endif.

corridorGoingDistance(50).


sectionProps(THICK/LENGTH,THICK,LENGTH,wall).
sectionProps(door(D,TH,LENGTH),TH,LENGTH,door(D,TH)).
sectionProps(THICK,THICK,_,wall):- number(THICK).


sectionProps(DATA,C1,C2,SECTION,THICK,LENGTH,KIND):-
  once(sectionProps(SECTION,THICK,LENGTH,KIND)),
  (nonvar(LENGTH)->true;
  DATA:corner(C1,XY1), DATA:corner(C2,XY2), point2Distance(XY1,XY2,LENGTH)).

sectionSize(XY0,XY,SECTION,LENGTH):-
  once(sectionProps(SECTION,_THICK,LENGTH,_KIND)),
  (nonvar(LENGTH)-> true;
  point2Distance(XY0,XY,LENGTH)).


%building upon SWI's ugraph package and data representation
%:- use_module(library(ugraphs)).


% the next node is linearly extrapolated,
% known the starting point, the actual node
% and the length of the edge
%!nextLineNode(DATA,XY00,LENGTH,XY0,L,XYN,LONGER) is det.
%+DATA:    data context (a Prolog module)
%+XY00:   the absolute starting point of the line
%+LENGTH: the length of the line up to the actual point
%+XY0:    the actual point
%+LS:      the length of the actual section
%-XYN:    the calculated endpoint of the section
%+CORNERS:the list of corners of the wall (in case
%         of XY00=XY0 sizes are calculated by using the last node
%+LONGER: the sum of lengths up to XYN
nextLineNode(DATA,X00/Y00,LENGTH,X0/Y0,LS,XN/YN,CORNERS,LONGER):-
    LENGTH>0->XN is LS/LENGTH*(X0-X00)+X0, YN is LS/LENGTH*(Y0-Y00)+Y0,
      LONGER is LENGTH+LS;
    last(CORNERS,LAST), DATA:corner(LAST,XL/YL),
      point2Distance(X00/Y00,XL/YL,TLENGTH),
      XN is LS/TLENGTH*(XL-X00)+X0, YN is LS/TLENGTH*(YL-Y00)+Y0,
      LONGER is LENGTH+LS.


%nextLineEdgeStartFound/12
%Returns all the coming section properties in a backtracking manner
%until the END condition (the node) is met.
%(Doesn't search for starting node any more.)
%
nextLineEdgeStartFound(DATA,[C0,C|CORNERS],REMCS,[S|SECTIONS],REMSS,
             END,XY00,XY0,LENGTH,SECTION,XY,XYN):-
   sectionProps(DATA,C0,C,S,_,LS,_KIND),
     nextLineNode(DATA,XY00,LENGTH,XY0,LS,XYNN,[C|CORNERS],LONGER),
     (S=SECTION,REMCS=[C|CORNERS],REMSS=SECTIONS,XY=XY0, XYN=XYNN;
     C \= END,
       nextLineEdgeStartFound(DATA,[C|CORNERS],REMCS,SECTIONS,REMSS,
                    END,XY00,XYNN,LONGER,SECTION,XY,XYN)).


%!nextLineEdge(+DATA, +CORNERS,-REMCS,+SECTIONS,-REMSS,
%     +START,+END,+XY0,XYP,+LENGTH,-SECTION,-XY1,-XY2) is nondet.
%!  nextLineEdge/13
%
% +DATA:       data context (a Prolog module)
% +CORNERS:    list of symbolic corners
% -REMCS?      list of remaining corners, having found SECTION
% +SECTIONS:   list of sections
% -REMSS:      list of remaining sections, having found SECTION
% +START:      starting corner of the interesting section
% +END:        end corner of the interesting section
% +XY0:        starting geometrical point of the line
% +XYP:        actual (previously calculated) geometrical point
% +LENGTH:     length of the line from START to section start
% -SECTION:    new section
% -XY:         starting geometrical point of the new section=XYP
% -XYN:        geometrical endpoints of the new section
%
%nextLineEdge/13
%Doesn't do anything but steps to the next node/corner recursively
%until the START condition (a matching node) is met
% Then it calls nextLineEdgeStartFound/12
nextLineEdge13(DATA,[C,C1|CORNERS],REMCS,[S|SECTIONS],REMSS,
             START,END,XY0,XYP,LENGTH,SECTION,XY,XYN):-
  C = START->nextLineEdgeStartFound(DATA,[C,C1|CORNERS],REMCS,
                                    [S|SECTIONS],REMSS,
                          END,XY0,XYP,LENGTH,SECTION,XY,XYN);
  sectionProps(DATA,C,C1,S,_,LS,_KIND),
    nextLineNode(DATA,XY0,LENGTH,XYP,LS,XYNN,[C1|CORNERS],LONGER),
    nextLineEdge13(DATA,[C1|CORNERS],REMCS,SECTIONS,REMSS,
                   START,END,XY0,XYNN,
                 LONGER,SECTION,XY,XYN).

nextLineEdge(DATA,[C,C1|CORNERS],REMCS,SECTIONS,REMSS,
             START,END,XY00,XYP,LENGTH,SECTION,XY,XYN):-
  ordMember(START,END,[C,C1|CORNERS])->
    nextLineEdge13(DATA,[C|CORNERS],REMCS,SECTIONS,REMSS,START,END,XY00,XYP,
                 LENGTH,SECTION,XY,XYN);
  ordMember(END,START,[C|CORNERS]), last(CORNERS,LC), DATA:corner(LC,XYLC),
      nextLineEdge13(DATA,[C|CORNERS],REMCS,SECTIONS,REMSS,
                     END,START,XYLC,XYP,
                 LENGTH,SECTION,XY,XYN).


% nextLineEdge/12 takes two section patterns, and
% inserts length parameter pair, then and calls nextLineEdge/12
% in each step it returns the remaining CORNERS and SECTIONS
% Found sections are returned in SECTIONS, in order SECT1-SECT2
% If START and END are in CORNERS, but in the opposite direction
% then changes START and END... and otherwise does the same
% Return DIR=1 means START and END can be found straightly
% DIR=1 means they can be found in the inverse direction
%
%!nextLineEdge(+DATA,+CORNERS,?REMCS,+SECTIONS,?REMSS,+START,+END,
%              -SECT1,-SECT2,-XY,-XYN,-DIR) is nondet.
nextLineEdge(DATA,[C|CORNERS],REMCS,SECTIONS,REMSS,
             START,END,SECT1,SECT2,XY,XYN,DIR):-
    DATA:corner(C,XY00),
    (ordMember(START,END,[C|CORNERS])-> DIR = 1,
      nextLineEdge13(DATA,[C|CORNERS],REMCORNERS,SECTIONS,REMSECTIONS,
                   START,END,XY00,XY00,0,SECT1,_,_),
      nextLineEdgeStartFound(DATA,REMCORNERS,REMCS,REMSECTIONS,
                             REMSS,END,XY00,XY00,
                 0,SECT2,XY,XYN);
    ordMember(END,START,[C|CORNERS])-> DIR = -1,
      nextLineEdge13(DATA,[C|CORNERS],REMCORNERS,SECTIONS,REMSECTIONS,
                    END,START,XY00,XY00,0,SECT1,_,_),
      nextLineEdgeStartFound(DATA,REMCORNERS,REMCS,REMSECTIONS,REMSS,
                             START,XY00,XY00,
                 0,SECT2,XY,XYN)
    ).


% nextLineEdge/11 inserts length parameter pair,
% then and calls nextLineEdge/13
% in each step it returns the remaining CORNERS and SECTIONS
% that belong to a single linear section
% Return DIR=1 means START and END can be found straightly
% DIR=1 means they can be found in the inverse direction
%
%!nextLineEdge(+DATA,+CORNERS,?REMCS,+SECTIONS,?REMSS,
%              +START,+END,-SECTION,-XY,-XYN,-DIR) is nondet.
nextLineEdge(DATA,[C|CORNERS],REMCS,SECTIONS,REMSS,
             START,END,SECTION,XY,XYN,DIR):-
    DATA:corner(C,XY00),
    (ordMember(START,END,[C|CORNERS])-> DIR = 1,
      nextLineEdge13(DATA,[C|CORNERS],REMCS,SECTIONS,REMSS,
                     START,END,XY00,XY00,0,SECTION,XY,XYN);
    ordMember(END,START,[C|CORNERS]), DIR = -1,
      nextLineEdge13(DATA,[C|CORNERS],REMCS,SECTIONS,REMSS,
                     END,START,XY00,XY00,0,SECTION,XY,XYN)).


%
%nextLineEdgeStartFound/9
%Returns all the coming section properties in a backtracking manner
%until the END condition (the node) is met
%Doesn't search for starting node any more.
%
nextLineEdgeStartFound(DATA,[C|CORNERS],_,SECTIONS,_,END,SECTION,XY,XYN):-
  DATA:corner(C,XY00),
  nextLineEdgeStartFound(DATA,[C|CORNERS],_,SECTIONS,_,END,
                         XY00,XY00,0,SECTION,XY,XYN).


%next1PolyEdge/6
%when just a single section is required
%checks, if there is an END in the remaining line-list
%
%!next1PolyEdge(+DATA,+LINES,+END,-SECTION,-XY,-XYN) is nondet.
next1PolyEdge(DATA,[line(CORNERS,SECTIONS)|LINES],END,SECTION,XY,XYN):-
    CORNERS=[C1|_],
      nextLineEdge(DATA,CORNERS,_,SECTIONS,_,C1,END,SECTION,XY,XYN,_);
    next1PolyEdge(DATA,LINES,END,SECTION,XY,XYN).

%when 2 sections are required
%checks, if there is a second object, and
%also an END in the remaining line-list
%
%!next1PolyEdge(+DATA,+LINES,+END,-SECT1,SECT2,-XY,-XYN) is nondet.
next1PolyEdge(DATA,[line(CORNERS,SECTIONS)|LINES],END,SECT1,SECT2,XY,XYN):-
    CORNERS=[C1|_],
      nextLineEdge(DATA,CORNERS,_,SECTIONS,_,C1,END,SECT1,SECT2,XY,XYN,_);
    next1PolyEdge(DATA,LINES,END,SECT1,SECT2,XY,XYN).


%!nextPolyEdge(DATA,LINES,START,END,SECT1,SECT2,XY,XYN) is nondet.
%!nextPolyEdge(DATA,LINES,START,END,SECT1,XY,XYN) is nondet.
%+DATA:     data context (a Prolog module)
%+LINES:    a list of line(CORNERS,SECTIONS) structures, where
%           CORNERS is a list of corners
%           SECTIONS is a list of sections
%+START:    a corner to start searching from (from CORNERS)
%+END:      a corner to finish searching (from CORNERS)
%?SECT1:    section found - a section pattern may also be given
%?SECT2:
%XY, XYN:   coordinate of the found section/of the next section
%
%nextPolyEdge/8
nextPolyEdge(DATA,[line(CORNERS,SECTIONS)|LINES],
             START,END,SECT1,SECT2,XY,XYN):-
  nextLineEdge(DATA,CORNERS,_,SECTIONS,_,START,END,SECT1,SECT2,XY,XYN,_);
    next1PolyEdge(DATA,LINES,END,SECT1,SECT2,XY,XYN).


% Polygon wall is split at a section...
% Remaining wall starts with the section found and corners
splitWallAtSection(SECTION,[line(CORNERS,[SECTION|SECTIONS])|LINES],
             [line(CORNERS,[SECTION|SECTIONS])|LINES]).
splitWallAtSection(SECTION,[line([_|CORNERS],
                                 [_|SECTIONS])|LINES],REST):-
   splitWallAtSection(SECTION,[line(CORNERS,SECTIONS)|LINES],REST).
splitWallAtSection(SECTION,[line([_],[])|LINES],REST):-
  splitWallAtSection(SECTION,LINES,REST).


% splitWallAtCorner(CORNER,[line(CORNERS,SECTIONS)|LINES],REST) is
% nondet.
% Polygon wall is split at the given corner
% Remaining wall starts with the corner found
% If the corner is the last in a wall section then
% the first of the next section is returned in REST
%
splitWallAtCorner(CORNER,[line([CORNER],[])],[]):- !.
splitWallAtCorner(CORNER,[line([_],[])|LINES],REST):- !,
    splitWallAtCorner(CORNER,LINES,REST).
splitWallAtCorner(CORNER,[line([CORNER|CORNERS],SECTIONS)|LINES],
             [line([CORNER|CORNERS],SECTIONS)|LINES]):- !.
splitWallAtCorner(CORNER,[line([_|CORNERS],[_|SECTIONS])|LINES],REST):-
   splitWallAtCorner(CORNER,[line(CORNERS,SECTIONS)|LINES],REST).

% +WALL:  polygon wall structure
% +START: starting corner of the wall section
% +END:   end corner of the wall section
% -REST1: rest wall sides structure including START
% -REST2: rest wall sides structure including END
splitBetweenCorners(START,END,WALL,REST1,REST2):-
    splitWallAtCorner(START,WALL,REST1),
      once(splitWallAtCorner(END,REST1,REST2)).

splitBetweenCorners(START,END,WALL,REST):-
  splitBetweenCorners(START,END,WALL,_,REST).

:-if(false).
splitBetweenCorners1Wall(START,END,WALL,[SIDE|REST]):-
    splitWallAtCorner(START,WALL,[SIDE|REST]),
      once(splitWallAtCorner(END,REST,_)).
:-endif.

%*********************** hogy ne kelljen ugyanazon az oldalon lennie
%TESZTELNI **************** ??????????????????????????????????
%doorInRoom(data,'F'/0/entryElevator,corridor,_XYR,_,DOOR,_XYD,_DIST).
%nextPolyEdge(.) ez a hib�s...
%==> k�l�n eset, ha egyenesen v ford�tva vannak rajta
% a tal�l illeszked�st, akkor is az eredetib�l-1 indulva keres tov�bb
% 8. szakaszban illeszkedik, akkor azt 7x megtal�lja
%
%1. az els� szakaszban csak START van ott
%2. az els� szakaszban START �s END is ott van
%3. az els� szakaszban START nincs ott
% nextPolyEdgeStartFound(DATA,POLYGON,END,SECTION,XY,XYN):-
% nextPolyEdge(DATA,POLYGON,START,END,SECTION,XY,XYN):-
% +DATA:    data context (a Prolog module)
% +POLYGON: a list of line(CORNERS,SECTIONS) structure
% +START:   start corner
% +END:     end corner
% ?SECTION: section found between START and END
% ?XY:      starting point of the section found
% ?XYN:     endpoint of the section found
nextPolyEdgeStartFound(DATA,[line(CORNERS,SECTIONS)|LINES],END,SECTION,XY,XYN):-
  once(member(END,CORNERS))-> % we are checking the end corner
    nextLineEdgeStartFound(DATA,CORNERS,_,SECTIONS,_,END,SECTION,XY,XYN);
  nextLineEdgeStartFound(DATA,CORNERS,_,SECTIONS,_,END,SECTION,XY,XYN);
  nextPolyEdgeStartFound(DATA,LINES,END,SECTION,XY,XYN).


nextPolyEdge(DATA,[line([_],[])|LINES],START,END,SECTION,XY,XYN):-
    !, nextPolyEdge(DATA,LINES,START,END,SECTION,XY,XYN).
nextPolyEdge(DATA,[line([START|CORNERS],SECTIONS)|LINES],
             START,END,SECTION,XY,XYN):-
    !, once(splitWallAtCorner(END,[line([START|CORNERS],SECTIONS)|LINES],_)),
    nextPolyEdgeStartFound(DATA,[line([START|CORNERS],SECTIONS)|LINES],
                                END,SECTION,XY,XYN).
nextPolyEdge(DATA,[line([END|CORNERS],SECTIONS)|LINES],
             START,END,SECTION,XY,XYN):-
    !, once(splitWallAtCorner(START,[line([END|CORNERS],SECTIONS)|LINES],_)),
    nextPolyEdgeStartFound(DATA,[line([END|CORNERS],SECTIONS)|LINES],
                                START,SECTION,XY,XYN).
nextPolyEdge(DATA,[line([_|CORNERS],[_|SECTIONS])|LINES],
             START,END,SECTION,XY,XYN):-
    nextPolyEdge(DATA,[line(CORNERS,SECTIONS)|LINES],
                              START,END,SECTION,XY,XYN).

% data:wall('F'/0/outsideWall,polygon,LS),
% nextPolyEdgeSection(DATA,LS,east4,east5,door(D,_,_),_,_).
% nextLineEdgeStartFound nem csekkolja a v�g�t,
% ha nincs END, akkor is siker�l
%
% START and END must be on the same linear side/section of the wall
% splitBetwCorners1Side splitBetwCorners
%  nextPolyEdgeStartFound(DATA,POLYGON,END,SECTION,XY,XYN):-
%  nextPolyEdgeSection(POLYGON,START,END,SECTION,XY,XYN):-
% +POLYGON: a list of line(CORNERS,SECTIONS) structure
% +START: start corner
% +END: end corner
% ?SECTION: section found between START and END
% ?XY: starting point of the section found
% ?XYN: endpoint of the section found
nextPolyEdgeSection(DATA,LINES,START,END,SECTION,XY,XYN):-
    once(splitBetweenCorners(START,END,LINES,
                           [line(CORNERS,SECTIONS)|REM],_)), !,
    (nextLineEdgeStartFound(DATA,CORNERS,_,SECTIONS,
                            START,END,SECTION,XY,XYN);
    nextPolyEdgeSection(DATA,REM,START,END,SECTION,XY,XYN)).
nextPolyEdgeSection(DATA,LINES,START,END,SECTION,XY,XYN):-
    once(splitBetweenCorners(END,START,LINES,
                           [line(CORNERS,SECTIONS)|REM],_)), !,
    (nextLineEdgeStartFound(DATA,CORNERS,_,SECTIONS,END,START,
                                SECTION,XY,XYN);
    nextPolyEdgeSection(DATA,REM,START,END,SECTION,XY,XYN)).


:-if(false).
nextWallEdge(DATA,[LAST],[SECTION],NODE0,LAST,NODE0,W):-
    once(sectionProps(SECTION,_THICK,W,_KIND)),
    ignore((var(W), DATA:corner(LAST,XY0), DATA:corner(NODE0,XY),
            point2Distance(XY0,XY,W))).
nextWallEdge(DATA,_NODE0,[FROM,TO|_NODES],
                      [SECTION|_SECTIONS],FROM,TO,W):-
    once(sectionProps(SECTION,_THICK,W,_KIND)),
    ignore((var(W), DATA:corner(FROM,XY0), DATA:corner(TO,XY),
            point2Distance(XY0,XY,W))).
nextWallEdge(DATA,NODE0,[_|NODES],[_|SECTIONS],FROM,TO,W):-
    nextWallEdge(DATA,NODE0,NODES,SECTIONS,FROM,TO,W).
:-endif.

%nextLineSection/10
% nextLineSection(+DATA,+START,+END,+POINTS,+SECTIONS,
% -XY,-XXYY,-P,-THICK,-KIND).
% DATA:    data context (a Prolog module)
% START:   starting point of the line
% END:     end point of the line
% POINTS:  list of points (symbolically)
% SECTIONS:list of sections
% XY:      starting point of the section
% XXYY:    endpoint of the section
% P:       endpoint symbolically
% THICK:   thickness of the wallsection
% KIND:    kind of the section

%nextLineSection/10
nextLineSection(_DATA,X0/Y0,XE/YE,[P],[SECTION],
              X0/Y0,XE/YE,P,T,K):-
    once(sectionProps(SECTION,T,_,K)), !. %number(SECTION), !.
nextLineSection(DATA,X0/Y0,XE/YE,[POINT|POINTS],[SECTION|SECTIONS],
              X/Y,XX/YY,P,THICK,KIND):-
    point2Distance(XE/YE,X0/Y0,L0E),
    once(sectionProps(SECTION,_,L,_)),
    ignore((DATA:corner(POINT,XYP),
            point2Distance(X0/Y0,XYP,L))),
    XN is X0+L/L0E*(XE-X0), YN is Y0+L/L0E*(YE-Y0),
    (X=X0, XX=XN, Y=Y0, YY=YN, P=POINT,
      once(sectionProps(SECTION,THICK,_,KIND));
    nextLineSection(DATA,XN/YN,XE/YE,POINTS,SECTIONS,
                     X/Y,XX/YY,P,THICK,KIND)).


%nextCornerLineSection(DATA,east1, b108,
% [elevatorSE, entrySW, corridorB108, door(left, 222), corridor221222,
% b108],
% [42/381, 42/263, door(corridorStairs, 2, 350), 10/60, door(222,
% 2, 70), 10/360],
% 1424/3171, 1043.0/3171.0, elevatorSE, 42, wall)
% nextCornerLineSection/10
% nextCornerLineSection(+DATA,+STARTC,+ENDC,+POINTS,+SECTIONS,
% -XY,-XXYY,-P,-THICK,-KIND).
% DATA:    data context (Prolog module)
% STARTC:  starting corner of the line
% ENDC:    end corner of the line
% POINTS:  list of points (symbolically)
% SECTIONS:list of sections
% XY:      starting point of the section
% XXYY:    endpoint of the section
% P:       endpoint symbolically
% THICK:   thickness of the wallsection
% KIND:    kind of the section
nextCornerLineSection(DATA,STARTC,ENDC,POINTS,SECTIONS,
                XY,XXYY,P,THICK,KIND):-
    DATA:corner(STARTC,START), DATA:corner(ENDC,END),
    nextLineSection(DATA,START,END,POINTS,SECTIONS,
                XY,XXYY,P,THICK,KIND).


%doorOfRoomside(DATA,SIDE,BUILD/FLOOR,DOOR,XYDOOR1,XYDOOR2) is nondet.
%+DATA:    data context (a Prolog module)
%+SIDE:    side structure, side(WALL,START,END)
%          where: WALL a wall ID, START,END: corner ID-s
%+BUILD/FLOOR: building ID/floor structure
%-DOOR:    door ID found
%-XYDOOR1: XY coordinates of DOOR one side
%-XYDOOR2: XY coordinates of DOOR other side
%
%checks, if the given WALL between nodes START and END
%contains a DOOR with coordinates XYD1,XYD2
%...and returns them upon backtracking
%
% ??? doorOfRoomside(side(outsideWall,southeast,southwest),'F'/0,DOOR,_,_).
% ?????????? CIKLIKUS FAL!!!!!
%
doorOfRoomside(DATA,side(WALL,START,END),B/FL,DOOR,XYD1,XYD2):-
  DATA:wall(B/FL/WALL,line,CORNERS,SECTIONS)->
    nextLineEdge(DATA,CORNERS,_,SECTIONS,_,START,END,
                 door(DOOR,_,_),XYD1,XYD2,_DIR);
  DATA:wall(B/WALL,line,CORNERS,SECTIONS)->
    nextLineEdge(DATA,CORNERS,_,SECTIONS,_,START,END,
                 door(DOOR,_,_),XYD1,XYD2,_DIR);
  DATA:wall(B/FL/WALL,POLY,LINES), once(POLY=polyline;POLY=polygon)->
    nextPolyEdgeSection(DATA,LINES,START,END,door(DOOR,_,_),XYD1,XYD2);
  DATA:wall(B/WALL,POLY,LINES), once(POLY=polyline;POLY=polygon),
    nextPolyEdgeSection(DATA,LINES,START,END,door(DOOR,_,_),XYD1,XYD2).


%door2OfRoomside(DATA,BFL,SIDE,DOOR1,DOOR2,XYDOOR1,XYDOOR2,PLUSMINUS)
%+DATA:    data context (Prolog module)
%+BFL:     BUILDING/FLOOR construction
%+SIDE:    side structure, side(WALL,START,END)
%          where: WALL a wall ID, START,END: corner ID-s
%-DOOR1:   door IDs found
%-DOOR2:
%-XYDOOR1: XY coordinates of DOOR1
%-XYDOOR2: XY coordinates of DOOR2
%-PLUSMINUS: 1, if doors are in the given order... -1 otherwise
%
%checks, if the given WALL between nodes START and END
% contains two doors DOOR1-DOOR2 in the order of START-END
% or opposite. Returns also doors coordinates: XYLRD1,XYLRD2
% If the order is the same, returns 1, -1 otherwise.
%door2OfRoomside(lh075,family/0,side(hallNorth,bathSW,roomNESE),bath,roomNE
door2OfRoomside(DATA,BFL,side(WALL,START,END),DOOR1,DOOR2,
                XYLD1-XYRD1,XYLD2-XYRD2,PLUSMINUS):-
  DATA:wall(BFL/WALL,line,CORNERS,SECTIONS),
    (nextLineEdge(DATA,CORNERS,_REMCS,SECTIONS,_REMSS,
                  START,END,door(DOOR1,_,_),
                  door(DOOR2,_,_),XYLD1,XYRD1,DIR)->
      PLUSMINUS is  - DIR;
    nextLineEdge(DATA,CORNERS,_REMCS,SECTIONS,_REMSS,
                 START,END,door(DOOR2,_,_),
                 door(DOOR1,_,_),XYLD2,XYRD2,DIR)->
      PLUSMINUS is DIR);
  DATA:wall(BFL/WALL,POLY,LINES), (once(POLY=polyline;POLY=polygon)),
    (nextPolyEdge(DATA,LINES,START,END,
                  door(DOOR1,_,_),door(DOOR2,_,_),XYLD1,XYRD1)->
      PLUSMINUS = -1;
    nextPolyEdge(DATA,LINES,START,END,
                 door(DOOR2,_,_),door(DOOR1,_,_),XYLD1,XYRD1)->
      PLUSMINUS = 1).


%doorInRoom/8
%finds rooms, their middlepoints, their doors and middlepoints
%and also the room level
% doorInRoom(DATA,?BUILD/FLOOR/ROOM,?KIND,-XYR,-LEVEL,-DOOR,-XYD,-DIST)
% is nondet.
%+DATA:  data context: a Prolog module
% BUILD: building identifier (bound)
% FLOOR: floor ID of the room
% ROOM:  pure room ID (without room(__) structures)
% KIND:  room kind,
% XYR:   room centroid point
% LEVEL: level elevation of door
% DOOR:  pure door ID (without door(__) structures)
% XYD:   door middle point
% DIST:  distance
doorInRoom(DATA,BFL/ROOM,KIND,XYR,LEVEL,DOOR,XYD,DIST):-
    DATA:room(BFL/ROOM,KIND,LEVEL,SIDES),
    member(SIDE,SIDES),doorOfRoomside(DATA,SIDE,BFL,DOOR,XYD1,XYD2),
    centerRoom(DATA,BFL/ROOM,XYR), middleSection(XYD1,XYD2,XYD),
    point2Distance(XYR,XYD,DIST).

%sameWallRoomside(+SIDE1,+SIDE2) is semidet.
% checks, if the two sides side(WALL,CORNER1,CORNER2)
% are the same or same, but just in the opposite order
sameWallRoomside(side(W,C1,C2),side(W,C1,C2)):- !.
sameWallRoomside(side(W,C1,C2),side(W,C2,C1)).


%doorSidesOfRoom(DATA,BUILD/FLOOR/ROOM,KIND,DOORSIDES)
%Finds the list of doors of a given room
%...so that it checks each nodes of wall sections
%...and returns them along with their wall sections
%
%a wall can be either a line, or a polygon
%even for polygon it is a sequence of line-sections
% +DATA:  data context (a Prolog module)
%?BUILD:  building identifier
%?FLOOR:  floor identifier (usually an integer)
%?ROOM:   room identifier
%?KIND:   kind of room (...see there)
%-DOORSIDES: list of door(ID,XY1-XY2)-SIDE structures
%         ...where SIDE is side(WALL,CORNER1,CORNER2)
%
doorSidesOfRoom(DATA,BFL/R,KIND,DOORSIDES):-
    DATA:room(BFL/R,KIND,_LEVEL,SIDES),
    setof(door(DOOR,XY1-XY2)-SIDE,
            (member(SIDE,SIDES),doorOfRoomside(DATA,SIDE,BFL,DOOR,XY1,XY2)),
         DOORSIDES).

%Finds the room where both of doors are on the same side
%is nondet (semidet?)
% +DATA:    data context (a Prolog module)
%?BFL/ROOM: if possible, specify Building and FLoor
%?KIND:     kind of room
%+DOOR1:    door ID (from)
%+DOOR2:    door ID (to)
%PLUSMINUS: which direction to turn related to direct line
sameWallDoors(DATA,BFL/ROOM,KIND,DOOR1,DOOR2,PLUSMINUS):-
  doorSidesOfRoom(DATA,BFL/ROOM,KIND,DOORSIDES), %ROOM not only for corridors
  once(member(door(DOOR1,_)-SIDE,DOORSIDES)),
  door2OfRoomside(DATA,BFL,SIDE,DOOR1,DOOR2,_D1XY,_D2XY,PLUSMINUS).


% door2DoorCornersInRoom(DATA,BUILD/FLOOR/ROOM,KIND,
%                        DOOR0,SIDE0,DOOR,SIDE) is nondet.
% +DATA:   data context (a Prolog module)
% FROM,TO: doors-corners in the following format door(DOOR,C1,C2)
% FRSIDE,TOSIDE: sides structures of room having the door
door2DoorCornersInRoom(DATA,BFLR,KIND,FROM,FRSIDE,TO,TOSIDE):-
  doorSidesOfRoom(DATA,BFLR,KIND,DOORLIST), DOORLIST=[_,_|_],
  member(FROM-FRSIDE,DOORLIST), member(TO-TOSIDE,DOORLIST), FROM\=TO.


%for Navi Path Generation
% door2DoorInRoom(DATA,BUILD/FL/ROOM,KIND,FL/DOOR0,FL/DOOR,DISTANCE)
% is nondet.
% finds door to door distance inside a room (typically
% corridors and/or halls)
%
% +DATA:  data context (a Prolog module)
% ?BUILD: building identifier (bound)
% ?FL:    vertical floor ID of the room
% ?ROOM:  pure room ID (without room(__) structures)
% ?KIND:  for a given kind or empty
% ?DOOR0: first door (as a door(DOOR,CENTER) structure)
% ?DOOR:  second door (as a door(DOOR,CNTER) structure)
% -DISTANCE: distance (on the same side a walking width
% from wall (by default 50cm) is calculated
door2DoorInRoom(DATA,B/FL/R,KIND,door(FL/DOOR0,XY0),
                      door(FL/DOOR,XY),DISTANCE):-
  door2DoorCornersInRoom(DATA,B/FL/R,KIND,door(DOOR0,XY01-XY02),SIDE0,
                         door(DOOR,XY1-XY2),SIDE),
      middleSection(XY01,XY02,XY0), middleSection(XY1,XY2,XY),
    (KIND=corridor, once(sameWallRoomside(SIDE0,SIDE))->
       point2Distance(XY0,XY,DIST), corridorGoingDistance(CDIST),
       DISTANCE is DIST+CDIST+CDIST;
     point2Distance(XY0,XY,DISTANCE)).

%door2DoorInRoom(DATA,BUILD/FLOOR/ROOM,KIND,DOOR0,DOOR) is nondet
% finds door to door relations inside a room
% (typically corridors and/or halls)
%+DATA:     data context (a Prolog module)
%?BUILD:    building identifier (bound)
%?FLOOR:    vertical floor of the room
%?ROOM:     pure room ID (without room(__) structures)
%?KIND:     door kind
%?DOOR0:    first door (pure door ID)
%?DOOR:     second door (pure door ID)
door2DoorInRoom(DATA,BFLR,KIND,DOOR0,DOOR):-
  door2DoorCornersInRoom(DATA,BFLR,KIND,door(DOOR0,_),_,door(DOOR,_),_).


%for Navi-Graph Generation
% stairs2DoorInRoom(DATA,BUILD/FLOOR/ROOM,KIND,
%                   STAIRS,DOOR,DISTANCE) is nondet
% finds stair to door relations inside a room
% (typically corridors and/or halls)
% +DATA:  data context (Prolog module)
% ?BUILD: building identifier (bound)
% ?FLOOR: vertical floor of the room
% ?ROOM:  pure room ID (without room(__) structures)
% ?KIND:  door kind
% ?CDIST: circumvention distance
% ?STAIRS:staircase (from one floor to another)
% ?DOOR:  door (pure door ID)
stairs2DoorInRoom(DATA,BUILD/FL/ROOM,KIND,CDIST,
                  stair(FL/STAIR,XYSTAIR),
                  door(FL/DOOR,XYDOOR),DISTANCE):-
    DATA:stairs(BUILD/(FL-_)/STAIR,floor(ROOM-_),_LEVEL,
              [C1,C2,_C3,_C4]),
      corner2MiddleDist(DATA,C1,C2,CDIST,XYSTAIR),
      doorInRoom(DATA,BUILD/FL/ROOM,KIND,_XYR,_L,DOOR,XYDOOR,_DIST),
      point2Distance(XYSTAIR,XYDOOR,DISTANCE).
stairs2DoorInRoom(DATA,BUILD/FL/ROOM,KIND,CDIST,
                  stair(FL/STAIR,XYSTAIR),
                  door(FL/DOOR,XYDOOR),DISTANCE):-
    DATA:stairs(BUILD/(_-FL)/STAIR,floor(_-ROOM),_LEVEL,
              [_C1,_C2,C3,C4]),
      corner2MiddleDist(DATA,C3,C4,CDIST,XYSTAIR),
      doorInRoom(DATA,BUILD/FL/ROOM,KIND,_XYR,_L,DOOR,XYDOOR,_DIST),
      point2Distance(XYSTAIR,XYDOOR,DISTANCE).

%stairs2stairs(DATA,BUILD,STAIRS0,STAIRS,DISTANCE) is nondet
% finds stairs bottom to stairs top relations inside a building
%+DATA:     data context (Prolog module)
%?BUILD:    building identifier (bound)
%?STAIRS0:  staircase one side
%?STAIRS:   staircase one side
%+CDIST:    circumvention distance
%DISTANCE:  horizontal distance of two stairs ends
stairs2stairs(DATA,BUILD,stair(FL0/STAIR,XYST0),
              stair(FL/STAIR,XYST),CDIST,DISTANCE):-
  DATA:stairs(BUILD/(FL0-FL)/STAIR,floor(_-_),_,
              [C1,C2,C3,C4]),
      corner2MiddleDist(DATA,C1,C2,CDIST,XYST0),
      corner2MiddleDist(DATA,C3,C4,CDIST,XYST),
      point2Distance(XYST0,XYST,DISTANCE).

%
% we can suppose that any visual transformation
% transmits a linear section into another one
% that is: only the endpoints are to be transformed
%
sectionsMinMax(DATA,[line([START|CORNERS],_)],XMIN/YMIN-XMAX/YMAX):- !,
  DATA:corner(START,X1/Y1), last(CORNERS,END), DATA:corner(END,X2/Y2),
  XMAX is max(X1,X2), YMAX is max(Y1,Y2),
  XMIN is min(X1,X2), YMIN is min(Y1,Y2).
sectionsMinMax(DATA,[line([START|CORNERS],_)|SECTIONS],
               XMIN/YMIN-XMAX/YMAX):-
  DATA:corner(START,X1/Y1), last(CORNERS,END), DATA:corner(END,X2/Y2),
  sectionsMinMax(DATA,SECTIONS,XMIN0/YMIN0-XMAX0/YMAX0),
  XMAX is max(max(X1,X2),XMAX0), YMAX is max(max(Y1,Y2),YMAX0),
  XMIN is min(min(X1,X2),XMIN0), YMIN is min(min(Y1,Y2),YMIN0).
%sectionsMinMax(DATA,SECTIONS,PROJECTOR,XMIN/YMIN-XMAX/YMAX)
%+DATA:      data context
%+SECTIONS:  a list of sections
%+PROJECTOR: a projector call (w/o last two args are points)
%+FLOOR:     floor ID
%+N:         floor ordinal number (in FLOORS list)
%+DX/DY/DZ:  difference by floors
sectionsMinMax(DATA,SECTIONS,PROJECTOR,FLOOR,N,DX/DY/DZ,XMIN/YMIN-XMAX/YMAX):-
  sectionsMinMax(DATA,SECTIONS,XMI/YMI-XMA/YMA),
  (DZ == -inf -> NDZ is -inf; NDZ is N*DZ),
    call(PROJECTOR,FLOOR,(XMI+N*DX)/(YMI+N*DY)/NDZ,XMIN/YMIN),
    call(PROJECTOR,FLOOR,(XMA+N*DX)/(YMA+N*DY)/NDZ,XMAX/YMAX).

wallMinMax(DATA,BUILDING/FLOOR/WALL,PROJECTOR,NTH,DXYZ,XMIN/YMIN-XMAX/YMAX):-
  DATA:wall(BUILDING/FLOOR/WALL,_,SECTIONS),
  sectionsMinMax(DATA,SECTIONS,PROJECTOR,FLOOR,NTH,DXYZ,XMIN/YMIN-XMAX/YMAX).

% computes minimal and maximal coordinates of walls being drawn
%+DATA:     data context
%+BUILDING: it works at the moment for the given building
%+PROJECT:  projector predicate to project a 2.5D point
%XMIN/YMIN-XMAX/YMAX: resulting 2D minimal and maximal points
wallsMinMax(DATA,BUILDING,FLOORS,WALLS,PROJECTOR,DXYZ,XMIN/YMIN-XMAX/YMAX):-
  aggregate_all(r(min(YMI),min(XMI),max(YMA),max(XMA)),FLOOR,
    (nth0(NTH,FLOORS,FLOOR), member(WALL,WALLS),
     wallMinMax(DATA,BUILDING/FLOOR/WALL,
                PROJECTOR,NTH,DXYZ,XMI/YMI-XMA/YMA)),R),
  R=r(YMIN,XMIN,YMAX,XMAX).

%genNaviGraph(DATA,BUILD,CDIST,EL,WGRAPH).
%+DATA:  data context (Prolog module)
%+BUILD: building identifier
%+CDIST: corridor and blockage circumvent distance
%+IFEL:  can elevators be used (true/false)
%-WGRAPH:resulting weighted graph
%
% graph model:
% - nodes: not corridor rooms (their centroid point)
%          doors and gates
% - arcs: doors are reachable from their rooms
%         doors are reachable from doors of the same room
% IMPORTANT!! the graph is symmetrical!! No one way streets!!
% @tbd: doors may have temporary opening/closing strategy!!!
% @tbd: dynamic graph creation strategy
genNaviGraph(DATA,BUILD,CDIST,IFEL,WGRAPH):-
%list of all not-corridor rooms and their doors
  setof(room(FL/ROOM,XYR)-door(FL/DOOR,XYD)-DIST,
        KIND^XYR^XYD^FL^LEVEL^(
             doorInRoom(DATA,BUILD/FL/ROOM,KIND,
                      XYR,LEVEL,DOOR,XYD,DIST),
                      KIND \= corridor),
        ROOMS_2DOORS_LIST),
  add_edges([],ROOMS_2DOORS_LIST,ROOMSDOORS),
%door-neighbour door links in rooms (mainly corridors)
  setof(FROM-TO-DISTANCE,
        KIND^ROOM^FLOOR^door2DoorInRoom(DATA,BUILD/FLOOR/ROOM,KIND,FROM,
                  TO,DISTANCE),DOORS_2DOORS_LIST),
  add_edges(ROOMSDOORS,DOORS_2DOORS_LIST,ROOMS_DOORS_LIST),
%stairs-doors links in corridors
  (setof(FROM-TO-DISTANCE,
        KIND^ROOM^FLOOR^stairs2DoorInRoom(DATA,BUILD/FLOOR/ROOM,
                                          corridor,CDIST,
                                          FROM,TO,DISTANCE),
        STAIRS_2DOORS_LIST)->true; STAIRS_2DOORS_LIST=[]),
  add_edges(ROOMS_DOORS_LIST,STAIRS_2DOORS_LIST,STAIRFUL_LIST),
%stairs (connecting floors) one side to the other
  (setof(STAIR0-STAIR-DISTANCE,
        stairs2stairs(DATA,BUILD,STAIR0,STAIR,CDIST,DISTANCE),
        STAIRS2STAIRSLIST)->true; STAIRS2STAIRSLIST=[]),
  add_edges(STAIRFUL_LIST,STAIRS2STAIRSLIST,ELEVATORLESS),
%elevator links
  (IFEL->
    setof(room(FL1/EL,XY1)-room(FL2/EL,XY2)-DIST,
        L1^W1^L2^W2^
        (DATA:room(BUILD/FL1/EL,elevator,L1,W1),
           centerRoom(DATA,BUILD/FL1/EL,XY1),
           DATA:room(BUILD/FL2/EL,elevator,L2,W2),
           centerRoom(DATA,BUILD/FL2/EL,XY2),
           FL1 \= FL2, DIST=0), ELEVATORS),
    add_edges(ELEVATORLESS,ELEVATORS,WGRAPH0),
    symmetric_closure(WGRAPH0,WGRAPH);
  symmetric_closure(ELEVATORLESS,WGRAPH)).

%pathLevelRaising(+DATA,+BUILDING,+PATH,+PREVLEVEL,+RAISING0,-RAISING)
%+DATA:      data context (Prolog module)
%+BUILDING:  building ID
%+PATH:      a list of locations in path
%+PREVLEVEL: the actual level (before PATH)
%+RAISING0-RAISING: accumulator to sum up raising
%at the moment stairs at doors/gates are not considered
%pathLevelRaising(_,_,_,_,_):- ! .  %shortcut that is always true
pathLevelRaising(_,_BFL,[],_,RAISING,RAISING):- ! .
%stairs connecing two levels, if it is raising
pathLevelRaising(DATA,B,[stair(FL0/STAIR,_XYST0),
                    stair(FL/STAIR,_XYST)|PATH],
                 PREVLEVEL,RAISING0,RAISING):-
    once(DATA:stairs(B/(FL0-FL)/STAIR,floor(_-_),LEV0-LEV,_)), !,
    LEVELDIFF is LEV-LEV0,
    LEVEL is PREVLEVEL+LEVELDIFF,
    RAIS is RAISING0+max(LEVELDIFF,0),
    pathLevelRaising(DATA,B,PATH,LEVEL,RAIS,RAISING).
%stairs connecing two levels, if it is descending
pathLevelRaising(DATA,B,[stair(FL0/STAIR,_XYST0),
                    stair(FL/STAIR,_XYST)|PATH],
                 PREVLEVEL,RAISING0,RAISING):-
    once(DATA:stairs(B/(FL-FL0)/STAIR,floor(_-_),LEV-LEV0,_)), !,
    LEVELDIFF is LEV-LEV0,
    LEVEL is PREVLEVEL+LEVELDIFF,
    RAIS is RAISING0+max(LEVELDIFF,0),
    pathLevelRaising(DATA,B,PATH,LEVEL,RAIS,RAISING).
pathLevelRaising(DATA,B,[room(FL0/ROOM,_),room(FL/ROOM,_)|PATH],
                 _PREVLEVEL,RAISING0,RAISING):-
    once(DATA:room(B/FL0/ROOM,elevator,_LEVEL0,_)),
    once(DATA:room(B/FL/ROOM,elevator,LEVEL,_)), !,
    pathLevelRaising(DATA,B,PATH,LEVEL,RAISING0,RAISING).
pathLevelRaising(DATA,B,[door(FL/DOOR1,XYD1)|PATH],
                 PREVLEVEL,RAISING0,RAISING):-
    once(doorInRoom(DATA,B/FL/_ROOM,_,_,LEVEL,DOOR1,XYD1,_)), !,
    RAIS is RAISING0+max(LEVEL-PREVLEVEL,0),
    pathLevelRaising(DATA,B,PATH,LEVEL,RAIS,RAISING).
pathLevelRaising(DATA,B,[room(FL/ROOM,_)|PATH],
                 PREVLEVEL,RAISING0,RAISING):-
    once(DATA:room(B/FL/ROOM,_KIND,LEVEL,_)), !,
    RAIS is RAISING0+max(LEVEL-PREVLEVEL,0),
    pathLevelRaising(DATA,B,PATH,LEVEL,RAIS,RAISING).
pathLevelRaising(DATA,B,[stair(FL/STAIRS,_)|PATH],
                 PREVLEVEL,RAISING0,RAISING):-
    once(DATA:stairs(B/FL/STAIRS,diff(_),LEVEL,_)),  !,
    RAIS is RAISING0+max(LEVEL-PREVLEVEL,0),
    pathLevelRaising(DATA,B,PATH,LEVEL,RAIS,RAISING).
%if only one side of a floor-crossing stair is passed
%just by chance, as an accident...
pathLevelRaising(DATA,B,[stair(FL/STAIRS,_)|PATH],LEVEL,RAISING0,RAISING):-
    once(DATA:stairs(B/(FL-_)/STAIRS,floor(_-_),_-_,_)),  !,
    pathLevelRaising(DATA,B,PATH,LEVEL,RAISING0,RAISING).
%if only one side of a floor-crossing stair is passed
%just by chance, as an accident...
pathLevelRaising(DATA,B,[stair(FL/STAIRS,_)|PATH],LEVEL,RAISING0,RAISING):-
    once(DATA:stairs(B/(_-FL)/STAIRS,floor(_-_),_-_,_)),  !,
    pathLevelRaising(DATA,B,PATH,LEVEL,RAISING0,RAISING).


pathLevelRaising(_,_,[],0):- !.
pathLevelRaising(DATA,B,[room(FL/ROOM,_)|PATH],RAISING):- !,
    DATA:room(B/FL/ROOM,_,LEVEL0,_),
    pathLevelRaising(DATA,B,PATH,LEVEL0,0,RAISING).
pathLevelRaising(DATA,B,[door(FL/DOOR,_)|PATH],RAISING):- !,
    once(doorInRoom(DATA,B/FL/_ROOM,_,_,LEVEL0,DOOR,_XY,_)),
    pathLevelRaising(DATA,B,PATH,LEVEL0,0,RAISING).


%pathNavi(START,END,PATH,DISTANCE,UPWALK).
%pathNaviGraph(UWG,START,END,PATH,DISTANCE).
%UWG: unsigned weighted graph structure
%+START: starting point structure eg. door(103,_XY)
%+END:   endpoint structure eg. room(a109,_XY)
%-PATH:  shortest path leading from START to END
%
%-DISTANCE: total length of PATH
%-UPWALK: raising of level to be made by walk
%
%throws existence error if either START or END doesnt exist
%pathNavi(door(103,_),room(a109,_),P,D).
pathNaviGraph(UWG,START,END,PATH,DISTANCE):- !,
  (\+ member(START-_,UWG)->
      existence_error(compound,START);
  \+ member(END-_,UWG)->
      existence_error(compound,END);
  min_path(UWG,START,END,PATH,DISTANCE)).

pathNavi(DATA,START,END,PATH,DISTMETER,UPMETER,_OPTIONS):- !,
  naviGraph(UWG), once(floors(DATA,B,_)),
  pathNaviGraph(UWG,START,END,PATH,DISTANCE), DISTMETER is DISTANCE / 100,
    pathLevelRaising(DATA,B,PATH,UPWALK), UPMETER is UPWALK / 100.


longestWall(DATA,BFL/ROOM,KIND,LONGEST,X1/Y1-X2/Y2):-
    DATA:room(BFL/ROOM,KIND,_LEVEL,SIDES),
    aggregate_all(max(sqrt((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1))),
      (member(side(WALL,FROM,TO),SIDES),
          DATA:corner(FROM,X1/Y1),DATA:corner(TO,X2/Y2)), LONGEST),
        member(side(WALL,FROM,TO),SIDES),
          DATA:corner(FROM,X1/Y1),DATA:corner(TO,X2/Y2),
    LONGEST is sqrt((X2-X1)*(X2-X1)+(Y2-Y1)*(Y2-Y1)).


calcCorridorLengths(DATA):-
  retractall(DATA:corridorLength(_,_)), fail;
  longestWall(DATA,BFLR,corridor,_LONGEST,XXYY),
    assert(DATA:corridorLength(BFLR,XXYY)),
    fail;
  true.


:- predicate_options(createNavi/2,2,
                     [circumvent(integer),
                     elevator(boolean)]).
createNavi(DATA,OPTIONS):-
    option(circumvent(CDIST),OPTIONS,50),
    option(elevator(EL),OPTIONS,false), once(floors(DATA,B,_)),
    calcCorridorLengths(DATA), genNaviGraph(DATA,B,CDIST,EL,UWG),
    setNaviGraph(UWG).


checkNaviGraph:-
    naviGraph(UWG),
    reportSymmetricError(UWG).


wall34(DATA,B/_FL/ID,_,POINTS-SECTIONS):-
    DATA:wall(B/ID,line,POINTS,SECTIONS).
wall34(DATA,B/_FL/ID,_,LINES):-
    DATA:wall(B/ID,_,LINES).
wall34(DATA,B/FL/ID,FL,POINTS-SECTIONS):-
    DATA:wall(B/FL/ID,line,POINTS,SECTIONS).
wall34(DATA,B/FL/ID,FL,LINES):-
    DATA:wall(B/FL/ID,_,LINES).


stairs(DATA,B/FL/ID,FL,stairs(ROOM,C1XY,C2XY,LENGTH,LEVEL)):-
    DATA:stairs(B/FL/ID,diff(ROOM),LEVEL,[C1,C2,LENGTH]),
    DATA:corner(C1,C1XY), DATA:corner(C2,C2XY).
stairs(DATA,B/FL/ID,FL,stairs(_ROOM,C2XY,C1XY,LENGTH,LEVEL)):-
    (DATA:stairs(B/(FL-_)/ID,floor(_),LEVEL,[C1,C2,C3,C4]);
    DATA:stairs(B/(_-FL)/ID,floor(_),LEVEL,[C1,C2,C3,C4])),
    DATA:corner(C1,C1XY), DATA:corner(C2,C2XY),
    DATA:corner(C3,_C3XY), DATA:corner(C4,C4XY),
    point2Distance(C1XY,C4XY,LENGTH).



:- begin_tests(planar,[
               ]).
%                   setup(consult(data))
%                       ,cleanup(doClearAllIndoor(data))]).
%
%:- run_tests(planar).
%

%
% splitWall: splitWallAtCorner/3, splitWallAtSection/3
%
test(splitWall,true(REM = LINESSECTIONS)):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LINESSECTIONS)),
  splitWallAtCorner(corridorAB101,LINESSECTIONS,REM).

test(splitWall,fail):-
  TESTDATA=data,
  TESTDATA:wall('F'/ -1/corridorEast,polygon,LINESSECTIONS),
  splitWallAtCorner(abcd,LINESSECTIONS,_).

test(splitWall,all(D=[null2,2])):-
  TESTDATA=data,
  TESTDATA:wall('F'/ -1/corridorEast,polygon,LINESSECTIONS),
  splitWallAtSection(door(D,_,_),LINESSECTIONS,_).

test(splitWall,all(D = [sideEntrance,mainEntrance])):-
  TESTDATA=data,
  TESTDATA:wall('F'/ 0/outsideWall,polygon,LINESSECTIONS),
  splitWallAtSection(door(D,_,_),LINESSECTIONS,_).

%the corner is the endpoint of the entire wall
test(splitWall,true(REM=[])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitWallAtCorner(corner0203,LS,REM).

%not existing corner
test(splitWall,fail):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitWallAtCorner(corridor0203,LS,_).

%the corner is the endpoint of a wall section
test(splitWall,true(REM=[line([corner02, corner0203],[30])])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitWallAtCorner(corner02,LS,REM).

%
% splitBetweenCorners/4 - splitting between START-END
%
%a common split inside a wall section
test(split2Wall,true(REM=[line([corridor102103, door(2, left),
                                door(2, right), corner02],
                               [16/20, door(2, 2, 89), 30/83]),
                          line([corner02, corner0203], [30])])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitBetweenCorners(corridor101102,corridor102103,LS,REM).

%a split exceeding a single wall section
test(split2Wall,true(REM=[line([corner02, corner0203], [30])])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitBetweenCorners(corridor101102,corner02,LS,REM).

%an empty split (START=END)
test(split2Wall,true(REM=[line([corridor102103, door(2, left),
                                door(2, right), corner02],
                               [16/20, door(2, 2, 89), 30/83]),
                          line([corner02, corner0203], [30])])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitBetweenCorners(corridor102103,corridor102103,LS,REM).

%an inverse direction split
test(split2Wall,fail):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitBetweenCorners(corridor102103,corridor103102,LS,_REM).

%START is the endpoint of a wall section
%END is the endpoint of the entire wall
test(split2Wall,true(REM=[])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitBetweenCorners(corner02,corner0203,LS,REM).

%END is the endpoint of a wall section
test(split2Wall,true(REM=[line([corner02, corner0203],[30])])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LS)),
  splitBetweenCorners(corridor101102,corner02,LS,REM).

%
%nextPolyEdge/6
%
%a common single door in a wall section
test(polyEdge,all(DOOR-TH-W = [null2-2-74])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LINESSECTIONS)),
  nextPolyEdge(TESTDATA,LINESSECTIONS,corridorAB101,corridor102103,
               door(DOOR,TH,W),_XY,_XYN).

%END is the endpoint of a wall section
test(polyEdge,all(DOOR-TH-W = [null2-2-74,2-2-89])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LINESSECTIONS)),
  nextPolyEdge(TESTDATA,LINESSECTIONS,corridorAB101,corner0203,
               door(DOOR,TH,W),_XY,_XYN).

test(polyEdge,all(DOOR-TH-W = [null2-2-74,2-2-89])):-
  TESTDATA=data,
  once(TESTDATA:wall('F'/ -1/corridorEast,polygon,LINESSECTIONS)),
  nextPolyEdge(TESTDATA,LINESSECTIONS,corridorAB101,corner02,
               door(DOOR,TH,W),_XY,_XYN).

%wall end points in inverted order
test(polyEdge,all(DOOR-TH-W = [null2-2-74,2-2-89])):-
  TESTDATA=data,
  TESTDATA:wall('F'/ -1/corridorEast,polygon,LINESSECTIONS),
  nextPolyEdge(TESTDATA,LINESSECTIONS,corner0203,corridorAB101,
               door(DOOR,TH,W),_XY,_XYN).

test(polyEdge,all(DOOR = [sideEntrance,mainEntrance])):-
  TESTDATA=data,
  TESTDATA:wall('F'/0/outsideWall,polygon,LINES),
  nextPolyEdge(TESTDATA,LINES,southeast,southwest,door(DOOR,_,_),_,_).


test(polyEdgeSection,fail):-
  TESTDATA=data,
  TESTDATA:wall('F'/0/outsideWall,polygon,LINES),
  nextPolyEdgeSection(TESTDATA,LINES,southeast,southwest,door(_,_,_),_,_).


% all corridors
test(rooms,all(ROOM=[entryElevator,mainHall,mainCorridor,
                     northCorridor,corridorBack,entryBack,entry110])):-
    TESTDATA=data,
    TESTDATA:room('F'/0/ROOM,corridor,_XY,_).

% all lecture rooms in all floors
test(rooms,all(FL/ROOM=[0/111,0/103,0/107,1/211,
                     -1/2, -1/3, -1/4, -1/a6, -1/b6,
                        -1/ab6, -1/null2])):-
    TESTDATA=data,
    TESTDATA:room('F'/FL/ROOM,lecture,_XY,_).


test(sameWallDoors,PM= -1):-
    TESTDATA=data,
    sameWallDoors(TESTDATA,'F'/0/mainCorridor,_K,101,102,PM), !.
test(sameWallDoors,PM= 1):-
    TESTDATA=data,
    sameWallDoors(TESTDATA,'F'/0/mainCorridor,_K,102,101,PM), !.
test(sameWallDoors,PM= 1):-
    TESTDATA=data,
    sameWallDoors(TESTDATA,'F'/1/mainCorridor,_K,220,217,PM), !.
test(sameWallDoors,PM= -1):-
    TESTDATA=data,
    sameWallDoors(TESTDATA,'F'/1/mainCorridor,_K,217,220,PM), !.


%doors of a given room
test(roomDoor,all(DOOR=[103,103104])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/103,lecture,_XY,_,DOOR,_,_DIST).
test(roomDoor,all(DOOR=[103104,104])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/104,office,_XY,_,DOOR,_,_DIST).
test(roomDoor,all(DOOR=[b101,101,a101])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/a101,office,_XY,_,DOOR,_,_DIST).
test(roomDoor,all(DOOR=[entry110,a110,b110])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/entry110,_K,_XY,_,DOOR,_,_DIST).


%doors of several rooms
test(roomDoor,all(DOOR=[mainEntrance,mainHall,elevator, %entryElevator
                       mainCorridor,mainHall,     %mainHall
                       mainCorridor,101,102,103,104, %mainCorridor
                           105,106,107,108,corridorStairs,
                       corridor,corridorStairs,   %northCorridor
                       111,corridor,entry110,109,stairs, %corridorBack
                       stairs,sideEntrance,       %entryBack
                       entry110,a110,b110         %entry110
                       ])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/_ROOM,corridor,_XYR,_,DOOR,_XYD,_DIST).
test(roomDoor,all(DOOR=[b101,101,a101])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/a101,office,_XY,_,DOOR,_,_DIST).
test(roomDoor,all(DOOR=[entry110,a110,b110])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/entry110,_K,_XY,_,DOOR,_,_DIST).

%stairs and doors in a room
test(stairsDoor,all(DOOR=[null2,23,4,womensRoom,bathroom,mensRoom,b6])):-
    TESTDATA=data,
    stairs2DoorInRoom(TESTDATA,'F'/ -1 / mainCorridor,_,50,_,door(-1/DOOR,_),_).

%stairs low end to high end in a building
test(stairs,all((FL0-FL)/STAIR=[(0- -1)/basementStairs,
                                (0-1)/level1Stairs
                      ])):-
    TESTDATA=data,
    stairs2stairs(TESTDATA,'F',stair(FL0/STAIR,_),stair(FL/STAIR,_),50,_DISTANCE).


test(doorInRoom,all(DOOR=[103,103104])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/103,lecture,_XY,_,DOOR,_,_DIST).

test(doorInRoom,all(DOOR=[mainCorridor,101,102,103,104,
                         105,106,107,108,corridorStairs])):-
    TESTDATA=data,
    doorInRoom(TESTDATA,'F'/0/mainCorridor,corridor,_XY,_,DOOR,_,_DIST).


test(door2Door,all(F-T = [109-111,109-corridor,
                          109-entry110,109-stairs,

                          111-109,111-corridor,
                          111-entry110,111-stairs,

                          corridor-109,corridor-111,
                          corridor-entry110,corridor-stairs,

                          entry110-109,entry110-111,
                          entry110-corridor,entry110-stairs,
                          stairs-109,stairs-111,
                          stairs-corridor,stairs-entry110])):-
    TESTDATA=data,
    door2DoorInRoom(TESTDATA,'F'/0/corridorBack,_KIND,F,T).
test(door2Door,all(F-T = [a110-b110,a110-entry110,b110-a110,b110-entry110,
                         entry110-a110,entry110-b110])):-
    TESTDATA=data,
    door2DoorInRoom(TESTDATA,'F'/0/entry110,_KIND,F,T).

%from a given door to another door in both rooms on both sides
test(door2Door,all(TO=[102,103,104,105,106,107,108,
                       corridorStairs,mainCorridor,a101,b101])):-
    TESTDATA=data,
  door2DoorInRoom(TESTDATA,'F'/0/_ROOM,_KIND,101,TO).

%from door to another door in a given room (usually corridor)
test(door2Door,all(F-T = [109-111,109-corridor,
                          109-entry110,109-stairs,
                          111-109,111-corridor,
                          111-entry110,111-stairs,
                          corridor-109,corridor-111,
                          corridor-entry110,corridor-stairs,
                          entry110-109,entry110-111,
                          entry110-corridor,
                          entry110-stairs,
                          stairs-109,
                          stairs-111,
                          stairs-corridor,
                          stairs-entry110])):-
    TESTDATA=data,
    door2DoorInRoom(TESTDATA,'F'/0/corridorBack,_KIND,F,T).


:- end_tests(planar).











