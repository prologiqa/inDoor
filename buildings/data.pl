:- module(data,[]).
%:- module(data,[signature/2,
%                corner/2,
%                wall/3, wall/4,
%				 options/1,
%                corridorLength/2, room/4,
%                blockage/2, stairs/4]).

%:- use_module(model).

%indoor navigation database
%data: University P�cs, Faculty of Sciences, Building F
%wire model: middleline+ wall thickness


%room(BUILD/FLOOR/ID,KIND,LEVEL,SIDES)
% ID:    ground, the name of the room
% BUILD: building ID
% FLOOR: floor number (0: ground floor
% KIND:  kind of room (office;lecture,common,storage;lab,corridor;
%                     elevator;bath;outside)
% LEVEL: floor height relative to the level height
% SIDES: list of wall sections: [side(WALL,CORNER1,CORNER2]
:- discontiguous room/4.
:- dynamic room/4.


%stairs(BUILD/FLOOR/ID,KIND,LEVEL,SIDES)
% ID:    ground, the name of the room
% BUILD: building ID
% FLOOR: floor number (0: ground floor)
%        or FLOOR=FL1-FL2 if it connects floors
% KIND: kind of stairs ('diff' if it just balances floor differences
%                      'floor' if it connects floors
% LEVEL: floor height relative to the level height
% STAIRS: list of wall sections: [side(WALL,CORNER1,CORNER2]
:- discontiguous stairs/4.
:- dynamic stairs/4.

%corridor(BUILDING/FLOOR/ROOM,LONGEST).
%ROOM/FLOOR/BUILDING: roomID/building ID/floor NR (normally integer)
%LONGEST:  the longer side of a corridor (X0/Y0)-(X/Y)
%SIDES:    sides of the room... see the description elsewhere
:- discontiguous corridorLength/2.
:- dynamic corridorLength/2.

:- discontiguous wall/4.
:- discontiguous wall/3.
:- dynamic wall/3, wall/4.

:- discontiguous corner/2.
:- dynamic corner/2.

:- discontiguous blockage/2.
:- dynamic blockage/2.


signature('s ez es MI Int�zet, de csak egy p�lda','PTE-TTK').

options([]).
/*
 * University of P�cs, Department of Sciences,
 * Institute of Mathematics and Informatics
 *
 * FLOOR '0'
 */

%coordinates: East/North, cms)
%wall(B/FL/ID,KIND,LINES).
%B:     ground, building identifier
%FL:    ground, floor identifier (usually integer)
%ID:    ground wall identifier
%KIND:  polyline;polygon
%LINES: ordered list of line(CORNERS,SECTIONS) structures

wall('F'/0/outsideWall,polygon,
     [line([southwest,balconyNW,103104,104105,105106,106107,
            ext107,107,107108,ab108,b108,west1],
           [57/485.0,57/292.0,45/274.0,45/319.0,45/298.0,
            45/285.0,45/279.0,45/311.0,45/298.0,45/330.0,45/407.0]),
      line([west1,west2],[33/430.0]),
      line([west2,west3,west4,west5,northWest],
           [42/775.0,42/337.0,54/444.0,54/361.0]),
      line([northWest,north1,north2,northEast],
           [30/561.0,30/228.0,30/573.0]),
      line([northEast,east7,east6,east5],
       [60/361.0,60/183.0,60/261.0]),
      line([east5,east4],[30/375]),
      line([east4,gate2Right,gate2Left,east3],
           [42/157.0,door(sideEntrance,2,180),42/775.0]),
      line([east3,east2],[42/684.0]),
      line([east2,door(mainEntrance,right),door(mainEntrance,left),east1],
           [42/140.24621207005913,door(mainEntrance,2,150),
            42/120.20815280171308]),
      line([east1,corridorA101NE,aa101,ab101,101102,
            middle102,103102,balconyNE,southeast],
           [45/628.0,45/275.0,45/315.0,45/582.0,
            45/297.0,45/297.0,45/292.0,45/485.0]),
      line([southeast,southwest],[32/1424])
     ]).

%Outside Wall corners
corner(southwest,0/0).
corner(balconyNW,0/485). %from the 1-st floor
corner(103104,0/777).
corner(104105,0/1051).
corner(105106,0/1370).
corner(106107,0/1668).
corner(ext107,0/1953).
corner(107,0/2232).
corner(107108,0/2543).
corner(ab108,0/2841).
corner(b108,0/3171).
corner(west1,0/3578).
corner(west2,430/3578).
corner(west3,430/4353).
corner(west4,430/4690).
corner(west45Middle,430/4912).
corner(west5,430/5134).
corner(northWest,430/5495).
corner(north1,991/5495).
corner(north2, 1219/5495).
corner(northEast,1792/5495).
corner(east7,1792/5134).
corner(east6,1792/4951).
corner(east5,1792/4690).
corner(east4,2167/4690).
corner(gate2Right,2167/4533).
corner(gate2Left,2167/4353).
corner(east3,2167/3578).
corner(east2,1483/3578).
corner(gate1Right,1458/3440).
corner(gate1Left,1441/3290).
corner(east1,1424/3171).
corner(corridorA101NE,1424/2543).
corner(aa101,1424/2268).
corner(ab101,1424/1953).
corner(101102,1424/1371).
corner(middle102,1424/1074).
corner(103102,1424/777).
corner(balconyNE,1424/485). %from the 1-st floor
corner(southeast,1424/0).

corner(middle111,1219/4353).
corner(northWest111,780/4353).

%wall(ID,KIND,CORNERS,SECTIONS).
%ID:      ground identifier
%KIND:    line
%CORNERS: ordered list of CORNER identifiers
%SECTIONS:ordered list of SECTION identifiers
wall('F'/0/north111,line,
     [gate2Left,middle111,door(left,111),door(right,111),
     northWest111,door(left,corridor),door(right,corridor),west3],
     [42/948,42/144,door(111,2,140),42/155,42/100,door(corridor,2,159),42/91]).

corner(southWest111,780/3578).

wall('F'/0/south111,line,
     [east2,southWest111],[42/703]).

room('F'/0/111,lecture,50,[side(outsideWall,gate2Left,east2),
          side(south111,east2,southWest111),
          side(corridorEast,southWest111,northWest111),
          side(north111,northWest111,gate2Left)]).

wall('F'/0/entrySouth,line,
     [east1,door(left,mainHall),door(right,mainHall),
            elevatorSE,entrySW,corridorB108],
           [42/210,door(mainHall,2,140),42/31,42/263,
            door(corridorStairs,2,350)]).

corner(door(left,mainHall),1205/3171).
corner(door(right,mainHall),1065/3171).
corner(entrySW,780/3171).
corner(elevatorSE,1043/3171).
corner(elevatorNW,780/3411).

room('F'/0/(entryElevator),corridor,0,[side(outsideWall,east2,east1),
                     side(entrySouth,east1,elevatorSE),
                     side(elevatorNortheast,elevatorSE,elevatorNE),
                     side(elevatorNortheast,elevatorNE,elevatorNW),
                     side(corridorEast,elevatorNW,southWest111),
                     side(south111,southWest111,east2)]).

wall('F'/0/elevatorNortheast,polyline,
     [line([elevatorNW,elevatorNE],[30/263]),
      line([elevatorNE,door(left,elevator),door(right,elevator),
            elevatorSE],[30/39,door(elevator,2,167),30/38])]).

corner(elevatorNE,1043/3411).
corner(door(left,elevator),1043/3372).
corner(door(right,elevator),1043/3201).


room('F'/0/elevator,elevator,0,[side(elevatorNortheast,elevatorNW,elevatorNE),
               side(elevatorNortheast,elevatorNE,elevatorSE),
               side(entrySouth,elevatorSE,entrySW),
               side(corridorEast,entrySW,elevatorNW)]).


room('F'/0/mainHall,corridor,0,[side(aa101North,corridorA101NE,corridorA101),
               side(corridorEast,corridorA101,entrySW),
               side(entrySouth,entrySW,east1),
               side(outsideWall,east1,corridorA101NE)]).

room('F'/0/mainCorridor,corridor,0,[
               side(corridorEast,entrySW,corridor102103),
               side(103,corridor102103,corridor103104),
               side(corridorWest,corridor103104,corridorB108),
               side(entrySouth,corridorB108,entrySW)]).

room('F'/0/northCorridor,corridor,50,[
               side(corridorWest,corridorB108,west2),
               side(outsideWall,west2,west3),
               side(north111,west3,northWest111),
               side(corridorEast,northWest111,entrySW),
               side(entrySouth,entrySW,corridorB108)]).


wall('F'/0/corridorBackNorth,line,[east5,a110Southwest,
                         door(right,entry110),door(left,entry110),
                   southEast109,door(right,109),door(left,109),west4],
     [30/573,30/44,door(entry110,2,141),30/43,30/87,door(109,2,85),30/389]).

corner(a110Southwest,1219/4690).
corner(southEast109,991/4690).


room('F'/0/corridorBack,corridor,50,[side(north111,middle111,west3),
                   side(outsideWall,west3,west4),
                   side(corridorBackNorth,west4,a110Southwest),
                   side(abc110Storage,a110Southwest,middle111)]).

room('F'/0/entryBack,corridor,185,[side(north111,gate2Left,middle111),
                   side(abc110Storage,middle111,a110Southwest),
                   side(corridorBackNorth,a110Southwest,east5),
                   side(outsideWall,east5,east4),
                   side(outsideWall,east4,gate2Left)]).


wall('F'/0/storage109110,line,
     [southEast109,109110,door(right,a110-storage),door(left,a110-storage),
      north1],[55/444,55/115,door(110-storage,2,83),55/163]).
corner(109110,991/5134).

room('F'/0/109,office,50,[side(corridorBackNorth,southEast109,west4),
          side(outsideWall,west4,west5),
          side(109110,west5,109110),
          side(storage109110,109110,southEast109)]).

room('F'/0/110,office,50,[side(109110,109110,west5),
          side(outsideWall,west5,northWest),
          side(outsideWall,northWest,north1),
          side(storage109110,north1,109110)]).

corner(backCorridorMiddle,1219/4521).

wall('F'/0/abc110Storage,line,
                 [middle111,backCorridorMiddle,a110Southwest,
                  door(right,a110),door(left,a110),
                  ab110,door(right,b110),door(left,b110),bc110,
                  door(right,c110),door(left,c110),north2],
     [door(stairs,2,168),10/169,55/151,door(a110,2,86),12/24,12/25,
           door(b110,2,86),55/72,55/137,
     door(c110,2,83),55/141]).

corner(ab110,1219/4951).
corner(bc110,1219/5134).

wall('F'/0/ab110,line,[east6,ab110],[12/573]).
wall('F'/0/bc110,line,[east7,bc110],[42/573]).
wall('F'/0/abc110,line,[bc110,109110],[42/228]).
wall('F'/0/109110,line,[109110,door(left,110),door(right,110),west5],
     [42/98,door(110,2,74),42/389]).


room('F'/0/entry110,corridor,50,[side(corridorBackNorth,a110Southwest,southEast109),
                   side(storage109110,southEast109,109110),
                   side(abc110,109110,bc110),
                   side(abc110Storage,bc110,a110Southwest)]).

room('F'/0/(110-storage),storage,50,[side(abc110,bc110,109110),
                  side(storage109110,109110,north1),
                  side(outsideWall,north1,north2),
                  side(abc110Storage,north2,bc110)]).

room('F'/0/c110,office,50,[side(bc110,east7,bc110),
           side(abc110Storage,bc110,north2),
           side(outsideWall,north2,northEast),
           side(outsideWall,northEast,east7)]).

room('F'/0/b110,office,50,[side(ab110,east6,ab110),
           side(abc110Storage,ab110,bc110),
           side(bc110,bc110,east7),
           side(outsideWall,east7,east6)]).

room('F'/0/a110,office,50,[side(corridorBackNorth,east5,a110Southwest),
           side(abc110Storage,a110Southwest,ab110),
           side(ab110,ab110,east6),
           side(outsideWall,east6,east5)]).

%corridorEast
wall('F'/0/corridorEast,line,[northWest111,southWest111,elevatorNW,entrySW,
                        corridorA101,corridorAA101,
                        door(left,101),door(right,101),corridorAB101,
                        corridor101102,door(left,102),door(right,102),
                        corridor102103],
    [42/775,42/167,42/240,
     door(mainCorridor,2,628),12/275,12/119,
     door(101,2,85),12/111,12/582,12/215,
     door(102,2,85),12/294]).


%KIND = line => linear wall, only endpoints are given explicitely
%(length(CORNERS)=length(SECTIONS)+1) >=2
wall('F'/0/103,line,[103102,corridor102103,
               door(103,left),door(103,right),corridor103104,
               door(left,103104),door(right,103104),103104],
     [35/644,35/107,door(103,2,136),35/107,35/203,door(103104,2,86),35/141]).

wall('F'/aa101North,line,[corridorA101NE,corridorA101],[35]).
wall('F'/0/aa101,line,[aa101,door(right,a101),door(left,a101),corridorAA101],
     [12/470,door(a101,2,85),12/89]).
wall('F'/0/ab101,line,[ab101,door(right,b101),door(left,b101),corridorAB101],
     [35/470,door(b101,2,85),35/89]).
wall('F'/0/101102,line,[101102,corridor101102],[35]).

%corridorEast
corner(corridorA101,780/2543).
corner(corridorAA101,780/2268).
corner(corridorAB101,780/1953).
corner(corridor101102,780/1371).
corner(corridorMiddle102,780/1074).
corner(corridor102103,780/777).


room('F'/0/aa101,office,0,[side(aa101,aa101,corridorAA101),
            side(corridorEast,corridorAA101,corridorA101),
            side(aa101North,corridorA101,corridorA101NE),
            side(outsideWall,corridorA101NE,aa101)]).

room('F'/0/a101,office,0,[side(ab101,ab101,corridorAB101),
            side(corridorEast,corridorAB101,corridorAA101),
            side(aa101,corridorAA101,aa101),
            side(outsideWall,aa101,ab101)]).

room('F'/0/b101,office,0,[side(101102,101102,corridor101102),
           side(corridorEast,corridor101102,corridorAB101),
           side(ab101,corridorAB101,ab101),
           side(outsideWall,ab101,101102)]).

room('F'/0/102,common,0,[side(103,103102,corridor102103),
          side(corridorEast,corridor102103,corridor101102),
          side(101102,corridor101102,101102),
          side(outsideWall,101102,103102)]).

room('F'/0/103,lecture,0,[side(outsideWall,103102,southeast),
          side(outsideWall,southeast,southwest),
          side(outsideWall,southwest,103104),
          side(103,103104,103102)]).


%corridorWest
wall('F'/0/corridorWest,line,[corridor103104,door(left,104),door(right,104),
                        corridor104105,door(left,105),door(right,105),
                        corridor105106,door(left,106),door(right,106),
                        corridor106107,door(left,107),door(right,107),
                        corridor107108,door(left,108),door(right,108),
                        corridor-ab108,corridorB108,west2],
     [12/95,door(104,2,85),12/94,
      12/34,door(105,2,85),12/200,
      12/121,door(106,2,85),12/92,
      12/316,door(107,2,85),12/474,
      12/23,door(108,2,94),12/181,12/330,42/407]).

%corridorWest
corner(corridor103104,430/777).
corner(corridor104105,430/1051).
corner(corridor105106,430/1370).
corner(corridor106107,430/1668).
corner(corridor107108,430/2543).
corner(corridor-ab108,430/2841).
corner(corridorB108,430/3171).

wall('F'/0/104105,line,[corridor104105,104105],[14]).
wall('F'/105106,line,[corridor105106,105106],[34]).
wall('F'/0/106107,line,[corridor106107,106107],[12]).
wall('F'/107108,line,[corridor107108,107108],[35]).
wall('F'/0/ab108,line,[corridor-ab108,door(right,ab108),door(left,ab108),ab108],
     [12/12,door(ab108,2,120),12/298]).

room('F'/0/104,office,0,[side(103,corridor103104,103104),
          side(outsideWall,103104,104105),
          side(104105,104105,corridor104105),
          side(corridorWest,corridor104105,corridor103104)]).

room('F'/0/105,office,0,[side(104105,corridor104105,104105),
          side(outsideWall,104105,105106),
          side(105106,105106,corridor105106),
          side(corridorWest,corridor105106,corridor104105)]).

room('F'/0/106,office,0,[side(105106,corridor105106,105106),
          side(outsideWall,105106,106107),
          side(106107,106107,corridor106107),
          side(corridorWest,corridor106107,corridor105106)]).

room('F'/0/107,lecture,0,[side(106107,corridor106107,106107),
          side(outsideWall,106107,107108),
          side(107108,107108,corridor107108),
          side(corridorWest,corridor107108,corridor106107)]).

room('F'/0/a108,office,0,[side(107108,corridor107108,107108),
          side(outsideWall,107108,ab108),
          side(ab108,ab108,corridor-ab108),
          side(corridorWest,corridor-ab108,corridor107108)]).

room('F'/0/b108,lab,0,[side(ab108,corridor-ab108,ab108),
          side(outsideWall,ab108,west1),
          side(outsideWall,west1,west2),
          side(corridorWest,west2,corridor-ab108)]).


stairs('F'/0/corridorStairs,diff(mainCorridor),-50,
              [corridorB108,entrySW,150]).

stairs('F'/0/corridorBackStairs,diff(corridorBack),135,
              [backCorridorMiddle,middle111,290]).

corner(stairsAB101,630/1953).
corner(stairs101102,630/1371).

stairs('F'/(0- -1)/basementStairs,floor(mainCorridor-mainCorridor),0- -273,
              [stairsAB101,corridorAB101,corridor101102,
               stairs101102]).

corner(stairsNE,1792/4690).
corner(stairsSE,1792/4521).

stairs('F'/(0-1)/level1Stairs,floor(entryBack-corridorBack),185-375,
              [stairsNE,stairsSE,backCorridorMiddle,a110Southwest]).



/*
 * University of P�cs, Department of Sciences,
 * Institute of Mathematics and Informatics
 *
 * FLOOR '1'
 */
corner(east8,1792/5827).
corner(west6,430/5827).
corner(nordmostEast,1792/6848).
corner(nordmostWest,991/6848).
corner(west7,430/6117).
corner(northwestConcave,991/6117).




%coordinates: East/North, cms)
%wall(B/FL/ID,KIND,LINES).
%B:     ground, building identifier
%FL:    ground, floor identifier (usually integer)
%ID:    ground wall identifier
%KIND:  polyline;polygon
%LINES: ordered list of line(CORNERS,SECTIONS) structures
wall('F'/1/outsideWall,polygon,
     [line([southwest,balconyNW,103104,104105,105106,106107,
            ext107,107,107108,ab108,b108,west1],
           [57/485.0,57/292.0,45/274.0,45/319.0,45/298.0,
            45/285.0,45/279.0,45/311.0,45/298.0,45/330.0,45/407.0]),
      line([west1,west2],[33/430.0]),
      line([west2,west3,west4,west45Middle,west5,northWest,west6, west7],
           [42/775.0,42/337.0,54/222.0,54/222.0,54/361.0, 54/332.0, 54/290.0]),
      line([west7,northwestConcave],[24/561.0]),
      line([northwestConcave,nordmostWest],[54/731.0]),
      line([nordmostWest,nordmostEast],[42/801.0]),
      line([nordmostEast,east8,northEast,east7,east6,east5],
       [42/1021.0,42/332,42/361.0,42/183.0,42/261.0]),
      line([east5,east4],[30/375]),
      line([east4,gate2Left,east3],[42/337,42/775.0]),
      line([east3,east2],[42/684.0]),
      line([east2,east1],[42/411.254179310071]),
      line([east1,corridorA101NE,aa101,ab101,101102,
            middle102,103102,balconyNE,southeast],
           [45/628.0,45/275.0,45/315.0,45/582.0,
            45/297.0,45/297.0,45/292.0,45/485.0]),
      line([southeast,southwest],[32])
     ]).



corner(balconyGateE,780/485).

wall('F'/1/corridorEast,line,[northWest111,
                              southWest111,elevatorNW,entrySW,
                              door(209,left),door(right,209),corridorA101,
                        door(210,left),door(210,right),corridorAB101,
                        door(211,left),door(211,right),corridor101102,
                        corridorMiddle102,
                        door(212,left),door(212,right),corridor102103,
                        door(213,left),door(213,right),balconyGateE],
    [35/775,door(entryElevator,2,167),10/240,
     10/476,door(209,2,100),10/52,
     10/317,door(210,2,100),10/173,
     10/241,door(211,2,100),10/241,10/297,
     10/20,door(212,2,86),10/191,
     10/0,door(213,2,96),10/196]).

%corridorWest
corner(balconyGateW,430/485).
corner(corridorExt107,430/1953).
corner(corridor107,430/2232).
%corner(corridor107108,430/2543).
corner(corridor220221,300/2543).
corner(corridorb108,430/3139).
corner(corridor221222,300/3171).

wall('F'/1/corridorWest,line,[balconyGateW,door(214,left),door(214,right),
                            corridor103104,door(215,left),door(215,right),
                        corridor104105,door(216,left),door(216,right),
                        corridor105106,door(217,left),door(217,right),
                        corridor106107,door(218,left),door(218,right),
                        corridorExt107,door(219,left),door(219,right),
                        corridor107,door(220,left),door(220,right),
                              corridor107108],
                                 [10/196,door(214,2,96),10/0,
                                  10/164,door(215,2,100),10/10,
                                  10/129,door(216,2,100),10/90,
                                  10/10,door(217,2,90),10/198,
                                  10/185,door(218,2,90),10/10,
                                  10/50,door(219,2,100),10/129,
                                  10/50,door(220,2,100),10/161]
                        ).

wall('F'/1/elevatorNorth,polyline,
     [line([elevatorNW,elevatorNE],[30/263]),
      line([elevatorNE,door(elevator,left),door(elevator,right),
            elevatorSE],[30/39,door(elevator,2,167),30/38])]).

wall('F'/1/entrySouth,line,
     [east1,elevatorSE,entrySW],
           [42/381,42/263]).

wall('F'/1/221222,line,
     [corridorB108, door(222,left),corridor221222, b108],
           [10/60,door(222,2,70),10/300]).

wall('F'/1/ab101,line,[ab101,corridorAB101],[35]).

wall('F'/1/east103,line,[103102,corridor102103],[35/644]).

wall('F'/1/west103,line,[corridor103104,103104],[35/430]).

wall('F'/1/balcony,line,[balconyNE,balconyGateE,
                         balconyGateW,balconyNW],
     [35/644,door(balcony,2,350),35/430]).

wall('F'/1/104105,line,[corridor104105,104105],[14]).

wall('F'/1/106107,line,[corridor106107,106107],[12]).

wall('F'/1/218219,line,[corridorExt107,ext107],[34]).

wall('F'/1/219220,line,[corridor107,107],[10]).

wall('F'/1/220221,line,[corridor107108,corridor220221,107108],[34,34]).

wall('F'/1/221,line,[corridor220221,door(221,left),door(221,right),
                     corridor221222],
     [10/35,door(221,2,100),10/493]).

wall('F'/1/222,line,[west2,corridorB108],[10]).

wall('F'/1/middle102,line,[middle102,corridorMiddle102],[10]).

wall('F'/1/corridorBackNorth,line,[east5,southEast109,
                                   bathCorridor,west4],
     [30/801,door(corridorBackNorth1,2,161),30/400]).

wall('F'/1/north111,line,
     [gate2Left,middle111,door(111,left),door(111,right),
     northWest111,door(corridor,left),door(corridor,right),west3],
     [42/948,42/144,door(111,2,140),42/155,42/100,door(corridor,2,159),42/91]).

wall('F'/1/south111,line,
     [east2,elevatorCorridor,southWest111,west2],
     [42/440, 42/263, door(main,2,350)]).

wall('F'/1/backCorridorEast,line,[southEast109,
                              door(208,right),door(208,left),207208,
                              door(207,right),door(207,left),north1,
                              door(206,right),door(206,left),205206,
                              doorRight205,door(205,left),northwestConcave],
     [54/140,door(208,2,85),16/219,
      16/63,door(207,2,85),16/213,
      16/118,door(206,2,85),45/129,
      55/120,door(205,2,138),50/32]).


corner(bathCorridor,830/4690).
corner(bathMiddle,830/4912).
corner(bath202,830/5134).
corner(202203,830/5580).
corner(203204,830/5827).


%corner(southEast109,991/4690).
corner(207208,991/5134).
%corner(north1,991/5495).
corner(205206,991/5827).
corner(doorRight205,991/5947).


wall('F'/1/backCorridorWest,line,
     [bathCorridor,door(womensRoom,right),
            bathMiddle,door(mensRoom,left),
            bath202,door(202,left),door(202,right),
            202203,door(203,right),203204],
           [door(womensRoom,2,60),16/162,
           16/162,door(mensRoom,2,60),
          16/341,door(202,2,85),16/20,
          door(204,2,85),16/162]).

wall('F'/1/bath,line,[bathMiddle,west45Middle],[10]).

wall('F'/1/bath202,line,[bath202,west5],[42]).


corner(corner203, 730/5580).
corner(corner202, 730/5495).

wall('F'/1/202203,polygon,
     [line([202203,corner203],[14]),
      line([corner203,corner202],[14]),
      line([corner202,northWest],[14])
     ]).

wall('F'/1/203204,line,[205206,203204,west6],[door(backGate,1,161),15/400]).

wall('F'/1/207208,line,[east7,207208],[42]).

wall('F'/1/206207,line,[northEast,north1],[31]).

corner(corner206, 1219/5827).
corner(corner205, 1219/5947).


wall('F'/1/205206,polygon,
     [line([east8,corner206],[8]),
      line([corner206,corner205],[45]),
      line([corner205,doorRight205],[14])
     ]).


%wall('F'/1/error,line,[corner206,corner205],[0/100]).


room('F'/1/corridorBack,corridor,375,[side(north111,gate2Left,west3),
                   side(outsideWall,west3,west4),
                   side(corridorBackNorth,west4,east5),
                   side(outsideWall,east5,east4),
                   side(outsideWall,east4,gate2Left)]).

room('F'/1/corridorBackNorth,corridor,375,
     [side(corridorBackNorth,southEast109,bathCorridor),
                   side(backCorridorWest,bathCorridor,203204),
                   side(203204,203204,205206),
                   side(backCorridorEast,205206,southEast109)]).


stairs('F'/1/corridorStairs,diff(mainCorridor),-50,
              [west2,southWest111,150]).


room('F'/1/201,lab,375,[side(north111,gate2Left,northWest111),
               side(corridorEast,northWest111,southWest111),
               side(south111,southWest111,east2),
               side(outsideWall,east2,east3),
               side(outsideWall,east3,gate2Left)]).

room('F'/1/202,office,375,[side(backCorridorWest,202203,bath202),
               side(bath202,bath202,west5),
               side(outsideWall,west5,northWest),
               side(202203,northWest,202203)]).

room('F'/1/203,office,375,[side(203204,west6,203204),
               side(backCorridorWest,203204,202203),
               side(202203,202203,northWest),
               side(outsideWall,northWest,west6)]).

room('F'/1/204,corridor,375,[
                   side(203204,west6,205206),
                   side(backCorridorEast,205206,northwestConcave),
                   side(outsideWall,northwestConcave,west6)                                   ]).

room('F'/1/205,office,375,[
            side(backCorridorEast,doorRight205,northwestConcave),
            side(outsideWall,northwestConcave,east8),
            side(205206,east8,doorRight205)
                      ]).


room('F'/1/206,office,375,[
            side(205206,east8,doorRight205),
            side(backCorridorEast,doorRight205,north1),
            side(206207,north1,northEast),
            side(outsideWall,northEast,east8)
                      ]).

room('F'/1/207,office,375,[
            side(206207,north1,northEast),
            side(outsideWall,northEast,east7),
            side(207208,east7,207208),
            side(backCorridorEast,207208,north1)
                      ]).

room('F'/1/208,office,375,[
            side(207208,east7,207208),
            side(backCorridorEast,207208,southEast109),
            side(corridorBackNorth,southEast109,east5),
            side(outsideWall,east5,east7)
                      ]).


room('F'/1/209,office,325,[side(aa101North,corridorA101NE,corridorA101),
               side(corridorEast,corridorA101,entrySW),
               side(entrySouth,entrySW,east1),
               side(outsideWall,east1,corridorA101NE)]).

room('F'/1/210,office,325,[side(ab101,ab101,corridorAB101),
            side(corridorEast,corridorAB101,corridorA101),
            side(aa101North,corridorA101,corridorA101NE),
            side(outsideWall,corridorA101NE,ab101)]).

room('F'/1/211,lecture,325,[side(ab101,ab101,corridorAB101),
            side(corridorEast,corridorAB101,corridorMiddle102),
            side(middle102,corridorMiddle102,middle102),
            side(outsideWall,middle102,ab101)]).

room('F'/1/212,office,325,[
            side(middle102,corridorMiddle102,middle102),
            side(outsideWall,middle102,103102),
            side(east103,103102,corridor102103),
            side(corridorEast,corridor102103,corridorMiddle102)]).

room('F'/1/213,office,325,[
            side(east103,103102,corridor102103),
            side(corridorEast,corridor102103,balconyGateE),
            side(balcony,balconyGateE,balconyNE),
            side(outsideWall,balconyNE,103102)]).

room('F'/1/214,office,325,[
            side(balcony,balconyGateW,balconyNW),
            side(outsideWall,balconyNW,103104),
            side(west103,103104,corridor103104),
            side(corridorWest,corridor103104,balconyGateW)]).

room('F'/1/215,office,325,[
            side(west103,103104,corridor103104),
            side(corridorWest,corridor103104,corridor104105),
            side(104105,corridor104105,104105),
            side(outsideWall,104105,103104)
                      ]).

room('F'/1/216,office,325,[
            side(104105,corridor104105,104105),
            side(outsideWall,104105,105106),
            side(105106,105106,corridor105106),
            side(corridorWest,corridor105106,corridor104105)
                      ]).

%mens room, 1 floor/south
room('F'/1/217,bath,325,[
            side(105106,105106,corridor105106),
            side(corridorWest,corridor105106,corridor106107),
            side(106107,corridor106107,106107),
            side(outsideWall,106107,105106)
                      ]).

%womens room, 1 floor/south
room('F'/1/218,bath,325,[
            side(106107,corridor106107,106107),
            side(outsideWall,106107,ext107),
            side(218219,ext107,corridorExt107),
            side(corridorWest,corridorExt107,corridor106107)
                      ]).

room('F'/1/219,office,325,[
            side(218219,ext107,corridorExt107),
            side(corridorWest,corridorExt107,corridor107),
            side(219220,corridor107,107),
            side(outsideWall,107,ext107)
                      ]).

room('F'/1/220,office,325,[
            side(219220,corridor107,107),
            side(outsideWall,107,107108),
            side(220221,107108,corridor107108),
            side(corridorWest,corridor107108,corridor107)
                      ]).

room('F'/1/221,office,325,[
            side(220221,107108,corridor220221),
            side(221,corridor220221,corridor221222),
            side(221222,corridor221222,b108),
            side(outsideWall,b108,107108)
                      ]).

room('F'/1/222,office,325,[
            side(221222,corridorB108,b108),
            side(outsideWall,b108,west2),
            side(222,west2,corridorB108)
                      ]).

room('F'/1/mainCorridor,corridor,325,[
               side(corridorEast,southWest111,balconyGateE),
               side(balcony,balconyGateE,balconyGateW),
               side(corridorWest,balconyGateW,corridor107108),
               side(220221,corridor107108,corridor220221),
               side(221,corridor220221,corridor221222),
               side(221222,corridor221222,corridorB108),
               side(222,corridorB108,west2),
               side(south111,west2,southWest111)
                                 ]).

room('F'/1/northCorridor,corridor,375,[
               side(outsideWall,west2,west3),
               side(north111,west3,northWest111),
               side(corridorEast,northWest111,southWest111),
               side(south111,southWest111,west2)]).

corner(elevatorCorridor,1043/3578).
wall('F'/1/elevatorCorridor,line,
     [elevatorCorridor,elevatorNE,
       door(elevator,left),door(elevator,right),elevatorSE],
     [door(elevatorCorridor,1,167),30/39,door(elevator,2,163),30/38]).


room('F'/1/entryElevator,corridor,325,[side(outsideWall,east2,east1),
                     side(entrySouth,east1,elevatorSE),
                     side(elevatorCorridor,elevatorSE,elevatorCorridor),
                     side(south111,elevatorCorridor,east2)]).

room('F'/1/elevatorCorridor,corridor,325,[
                     side(elevatorCorridor,elevatorCorridor,elevatorNE),
                     side(elevatorNorth,elevatorNE,elevatorNW),
                     side(corridorEast,elevatorNW,southWest111),
                     side(south111,southWest111,elevatorCorridor)]).

room('F'/1/elevator,elevator,325,[side(elevatorNorth,elevatorNW,elevatorNE),
               side(elevatorCorridor,elevatorNE,elevatorSE),
               side(entrySouth,elevatorSE,entrySW),
               side(corridorEast,entrySW,elevatorNW)]).

/*
 * University of P�cs, Department of Sciences,
 * Institute of Mathematics and Informatics
 *
 * FLOOR '-1' (basement)
 */

corner(ab06,430/2543).

%coordinates: East/North, cms)
%wall(B/FL/ID,KIND,LINES).
%B:     ground, building identifier
%FL:    ground, floor identifier (usually integer)
%ID:    ground wall identifier
%KIND:  polyline;polygon
%LINES: ordered list of line(CORNERS,SECTIONS) structures
wall('F'/ -1/outsideWall,polygon,
     [line([southwest,balconyNW,103104,104105,105106,106107,
            ext107,107,107108],
           [57/485.0,57/292.0,45/274.0,45/319.0,45/298.0,
            45/285.0,45/279.0,45/311.0]),
      line([107108,ab06,corridorA101NE],[45,45]),
      line([corridorA101NE,aa101,ab101,101102,
            middle102,103102,balconyNE,southeast],
           [45/275.0,45/315.0,45/582.0,
            45/297.0,45/297.0,45/292.0,45/485.0]),
      line([southeast,middle0203,southwest],[32,32])
     ]).

corner(corner02,780/585).
corner(corner03,624/585).
corner(corner0203,702/585).
corner(middle0203,702/0).

%corridorEast
wall('F'/ -1/corridorEast,polygon,
     [line([corridorAB101,corridor101102,door(null2,left),door(null2,right),
            corridorMiddle102,corridor102103,
            door(2,left),door(2,right),corner02],
    [16/582,16/73,door(null2,2,74),16/150,16/297,
     16/20,door(2,2,89),30/83]),
      line([corner02,corner0203],[30])]).

corner(womensBathCorner,430/1556).
corner(mensBathCorner,430/1767).

wall('F'/ -1/corridorWest,line,
            [corridor103104,door(4,left),door(4,right),
             corridor105106,door(womensRoom,left),door(womensRoom,right),
             womensBathCorner,door(bathroom,left),door(bathroom,right),
             mensBathCorner,door(mensRoom,left),door(mensRoom,right),
             corridorExt107,door(a6,left),door(a6,right),ab06],
            [16/90,door(4,2,95),16/408,
             16/55,door(womensRoom,2,75),16/56,
             16/96,door(bathroom,2,75),16/40,
             16/55,door(mensRoom,2,75),16/56,
             16/100,door(a6,2,84),16/406]).


corner(cellar0203,624/777).

wall('F'/ -1/103,line,[103102,corridor102103,
               door(left,23),door(right,23),
                       cellar0203,corridor103104,103104],
     [52/644,52/6,door(23,2,144),52/6,52/194,52/430]).


wall('F'/ -1/23,polygon,
     [line([cellar0203,door(3,right),door(3,left),corner03],
           [30/20,door(3,2,89),30/83]),
      line([corner03,corner0203],[30]),
      line([corner0203,middle0203],[30])]).


wall('F'/ -1/ab06,line,[ext107,corridorExt107,
                        door(b6,left),door(b6,right),corridorAB101,
                        door(ab6,right),door(ab6,left),ab101],
     [42,42/20,door(b6,2,58),42/272,42/324,door(ab6,2,300),42/20]).

wall('F'/ -1/101102,line,[101102,corridor101102],[46]).


corner(mensWomensMiddle,177/1668).
corner(mensWomensCorner,177/1556).
corner(womensBath,340/1556).
corner(mensBath,340/1767).


wall('F'/ -1/mensWomens, polygon,
     [line([womensBathCorner,womensBath,mensWomensCorner],
           [18,18]),
      line([mensWomensCorner,mensWomensMiddle],[18]),
      line([mensWomensMiddle,106107],[30])]).


wall('F'/ -1/mensWomensBath, polygon,
     [line([mensBathCorner,mensBath],
           [18]),
      line([mensBath,womensBath],[18])]).


room('F'/ -1/mainCorridor,corridor,-273,[
               side(corridorEast,corridorAB101,corridor102103),
               side(103,corridor102103,corridor103104),
               side(corridorWest,corridor103104,corridorExt107),
               side(ab06,corridorExt107,corridorAB101)
                                 ]).


room('F'/ -1/entry23,corridor,-392,[
               side(corridorEast,corridor102103,corner02),
               side(corridorEast,corner02,corner0203),
               side(23,corner0203,corner03),
               side(23,corner03,cellar0203),
               side(103,cellar0203,corridor102103)]).


room('F'/ -1/2,lecture,-392,[
               side(corridorEast,corridor102103,corner0203),
               side(23,corner0203,middle0203),
               side(outsideWall,middle0203,103102),
               side(103,103102,corridor102103)
                                 ]).


room('F'/ -1/3,lecture,-392,[
               side(23,cellar0203,middle0203),
               side(outsideWall,middle0203,103104),
               side(103,103104,cellar0203)
                                 ]).


room('F'/ -1/4,lecture,-273,[
               side(103,103104,corridor103104),
               side(corridorWest,corridor103104,corridor105106),
               side(105106,corridor105106,105106),
               side(outsideWall,105106,103104)
                                 ]).


room('F'/ -1/womensRoom,bath,-273,[
               side(105106,corridor105106,105106),
               side(outsideWall,105106,106107),
               side(mensWomens,106107,womensBathCorner),
               side(corridorWest,womensBathCorner,corridor105106)
                                 ]).


room('F'/ -1/mensRoom,bath,-273,[
               side(ab06,ext107,corridorExt107),
               side(corridorWest,corridorExt107,mensBathCorner),
               side(mensWomensBath,mensBathCorner,womensBath),
               side(mensWomens,womensBath,106107),
               side(outsideWall,106107,ext107)
                                 ]).


room('F'/ -1/bathroom,bath,-273,[
               side(corridorWest,mensBathCorner,womensBathCorner),
               side(mensWomens,womensBathCorner,womensBath),
               side(mensWomensBath,womensBath,mensBathCorner)
                                 ]).


room('F'/ -1/a6,lecture,-273,[
               side(ab06,corridorExt107,ext107),
               side(outsideWall,ext107,ab06),
               side(corridorWest,ab06,corridorExt107)
                                 ]).


room('F'/ -1/b6,lecture,-273,[
               side(corridorWest,ab06,corridorExt107),
               side(ab06,corridorExt107,ab101),
               side(outsideWall,ab101,ab06)
                                 ]).


room('F'/ -1/ab6,lecture,-273,[
               side(ab06,corridorAB101,ab101),
               side(outsideWall,ab101,101102),
               side(101102,101102,corridor101102),
               side(corridorEast,corridor101102,corridorAB101)
                                 ]).



room('F'/ -1/null2,lecture,-273,[
               side(101102,101102,corridor101102),
               side(corridorEast,corridor101102,corridor102103),
               side(103,corridor102103,103102),
               side(outsideWall,103102,101102)
                                 ]).

stairs('F'/ -1/basementStairs,diff(mainCorridor),-119,
              [cellar0203,corridor102103,250]).

corner(basementStairsCorner,624/1074).

blockage('F'/ -1/mainCorridor,[cellar0203,basementStairsCorner]).
