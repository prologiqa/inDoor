:- module(lh075,[]).

%:- module(lh075,[signature/2,
%                corner/2,
%                wall/3, wall/4,
%                options/1,
%                corridorLength/2, room/4,
%                blockage/2, stairs/4]).
%:- use_module(model).
%@tbd test for errors!!

%indoor navigation database
%data: Test data, LH075 type design of a family house
%wire model: middleline+ wall thickness


%room(BUILD/FLOOR/ID,KIND,LEVEL,SIDES)
% ID:    ground, the name of the room
% BUILD: building ID
% FLOOR: floor number (0: ground floor
% KIND:  kind of room (office;lecture,common,storage;lab,corridor;
%                     elevator;bath;outside)
% LEVEL: floor height relative to the level height
% SIDES: list of wall sections: [side(WALL,CORNER1,CORNER2]
:- discontiguous room/4.
:- dynamic room/4.


%stairs(BUILD/FLOOR/ID,KIND,LEVEL,SIDES)
% ID:    ground, the name of the room
% BUILD: building ID
% FLOOR: floor number (0: ground floor)
%        or FLOOR=FL1-FL2 if it connects floors
% KIND: kind of stairs ('diff' if it just balances floor differences
%                      'floor' if it connects floors
% LEVEL: floor height relative to the level height
% STAIRS: list of wall sections: [side(WALL,CORNER1,CORNER2]
:- discontiguous stairs/4.
:- dynamic stairs/4.

%corridor(BUILDING/FLOOR/ROOM,LONGEST).
%ROOM/FLOOR/BUILDING: roomID/building ID/floor NR (normally integer)
%LONGEST:  the longer side of a corridor (X0/Y0)-(X/Y)
%SIDES:    sides of the room... see the description elsewhere
:- discontiguous corridorLength/2.
:- dynamic corridorLength/2.

:- discontiguous wall/4.
:- discontiguous wall/3.
:- dynamic wall/3, wall/4.

:- discontiguous corner/2.
:- dynamic corner/2.

:- discontiguous blockage/2.
:- dynamic blockage/2.


options([floorheight(-inf),scale(0.5* -0.5),diff(50/400),xychange(false)]).

signature('lak�h�z t�pusterv','LH075').


%Outside Wall corners (HOR/VERT)
corner(balconyNW,0/390).
corner(northWest,0/720).
corner(roomNWkitchen,405/720).
corner(bathNW,805/720).
corner(bathNE,1100/720).
corner(northEast,1440/720).
corner(roomNESEoutside,1440/350).
corner(southEast,1440/0).
corner(roomSESW,1100/0).
corner(roomSEtoilet,1100/120).
corner(toiletEntrySE,990/120).
corner(kitchenEntrySE,805/120).
corner(kitchenSE,805/0).
corner(balconySE,405/0).
corner(balconyNE,405/390).


%coordinates: East/North, cms)
%wall(B/FL/ID,KIND,LINES).
%B:     ground, building identifier
%FL:    ground, floor identifier (usually integer)
%ID:    ground wall identifier
%KIND:  polyline;polygon
%LINES: ordered list of line(CORNERS,SECTIONS) structures
wall(family/0/outsideWall,polygon,
     [line([balconyNW,northWest],[30/330.0]),
      line([northWest,roomNWkitchen,bathNW,bathNE,northEast],
           [30/405.0,30/400.0,30/295.0,30/340.0]),
      line([northEast,roomNESEoutside,southEast],
           [30/370.0,30/350.0]),
      line([southEast,roomSESW],[30/340.0]),
      line([roomSESW,roomSEtoilet],[30/120.0]),
      line([roomSEtoilet,toiletEntrySE,
            door(mainEntrance,right),door(mainEntrance,left),kitchenEntrySE],
           [30/110.0,30/10.0,door(mainEntrance,2,100),30/75.0]),
      line([kitchenEntrySE,kitchenSE],
           [30/120.0]),
      line([kitchenSE,balconySE],[30/400.0]),
      line([balconySE,door(balconyDoor,right),door(balconyDoor,left),balconyNE],
           [30/90.0,door(balconyDoor,2,100),30/200.0]),
      line([balconyNE,balconyNW],
           [30/405.0])
     ]).

corner(bathSW,805/405).
corner(bathSE,1100/405).
corner(roomNECorner,1210/405).
corner(roomNESE,1210/350).
corner(roomSECorner,1210/295).
corner(roomSEtoiletN,1090/295).
corner(toiletNW,990/295).
corner(kitchenEntryNW,805/295).


%wall(ID,KIND,CORNERS,SECTIONS).
%ID:      ground identifier
%KIND:    line
%CORNERS: ordered list of CORNER identifiers
%SECTIONS:ordered list of SECTION identifiers
wall(family/0/kitchenRoomNW,line,
     [balconyNE,door(roomNW,left),door(roomNW,right),roomNWkitchen],
     [30/180.0,door(roomNW,2,90),30/60.0]).

room(family/0/roomNW,sleeping,0,[side(outsideWall,balconyNE,balconyNW),
          side(outsideWall,balconyNW,northWest),
          side(outsideWall,northWest,roomNWkitchen),
          side(kitchenRoomNW,roomNWkitchen,balconyNE)]).

%from south to north
wall(family/0/kitchenEast,line,
     [kitchenSE,kitchenEntrySE,kitchenEntryNW,
      door(kitchen,left),door(kitchen,right),bathSW,bathNW],
	 [30/120.0,10/175.0,10/10.0,door(kitchen,2,90),10/10.0,10/315.0]).

room(family/0/kitchen,living,0,[side(outsideWall,roomNWkitchen,bathNW),
          side(kitchenEast,bathNW,kitchenSE),
          side(outsideWall,kitchenSE,balconySE),
          side(outsideWall,balconySE,balconyNE),
          side(kitchenRoomNW,balconyNE,roomNWkitchen)]).

wall(family/0/hallNorth,polygon,[
     line([bathSW,door(bath,left),door(bath,right),
          bathSE,door(roomNE,left),door(roomNE,right),roomNECorner],
	  [10/195.0,door(bath,2,90),10/10.0,10/10.0,
          door(roomNE,2,90),10/315.0]),
     line([roomNECorner,roomNESE],[10/55.0]),
     line([roomNESE,roomNESEoutside],[10/230.0])
    ]).

corner(toiletNE,1100/295).

wall(family/0/hallSouth,polygon,[
     line([kitchenEntryNW,door(hall,left),door(hall,right),toiletNW,
          toiletNE,door(roomSE,left),door(roomSE,right),roomSECorner],
	  [10/45.0,door(hall,2,130),10/10.0,
          10/110.0,10/10.0,door(roomSE,2,90),10/10.0]),
     line([roomSECorner,roomNESE],[10/55.0])
    ]).

wall(family/0/bathE,line,
     [bathNE,bathSE], [10/315.0]).

room(family/0/bath,bath,0,[side(outsideWall,bathNW,bathNE),
          side(bathE,bathNE,bathSE),
          side(hallNorth,bathSE,bathSW),
          side(kitchenEast,bathSW,bathNW )]).

room(family/0/hall,corridor,0,[side(kitchenEast,kitchenEntryNW,bathSW),
                               side(hallNorth,bathSW,roomNECorner),
                               side(hallNorth,roomNECorner,roomNESE),
                               side(hallSouth,roomNESE,roomSECorner),
                               side(hallSouth,roomSECorner,kitchenEntryNW)]).

room(family/0/roomNE,sleeping,0,[side(outsideWall,bathNE,northEast),
          side(outsideWall,northEast,roomNESEoutside),
          side(hallNorth,roomNESEoutside,roomNESE),
          side(hallNorth,roomNESE,roomNECorner),
          side(hallNorth,roomNECorner,bathSE),
          side(bathE,bathSE,bathNE)]).

wall(family/0/toiletE,line,
     [roomSEtoilet,toiletNE], [10/175.0]).

wall(family/0/toiletW,line,
     [toiletEntrySE,door(toilet,right),door(toilet,left),toiletNW],
     [10/80.0,door(toilet,2,75),10/20.0]).

room(family/0/roomSE,sleeping,0,[side(outsideWall,roomNESEoutside,southEast),
          side(outsideWall,southEast,roomSESW),
          side(outsideWall,roomSESW,roomSEtoilet),
          side(toiletE,roomSEtoilet,toiletNE),
          side(hallSouth,toiletNE,roomSECorner),
          side(hallSouth,roomSECorner,roomNESE),
          side(hallNorth,roomNESE,roomNESEoutside)]).

room(family/0/toilet,bath,0,[side(toiletE,toiletNE,roomSEtoilet),
                             side(outsideWall,roomSEtoilet,toiletEntrySE),
                             side(toiletW,toiletEntrySE,toiletNW),
                             side(hallSouth,toiletNW,toiletNE)]).

room(family/0/entry,corridor,0,[side(outsideWall,toiletEntrySE,kitchenEntrySE),
                                side(kitchenEast,kitchenEntrySE,kitchenEntryNW),
                                side(hallSouth,kitchenEntryNW,toiletNW),
                                side(toiletW,toiletNW,toiletEntrySE)]).


















