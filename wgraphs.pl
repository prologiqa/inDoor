/*  File:    wgraphs.pl
 *  Author: Kili�n Imre, 2021-22.
 *  some pieces taken over from:
 *        - Eike SR (GitHub https://github.com/eikesr/wgraphs)

    Modified by Kili�n Imre 28-Aug-2021. P�cs, Hungary
    - adaptation to node-edgelist-weight representation
    - definition of min_path/4
    - extended with other wgraph creation and maintenance 3-12-2022.
*/

:- module(wgraphs, [vertices_edges_to_wgraph/3,
              add_edges/3,
              error/2, reportError/1,
              symmetricError/2, reportSymmetricError/1,
              union/3,symmetric_closure/2, transpose/2,
                   min_path/5
                   ]).
%THROWS...
%
% union/3 THROWS
%                error(domain_error(non_conflicting_graph_edges,...))



% Using Carlo's, initial Dijkstra algorithm implementation
% (Purpose: learn graph programming with attribute variables
% https://copyprogramming.com/howto/dijkstras-shortest-path-algorithm)
:- use_module(dijkstraCarlo).
:- use_module(listExt,[neighbour2/3]).

:-op(400,xfy,:).

p_to_s_graph(P_Graph, S_Graph) :-
    sort(P_Graph, EdgeSet),
    p_to_s_vertices(EdgeSet, VertexBag),
    sort(VertexBag, VertexSet),
    p_to_s_group(VertexSet, EdgeSet, S_Graph).


p_to_s_vertices([], []).
p_to_s_vertices([A-Z-_|Edges], [A,Z|Vertices]) :-
    p_to_s_vertices(Edges, Vertices).


p_to_s_group([], _, []).
p_to_s_group([Vertex|Vertices], EdgeSet, [Vertex-Neibs|G]) :-
    p_to_s_group(EdgeSet, Vertex, Neibs, RestEdges),
    p_to_s_group(Vertices, RestEdges, G).


p_to_s_group([V1-X-W|Edges], V2, [X-W|Neibs], RestEdges) :- V1 == V2,
    !,
    p_to_s_group(Edges, V2, Neibs, RestEdges).
p_to_s_group(Edges, _, [], Edges).


%!  vertices_edges_to_wgraph(+Vertices, +Edges, -WGraph) is det.
%
%   Create a WGraph from Vertices and edges.   Given  a graph with a
%   set of Vertices and a set of   Edges,  WGraph must unify with the
%   corresponding S-representation. Note that   the vertices without
%   edges will appear in Vertices but not  in Edges. Moreover, it is
%   sufficient for a vertice to appear in Edges.
%
%   ==
%   ?- vertices_edges_to_wgraph([],
%                               [1-3-1.2,2-4-2.3,4-5-3.4,1-5-4.5], L).
%   L = [1-[3-1.2,5-4.5], 2-[4-2.3], 3-[], 4-[5-3.4], 5-[]] ==
%
%   In this case all  vertices  are   defined  implicitly.  The next
%   example shows three unconnected vertices:
%
%   ==
%   ?-
%   vertices_edges_to_wgraph([6,7,8],
%                            [1-3-1.2,2-4-2.3,4-5-3.4,1-5-4.5], L).
%   L = [1-[3,5], 2-[4], 3-[], 4-[5], 5-[], 6-[], 7-[], 8-[]] ==

vertices_edges_to_wgraph(Vertices, Edges, Graph) :-
    sort(Edges, EdgeSet),
    p_to_s_vertices(EdgeSet, IVertexBag),
    append(Vertices, IVertexBag, VertexBag),
    sort(VertexBag, VertexSet),
    p_to_s_group(VertexSet, EdgeSet, Graph).


%!  edges(+WGraph, -Edges) is det.
%
%   Edges is the set of edges in WGraph. Each edge is represented as
%   a pair From-To, where From and To are vertices in the graph.
edges(Graph, Edges) :-
    s_to_p_graph(Graph, Edges).


s_to_p_graph([], []) :- !.
s_to_p_graph([Vertex-Neibs|G], P_Graph) :-
    s_to_p_graph(Neibs, Vertex, P_Graph, Rest_P_Graph),
    s_to_p_graph(G, Rest_P_Graph).

s_to_p_graph([], _, P_Graph, P_Graph) :- !.
s_to_p_graph([Neib-Weight|Neibs], Vertex, [Vertex-Neib-Weight|P], Rest_P) :-
    s_to_p_graph(Neibs, Vertex, P, Rest_P).


%!  union(+Graph1, +Graph2, -NewGraph)
%
%   NewGraph is the union of Graph1 and Graph2. Example:
%
%   ```
%   ?- union([1-[2-1.2],2-[3-2.3]],
%                   [2-[4-3.4],3-[1-4.5,2-5.6,4-6.7]],L).
%   L = [1-[2-1.2], 2-[3-2.3,4-3.4], 3-[1-4.5,2-5.6,4-6.7]]
%   ```

union(Set1, [], Set1) :- !.
union([], Set2, Set2) :- !.
union([Head1-E1|Tail1], [Head2-E2|Tail2], Union) :-
    compare(Order, Head1, Head2),
    union(Order, Head1-E1, Tail1, Head2-E2, Tail2, Union).


union(=, HEAD-E1, _, _-E2, _, _) :-
    member(NODE-W1,E1), member(NODE-W2,E2), W1\=W2
      ->throw(error(domain_error(non_conflicting_graph_edges, HEAD-E1/HEAD-E2),
                    context(union/3, edge:(HEAD-NODE)))).
union(=, Head-E1, Tail1, _-E2, Tail2, [Head-Es|Union]) :-
    ord_union(E1, E2, Es),
    union(Tail1, Tail2, Union).
union(<, Head1, Tail1, Head2, Tail2, [Head1|Union]) :-
    union(Tail1, [Head2|Tail2], Union).
union(>, Head1, Tail1, Head2, Tail2, [Head2|Union]) :-
    union([Head1|Tail1], Tail2, Union).


%!  add_edges(+Graph, +Edges, -NewGraph)
%
%   Unify NewGraph with a new graph obtained by adding the list of Edges
%   to Graph. Example:
%
%   ```
%   ?- add_edges([1-[3-1.2,5-2.3],2-[4-3.4],3-[],4-[5-4.5],
%                 5-[],6-[],7-[],8-[]],
%                [1-6-5.6,2-3-6.7,3-2-7.8,5-7-8.9,3-2-9.1,4-5-9.2],
%                NL).
%   NL = [1-[3-1.2,5-4.5,6-5.6], 2-[3-6.7,4-3.4], 3-[2-7.8], 4-[5-9.2],
%         5-[7], 6-[], 7-[], 8-[]]
%   ```

add_edges(_, Edges, _) :-
    sort(Edges,SortEdges),
    conflicting_sortedges(SortEdges,FROM,TO-W1/TO-W2)
      ->throw(error(domain_error(non_conflicting_graph_edges,
                                 TO-W1/TO-W2),
                    context(add_edges/3, edge:(FROM-TO)))).
add_edges(Graph, Edges, NewGraph) :-
    p_to_s_graph(Edges, G1),
    union(Graph, G1, NewGraph).

conflicting_sortedges(SortEdges,FROM,TO-W1/TO-W2):-
    neighbour2(FROM-TO-W1,FROM-TO-W2,SortEdges).

%!  transpose(Graph, NewGraph) is det.
%
%   Unify NewGraph with a new graph obtained from Graph by replacing
%   all edges of the form V1-V2 by edges of the form V2-V1. The cost
%   is O(|V|*log(|V|)). Notice that an undirected   graph is its own
%   transpose. Example:
%
%     ==
%     ?- transpose([1-[3-1.2,5-2.3],2-[4-3.4],3-[],4-[5-4.5],
%                   5-[],6-[],7-[],8-[]], NL).
%     NL = [1-[],2-[],3-[1],4-[2-3.4],5-[1-2.3,4-4.5],6-[],7-[],8-[]]
%     ==
%
%   @compat  This  predicate  used  to   be  known  as  transpose/2.
%   Following  SICStus  4,  we  reserve    transpose/2   for  matrix
%   transposition    and    renamed    wgraph    transposition    to
%   transpose/2.

transpose(Graph, NewGraph) :-
    edges(Graph, Edges),
    vertices(Graph, Vertices),
    flip_edges(Edges, TransposedEdges),
    vertices_edges_to_wgraph(Vertices, TransposedEdges, NewGraph).

flip_edges([], []).
flip_edges([Key-Val-Weight|Pairs], [Val-Key-Weight|Flipped]) :-
    flip_edges(Pairs, Flipped).


invertEdge(FROM-TO-W,TO-FROM-W).

%symmetric_closure(+Graph, -Closure):-
symmetric_closure(Graph, Closure):-
    edges(Graph,Edges),
    maplist(invertEdge,Edges,InvertEdges),
    add_edges(Graph,InvertEdges,Closure).


%error(WGR,ERROR) is semidet.
%each vertices in edges must also be defined as vertices
%when the first error is reported, returns an error structure
error(WGR,ERROR):-
    maplist(arg(1),WGR,ALLNODES),
    member(_NODE-CHILDREN,WGR),
      member(CHILD-_W,CHILDREN),
        \+ member(CHILD,ALLNODES)->
            ERROR = error(existence_error('referred node',CHILD),wgraphs).

%reportError(WGR) is semidet.
%displays error message, if any
reportError(WGR):-
   error(WGR,ERROR), print_message(error,ERROR).

%each vertices must have the opposite pair, with the same weight
symmetricError(SWGR,ERROR):-
    error(SWGR,ERROR)->true;
    edges(SWGR,ALLEDGES),
    member(FROM-TO-W,ALLEDGES),
      \+ member(TO-FROM-W,ALLEDGES)->
        (member(TO-FROM-_,ALLEDGES)->
          ERROR = error(existence_error('asymmetric: weight',
                                        TO-FROM-W),wgraphs);
          ERROR = error(existence_error('asymmetric: arc',
                                        TO-FROM),wgraphs)).

reportSymmetricError(WGR):-
   symmetricError(WGR,ERROR), print_message(error,ERROR).


min_path(GRAPH,START,GOAL,PATH,DISTANCE):-
    dijkstra(GRAPH,START,SOLUTION),
    once(member(s(GOAL,DISTANCE,PATH),SOLUTION)).

:- begin_tests(wgraphs).
test(vertices_edges_to_wgraph,
     WGR = [1-[3-1.2,5-4.5], 2-[4-2.3], 3-[], 4-[5-3.4], 5-[]]):-
     vertices_edges_to_wgraph([],[1-3-1.2,2-4-2.3,4-5-3.4,1-5-4.5], WGR).


test(union,error(domain_error(non_conflicting_graph_edges,_-_/_-_))):-
    union([1-[2-1.2],2-[3-2.3],3-[2-6.5]],
                 [2-[4-3.4],3-[1-4.5,2-5.6,4-6.7]],_WGR).

test(union,WGR = [1-[2-1.2], 2-[3-2.3, 4-3.4], 3-[1-4.5, 2-5.6, 4-6.7]]):-
    union([1-[2-1.2],2-[3-2.3]],[2-[4-3.4],3-[1-4.5,2-5.6,4-6.7]],WGR).

test(union,WGR=[1-[2-1.2], 2-[3-2.3], 3-[1-4.5,2-5.6,4-6.7]]):-
    union([1-[2-1.2],2-[3-2.3]],[3-[1-4.5,2-5.6,4-6.7]],WGR).

test(add_edges,WGR=[1-[3-1.2,5-2.3,6-5.6],2-[3-6.7,4-3.4],
                    3-[2-7.8], 4-[5-9.2], 5-[7-8.9],
                    6-[], 7-[], 8-[]]):-
     add_edges([1-[3-1.2,5-2.3],2-[4-3.4],3-[],
                 5-[],6-[],7-[],8-[]],
                [1-6-5.6,2-3-6.7,3-2-7.8,5-7-8.9,4-5-9.2],
                WGR).

test(add_edges,error(domain_error(non_conflicting_graph_edges,
                                 _-_/_-_))):-
     add_edges([1-[3-1.2,5-2.3],2-[4-3.4],3-[],
                 5-[],6-[],7-[],8-[]],
                [1-6-5.6,2-3-6.7,3-2-7.8,5-7-8.9,3-2-9.1,4-5-9.2],
                _WGR).

test(add_edges,error(domain_error(non_conflicting_graph_edges,_-_/_-_))):-
     add_edges([1-[3-1.2,5-2.3],2-[4-3.4],3-[],4-[5-4.5],
                 5-[],6-[],7-[],8-[]],
                [1-6-5.6,2-3-6.7,3-2-7.8,5-7-8.9,4-5-9.2],
                _WGR).
test(add_edges,
    WGR = [1-[2-1.2], 2-[3-2.3, 4-3.4], 3-[1-4.5, 2-5.6, 4-6.7], 4-[]]):-
    add_edges([],[1-2-1.2,2-3-2.3,2-4-3.4,3-1-4.5,3-2-5.6,3-4-6.7],WGR).

test(transpose,
    WGR=[1-[],2-[],3-[1-1.2],4-[2-3.4],5-[1-2.3,4-4.5],6-[],7-[],8-[]]):-
    transpose([1-[3-1.2,5-2.3],2-[4-3.4],3-[],4-[5-4.5],
                   5-[],6-[],7-[],8-[]], WGR).

test(symmetric_closure,
    WGR=[1-[3-1.2,5-2.3],2-[4-3.4],3-[1-1.2],4-[2-3.4,5-4.5],
         5-[1-2.3,4-4.5],6-[],7-[],8-[]]):-
    symmetric_closure([1-[3-1.2,5-2.3],2-[4-3.4],3-[],4-[5-4.5],
                   5-[],6-[],7-[],8-[]], WGR).

test(error,fail):-
    error([1-[6-1.2,5-2.3],2-[4-3.4],3-[1-1.2],4-[2-3.4,5-4.5],
         5-[1-2.3,4-4.5],6-[],7-[],8-[]],_).

test(error,ERROR=error(existence_error('referred node',6),wgraphs)):-
    error([1-[6-1.2,5-2.3],2-[4-3.4],3-[1-1.2],4-[2-3.4,5-4.5],
         5-[1-2.3,4-4.5]],ERROR).

test(symmetricError,fail):-
    symmetricError([1-[3-1.2,5-2.3],2-[4-3.4],3-[1-1.2],4-[2-3.4,5-4.5],
         5-[1-2.3,4-4.5],6-[],7-[],8-[]],_).

test(symmetricError,E=error(existence_error('asymmetric: arc',
                                        _TO-_FROM),wgraphs)):-
    symmetricError([1-[5-2.3],2-[4-3.4],3-[1-1.2],4-[2-3.4,5-4.5],
         5-[1-2.3,4-4.5],6-[],7-[],8-[]],E).

test(symmetricError,E=error(existence_error('asymmetric: weight',
                                        _TO-_FROM-_W),wgraphs)):-
    symmetricError([1-[5-2.4],2-[4-3.4],3-[1-1.2],4-[2-3.4,5-4.5],
         5-[1-2.3,4-4.5],6-[],7-[],8-[]],E).

:- if(false).
test(2) :-
    open('salesman.pl', read, F),
    readf(F, L),
    close(F),
    nl,
    dijkstra(L, penzance, R),
    maplist(writeln, R).

readf(F, [d(X,Y,D)|R]) :-
    read(F, dist(X,Y,D)), !, readf(F, R).
readf(_, []).
:- endif.

:- end_tests(wgraphs).






