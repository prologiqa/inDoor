p%indoor navigation program with SWI-Prolog/WEB server
% or XPCE GUI package for local operation
%
% how to operate it:
% run/0   is an event loop that opens an xpce window and processes
%         the user's input... Closes XPCE at the quit command
% serve/1 is the Web-application operating mode
% stop/1  Stopping the server
%
% test/0..1, and test0/0..1 predicates for interactive testing
%         through programmed test cases
%
%@tbd: loadContext should throw error for non-existing datafile
%
:- module(indoor,[clos/0, clos/1         %close XPCE GUI
              ,clear/0, clear/1          %clear XPCE GUI
              ,init/2
              ,run/0, run/2              %only in XPCE modus (+OPTIONS)
              ,serve/1, stop/1           %run in HTTP server modus
              ,test/0 ,test/1, test/2, test/3 %test with XPCE GUI
              ,test0/0 ,test0/1          %test without GUI
          ]).

:- use_module(planar).
:- use_module(model).
:- use_module(display).


%@tbd:
%:- pathNavi(room(a101),door(elevator),P,W).
%:- pack_install(http).
%@tbd: calling with installation ID

%problem:
%checkElement(room/5,102).
%spy(checkSides).

% InDoor Navigation options
%:- nb_value(options).     global that stores option settings


%
% InDoor Navigation for Web GUI
%
%
:- use_module(library(dcg/high_order)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_parameters)).

:- use_module(library(http/http_json)).
:- use_module(library(http/http_files)).
:- use_module(library(http/http_server_files)).

:- use_module(library(debug)).


:- writeln(':- PORT=5000, serve(PORT).').
:- writeln('browser URL: http://localhost:PORT/?from=102&to=220').
:- writeln(':- PORT=5000, stop(PORT).').


:- http_handler('/', navigateHTTPhandler, []).

serve(Port) :-
        init(data,[xpce(false)]),
        http_server(http_dispatch, [port(Port)]).

stop(Port) :- http_stop_server(Port,[]).


%
% if DATA is a loaded context -> true
% if DATA is not loaded -> loads it + true
% if DATA doesnt exist ->false
optionalLoadContext(DATA):-
  currentContext(DATA)->true;
  loadContext(DATA)->true.

exists_value(NAME):-
  catch(nb_getval(NAME,_VALUE),
        error(existence_error(variable, NAME), _),fail).

optionalInitParameters(DATA,OPTIONS):-
  exists_value(frames)->true;
  initParameters(DATA,OPTIONS).


:- http_handler(root(.), navigateHTTPhandler, []).


item2Option(PRE,ITEM)-->
  {term_to_atom(ITEM,ITEMA)},
  ({ITEMA=PRE}->html(option([value=ITEMA,selected],ITEMA));
  html(option([value=ITEMA],ITEMA))).

list2Options(ITEMS,DEF)--> sequence(item2Option(DEF),ITEMS).


%are_http_parameters(REQUEST,PARAMS,ATTRDECL) is semidet.
%+REQUEST: HTTP request structure
%?PARAMS:  parameter list to be queried
%          empty values will be filled
%+ATTRDECL:attribute declaration facts
%Fails, if a requested parameter doesn't exist
are_http_parameters(REQUEST,PARAMS,ATTRDECL):-
  catch(http_parameters(REQUEST,PARAMS,ATTRDECL),_E,fail).


%+FRDOOR: boolean, from door? (oder room?)
%+FRFLA:  from floor (usually integer, but who knows)
%+FROM:   from door/room
%+TODOOR: boolean, to door? (oder room?)
%+TOFLA:  to floor (usually integer, but who knows)
%+TO:     to door/room
%+ELVTOR: boolean, use elevator (or not?)
%+FLH:    floorheight, if>=0 then 3D visualisation
%Generates a HTML internal form structure
htmlNaviForm(DATA,FRDOORDEF,FLFRDEF,FROMDEF
            ,TODOORDEF,FLTODEF,TODEF
            ,ELVTORDEF,FLHDEF,TITLE)-->
    {once(floors(DATA,B,FLOORS)), once(rooms(DATA,B,_FL,ROOMS)),
    (FRDOORDEF=true->FROMCHECKED=[checked];FROMCHECKED=[]),
    (TODOORDEF=true->TOCHECKED=[checked];TOCHECKED=[]),
    (ELVTORDEF=true->ELVCHECKED=[checked];ELVCHECKED=[])},
    {DATA:signature(LONGTITLE,TITLE)},
    [h1(LONGTITLE)
       ,form([
           label([for="elvname"],'Use elevator?'),
           input([type="checkbox",id="elevator",name="elevator"|ELVCHECKED]),
           label([for="floorheight"],'Floors difference:'),
           input([type="text", name="floorheight", value=FLHDEF]),
           \['<br>'],
           label([for="fname"],'From[door]:'),
           input([type="checkbox",id="frdoor",name="frdoor"|FROMCHECKED]),
           select([id="frfloor",name="frfloor"],
                  \list2Options(FLOORS,FLFRDEF)),
           input([type="text", name="from", value=FROMDEF, list="allRooms"]),
           datalist([id="allRooms"],\list2Options(ROOMS,[])),
           \['<br>'],
           label([for="fname"],'To[door]:'),
           input([type="checkbox",id="todoor",name="todoor"|TOCHECKED]),
           select([id="tofloor",name="tofloor"],
                  \list2Options(FLOORS,FLTODEF)),
           input([type="text", name="to", value=TODEF, list="allRooms"]),
           datalist([id="allRooms"],\list2Options(ROOMS,[])),
           input([type="submit", value="search"])
           ])
    ] .

atom_to_term('',''):- !.
atom_to_term(ATOM,TERM):- term_to_atom(TERM,ATOM).

request2DoorRoom(true,FLOORA,IDA,door(FLOOR/ID,_)):-
  !, atom_to_term(FLOORA,FLOOR), atom_to_term(IDA,ID).
request2DoorRoom(false,FLOORA,IDA,room(FLOOR/ID,_)):-
  atom_to_term(FLOORA,FLOOR), atom_to_term(IDA,ID).

%+FRDOOR: boolean, from door? (oder room?)
%+FRFLA:  from floor (usually integer, but who knows)
%+FROM:   from door/room
%+TODOOR: boolean, to door? (oder room?)
%+TOFLA:  to floor (usually integer, but who knows)
%+TO:     to door/room
%+ELVTOR: boolean, use elevator (or not?)
%+FLH:    floorheight, if>0 then 3D visualisation
%The procedure calculates and displays the path on HTML
htmlGenNaviPath(DATA,FRDOOR,FRFLA,FROMA
           ,TODOOR,TOFLA,TOA
           ,ELVTOR,FLH)-->
  {debug(inDoor,'htmlGenNaviPath(FRDOOR:~w,FRFL:~w,FROM:~w,\c
                  TODOOR:~w,TOFL:~w,TO:~w,ELV:~w,FLH:~w)',
         [FRDOOR,FRFLA,FROMA,TODOOR,TOFLA,TOA,ELVTOR,FLH])}
  ,{request2DoorRoom(FRDOOR,FRFLA,FROMA,FROM)
  ,request2DoorRoom(TODOOR,TOFLA,TOA,TO)
  ,OPTIONS = [elevator(ELVTOR),floorheight(FLH)]}
  ,{createNavi(DATA,OPTIONS)}
  ,(
    {catch(pathNavi(DATA,FROM,TO,PATH,_DISTANCE,_UPWALK,OPTIONS),
           error(existence_error(_,_),_), fail)},
    {PATH \= []}, htmlNaviPath(DATA,PATH,OPTIONS)
  ).

%decodes HTTP parameters,
%and generates HTML image
%+Request: HTTP request structure
%-DATA: Data context that will be a Window title
pageHTMLContent(Request,TITLE) -->
  %the call always succeeds <- each param has default value
  {are_http_parameters(Request,
                      [context(DATA,[default(data)])
                      ,frdoor(FRDOOR,[boolean,default(false)])
                      ,frfloor(FRFL,[default('')])
                      ,from(FROM,[default('')])
                      ,todoor(TODOOR,[boolean,default(false)])
                      ,tofloor(TOFL,[default('')])
                      ,to(TO,[default('')])
                      ,elevator(ELVTOR,[boolean,default(false)])
                      ,floorheight(FLH,[integer,default(800)])
                      ],
                      [attribute_declarations(param)])}
  ,{debug(inDoor,'Data context:[~w]',[DATA])}
  ,({optionalLoadContext(DATA), optionalInit(DATA,[])} ->
    {OPTIONS=[xpce(false),floorheight(FLH)]}
    ,{debug(inDoor,'Search path from door:[~w]~w/~w, To door:[~w]~w/~w FLH:~w',
                [FRDOOR,FRFL,FROM,TODOOR,TOFL,TO,FLH])}
    ,{optionalInitParameters(DATA,OPTIONS)}
    ,htmlNaviForm(DATA,FRDOOR,FRFL,FROM,TODOOR,TOFL,TO,ELVTOR,FLH,TITLE)
    ,{htmlNaviPicture(DATA,OPTIONS,PICTURE,PATH)}
      %,{debug(inDoor,'Picture:[~w]',[PICTURE])}
    ,({htmlGenNaviPath(DATA,FRDOOR,FRFL,FROM,
                        TODOOR,TOFL,TO,ELVTOR,FLH,PATH,[])} ->
      [p('Path found from: [~w]~w/~w to: [~w]~w/~w elevator:~w'-
               [FRDOOR,FRFL,FROM,TODOOR,TOFL,TO,ELVTOR])
      ,svg([height(8000),width(1000)],PICTURE)
      ]
    ; %when a path is not found
      {PATH=[]}
      ,[p('No path from: [~w]~w/~w To: [~w]~w/~w elevator:~w'-
            [FRDOOR,FRFL,FROM,TODOOR,TOFL,TO,ELVTOR])
      ,svg([height(8000),width(1000)],PICTURE)
      ])
  ; %when the data context doesnt exist
  {TITLE=DATA}, [p('Context file ~w doesn''t exist!'-[DATA])]).


param(context,[]).
param(from,[]).
param(to,[]).
param(frfloor,[]).
param(tofloor,[]).
param(frdoor,[]).
param(todoor,[]).
param(floorheight,[]).
param(elevator,[]).


navigateHTTPhandler(REQUEST):-
  pageHTMLContent(REQUEST,TITLE,HTML,[]),
    reply_html_page(title(TITLE),
                    HTML).


html_write:layout(svg,0-0,1-2).
html_write:layout(circle,0-0,empty).
html_write:layout(rect,0-0,empty).
html_write:layout(polygon,0-0,0-0).
html_write:layout(polyline,0-0,0-0).
html_write:layout(line,0-0,0-0).
html_write:layout(path,0-0,empty).


%
% InDoor Navigation for XPCE
%
%
:- use_module(library(pce)).
:- use_module(library(pce_main)).
:- use_module(library(pce_tick_box)).


location2Atom(room,FLOOR,ID,ATOM):- term_to_atom(FLOOR/ID,ATOM).

packLocation(LOCID,FLOOR,LOC,LOCSTR):-
  LOCSTR =.. [LOCID,FLOOR/LOC,_].


%+DOOROOM:   door/room - kind of location
%+FLOOR:     floor identifier (usually numbers)
%+LOCS:      location identifiers
%-LOCSTRS:   location structures
%-LOCATOMS:  location atoms (FLOOR/LOC)
locsAtoms(DOOROOM,FLOOR,LOCS,LOCSTRS,LOCATOMS):-
  maplist(packLocation(DOOROOM,FLOOR),LOCS,LOCSTRS),
  maplist(term_to_atom,LOCS,LOCATOMS).

%creates a location dictionary indexed by (room;door) and floor ID
%locations(+DATA,-LOCDICT) is det.
locations(DATA,LOCDICT):-
  once(floors(DATA,B,_)),
  setof(FL-ROOMS,
        setof(ROOM,DATA:K^L^S^ROOM^room(B/FL/ROOM,K,L,S),ROOMS),
        ROOMSMX), dict_create(ROOMSDICT,rooms,ROOMSMX),
  setof(FL-DOORS,
        setof(DOOR,planar:K^XY^DIST^L^R^XYD^
        doorInRoom(DATA,B/FL/R,K,XY,L,DOOR,XYD,DIST),DOORS),
        DOORSMX), dict_create(DOORSDICT,doors,DOORSMX),
  dict_create(LOCDICT,locations,[door-DOORSDICT,room-ROOMSDICT]).


fireLocFloor(MENU,LOCDICT,DOOROOM,FL):-
  LOCS = LOCDICT.DOOROOM.FL,
  debug(inDoor,"fireLocFloor(.,~w,~w,~w)",[LOCDICT,DOOROOM,FL]),
  locsAtoms(DOOROOM,FL,LOCS,_LOCSTRS,LOCATOMS),
  send(MENU,clear), send_list(MENU,append,LOCATOMS).

makeDialog(DATA,SEARCH_CALL) :-
  new(POPUP, dialog("What are you searching for?")),

  once(floors(DATA,_B,FLOORS)), FLOORS = [FL0|FLRS], locations(DATA,LOCDICT),

  new(FROM, menu("Start from", cycle)),

  (FLRS==[]->
     new(LOCFROM, menu("From",marked
                   ,message(@prolog,fireLocFloor,FROM,
                            prolog(LOCDICT),@arg1,FL0)))
    ,send(FROM,right,LOCFROM)
   ;
   new(FLFROM, menu("Floor", cycle)),
     send_list(FLFROM, append, FLOORS),
     new(LOCFROM, menu("From",marked)),
     send(LOCFROM, message
                   ,message(@prolog,fireLocFloor,FROM,
                            prolog(LOCDICT)
                            ,@arg1,FLFROM?selection)),
     send(FLFROM,message,message(@prolog,fireLocFloor,FROM,
                           prolog(LOCDICT),
                           LOCFROM?selection,@arg1)),
     send(FLFROM,right,LOCFROM), send(FROM,right,FLFROM)
  ),
  send_list(LOCFROM, append, [room, door]),

  get(LOCFROM,selection,DOOROOMFROM),
  fireLocFloor(FROM,LOCDICT,DOOROOMFROM,FL0),

  new(TO,  menu('Destination', cycle)),

  (FLRS==[]->
     new(LOCTO, menu("To",marked
                   ,message(@prolog,fireLocFloor,TO,
                            prolog(LOCDICT),@arg1,FL0)))
    ,send(TO,right,LOCTO);

   new(FLTO,  menu('Floor', cycle)),
     send_list(FLTO, append, FLOORS),
     new(LOCTO, menu("To",marked)),
     send(LOCTO, message
                   ,message(@prolog,fireLocFloor,TO,
                            prolog(LOCDICT),
                            @arg1,FLTO?selection)),
     send(FLTO,message,message(@prolog,fireLocFloor,TO,
                          prolog(LOCDICT),
                          LOCTO?selection,@arg1)),
     send(FLTO,right,LOCTO), send(TO,right,FLTO)
  ),
  send_list(LOCTO, append, [room, door]),

  get(LOCTO,selection,DOOROOMTO),
  fireLocFloor(TO,LOCDICT,DOOROOMTO,FL0),

  (FLRS=[]->
     send_list(POPUP,append, [LOCFROM,
                           FROM,LOCTO,TO]);
   send_list(POPUP,append,[LOCFROM,
                            FROM,FLFROM,LOCTO,
                            TO,FLTO])),

  send(POPUP,append,new(TB,tick_box("Use elevator:")),next_row),
  send(POPUP, append,new(FLH,text_item(floor_height)),right),
  send(FLH,length,8),
  (FLRS=[]->
   send_list(POPUP,append, [
             button(exit,and(message(POPUP, destroy),
                             message(@prolog,clos,prolog(DATA)))),
             button(search, message(@prolog,SEARCH_CALL,prolog(DATA),
                                    prolog(LOCDICT),
                                    LOCFROM?selection,
                                    prolog(FL),FROM?selection,
                                    LOCTO?selection,
                                    prolog(FL),TO?selection,
                                    FLH?selection,
                                    prolog(POPUP))
                    )])
  ;
   send_list(POPUP,append, [
             button(exit,and(message(POPUP, destroy),
                             message(@prolog,clos,prolog(DATA)))),
             button(search, message(@prolog,SEARCH_CALL,prolog(DATA),
                                    prolog(LOCDICT),
                                    LOCFROM?selection,
                                    FLFROM?selection,FROM?selection,
                                    LOCTO?selection,
                                    FLTO?selection,TO?selection,
                                    FLH?selection,
                                    prolog(POPUP))
                    )])
  ),


  send(TB,message,message(@prolog,reCreateNavi,DATA,TB?selection)),

  send(POPUP,append,label(reporter),below),

  send(POPUP, default_button, search),

  send(POPUP, open).

xpce2Boolean(@on,true).
xpce2Boolean(@off,false).

reCreateNavi(DATA,ELXPCE):-
  debug(inDoor,'use elevator:~w\n',[ELXPCE]),
  once(xpce2Boolean(ELXPCE,EL)),
  debug(inDoor,'use elevator:~w\n',[EL]),
  createNavi(DATA,[elevator(EL)]).

findDisplayPath(DATA,_LOCDICT,
                DOOROOMFR,FLFROMID,FROMID,
                DOOROOMTO,FLTOID,TOID,FLHS,
               POPUP):-
  term_to_atom(FROMX,FROMID), term_to_atom(FLFROM,FLFROMID),
  FROM =.. [DOOROOMFR,FLFROM/FROMX,_],
  term_to_atom(TOX,TOID), term_to_atom(FLTO,FLTOID),
  TO =.. [DOOROOMTO,FLTO/TOX,_],
  (atom_number(FLHS,FLH)->true;FLH= -inf),
  debug(inDoor,"findDisplayPath(~w->~w):~w\n",[FROM,TO,FLH]),
  nb_getval(options,OPTS), OPTIONS=[floorheight(FLH)|OPTS],
  clearNaviPicture(DATA), createNaviPicture(DATA,OPTIONS),
  pathNavi(DATA,FROM,TO,PATH,W,UP,OPTIONS),
  debug(inDoor,"Walk: ~w Up: ~w\n",[W,UP]),
  drawNaviPath(DATA,PATH,OPTIONS),
  send(POPUP,
       report(inform,"%5.2f meters walk, and %4.2f meters upstairs",W,UP)).


% operation modes and their options
%
% queries and their options
%
%creates navi graph description and building walls
%in both modi (xpce and html)

:- predicate_options(init/2,2,[xpce(boolean),        %=true
                               scale(false),         %=0.15*0.15
                               diff(false),
                               floorheight(integer), %=300
                               framedist(integer),   %=300
                               paththickcol(false),  %red-4
                               stairsthick(integer), %=1
                               xychange(false),      %=false
                               xpce(boolean)]).      %true
%circumvent(integer)=50 <<<---- bypassdist(ance)
%
%init(+DATA,+OPTIONS)...
%Fails if DATA as a context doesnt exist
%OPTIONS parameter comes first, then the context-options
init(DATA,OPTIONS):-
  loadContext(DATA),
  DATA:options(OPTS0), append(OPTIONS,OPTS0,OPTS),
  nb_setval(options,OPTS), display:initParameters(DATA,OPTS),
    (option(xpce(true),OPTS)->createNaviPicture(DATA,OPTS);true),
    createNavi(DATA,OPTS).


optionalInit(DATA,OPTIONS):-
  nb_setval(options,OPTIONS),
    display:initParameters(DATA,OPTIONS),
    createNavi(DATA,OPTIONS).


%the run(..) family serves only for XPCE GUI
%the procedure itself inserts an xpce(true) option
%
:- predicate_options(run/2,2,[ scale(false),         %=0.15*0.15
                               diff(false),
                               floorheight(integer), %=300
                               framedist(integer),   %=300
                               paththickcol(false),  %red-4
                               stairsthick(integer), %=1
                               xychange(false),      %=false
                               xpce(boolean)]).      %true
run(DATA,OPTIONS,_ARGV):-
    XPCEOPTS = [xpce(true)|OPTIONS],
    init(DATA,XPCEOPTS),
    clearNaviPicture(DATA), createNaviPicture(DATA,XPCEOPTS),
    makeDialog(DATA,findDisplayPath).

run(DATA,OPTIONS):-
  run(DATA,OPTIONS,[]).


run:- run(data,[floorheight(800)]).


clos(DATA):- closeNaviPicture(DATA).

clos:- clos(data).


clear(DATA):- clearNaviPicture(DATA).

clear:- clear(data).

%
% InDoor Navigation... Tests...
%
%
testcase(0,'F',door(0/103,_),room(0/109,_)).
testcase(1,'F',door(0/102,_),room(0/b101,_)).
testcase(2,'F',room(0/b101,_),door(0/102,_)).
testcase(3,'F',room(0/104,_),room(0/b108,_)).
testcase(4,'F',room(0/103,_),room(0/a110,_)).
testcase(5,'F',door(0/sideEntrance,_),room(0/110,_)).
testcase(6,'F',door(0/mainCorridor,_),room(0/111,_)).
testcase(7,'F',door(0/mainEntrance,_),door(0/sideEntrance,_)).
testcase(8,'F',door(0/104,_),room(0/b101,_)).
testcase(9,'F',room(-1/2,_),door(-1/null2,_)).

testcase(10,'F',room(1/206,_),door(0/104,_)).
testcase(11,'F',door(0/mainEntrance,_),door(1/mensRoom,_)).
testcase(12,'F',room(0/102,_),door(1/221,_)).
testcase(13,'F',door(0/mainEntrance,_),room(0/elevator,_)).
testcase(14,'F',room(1/elevator,_),room(0/elevator,_)).
testcase(15,'F',door(-1/2,_),room(0/102,_)).
testcase(16,'F',door(1/elevator,_),door(1/211,_)).
testcase(17,'F',door(-1/2,_),door(-1/4,_)).
testcase(18,'F',room(-1/2,_),room(1/212,_)).
testcase(20,_,room(0/bath,_),room(0/roomNE,_)).
testcase(21,_,door(0/toilet,_),door(0/mainEntrance,_)).


:- write(":-init(data,[xpce(true),floorheight(800)])\n").

%test of the system, only with XPCE
:- predicate_options(test/3,3,[xpce(boolean),        %=true
                               scale(false),         %=0.15*0.15
                               diff(false),          %=0/0
                               floorheight(integer), %=300
                               framedist(integer),   %=300
                               paththickcol(false),  %red-4
                               stairsthick(integer), %=1
                               xychange(false),      %=false
                               xpce(boolean)]).      %true

test(DATA,TCASE,OPTIONS):-
  testcase(TCASE,B,FROM,TO),
    OPTS = [xpce(true)|OPTIONS],
    writeln(B/FROM-TO), init(DATA,OPTS),
    clearNaviPicture(DATA), createNaviPicture(DATA,OPTS),
    createNavi(DATA,OPTS), pathNavi(DATA,FROM,TO,PATH,_W,_UP,OPTS),
    drawNaviPath(DATA,PATH,OPTS).

test(TCASE,OPTIONS):- test(data,TCASE,OPTIONS).

test(TCASE):-
  test(TCASE,[circumvent=50]).

test:- test(0).

%test of the system - without any presentation
test0(TCASE):-
  DATA=data, testcase(TCASE,BUILDING,FROM,TO), OPTS=[circumvent=50],
    createNavi(DATA,OPTS),
    pathNavi(DATA,FROM,TO,PATH,LENGTH,UPSTAIRS,OPTS),
    write('Building: '), writeln(BUILDING),
    write('Route: '), writeln(FROM-TO),
    write('Path: '), writeln(PATH),
    write('Length: '), writeln(LENGTH),
    write('Upstairs: '), writeln(UPSTAIRS).

test0:-  test0(0).


:-writeln(':-test.          %example navigation w displaying').
:-writeln(':-test(TESTNR).  %example navigation w displaying').

:-writeln(':-test0.         %example navigation w/o displaying').
:-writeln(':-test0(TESTNR). %example navigation w/o displaying\n\n').


:-writeln(':-doCheckAllIndoor.').

:-writeln(':-run(data,[xpce(true),floorheight(800)]).').
:- op(1,fx,@).














