:- module(listExt,[neighbour2/3, circNeighbour2/3,
                   ordMember/3, member2/4,
                   circular/2, revapp/3,
                  duplicates/2]).
%
%planar graph operations / operations on the wall-graph
%Created by: Kili�n Imre, 30-July-2021
%
%
%
%list operations extensions
%
%if X,Y are neighbouring elements in the list
neighbour2(X,Y,[X,Y|_]).
neighbour2(X,Y,[_,Z|LIST]):- neighbour2(X,Y,[Z|LIST]).


%member2: members of 2 parallel lists
member2(X1,[X1|_],X2,[X2|_]).
member2(X1,[_|T1],X2,[_|T2]):-
  member2(X1,T1,X2,T2).

%circMember2(X,Y,LIST).
%the last element is the neighbour of the first one
%the list must have at least two members!
%
circNeighbour2(LAST,FIRST,FIRST,[LAST]).
circNeighbour2(X,Y,_,[X,Y|_]).
circNeighbour2(X,Y,FIRST,[_,Z|LIST]):-
  circNeighbour2(X,Y,FIRST,[Z|LIST]).

circNeighbour2(X,Y,[FIRST|LIST]):- circNeighbour2(X,Y,FIRST,[FIRST|LIST]).


%if X and Y are both members of the list, in the given order
ordMember(X,Y,[X|T]):- member(Y,T).
ordMember(X,Y,[_|T]):- ordMember(X,Y,T).


%CYCLIC is a circularly shifted variant of LIST
circular(LIST,CYCLIC):-
    append(HEAD,[FIRST|TAIL],LIST),
    append([FIRST|TAIL],HEAD,CYCLIC).

%duplicates(+LIST,-DUPS).
%finds duplicate/more than once occuring elements in LIST
%DUPS is an ordered list of such elements
duplicates(LIST,DUPS):-
    findall(X,(member(X,LIST),findall(X,member(X,LIST),XS),
               XS=[_,_|_]),DUPALL),
    sort(DUPALL,DUPS).


revapp([],L,L).
revapp([X|L1],L2,L3):- revapp(L1,[X|L2],L3).

revapp([],[],L,L).
revapp([X|L1],[_|L],L2,L3):- revapp(L1,L,[X|L2],L3).




