:-module(geometry,[point2Distance/3,
                   isSame/2, isParallel/2,
                   perpendicular/3, perpendicular/4,
                   distance/3, parallelNearer/4, parallelFarther/4,
                   isInside/2, isNearer/3,
                   intersect/3, intersectSurface/4, intersectSurface/5,
                   linear/3, middleSection/3,
                   moveSection/3, moveSectionTo/3,
                   normSizes/3, normRectSizes/3,
                   inflateSect/4, inflatePolygon/3,
                   inflatePolyline/3, inflatePolyline/4,
                   perimeter/2, perimeter/4,
                   shiftPolygon/2, orthogonalShift/3,
                   turn/3,
                   areaPolygon/2,
                   centroidPolygon/2]).


% @tbd some more tests with more complicated polygons
%
% @tbd point2Line mindk�t ir�nyban tesztelni
% @tbd line2Point �t�rni az el�z�v�
% @tbd lineCircleIntersection kanonikus egyenessel tesztelni

:- use_module(listext).


%borders are NOT symmetric!!!
%default case: left closed, right open
isBetweenOver(X,X,X):- ! .
isBetweenOver(X0,X,X1):- X0<X1, X0 =< X, ! .
isBetweenOver(X0,X,X1):- X0>X1, X0 >= X .


%borders are NOT symmetric!!!
%default case: left closed, right open
isBetween(X1,X,_):- X1 =:= X, !.
isBetween(X1,X,X2):- X1<X, X<X2, ! .
isBetween(X1,X,X2):- X2<X, X<X1 .

% <> both sides open interval
% == both sides closed interval
% => left closed, right open
% <= left open, right closed
isBetween(X1,X,X2,==):- X1=<X, X=<X2, ! .
isBetween(X1,X,X2,==):- X2=<X, X=<X1 .
isBetween(X1,X,X2,<=):- X1<X, X=<X2, ! .
isBetween(X1,X,X2,<=):- X2=<X, X<X1 .
isBetween(X1,X,X2,=>):- X1=<X, X<X2, ! .
isBetween(X1,X,X2,=>):- X2<X, X=<X1 .
isBetween(X1,X,X2,<>):- X1<X, X<X2, ! .
isBetween(X1,X,X2,<>):- X2<X, X<X1 .


%
% rangeIntersection(LOW1-UP1,LOW2-UP2,LOW-UP)
% LOW1-UP1:  first section/range
% LOW2-UP3:  second section/range
% LOW-UP:    result section/range
% sections are from left closed, from right open
% WARNING: ASSYMETRIC PARAMETERS!!
% result range direction follows that of the first one
% WARNING: no check of X-X ranges (they are also returned)
%
rangeIntersection(L0-U0,L1-U1,L-U):-
  L0=<U0, !, (L1=<U1 -> L is max(L0,L1), U is min(U0,U1), L=<U;
              rangeIntersection(L0-U0,U1-L1,U-L)).
rangeIntersection(U0-L0,L1-U1,U-L):-
  U0>L0, (L1=<U1 -> L is max(L0, L1), U is min(U0,U1), L=<U;
          rangeIntersection(U0-L0,U1-L1,L-U)).

% rayRangeIntersection(LOW0-UP0,LOW1-UP1,LOW-UP)
% LOW0-UP0:  ray, starting at LOW0, passing also UP0
% LOW1-UP1:  section/range to intersect with ray
% LOW-UP:    result section
% sections are from left closed, from right open
% WARNING: ASSYMETRIC PARAMETERS!!
% result range direction follows that of the ray
% WARNING: no check of X-X ranges (they are also returned)
%
rayRangeIntersection(L0-U0,L1-U1,L-U):-
  L0=<U0, !, (L1=<U1 -> L is max(L0,L1), U1>=L0, U=U1, L=<U;
            rayRangeIntersection(L0-U0,U1-L1,U-L)).
rayRangeIntersection(U0-L0,L1-U1,U-L):-
  U0>L0, !, (L1=<U1 -> L1=<U0, L is L1, U is min(U0,U1), L=<U;
            rayRangeIntersection(U0-L0,U1-L1,U-L)).

%creates DISTANCE as a float
point2Distance(X0/Y0,X1/Y1,DISTANCE):-
  DISTANCE  is sqrt((X1-X0)*(X1-X0)+(Y1-Y0)*(Y1-Y0)).

middleSection(X0/Y0,X/Y,XM/YM):- XM is (X+X0)/2, YM is (Y+Y0)/2.


parallelSame(line(A1,B1,C1),line(A2,B2,C2)):-
  A1*A2 =:= 0.0 -> B1*C2 =:= C1*B2; %horizontal lines
  A1*C2 =:= C1*A2. %vertical/other lines

isParallel(line(A1,B1,_C1),line(A2,B2,_C2)):-
  A1*B2 =:= A2*B1.

isSame(LINE1,LINE2):-
  isParallel(LINE1,LINE2),
  parallelSame(LINE1,LINE2).


%normParallelLine(+LINE0,+LINE,-LINE1)
%If LINE0 is parallel with LINE
%LINE1 is the normalized version of LINE,
%according to LINE0
normParallelLine(line(A0,B0,C0),line(A1,B1,C1),line(A0,B,C)):-
  A1=\=0, !, isParallel(line(A0,B0,C0),line(A1,B1,C1)),
    B is B1*A0/A1, C is C1*A0/A1.
normParallelLine(line(A0,B0,C0),line(A1,B1,C1),line(A,B0,C)):-
  B1=\=0, !, isParallel(line(A0,B0,C0),line(A1,B1,C1)),
    A is A1*B0/B1, C is C1*B0/B1.

%
%perpendicular vectors is nondet
%clockwise result first
%counterclockwise then
%perpendicularVector(X/Y0,Y/X):- Y is -Y0.
%perpendicularVector(X0/Y,Y/X):- X is -X0.


projectPerpendicularly(line(A,B,C),X0/Y0,X/Y):-
  DIST is abs(A*X0+B*Y0+C)/sqrt(A*A+B*B), DIST >0,
  X is X0 - DIST * A / sqrt(A * A + B * B),
  Y is Y0 - DIST * B / sqrt(A * A + B * B).


%findPerpendicular(+LINE1,+XY0,-LINE2)
%finds a line that is perpendicular to LINE1, and passes XY0
%(XY0 should be on LINE1 - (later could be somewhere else))
findPerpendicular(line(A0,B0,C0),X0/Y0,line(A,B,C)):-
  A0*X0+B0*Y0+C0=:=0, !, A = B0, B is -A0, C is -A*X0-B*Y0.
findPerpendicular(line(A0,B0,C0),X0/Y0,LINE):-
  A0*X0+B0*Y0+C0=\=0, !, projectPerpendicularly(line(A0,B0,C0),X0/Y0,XY),
  point2Line(X0/Y0,XY,LINE).
findPerpendicular(line(XY0-XY1),X0/Y0,line(A,B,C)):-
  point2Line(XY0,XY1,LINE0),
  %A = B0, B is -A0, C is -A*X0-B*Y0.
  findPerpendicular(LINE0,X0/Y0,line(A,B,C)).


%the slope of a perpendicular line is
%the negative reciprocal of the original slope
%isPerpendicular(+LINE1,+LINE2) is semidet.
%+LINE1, +LINE2: line(A,B,C), normal form of lines
isPerpendicular(line(A1,B1,_C1),line(A2,B2,_C2)):-
  A1*A2 + B1*B2 =:= 0.

perpendicular(LINE1,LINE2,XY):-
  ground(LINE1-LINE2), !, isPerpendicular(LINE1,LINE2),
  intersect(LINE1,LINE2,XY).
perpendicular(LINE1,LINE2,XY):-
  ground(LINE1), ground(XY), !, findPerpendicular(LINE1,XY,LINE2).

%perpendicular(+LINE,+XY0,-XY,+DIST) is nondet.
%finds a perpendicular line that passes XY0,
%and finds XY a point that is in the given DISTance from LINE
%There are 2 such points!
%+XY0: a point on LINE
%?XY: a point, far from LINE
%+DIST: the distance from XY
%+LINE1: line(A,B,C), normal form of lines
perpendicular(LINE0,XY0,XY,DIST):-
  ground(LINE0-XY0-DIST), findPerpendicular(LINE0,XY0,LINE),
  intersect(LINE,circle(XY0,DIST),XY).
perpendicular(LINE0,XY0,XY,DIST):-
  ground(LINE0-XY), projectPerpendicularly(LINE0,XY0,XY),
  distance(XY0,XY,DIST).


%
% finds an ortogonally shifted section
% left (counterclockwise) to XY0->XY1
% at the given DISTANCE
orthogonalShift(XY0-XY1,DISTANCE,XY01-XY11):-
  lineDistance(line(XY0-XY1),DISTANCE,XY11,_),
  lineDistance(line(XY1-XY0),DISTANCE,_,XY01), !.


%lineAngle(+ line(A,B,C),SIN,COS).
%lineAngle(+ XY1-XY2,SIN,COS).
lineAngle(line(A,B,C),SIN,COS):-
  %tbd: finding the common point and two more points on the lines
  !, line2Point(A,B,C,XY1-XY2), lineAngle(XY1-XY2,SIN,COS).
lineAngle(X1/Y1-X2/Y2,SIN,COS):- point2Distance(X1/Y1,X2/Y2,DIST),
  SIN is (Y2-Y1)/DIST, COS=(X2-X1)/DIST.

%lineAngle(+ line(A,B,C),ANGLE).
lineAngle(line(A,B,_C),ANGLE):-
  ANGLE is atan2(-A,B).


centSumPolygon(X0/Y0,[X/Y],SX0/SY0,SX/SY):- !,
  SX is SX0+(X+X0)*(X*Y0-X0*Y), SY is SY0+(Y+Y0)*(X*Y0-X0*Y).
centSumPolygon(XY0,[X1/Y1,X2/Y2|POLYGON])-->
  centSumPolygon(X2/Y2,[X1/Y1]),
  centSumPolygon(XY0,[X2/Y2|POLYGON]).

%baricenter, center of mass, weight-center
centroidPolygon([XY0|POLYGON],X/Y):-
  centSumPolygon(XY0,[XY0|POLYGON],0/0,XS/YS),
    signedAreaPolygon([XY0|POLYGON],AREA), X is XS/6/AREA, Y is YS/6/AREA.


% finds a point in a given orthogonal distance
% from a given point of a line
% lineDistance(+line(A,B,C),+XY0,+DIST,-X1Y1,-X2Y2)
% +A,B,C: line coefficients
% +X0/Y0: reference point laying on the line(A,B,C)
% +DIST: given distance
% -X1/Y1: point 1 found
% -X2/Y2: point 2 found
lineDistance(line(A,B,C),X0/Y0,DIST,X1/Y1,X2/Y2):-
  assertion(A*X0+B*Y0+C=:=0), assertion(DIST>0), !,
  LENGTH is sqrt(A*A+B*B), DX is A/LENGTH, DY is -B/LENGTH,
  X1 is X0 - DY*DIST, Y1 is Y0 + DX*DIST,
  X2 is X0 + DY*DIST, Y2 is Y0 - DX*DIST.

lineDistance(line(XY0-XY1),X0/Y0,DIST,X1/Y1,X2/Y2):-
  point2Line(XY0,XY1,LINE), !,
  lineDistance(LINE,X0/Y0,DIST,X1/Y1,X2/Y2).


% finds the pair of points in a given orthogonal distance
% from another point of the line, when the line is given by two points
% the given point to set perpendicular is the second point of the line
% (the tip of the arrow)
% counterclockwise: (left) first result,
% clockwise:        (right) second result
% +X0/Y0: starting point
% +X/Y:   endpoint
% +DIST:  distance
% -XL/YL: left point (counterclockwise)
% -XR/YR: right point (clockwise)
lineDistance(line(X0/Y0-X/Y),DIST,XL/YL,XR/YR):-
  assertion(DIST>0),
  DX is X-X0, DY is Y-Y0, LENGTH is sqrt(DX*DX + DY*DY),
  XL is X-DY/LENGTH*DIST, YL is Y+DX/LENGTH*DIST,
  XR is X+DY/LENGTH*DIST, YR is Y-DX/LENGTH*DIST.


%Finds the perpendicular distance of point from a line
%distance(+line(A,B,C),+X0/Y0,-DIST) semidet. fails on incorrect data
distance(X0/Y0,X/Y,DIST):-
  !, DIST is sqrt((X-X0)*(X-X0)+(Y-Y0)*(Y-Y0)).
distance(line(A,B,C),X0/Y0,DIST):-
  ground(A-B-C-X0-Y0), !,
  DIST is abs(A*X0+B*Y0+C)/sqrt(A*A+B*B).
distance(line(XY1-XY2),XY0,DIST):-
  ground(XY1-XY2-XY0), !, point2Line(XY1,XY2,LINE0),
  distance(LINE0,XY0,DIST).
%Finds the distance between two parallel lines
%distance(+line(A1,B1,C1),+line(A2,B2,C2),-DIST) semidet
distance(line(A,B,C1),line(A,B,C2),DIST):-
  ground(A-B-C1-C2), !, DIST is abs(C2-C1)/sqrt(A*A+B*B).
% Finds the parallel lines in a given distance from a given line
% distance(+line(A1,B1,C1),-line(A2,B2,C2),+DIST) nondet
distance(line(A,B,C1),line(A,B,C),DIST):-
  ground(A-B-C1-DIST), !, CDIST is DIST*sqrt(A*A+B*B),
  (C is C1+CDIST; C is C1-CDIST).
distance(LINE1,LINE2,DIST):-
  ground(LINE1-LINE2), !, normParallelLine(LINE1,LINE2,LINE),
  distance(LINE1,LINE,DIST).
%line in line(XY0-XY) format
distance(line(X0/Y0-X1/Y1),LINE,DIST):-
  !, point2Line(X0/Y0,X1/Y1,LINE0),
  distance(LINE0,LINE,DIST).


isNearer(XY,XYTHIS,XYTHAN):-
  distance(XY,XYTHIS,THISDIST), distance(XY,XYTHAN,THANDIST),
  THISDIST<THANDIST, !.


% Finds the nearer parallel line in a given distance from a given line
% parallelNearer(LINE0,LINE,+DIST,+REF) nondet
%+LINE0: given reference line
%-LINE:  result line
%+DIST:  given distance
%+REF:   reference point X0/Y0
parallelNearer(LINE0,L1,DISTANCE,REF):-
  setof(D-L,(distance(LINE0,L,DISTANCE),
             distance(L,REF,D)),[_-L1,_]).

% Finds the farther parallel line in a given distance from a given line
% parallelFarther(LINE0,LINE,+DIST,+REF) nondet
%+LINE0: given reference line
%-LINE:  result lineintersectSurface(line(600/777-600/1960),sect(580.0/1321.0,580.0/2011.0),X).
%+DIST:  given distance
%+REF:   reference point X0/Y0
parallelFarther(LINE0,L2,DISTANCE,REF):-
  setof(D-L,(distance(LINE0,L,DISTANCE),
               distance(L,REF,D)),[_,_-L2]).


%turn left: -90
%turn right: +90
turn(-90,X0/Y0-X/Y,Y0/X-Y/X0).
turn(90,X0/Y0-X/Y,Y/X0-Y0/X).

moveSection(X0/Y0-X/Y,XD/YD,XR0/YR0-XR/YR):-
  XR0 is X0+XD, XR is X+XD, YR0 is Y0+YD, YR is Y + YD.

moveSectionTo(X0/Y0-X/Y,XTO/YTO,XR0/YR0-XR/YR):-
  XD is XTO-X0, YD is YTO-Y0,
  moveSection(X0/Y0-X/Y,XD/YD,XR0/YR0-XR/YR).


normSizes(HEIGHT*WIDTH,RATEH/RATEW,H*W):- !,
  H is HEIGHT*RATEH, W is WIDTH*RATEW.
normSizes(HEIGHT
          *WIDTH,RATE,H*W):-
  normSizes(HEIGHT*WIDTH,RATE/RATE,H*W).


%XY0 (startpoint) remains in place, XY moves
normRectSizes(X0/Y0-X/Y,NORM,X0/Y0-XR/YR):-
  point2Distance(X0/Y0,X/Y,LENGTH),
  XR is X0+(X-X0)*NORM/LENGTH, YR is Y0+(Y-Y0)*NORM/LENGTH.

%
% section inflation from a central point by a fixed distance
%inflateSect(XY1-XY2,X0/Y0,DIST,XY3-XY4)
%+ XY1-XY2: original section (to be inflated)
%+ XY0:     central point to inflate from (origo)
%+ DIST:    parallel distance
%- XY3-XY4: result section (being inflated)
inflateSect(XY1,XY2,XY0,DIST,XY3,XY4):-
  parallelFarther(line(XY1-XY2), PARLINE, DIST, XY0),
  intersect(line(XY0-XY1), PARLINE, XY3), intersect(line(XY0-XY2), PARLINE, XY4).

inflateSect(sect(XY1,XY2),XY0,DIST,sect(XY3,XY4)):-
  inflateSect(XY1,XY2,XY0,DIST,XY3,XY4).


%
% polygon inflation from the centroid point by a fixed distance
%inflatePolygon(XY1-XY2,DIST,XY3-XY4)
%+ XY1-XY2: original section (to be inflated)
%+ DIST:    parallel distance
%- XY3-XY4: result section (being inflated)
%inflatePolygon(XY1-XY2,DIST,XY0,XY3-XY4)
%+ XY0:     central point to inflate from (origo)
inflatePolygon([_],_,_,[]):- !.
%inflatePolygon([XY0,XY|POLY],DIST,CENT,[XY0GR,XYGR|GROWN]):-
%  inflateSect(XY0,XY,CENT,DIST,XY0GR,XYGR),intersectSurface(line(600/777-600/1960),sect(580.0/1321.0,580.0/2011.0),X).
%  inflatePolygon([XY|POLY],DIST,CENT,[XYGR|GROWN]).
%inflatePolygon([XY0,XY|POLY],DIST,CENT,[XY0GR|GROWN]):-
%  parallelFarther(line(XY0-XY), PARLINE, DIST, CENT),
%  intersect(line(CENT-XY0), PARLINE, XY0GR),
%  inflatePolygon([XY|POLY],DIST,CENT,GROWN).
inflatePolygon(POLY,DIST,CENT,[LASTCUT|CUTS]):-
  findall(LINE,(neighbour2(XY0,XY,POLY),
                parallelFarther(line(XY0-XY),LINE,DIST,CENT)),
          LINES), last(LINES,LASTLINE),
  findall(CUT,(neighbour2(L1,L2,[LASTLINE|LINES]),
              intersect(L1,L2,CUT)),CUTS), last(CUTS,LASTCUT).


%inflatePolygon(POLYGON,DIST,INFLATED) is det
%the operation is seamless only for convex polygons!!
%+POLYGON:   polygon(POLYLINE) input polygon
%+POLYLINE:  LIST of 2DNODES
%+DIST:      absolute distance to inflate with
%-INFLATED:  inflated polygon
inflatePolygon(polygon(POLY),DIST,polygon(GROWN)):-
  last(POLY,XY0), centroidPolygon([XY0|POLY],CENT),
  inflatePolygon([XY0|POLY],DIST,CENT,[_|GROWN]).


%generateLineCap(XY0,XYL,XYR,N,CAP,CAPE)
%XY0: end of line 2D point
%XYL: end of left inflated polyline
%XYR: end of right inflated polyline
%N:   number of points in cap
%N=2 orthogonal cut
%N=1 one more point is generated (a tip)
generateLineCap(_,XYL,XYR,_,2,[XYL,XYR|END],END):- !.
generateLineCap(XY0,XYL,XYR,DIST,3,[XYL,XYM,XYR|END],END):- !,
  lineDistance(line(XYL-XY0),DIST,XYM,_).
%generateLineCap(XY0,XY1,XY2,N,CAP):- N>3, true.

%if both lines are the same (each others continuation)
%then the continuation point is taken as intersection
intersectConnectLines(line(_-XY),line(XY-_),XY):- ! .
intersectConnectLines(line(XY-_),line(_-XY),XY):- ! .
intersectConnectLines(line(XY1-XY2),line(XY3-XY4),INTERSECTION):-
  intersect(line(XY1-XY2),line(XY3-XY4),INTERSECTION).

% polyline inflation parallel sections are found,
% (polygon buffering, polygon offsetting)
% end points are circumscribed by N new break points
% (practically N+2 half-angles)
%inflatePolyline(POLY,DIST,REF,N,GROWN)
%+ POLY:    list of nodes
%+ REF:     reference point that is not on the polyline
%+ DIST:    parallel distance
%+ N:       N angle to circumscribe end points
%-GROWN:    grown polygon
inflatePolyli([XY0],line(_-XYL),line(_-XYR),
              DIST,_REF,N,CAP,CAPEND,RIGHT,RIGHT):-
%here should we build the cap
  !, generateLineCap(XY0,XYL,XYR,DIST,N,CAP,CAPEND).
inflatePolyli([XY1,XY2|POLY],XYXYL,XYXYR,DIST,REF,N,
              [XYL|LEFT],LE,RIGHT0,RIGHT):-
  lineDistance(line(XY2-XY1),DIST,XY1R,XY1L),
  lineDistance(line(XY1-XY2),DIST,XY2L,XY2R),
  intersectConnectLines(XYXYL,line(XY1L-XY2L),XYL),
  intersectConnectLines(XYXYR,line(XY1R-XY2R),XYR),
  inflatePolyli([XY2|POLY],line(XY1L-XY2L),line(XY1R-XY2R),
                DIST,REF,N,LEFT,LE,[XYR|RIGHT0],RIGHT).

inflatePolyline([XY1,XY2|POLY],DIST,REF,N,LEFT,LEND,RIGHT,REND):-
  lineDistance(line(XY2-XY1),DIST,XY1R,XY1L),
  lineDistance(line(XY1-XY2),DIST,XY2L,XY2R),
  generateLineCap(XY1,XY1R,XY1L,DIST,3,CAP,CAPE),
  inflatePolyli([XY2|POLY],line(XY1L-XY2L),line(XY1R-XY2R),
                DIST,REF,N,LEFT,LEND,REND,RIGHT),
  REND=CAP, CAPE=[].


%inflatePolyline(POLYLINE,DIST,INFLATED) is det
%+POLYLINE:  list of polyline nodes
%+POLYLINE:  LIST of 2DNODES
%+DIST:      absolute distance to inflate with
%-INFLATED:  inflated polygon
inflatePolyline([XY1,XY2|POLY],DIST,N,polygon(GROWN)):-
  %finding a reference point which is guaranteed off from polyline
  perimeter([XY1,XY2|POLY],LENGTH), middleSection(XY1,XY2,XY0),
  lineDistance(line(XY1-XY2),XY0,LENGTH,REF,_),
  inflatePolyline([XY1,XY2|POLY],DIST,REF,N,LEFT,LEND,RIGHT,_REND),
  LEND=RIGHT, GROWN=LEFT.

inflatePolyline(POLYLINE,DIST,POLYGON):-
  inflatePolyline(POLYLINE,DIST,3,POLYGON).


%shiftPolygon(+POLYGON,-SHIFTED) is nondet.
%nondeterministically returns all shifted versions
%of POLYGON
%POLYGON: is just a polyline: the last node is NOT repeated as first
shiftPolygon(POLYGON,SHIFTED):-
  append(POLY,GON,POLYGON), POLY \= [], append(GON,POLY,SHIFTED).


areaPolygon(X0/Y0,[X/Y],AREA):- !,
    AREA is X*Y0-X0*Y.
areaPolygon(X0/Y0,[X1/Y1,X2/Y2|CORNERS],SUMAREA):-
    areaPolygon(X0/Y0,[X2/Y2|CORNERS],AREA),
    SUMAREA is X1*Y2-X2*Y1 + AREA.

signedAreaPolygon([C0|CORNERS],AREA):-
    areaPolygon(C0,[C0|CORNERS],SUMAREA), AREA is SUMAREA/2.

areaPolygon([C0|CORNERS],AREA):-
    signedAreaPolygon([C0|CORNERS],SIGNEDAREA), AREA is abs(SIGNEDAREA).


perimeter(section(FROM,TO),PERI):-
  !, point2Distance(FROM,TO,PERI).
perimeter([_],0):- !.
perimeter([P1,P2|POLYLINE],PERIMETER):- is_list(POLYLINE), !,
  point2Distance(P1,P2,DIST), perimeter([P2|POLYLINE],PERI0),
  PERIMETER is PERI0 + DIST.
perimeter(polygon([FIRST|POLYGON]),PERI):-
  !, perimeter([FIRST|POLYGON],PER0), last(POLYGON,LAST),
  point2Distance(LAST,FIRST,DIST), PERI is PER0 + DIST.


perimeterUntil([],_,0):- !.
perimeterUntil([NODE|_],NODE,0):- !.
perimeterUntil([NODE1,NODE2|POLY],TO,PERI):-
  point2Distance(NODE1,NODE2,DIST),
  perimeterUntil([NODE2|POLY],TO,PERI0), PERI is PERI0+DIST.

perimeter(section(FROM,TO),FROM,TO,PERI):-
  !, perimeter(section(FROM,TO),PERI).
perimeter(section(FROM,TO),TO,FROM,PERI):-
  !, perimeter(section(FROM,TO),PERI).
perimeter(polygon(POLY),FROM,TO,PERI):-
  append(POLY,POLY,POLY2), perimeter(POLY2,FROM,TO,PERI).
perimeter([],_,_,0):- ! .
perimeter([FROM|POLY],FROM,TO,PERI):- !,
  perimeterUntil([FROM|POLY],TO,PERI).
perimeter([_|POLY],FROM,TO,PERI):- !,
  perimeter(POLY,FROM,TO,PERI).

perimeter(polygon(POLY),FROM,TO,PERITO,PERIBACK):-
  perimeter(polygon(POLY),PERIALL),
  perimeter(polygon(POLY),FROM,TO,PERITO),
  PERIBACK is PERIALL-PERITO.


%finds the X/Y values of a given linear equation
%linear(+LINE,?X,?Y) is det.
linear(line(A,B,C),X,Y):- number(X), B =\=0, !, Y is (-C-A*X)/B.
linear(line(A,B,C),X,Y):- number(Y), A =\=0, !, X is (-C-B*Y)/A.
linear(line(X1/Y1-X2/Y2),X,Y):- number(X), X1 =\= X2, !,
  Y is (X*(Y2-Y1) + Y1*(X2-X1) - X1*(Y2-Y1))/(X2-X1).
linear(line(X1/Y1-X2/Y2),X,Y):- number(Y), Y1 =\= Y2, !,
  X is  (Y*(X2-X1) + X1*(Y2-Y1) - Y1*(X2-X1))/(Y2-Y1).


point2Line(X1/Y1,X2/Y2,line(A,B,C)):-
  %converting from standard form to two points form
  var(X1), var(X2), var(Y1), var(Y2), !,
  %vertical line
  (B =:= 0 ->  Y1 = 0, Y2 = 1, X1 is -C/A, X2 = X1;
  %horizontal line
  A =:= 0 -> X1 = 0, X2 = 1, Y1 is -C/B, Y2=Y1).
point2Line(X1/Y1,X2/Y2,line(A,B,C)):-
  %converting from two points form to standard form
  ground(X1-Y1-X2-Y2), A is Y2-Y1, B is X1-X2, C is - (A*X1+B*Y1).

%linear(+LINE,+X,+Y,-DIFF) is det.
linear(line(A,B,C),X,Y,DIFF):- number(X), number(Y), !, DIFF is A*X+B*Y+C.
linear(line(XY0-XY),X,Y,DIFF):- ground(XY0-XY), point2Line(XY0,XY,LINE),
  linear(LINE,X,Y,DIFF).

%linear2(+LINE,+X1/Y1,+X2/Y2,-DIFF) is det.
linear2(LINE,X1/Y1,X2/Y2,DIFF1,DIFF2):-
  linear(LINE,X1,Y1,DIFF1), linear(LINE,X2,Y2,DIFF2).


isOnLine(X/Y,sect(XY0,XY1)):- isOnSection(X/Y,sect(XY0,XY1)), ! .
%  isOnLine(X/Y,LINE).
isOnLine(X/Y,LINE):- once(linear(LINE,X,Y)).


isOnSection(X/Y,sect(X0/Y0,X1/Y1)):-
  isOnLine(X/Y,line(X0/Y0-X1/Y1)), isBetween(X0,X,X1), isBetween(Y0,Y,Y1).


%checks if XY1 and XY2 are on the same side of LINE
%DIFFRATE>0 ==> on the same side
%DIFFRATE<0 ==> on the opposite side
%fails, if either are on the line
lineSides(LINE,XY1,XY2,DIFFRATE):-
  linear2(LINE,XY1,XY2,DIFF1,DIFF2),
  DIFF2 =\= 0, DIFFRATE is DIFF1/DIFF2.


intersects(ray(XY0,XY1),POLY,CUTS):-
  findall(CUT,(neighbour2(NODE0,NODE,POLY),
             intersect(ray(XY0,XY1),sect(NODE0,NODE),CUT)),CUTS).

%POLY: a polygon in polyline format
%          (list of nodes, the last is the same as the first)
pointInPolygon(X0/Y0,POLY):-
  X1 is X0+1000, Y1 is Y0 + 1000,
  intersects(ray(X0/Y0,X1/Y1),POLY,CUTS),
  length(CUTS,CUTLN), CUTLN mod 2 =:= 1.

%
% ray casting algorithm: if a ray starting from POINT
% cuts POLYGON odd times  ==> it is inside
% cuts POLYGON even times ==> it is outside
% Works seamlessly also for concave polygons!
% If the point is on the perimeter, then it is inside...
%
isInside(X0/Y0,polygon(POLYLINE)):-
  last(POLYLINE,LAST), POLYGON=[LAST|POLYLINE],
  (isOnBorder(X0/Y0,polygon(POLYLINE))->true; pointInPolygon(X0/Y0,POLYGON)).
isInside(polygon(POLY),polygon(POLYLINE)):-
  foreach(member(XY,POLY), isInside(XY,polygon(POLYLINE))).


%POLY: a polygon in polyline format
%          (list of nodes, the last is the same as the first)
pointOnPolygon(X0/Y0,POLY):-
  once((neighbour2(NODE0,NODE,POLY),
    isOnSection(X0/Y0,sect(NODE0,NODE)))).


isOnBorder(X0/Y0,polygon(POLYLINE)):-
  last(POLYLINE,LAST), POLYGON=[LAST|POLYLINE],
  pointOnPolygon(X0/Y0,POLYGON).


intersect(A1,B1,C1,A2,B2,C2,NOM0,line(A1,B1,C1)):-
  NOM0 =:= 0.0, !, parallelSame(line(A1,B1,C1),line(A2,B2,C2)).
% Cramer's rule
intersect(A1,B1,C1,A2,B2,C2,NOMINATOR,X/Y):-
  X is (B1*C2-B2*C1)/NOMINATOR, Y is (C1*A2-C2*A1)/NOMINATOR.

% intersection of lines either in canonical or in twopoints form
%
%intersection of two lines in standard nomination
intersect(line(A1,B1,C1),line(A2,B2,C2),XY):-
  !, NOMINATOR is A1*B2-A2*B1, intersect(A1,B1,C1,A2,B2,C2,NOMINATOR,XY).
%intersection of a lines given with two points and a shape
intersect(line(X0/Y0-X1/Y1),SHAPE,XY):-
  SHAPE \= circle(_,_), !, point2Line(X0/Y0,X1/Y1,LINE0), intersect(LINE0,SHAPE,XY).
%intersection of two lines second given with two points
intersect(LINE0,line(X0/Y0-X1/Y1),XY):-
  !, point2Line(X0/Y0,X1/Y1,LINE), intersect(LINE0,LINE,XY).

% intersection of rays and/or lines
% ray are from right open (exceed over endpoint)
%
%intersection of a ray and a line (canonical)
intersect(ray(X0/Y0,X1/Y1),line(A,B,C),X/Y):-
  !, intersect(line(X0/Y0-X1/Y1),line(A,B,C),X/Y),
  isBetweenOver(X0,X,X1), isBetweenOver(Y0,Y,Y1).
%intersection of a ray and a line (two points)
intersect(ray(X0/Y0,X1/Y1),line(X2/Y2-X3/Y3),X/Y):-
  !, intersect(line(X0/Y0-X1/Y1),line(X2/Y2-X3/Y3),X/Y),
  isBetweenOver(X0,X,X1), isBetweenOver(Y0,Y,Y1).
%intersection of a ray and a section (two points)
intersect(ray(X0/Y0,X1/Y1),sect(X2/Y2,X3/Y3),RESULT):-
  !, intersect(line(X0/Y0-X1/Y1),sect(X2/Y2,X3/Y3),XY),
  (XY=sect(X2/Y2,X3/Y3) ->
    rayRangeIntersection(X0-X1,X2-X3,X4-X5),
    rayRangeIntersection(Y0-Y1,Y2-Y3,Y4-Y5), RESULT=sect(X4/Y4,X5/Y5);
  XY=X/Y -> isBetweenOver(X0,X,X1), isBetweenOver(Y0,Y,Y1),
  RESULT = XY).




% intersection of sections and/or lines
% sections are from right open (endpoint doesn't belong to it)
%
%intersection of a linear section and a line (canonical)
intersect(sect(X0/Y0,X1/Y1),line(A,B,C),X/Y):-
  !, intersect(line(X0/Y0-X1/Y1),line(A,B,C),X/Y),
  isBetween(X0,X,X1), isBetween(Y0,Y,Y1).
%intersection of a linear section and a line (two points)
intersect(sect(X0/Y0,X1/Y1),line(X2/Y2-X3/Y3),X/Y):-
  !, intersect(line(X0/Y0-X1/Y1),line(X2/Y2-X3/Y3),X/Y),
  isBetween(X0,X,X1), isBetween(Y0,Y,Y1).
%intersection of two linear sections
intersect(LINESECT,sect(X2/Y2,X3/Y3),RESULT):-
  !, intersect(LINESECT,line(X2/Y2-X3/Y3),XY),
  (XY=LINESECT -> RESULT=sect(X2/Y2,X3/Y3);
   XY=X/Y, isBetween(X2,X,X3), isBetween(Y2,Y,Y3)->RESULT=XY).

% intersection of a line and a polygon/polyline
%
% a polygon (a closed curve)
intersect(SECTLINE,polygon(POLYLINE),XYLIST):-
  is_list(POLYLINE), !, last(POLYLINE,LAST),
  poly2Sections([LAST|POLYLINE],SECTIONS),
  convlist(intersect(SECTLINE),SECTIONS,XYLIST).
% a plain list of nodes is a polyline (not a closed curve)
intersect(SECTLINE,POLYLINE,XYLIST):-
  is_list(POLYLINE), !, poly2Sections(POLYLINE,SECTIONS),
  convlist(intersect(SECTLINE),SECTIONS,XYLIST).


% intersection of a line and a circle
%
%intersect(line(A,B,C),circle(X0/Y0,R),XY) is nondet

intersect(line(A,B,C), circle(XY0,R), XY) :- !,
  point2Line(XY1,XY2,line(A,B,C)),
  intersect(line(XY1-XY2), circle(XY0,R), XY).

intersect(line(X1/Y1-X2/Y2), circle(0/0,R), X/Y) :- !,
  DX is X2 - X1, DY is Y2-Y1, DR2 is DX*DX + DY*DY,
  D is X1*Y2-X2*Y1, DISCR is R*R*DR2-D*D,
  (DISCR > 0 ->
     (X is (D * DY + sign(DY) * DX * sqrt(DISCR)) / DR2,
     Y is (0-D * DX + abs(DY) * sqrt(DISCR)) / DR2;
     X is (D * DY - sign(DY) * DX * sqrt(DISCR)) / DR2,
     Y is (0-D * DX - abs(DY) * sqrt(DISCR)) / DR2);
  DISCR =:= 0, X is D*DY / DR2, Y is (0-D*DX)/DR2).
intersect(line(X1/Y1-X2/Y2), circle(X0/Y0,R), X/Y) :-
  XX1 is X1 -X0, XX2 is X2-X0, YY1 is Y1-Y0, YY2 is Y2-Y0,
  intersect(line(XX1/YY1-XX2/YY2), circle(0/0,R), XX/YY),
  X is XX + X0, Y is YY + Y0.

intersect(circle(X1/Y1,R1), circle(X2/Y2,R2), X/Y):-
  distance(X1/Y1,X2/Y2,DIST),
  (DIST>R1+R2->  false; %they are too far apart
  DIST<abs(R1-R2) -> false; %one contains the other
  DIST =:= 0 -> false; %null distance, common origo
  DX is (X2-X1)/DIST,
    DY is(Y2-Y1)/DIST, %a unit vector from one circle to other
    %where the intersection line crosses the line linking the circles
    A is (R1*R1-R2*R2+DIST*DIST)/2/DIST,
    PX is X1+A*DX, PY is Y1+A*DY,
    %orthogonal heigth from the middle intersection point
    H is sqrt(R1*R1-A*A),
    (H =:= 0 -> X is PX+H*DY, Y is PY-H*DX;
    X is PX+H*DY, Y is PY-H*DX;
    X is PX-H*DY, Y is PY+H*DX)).

%intersect(line(600/777-600/1960),sect(580.0/1321.0,580.0/2011.0),X).
%intersect(line(1183,0,-709800),sect(580.0/1321.0,580.0/2011.0),X).
%intersect(line(1,0,1),line(1,0,3),X).

/*
Line: endless
Section: closed from left (from startpoint)
Cases of section and line intersection:
- no cut-point (neither cut-section) of line and section
- the section itself is the intersection (part of the line)
- the cut-point = startpoint, endpoint: same side (as previously)
- the cut-point = startpoint, endpoint: other side (as previously)
- cut-point = intermediate point of the section

*/

%cutPoly(LINESECT,POLYLINE,DIFFRATE,CUTXY,LEFT,RIGHT)
% LINESECT: line or sect to cut the polygon/polyline
% POLYLINE: list of points to spawn a polyline/polygon
% DIFFRATE: difference rate of RXY-TO wrt LINE (<0 => opposite)
% CUTXY: cutpoint of LINESECT and the next polygon edge
% LEFT: polygon on the left side
% RIGHT: polygon on the right side
% [POINTS]:  list of cut points (having two elements)
%
cutPoly(LINESECT,[FROM,TO|NODES],DIFFRATE,FROM,
          [FROM|XYL1],[FROM|XYL2],POINTS):-
  DIFFRATE>0, !, cutPoly(LINESECT,[TO|NODES],TO,XYL1,XYL2,POINTS);
  !, cutPoly(LINESECT,[TO|NODES],TO,XYL2,XYL1,POINTS).
%if the cut point is an intermediate point of the section
%then the start and endpoints are on the opposite side
cutPoly(LINESECT,[FROM|NODES],_DIFF,XY,
          [FROM,XY|XYL1],[XY|XYL2],POINTS):-
  cutPoly(LINESECT,NODES,XY,XYL2,XYL1,POINTS).


%cutPoly(LINESECT,POLYLINE,DIFFRATE,RXY,LEFT,RIGHT,POINTS)
% +LINESECT: line or sect to cut the polygon/polyline
% +POLYLINE: list of points to spawn a polyline/polygon
% +DIFFRATE: difference rate of RXY-TO wrt LINE (<0 => opposite)
% +RXY:      reference point
% -LEFT:     polygon on the left side
% -RIGHT:    polygon on the right side
% [POINTS]:  list of cut points (having two elements)
%
% if we don't have any more edges/nodes??? of the polygon
cutPoly(_,[],_,[],[],[]):- !.
cutPoly(_,[_],_,[],[],[]):- !.
cutPoly(LINESECT,[FROM,TO|NODES],RXY,XYL1,XYL2,[POINT|POINTS]):-
  intersect(LINESECT,sect(FROM,TO),XY),
  (XY=sect(FROM,TO), !, POINT = FROM,
    XYL1 = [FROM|XYL10], XYL2 = [sect(FROM,TO)|XYL20],
    cutPoly(LINESECT,[TO|NODES],RXY,XYL10,XYL20,POINTS);
  lineSides(LINESECT,RXY,TO,DIFFRATE), !,
    POINT = XY,
    cutPoly(LINESECT,[FROM,TO|NODES],DIFFRATE,XY,XYL1,XYL2,POINTS)).
%section doesn't cut the line or it is completely on the line
% ->inserted at the left polygon
cutPoly(LINESECT,[FROM,TO|NODES],_REF,[FROM|XYL1],XYL2,POINTS):-
  cutPoly(LINESECT,[TO|NODES],TO,XYL1,XYL2,POINTS).


%a list of nodes is converted to a list of sections
%(intermediate ponts are duplicated in sections)
poly2Sections(POLYLINE,SECTIONS):-
  findall(sect(FROM,TO),neighbour2(FROM,TO,POLYLINE),SECTIONS).

preparePoly([_,sect(FROM,TO)],sect(FROM,TO)).
preparePoly([sect(FROM,TO),_],sect(FROM,TO)).
preparePoly(POLY,polygon(POLY)).

% @tbd let us find a point to the right/outside not centroid!
% @tbd test: line passes an edge in the opposite direction
% @tbd test: more complicated cases
%
% surface: it means, the results are (usually) two shapes/polygons
% polygon intersection problem, returning partial polygons
% centroid: generates a point that is for sure inside
% intersectSurface(+LINESECT,polygon(+POLYLINE),-POLYGON1,-POLYGON2,-POINTS)
% +LINESECT: line or section to cut a polygon with
% -POLYGON1:
% -POLYGON2: polygon(POLYLINE) structure to describe a polygon
% -POINTS: a list of cut points (for convex polygons max 2 points)
% ...the order of 2 points corresponds to nodes of POLYGON1
% The predicate fails if the line doesn't cut the polygon at all.
% intersectSurface for a section is true if cutpoints fall
% between (or equal) the endpoints of the section
%
% intersectSurface(line(43/77-78/196),polygon([58/132,58/201,83/201,83/132]),P1,P2).
% intersectSurface(line(600/777-600/1960),
%  polygon([580.0/1321.0, 580.0/2011.0, 830.0/2011.0,830.0/1321.0],P1,P2).
%
intersectSurface(sect(X0/Y0,X1/Y1),polygon(POLYLINE),POLYGON1,POLYGON2,POINTS):-
  !, intersectSurface(line(X0/Y0-X1/Y1),polygon(POLYLINE),
                     POLYGON1,POLYGON2,POINTS),
  forall(member(X/Y,POINTS), (isBetween(X0,X,X1,==), isBetween(Y0,Y,Y1,==))).


intersectSurface(LINESECT,polygon(POLYLINE),POLYGON1,POLYGON2,POINTS):-
  last(POLYLINE,LAST), POLYGON=[LAST|POLYLINE],
  centroidPolygon(POLYLINE,CXY),
  cutPoly(LINESECT,POLYGON,CXY,POLY1,POLY2,POINTS), POINTS = [_|_],
  once(preparePoly(POLY1,POLYGON1)), once(preparePoly(POLY2,POLYGON2)).

intersectSurface(LINESECT,polygon(POLYLINE),POLYGON1,POLYGON2):-
  intersectSurface(LINESECT,polygon(POLYLINE),POLYGON1,POLYGON2,_POINTS).


:-begin_tests(geometry).

%
% range arithmetic tests
% (should they go to some arithmetic extension module?)
%
%range intersection with an overlapping subrange
test(rangeIntersection,true(X=2-4)):- rangeIntersection(1-4,2-8,X).
%range intersection with an overlapping subrange
test(rangeIntersection,true(X=2-3)):- rangeIntersection(2-8,1-3,X).
%range intersection when the second is a subrange of the first
test(rangeIntersection,true(X=2-4)):- rangeIntersection(1-8,2-4,X).
%range intersection when the first is a subrange of the second
test(rangeIntersection,true(X=2-5)):- rangeIntersection(2-5,1-10,X).
%range intersection with no overlapping subrange
test(rangeIntersection,fail):- rangeIntersection(1-2,3-8,_).
%range intersection with one joining (but not intersecting) point
test(rangeIntersection,true(X=2-2)):- rangeIntersection(1-2,2-8,X).

%range intersection with an overlapping subrange
test(rangeIntersection,true(X=4-2)):- rangeIntersection(4-1,2-8,X).
%range intersection with an overlapping subrange
test(rangeIntersection,true(X=3-2)):- rangeIntersection(8-2,1-3,X).
%range intersection when the second is a subrange of the first
test(rangeIntersection,true(X=4-2)):- rangeIntersection(8-1,2-4,X).
%range intersection when the first is a subrange of the second
test(rangeIntersection,true(X=5-2)):- rangeIntersection(5-2,1-10,X).
%range intersection with no overlapping subrange
test(rangeIntersection,fail):- rangeIntersection(2-1,3-8,_).
%range intersection with one joining (but not intersecting) point
test(rangeIntersection,true(X=2-2)):- rangeIntersection(2-1,2-8,X).

%ray intersection with an overlapping subrange
test(rayIntersection,true(X=2-8)):- rayRangeIntersection(1-4,2-8,X).
%ray intersection with an overlapping subrange
test(rayIntersection,true(X=2-3)):- rayRangeIntersection(2-8,1-3,X).
%ray intersection
test(rayIntersection,true(X=2-4)):- rayRangeIntersection(1-8,2-4,X).
%ray intersection
test(rayIntersection,true(X=2-10)):- rayRangeIntersection(2-5,1-10,X).
%ray intersection when there is no common subinterval
test(rayIntersection,fail):- rayRangeIntersection(3-8,1-2,_).
%ray intersection with one joining (but not intersecting) point
test(rayIntersection,true(X=1-1)):- rayRangeIntersection(1-2,0-1,X).

% ray intersection with negative rays
%
%ray intersection with a proper subrange
test(rayIntersection,true(X=3-2)):- rayRangeIntersection(4-1,2-3,X).
%ray intersection with an overlapping subrange
test(rayIntersection,true(X=4-3)):- rayRangeIntersection(4-1,3-6,X).
%ray intersection when there is no common subinterval
test(rayIntersection,fail):- rayRangeIntersection(5-3,6-8,_).
%ray intersection with one joining (but not intersecting) point
test(rayIntersection,true(X=1-1)):- rayRangeIntersection(1-2,0-1,X).


%same vertical lines are always parallel
test(parallelLines,true):-
  isParallel(line(1,0,1),line(1,0,1)).

%different vertical lines
test(parallelLines,true):-
  isParallel(line(1,0,1),line(1,0,2)).

%different horizontal lines
test(parallelLines,true):-
  isParallel(line(0,1,1),line(0,1,2)).

%skewed lines with same x,y-factors
test(parallelLines,true):-
  isParallel(line(2,1,1),line(2,1,2)).

%skewed lines with different x,y-factors
test(parallelLines,true):-
  isParallel(line(2,1,1),line(4,2,2)).

%same vertical lines
test(sameLines,true):-
  isSame(line(1,0,1),line(1,0,1)).

%different vertical lines
test(sameLines,fail):-
  isSame(line(1,0,1),line(1,0,2)).

%same horizontal lines
test(sameLines,true):-
  isParallel(line(0,1,1),line(0,1,1)).

%different horizontal lines
test(sameLines,fail):-
  isSame(line(0,1,1),line(0,1,2)).

%skewed lines with same x,y-factors
test(sameLines,true):-
  isSame(line(2,1,10),line(2,1,10)).

%skewed lines with same x,y-factors but different constant
test(sameLines,fail):-
  isSame(line(2,1,10),line(4,2,10)).

%skewed lines with different x,y-factors
test(sameLines,true):-
  isSame(line(2,1,1),line(4,2,2)).

test(projectPerpendicularly,true(XY=0.0/5.0)):-
  projectPerpendicularly(line(0,1,-5),0/10,XY).

test(perpendicular,true):-
  isPerpendicular(line(5,4,1),line(4,-5,-44)).

test(perpendicular,true(LINE=line(-5,-4,46))):-
  findPerpendicular(line(4,-5,-4),6/4,LINE).

test(perpendicular,true(LINE=line(-5.0,0.0,-0.0))):-
  findPerpendicular(line(0,1,-5),0/10,LINE).

test(perpendicular,set(XY=[0.0/5.0,0.0/15.0])):-
  perpendicular(line(0,1,-5), 0/10, XY, 5).


test(area):- areaPolygon([0/0,0/5,5/5,5/0],A), A==25.

test(area):- areaPolygon([3/5,6/8,9/6,5/0],A), A==23.5 .

test(centroid):- centroidPolygon([0/0,0/5,5/5,5/0],A), A==2.5/2.5 .

test(centroid):- centroidPolygon([1/1,2/4,5/4,11/1],X/Y),
  X/Y==4.9743589743589745/2.230769230769231.

test(point2Line,true((A=6,B=1,C= -26))):-
  point2Line(4/2,3/8,line(A,B,C)).

test(point2Line,true((A=1,B= 5,C= -21))):-
  point2Line(6/3,1/4,line(A,B,C)).


test(inflatePolygon,true(INFL=polygon([-1.0/ -1.0,
                                       -1.0/3.414213562373095,
                                       3.414213562373095/ -1.0]))):-
  inflatePolygon(polygon([0/1,1/0,0/0]),1,INFL).
test(inflatePolygon,true(INFL=polygon([-1.0/ -1.0, -1.0/2.0,
                                       2.0/ 2.0, 2.0/ -1.0]))):-
  inflatePolygon(polygon([0/1,1/1,1/0,0/0]),1,INFL).


test(inflatePolyline,fail):-
  inflatePolyline([2/0],1,_POLYGON).

test(inflatePolyline,true(POLYGON=polygon([2.0/1.0,3.0/0.0,2.0/ -1.0,
                                           0.0/ -1.0,-1.0/0.0,0.0/1.0]))):-
  inflatePolyline([0/0,2/0],1,POLYGON).

test(inflatePolyline,true(POLYGON=polygon([1.0/1.0,
                                           2.0/1.0,3.0/0.0,2.0/ -1.0,
                                           1.0/ -1.0,
                                           0.0/ -1.0,-1.0/0.0,0.0/1.0]))):-
  inflatePolyline([0/0,1/0,2/0],1,POLYGON).

test(inflatePolyline,
     true(POLYGON=polygon([9.292893218813452/10.707106781186548,
                           10.707106781186548/10.707106781186548,
                           10.707106781186548/9.292893218813452,
                           0.7071067811865475/ -0.7071067811865475,
                           -0.7071067811865476/ -0.7071067811865476,
                           -0.7071067811865475/0.7071067811865475]))):-
  inflatePolyline([0/0,10/10],1,POLYGON).


%
% distance tests
%
%horizontal lines normalized
test(parallelDistance,true(DIST=:=2)):-
  distance(line(1,0,8),line(1,0,10),DIST).
test(parallelDistance,all(PARALLEL=[line(1,0,10.0),line(1,0,6.0)])):-
  distance(line(1,0,8),PARALLEL,2).
%vertical lines normalized
test(parallelDistance,true(DIST=:=2.0)):-
  distance(line(0,1,8),line(0,1,10),DIST).
test(parallelDistance,all(PARALLEL=[line(0,1,10.0),line(0,1,6.0)])):-
  distance(line(0,1,8),PARALLEL,2).
%aslope lines normalized
test(parallelDistance,true(DIST=:=1.414213562373095)):-
  distance(line(1,-1,8),line(1,-1,10),DIST).
test(parallelDistance,all(PARALLEL=[line(1,-1,10.0),line(1,-1,6.0)])):-
  distance(line(1,-1,8),PARALLEL,1.414213562373095).
%aslope lines not normalized
test(parallelDistance,true(DIST=:=1.414213562373095)):-
  distance(line(1,-1,8),line(5,-5,50),DIST).

%inflating shapes
%inflating a section from a reference point
test(inflateShapes,true(SECT=sect(-0.0/2.414213562373095, 2.414213562373095/ -0.0))):-
  inflateSect(sect(0/1,1/0),0/0,1,SECT).

%
% parallel lines in a given distance nearer/farther
%
test(parallelDistance,true(LINE=line(1,-1,6.0))):-
  parallelNearer(line(1,-1,8),LINE,1.414213562373095,0/0).
test(parallelDistance,true(LINE=line(1,-1,10.0))):-
  parallelFarther(line(1,-1,8),LINE,1.414213562373095,0/0).

%
% distances of points along a line
%
test(lineDistance,true((XY1= -3.5355339059327373/3.5355339059327373,
                       XY2= 3.5355339059327373/ -3.5355339059327373))):-
  lineDistance(line(1,-1,0),0/0,5,XY1,XY2).

test(lineDistance,true((XYLEFT= 0.29289321881345254/1.7071067811865475,
                       XYRIGHT = 1.7071067811865475/0.29289321881345254))):-
  lineDistance(line(0/0-1/1),1,XYLEFT,XYRIGHT).

%
% intersection of lines
%
test(lineIntersection,true((X=:=3/7, Y=:= -11/7))):-
  intersect(line(3,4,5),line(2,5,7),X/Y).

test(lineIntersection,true((X=:=1/11, Y=:= -14/11))):-
  intersect(line(9,3,3),line(4,5,6),X/Y).

%paralell but different lines have no intersection
test(lineIntersection,fail):-
  intersect(line(1,0,2),line(1,0,1),_).

%paralell but different lines have no intersection
test(lineIntersection,fail):-
  intersect(line(1,2,2),line(2,4,1),_).

test(lineIntersection,true((X=:=10, Y=:= 10))):-
  intersect(line(0/0-10/10),line(0/10-5/10),X/Y).

test(lineIntersection,true((X=:=0, Y=:= 0))):-
  intersect(sect(0/0,10/10),sect(0/0,0/10),X/Y).

test(lineIntersection,true((X=:=5, Y=:=5))):-
  intersect(sect(0/0,10/10),sect(10/0,0/10),X/Y).

test(lineIntersection,true((X=:=5, Y=:=5))):-
  intersect(sect(0/0,10/10),sect(0/10,10/0),X/Y).

%sections are from right open
test(lineIntersection,fail):-
  intersect(sect(0/0,10/10),sect(0/10,0/0),_XY).

test(lineIntersection,true(R=sect(0/0,5/5))):-
  intersect(line(1,-1,0),sect(0/0,5/5),R).

%section is part of the line
test(lineIntersection,true(R=sect(5/5,0/0))):-
  intersect(line(1,-1,0),sect(5/5,0/0),R).

test(lineIntersection,fail):-
  intersect(sect(0/0,9/9),sect(0/10,5/10),_XY).

%the intersection of same lines are themselves
test(lineIntersection,R=line(3,4,5)):-
  intersect(line(3,4,5),line(3,4,5),R).

%the intersection of same lines are themselves
test(lineIntersection,R=line(3,4,5)):-
  intersect(line(3,4,5),line(6,8,10),R).

%intersection of a section with a line it is a section of

%intersecting parallel lines should fail
test(lineIntersection,fail):-
  intersect(line(6,12,5),line(2,4,5),_).

%rays: there is one intersection point
test(lineIntersection,XY=5/5):-
  intersect(ray(0/0,10/10),sect(0/10,10/0),XY).

% rays: the only intersection point is
% outside of the given ray section
test(lineIntersection,XY=5/5):-
  intersect(ray(0/0,1/1),sect(0/10,10/0),XY).

%rays: no intersection points
test(lineIntersection,fail):-
  intersect(ray(1/1,0/0),sect(0/10,10/0),_XY).

%rays: intersection is the entire section
test(lineIntersection,true(XY=sect(5/5,10/10))):-
  intersect(ray(0/0,1/1),sect(5/5,10/10),XY).

%intersecting a line and a circle
test(lineCircleIntersection,set(XY=[3.5355339059327378/3.5355339059327378,
                                    -3.5355339059327378/ -3.5355339059327378
                                  ])):-
  intersect(line(0/0 - 10/10),circle(0/0,5),XY).

test(lineCircleIntersection,set(XY=[3.0/ -2.0,6.0/ -5.0])):-
  intersect(line(-10/11 - 10/ -9),circle(3/ -5,3),XY).

test(lineCircleIntersection,set(XY=[0.0/ -4.0,0.0/4.0])):-
  intersect(line(0/ -3 - 0/6),circle(0/0,4),XY).

test(lineCircleIntersection,set(XY=[1.0/ -2.0,8.0/5.0])):-
  intersect(line(6/3-10/7),circle(4/2,5),XY).

test(lineCircleIntersection,XY=0/5):-
  intersect(line(0/0-0/20),circle(5/5,5),XY).

test(lineCircleIntersection,fail):-
  intersect(line(0/0-0/20),circle(6/5,5),_).

%two circles are too far from each other
test(circleIntersection,fail):-
  intersect(circle(-10/0,5),circle(10/0,5),_).

%circles have a common origo
test(circleIntersection,fail):-
  intersect(circle(-10/0,10),circle(-10/0,5),_).

%first circle contains the second completely
test(circleIntersection,fail):-
  intersect(circle(-10/0,10),circle(-8/0,5),_).

%one single common touching point
test(circleIntersection,XY=0.0/0.0):-
  intersect(circle(-10/0,10),circle(10/0,10),XY).

%regular case: two intersection points
test(circleIntersection,set(XY=[ 0.0/ -8.660254037844387,
                            0.0/ 8.660254037844387])):-
  intersect(circle(-5/0,10),circle(5/0,10),XY).

%a canonical line cuts a convex rectangle (through its edges)
test(surfaceIntersection,
     true((XYL2=polygon([0/1,0/5,4/5]),
           XYL1=polygon([5/0,0/0,0/1,4/5,5/5])))):-
  intersectSurface(line(1,-1,1),polygon([0/0,0/5,5/5,5/0]),XYL1,XYL2,[0/1,4/5]).

%a line with two points cuts a convex rectangle (through its edges)
test(surfaceIntersection,
     true((XYL2=polygon([0/1,0/5,4/5]),
           XYL1=polygon([5/0,0/0,0/1,4/5,5/5])))):-
  intersectSurface(line(-1/0-0/1),
                   polygon([0/0,0/5,5/5,5/0]),XYL1,XYL2,[0/1,4/5]).

%a line cuts a convex rectangle through two nodes
test(surfaceIntersection,
     true((XYL2=polygon([0/0,0/5,5/5]),
           XYL1=polygon([5/0,0/0,5/5])))):-
  intersectSurface(line(1,-1,0),polygon([0/0,0/5,5/5,5/0]),XYL1,XYL2,[0/0,5/5]).

%a line cuts a triangle through a single node
test(surfaceIntersection,
     true((XYL2=polygon([0/5]),
           XYL1=polygon([5/0,0/0,0/5])))):-
  intersectSurface(line(1,-1,5),polygon([0/0,0/5,5/0]),XYL1,XYL2,[0/5]).

%a line cuts a convex rectangle through a single node
test(surfaceIntersection,
     true((XYL2=polygon([0/5]),
           XYL1=polygon([5/5,5/0,0/0,0/5])))):-
  intersectSurface(line(1,-1,5),polygon([5/0,0/0,0/5,5/5]),XYL1,XYL2,[0/5]).

%a line cuts a triangle on a complete edge
test(surfaceIntersection,
     true((XYL2=sect(0/0,5/5),
           XYL1=polygon([5/0,0/0,5/5])))):-
  intersectSurface(line(1,-1,0),polygon([0/0,5/5,5/0]),XYL1,XYL2,[0/0,5/5]).

%a line cuts a triangle on a complete edge
test(surfaceIntersection,
     true((XYL2=sect(5/5,0/0),
           XYL1=polygon([0/0,5/0,5/5])))):-
  intersectSurface(line(1,-1,0),polygon([5/0,5/5,0/0]),XYL1,XYL2,[0/0,5/5]).

%a line doesnt cut the triangle at all
test(surfaceIntersection,fail):-
  intersectSurface(line(1,-1,10),polygon([5/0,5/5,0/0]),
                   _XYL1,_XYL2,_CUTS).

%intersection with a section - succeeds
test(surfaceIntersection,
     true((XYL2=sect(5/5,0/0),
           XYL1=polygon([0/0,5/0,5/5])))):-
  intersectSurface(sect(5/5,-1/ -1),polygon([5/0,5/5,0/0]),
                   XYL1,XYL2,[0/0,5/5]).

%intersection with a section - a complete edge
test(surfaceIntersection,true((XYL2=sect(5/5,0/0),
           XYL1=polygon([0/0,5/0,5/5])))):-
  intersectSurface(sect(5/5,0/0),polygon([5/0,5/5,0/0]),XYL1,XYL2,[0/0,5/5]).

test(perimeter,PERI=:=15):-
    perimeter([0/0,0/5,5/5,5/0],PERI).

test(perimeter,PERI=:=20):-
    perimeter(polygon([0/0,0/5,5/5,5/0]),PERI).

test(perimeter,true((P1=:=10.0, P2=:=sqrt(50)))):-
     perimeter(polygon([5/0,5/5,0/0]),0/0,5/5,P1,P2).


test(pointInPolygon,true):-
    isInside(5/5,polygon([0/0,0/10,10/10,10/0])).

test(pointInPolygon,true):-
    isInside(0/0,polygon([0/0,0/10,10/10,10/0])).

test(pointInPolygon,true):-
    isInside(10/10,polygon([0/0,0/10,10/10,10/0])).

test(pointInPolygon,fail):-
    isInside(20/20,polygon([0/0,0/10,10/10,10/0])).

test(polygonInPolygon,true):-
    isInside(polygon([0/0,5/5,5/0]),polygon([0/0,0/10,10/10,10/0])).

test(polygonInPolygon,fail):-
    isInside(polygon([0/0,11/11,5/0]),polygon([0/0,0/10,10/10,10/0])).

:-end_tests(geometry).











