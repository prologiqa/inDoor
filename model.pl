:- module(model,[doCheckAll/2, doCheckAllIndoor/1, doCheckAllIndoor/0,
                doClearAll/2, doClearAllIndoor/1, doClearAllIndoor/0
                ,loadContext/1, clearContext/1 %load/clear installation context
                ,currentContext/1      %test if context is loaded
                ]).
%
% Metamodel driven semantic checks
%
% Meta2model:
%element(METAMODEL,CONTEXT,ELEMPATTERN,ARGCHECKS,STRCHECK)
%+METAMODEL:   atom, the name/ID of metamodel
%?CONTEXT:     nonvar, installation context ID: a Prolog module name
%+ELEMPATTERN: Prolog pattern of the element
%+ARGCHECKS:   list of argument check calls
%+STRCHECK:    check call for each arguments
%
%
%
% Multiplication model
% - metamodel driven operations can process several metamodels
%   metamodel ID should be given in the first arg explicitely
%   At the moment the metamodel is a part of the present module,
%   later it can be placed to separate modules...
% - for the metamodel (inDoor) there may be different installations
%   (contexts). This is given in the second arg, and will function
%   as separate Prolog modules... Therefore it might be a variable
%   which will be received as a parameter
% - for a given inDoor installation there might be different buildings
%   they are modelled as the first component in the room/door/wall ID-s
%
%@tbd both sides of a door must be on the same level
%
:- use_module(geometry).
:- use_module(listExt).


:- discontiguous element/5.

%to some fact-base extension module
%argAmbiguous(+NTH,+MOD,+PATTERN,-AMBARGS) is nondet.
%checks, if nth argument in PATTERN is ambigous (is an ID)
%if yes, double argument values are returned in a list
argAmbiguous(NTH,MOD,PATTERN,AMBARGS):-
    findall(ARG,(call(MOD:PATTERN), arg(NTH,PATTERN,ARG)),ARGS),
    duplicates(ARGS,AMBARGS).

row(M, N, Row) :-
    nth1(N, M, Row).

column(M, N, Col) :-
    transpose(M, MT),
    row(MT, N, Col).

symmetrical(M) :-
    transpose(M, M).

%
%@tbd: ID-s are unique, eg. data:corner(west2,X) is not unique
%
transpose([[]|_], []) :- !.
transpose([[I|Is]|Rs], [Col|MT]) :-
    first_column([[I|Is]|Rs], Col, [Is|NRs]),
    transpose([Is|NRs], MT).

first_column([], [], []).
first_column([[]|_], [], []).
first_column([[I|Is]|Rs], [I|Col], [Is|Rest]) :-
    first_column(Rs, Col, Rest).

unique(DATA,ID,CALL):- bagof(ID,DATA:CALL,IDS), IDS=[ID].

%should be loaded by loadContext/1
%:-use_module(data).

type(section,ATOM,ground(ATOM)).

checkUnique(DATA,ID,CALL,ERR,MSG):-
    \+ unique(DATA,ID,CALL)-> ERR=true, MSG=' not unique'.

checkType(section,SECTION):-
    type(section,SECTION,CALL), once(call(CALL)).

checkCorner(DATA,CORNER):-
    ground(CORNER), DATA:corner(CORNER,_X/_Y).

checkCorner(DATA,CORNER,KIND):- KIND \= line, !,
    checkCorner(DATA,CORNER).
checkCorner(DATA,CORNER,line):-
    CORNER=door(_,_)->true;
    ground(CORNER), DATA:corner(CORNER,_X/_Y).

checkCorners(DATA,CORNERS):-
    forall(member(CORNER,CORNERS),checkCorner(DATA,CORNER)).

circularWalls([side(WALL0,CORNER0,CE0)|SIDES],ERR,MESSAGE):- !,
  circularWalls(CORNER0,CEE,
                [side(WALL0,CORNER0,CE0)|SIDES],ERR,MESSAGE),
  once(ERR\==true;CEE==CORNER0), MESSAGE=' Room walls are not circular'.

circularWalls(CORNER,CE,[side(_WALL,CO,CE)],ERR,MSG):-
    !, (CO\==CORNER, ERR=true, MSG=' Room walls are not circular';true).
circularWalls(CORNER,CEE,[side(_WALL,CO,CE)|WALLS],ERR,MSG):-
    !,
    (CO\==CORNER->ERR=true, MSG=' Room walls are not circular!';
    circularWalls(CE,CEE,WALLS,ERR,MSG)).


%element(DATA,PATTERN,ARGCALLS,CALL).
%DATA:   Prolog module to search for data clauses
definedWall(DATA,WALL):- DATA:wall(WALL,_,_), !; DATA:wall(WALL,_,_,_), ! .
definedWall(DATA,BUILD/_/(FL/WALL)):- definedWall(DATA,BUILD/FL/WALL).
definedWall(DATA,BUILD/_/WALL):- definedWall(DATA,BUILD/WALL).

anyWall(DATA,B/F/W,KIND,LINES):- DATA:wall(B/F/W,KIND,LINES).
anyWall(DATA,B/_/W,KIND,LINES):- DATA:wall(B/W,KIND,LINES).

anyWall(DATA,B/F/W,KIND,CORNERS,SECTIONS):-
    DATA:wall(B/F/W,KIND,CORNERS,SECTIONS).
anyWall(DATA,B/_/W,KIND,CORNERS,SECTIONS):-
    DATA:wall(B/W,KIND,CORNERS,SECTIONS).

checkSide(_DATA,_BUILDFLOOR,SIDE3,ERR,MESSAGE):-
    \+ functor(SIDE3,side,3), !,
    ERR=true, format(atom(MESSAGE),'Side ~w is ill-formed:',[SIDE3]).
checkSide(DATA,BUILDFLOOR,side(WALL,C1,C2),ERR,MESSAGE):-
    \+ definedWall(DATA,BUILDFLOOR/WALL)->
      ERR=true, format(atom(MESSAGE),' Wall ~w is undefined!',[WALL]);
    \+ DATA:corner(C1,_)->
      ERR=true, format(atom(MESSAGE),' Corner ~w is undefined!',[C1]);
    \+ DATA:corner(C2,_)->
      ERR=true, format(atom(MESSAGE),' Corner ~w is undefined!',[C2]);
    anyWall(DATA,BUILDFLOOR/WALL,_,LINES)->
        findall(CORNERS,member(line(CORNERS,_),LINES),CORNERSLIST),
        %l�tezik-e olyan cornerlist, amelyben mindkett� C1 C2 benne van
        (member(CORNERS,CORNERSLIST),
           member(C1,CORNERS), member(C2,CORNERS), !;
        ERR=true,
          format(atom(MESSAGE),'Corners ~w, ~w are not on the same side',
                [C1,C2])
        );
        %flatten(CORNERSLIST,ALLCORNERS),
        %(\+ member(C1,ALLCORNERS)-> ERR=true,
        %   format(atom(MESSAGE),' Corner ~w is missing from list!',[C1]);
        % \+ member(C2,ALLCORNERS)-> ERR=true,
        %   format(atom(MESSAGE),' Corner ~w is missing from list!',[C2]);
        %true);
    anyWall(DATA,BUILDFLOOR/WALL,_,CORNERS,SECTIONS)->
        (\+ member(C1,CORNERS)-> ERR=true,
           format(atom(MESSAGE),
                  ' Corner ~w is missing from wall ~w!',[C1,WALL]);
         \+ member(C2,CORNERS)-> ERR=true,
           format(atom(MESSAGE),
                  ' Corner ~w is missing from wall! ~w',[C2,WALL]);
         DATA:corner(C1,XYC1), DATA:corner(C2,XYC2),
           point2Distance(XYC1,XYC2,ALLLENGTH),
           aggregate_all(sum(SIZE),
                     (planar:nextLineEdge(DATA,CORNERS,_,SECTIONS,_,
                                   C1,C2,SECTION,X,Y,_DIR),
                      planar:sectionSize(X,Y,SECTION,SIZE)),
                     ALLSIZE),
      ALLSIZE =\= ALLLENGTH->ERR=true,
           format(atom(MESSAGE),
             ' Sum of wall ~w sections ~w differs from corners distance! ~w',
                  [WALL,ALLSIZE,ALLLENGTH]);
        true);
    true.


checkSides(DATA,BUILDFLOOR,SIDES,ERR,MESSAGE):-
     \+ is_list(SIDES) -> ERR=true, MESSAGE='Format: non list!';
     length(SIDES,NRSIDES), NRSIDES=<2 ->
       ERR=true, MESSAGE='Format: min 2 sides!';
     member(SIDE,SIDES),
       checkSide(DATA,BUILDFLOOR,SIDE,ERR,MESSAGE), ERR==true, !;
     true.

checkSection(S,ERR,ERRMSG):-
    S \= _/_ -> ERR=true,
      format(atom(ERRMSG),'incorrect format of size:~w',[S]);
    S = TH/_, \+ number(TH), 0<TH, TH<10 -> ERR=true,
      format(atom(ERRMSG),'incorrect datatype of thickness:~w',[S]);
    S = _/SIZE, \+ float(SIZE) -> ERR=true,
      format(atom(ERRMSG),'incorrect datatype of thickness:~w',[S]);
    true.


checkLineSections(_DATA,[_],[],ERR,_ERRMSG):- ignore(ERR=false), !.
%we are just at a door...
checkLineSections(DATA,[door(DOOR,_),door(DOOR,D)|CORNERS],
                  [S|SECTIONS],ERR,ERRMSG):- !,
  (S \= door(_,_,_) -> ERR = true,
     format(atom(ERRMSG), '~w should be a door(_,_,_) construction',[S]);
  S = door(DOOR,SIZE,TH),
    (\+ integer(TH) -> ERR = true,
        format(atom(ERRMSG), 'Door ~w thickness should be integer!',[S]);
     \+ number(SIZE) ->  ERR=true,
        format(atom(ERRMSG), 'Door ~w size should be number!',[S]);
     checkLineSections(DATA,[door(DOOR,D)|CORNERS],SECTIONS,ERR,ERRMSG))).

checkLineSections(DATA,[door(_,_),C2|CORNERS],[S|SECTIONS],ERR,ERRMSG):- !,
  ( \+ DATA:corner(C2,_) -> ERR=true,
     format(atom(ERRMSG),'Corner ~w doesnt exist',[C2]);
  checkSection(S,ERR,ERRMSG), ERR==true -> true;
  checkLineSections(DATA,[C2|CORNERS],SECTIONS,ERR,ERRMSG)).

checkLineSections(DATA,[C1,door(DOOR,D)|CORNERS],[S|SECTIONS],ERR,ERRMSG):- !,
  ( \+ DATA:corner(C1,_) -> ERR=true,
     format(atom(ERRMSG),'Corner ~w doesnt exist',[C1]);
  checkSection(S,ERR,ERRMSG), ERR==true -> true;
  checkLineSections(DATA,[door(DOOR,D)|CORNERS],SECTIONS,ERR,ERRMSG)).

checkLineSections(DATA,[C1,C2|CORNERS],[S|SECTIONS],ERR,ERRMSG):-
  \+ DATA:corner(C1,_) -> ERR=true,
     format(atom(ERRMSG),'Corner ~w doesnt exist',[C1]);
  \+ DATA:corner(C2,_) -> ERR=true,
     format(atom(ERRMSG),'Corner ~w doesnt exist',[C1]);
  DATA:corner(C1,XYC1), DATA:corner(C2,XYC2),
    once((S=_/SIZE;S=door(_,_,SIZE)))->point2Distance(XYC1,XYC2,DIST),
    (DIST =\= SIZE -> ERR=true,
      format(atom(ERRMSG),'~w, between ~w-~w: ~w',[SIZE,C1,C2,DIST]);
      %term_string(C1,C1S), term_string(C2,C2S),
      %atomic_list_concat(['between ',C1S,' and ',C2S],ERRMSG)
    \+ float(SIZE) -> ERR=true,
      format(atom(ERRMSG),'size ~w must be float!',[SIZE]);
    checkLineSections(DATA,[C2|CORNERS],SECTIONS,ERR,ERRMSG)).

checkLine(DATA,line(CORNERS,SECTIONS),ERR,ERRMSG):-
    length(CORNERS,CL), length(SECTIONS,SL), SL =\= CL-1 ->
      ERR=true, ERRMSG= ' Number of corners and sections!';
    \+ forall(member(CORNER,CORNERS),checkCorner(DATA,CORNER,line)) ->
      ERR=true, ERRMSG= ' Problem with corners.';
    \+ forall(member(SECTION,SECTIONS),checkType(section,SECTION))->
      ERR=true, ERRMSG= ' Problem with sections.';
    checkLineSections(DATA,CORNERS,SECTIONS,ERR,MSG), ERR=true ->
      atom_concat('Section/length error: ',MSG,ERRMSG).

checkWall3(DATA,LINES,ERR,MESSAGE):-
    nth1(NTH,LINES,LINE), checkLine(DATA,LINE,ERR,ERRMSG),
    ERR==true, !, format(atom(MESSAGE),'Error ~w-th line: ~w',[NTH,ERRMSG]).

checkWall4(DATA,KIND,POINTS,SECTIONS,ERR,MESSAGE):-
    length(POINTS,PL), length(SECTIONS,SL),
    (KIND=line, SL =\= PL-1 ->
       ERR=true, MESSAGE=' Linear wall points and sections number mismatch!';
     KIND=polyline, SL =\= PL-1->
       ERR=true, MESSAGE=' Polyline wall points and sections number mismatch!';
     KIND=polygon, SL=\=PL->
       ERR=true, MESSAGE=' Polygon wall points and sections number mismatch!';
     checkLineSections(DATA,POINTS,SECTIONS,ERR,MESSAGE)).


%element(MOD,PATTERN,ARGCALLS,CALL).
%The predicate is a metaclass, that is
%its lines/facts describe classes, like 'corner'
%
%that is one Prolog fact describes one class in the model
%along with its checking procedures
%
%PATTERN:  element pattern with variables
%CALL:     call to test the validity of PATTERN itself
%ARGCALLS: list of calls to check the validity of PATTERN args
element(inDoor,DATA,corner(ID,X/Y),
        [ground(ID),(number(X), number(Y))],
        checkUnique(DATA,ID,corner(ID,X/Y))).


roomKind(lecture).
roomKind(lab).
roomKind(common).
roomKind(corridor).
roomKind(elevator).
roomKind(office).
roomKind(storage).
roomKind(bath).
roomKind(living).
roomKind(sleeping).

element(inDoor,DATA,room(BUILD/FLOOR/ID,KIND,LEVEL,SIDES),
    [(ground(ID),
        atomic(FLOOR),once(integer(FLOOR)), -50<FLOOR, FLOOR<50,
        atomic(BUILD)),
     once(roomKind(KIND)),
     (once(integer(LEVEL)), -1000=<LEVEL, LEVEL=<1000),
     checkSides(DATA,BUILD/FLOOR,SIDES)
    ],
    %(checkUnique(DATA,ID,room(BUILD/FLOOR/ID,KIND,LEVEL,SIDES)),
       circularWalls(SIDES)).


checkStairs(DATA,[C1,C2,C3,C4],ERR,MESSAGE):-
    !,
      (\+ DATA:corner(C1,_CXY1)-> ERR=true,
           format(atom(MESSAGE), "Stairs\' corner ~w doesnt exist.",[C1]);
      \+ DATA:corner(C2,_CXY2)-> ERR=true,
           format(atom(MESSAGE), "Stairs\' corner ~w doesnt exist.",[C2]);
      \+ DATA:corner(C3,_CXY3)-> ERR=true,
           format(atom(MESSAGE), "Stairs\' corner ~w doesnt exist.",[C3]);
      \+ DATA:corner(C4,_CXY4)-> ERR=true,
           format(atom(MESSAGE), "Stairs\' corner ~w doesnt exist.",[C4]));
    ERR=false.
checkStairs(DATA,[C1,C2,DEPTH],ERR,MESSAGE):-
    !,
      (\+ DATA:corner(C1,_CXY1)-> ERR=true,
           format(atom(MESSAGE), "Stairs\' corner ~w doesnt exist.",[C1]);
      \+ DATA:corner(C2,_CXY2)-> ERR=true,
           format(atom(MESSAGE), "Stairs\' corner ~w doesnt exist.",[C2]);
      \+ number(DEPTH), ERR=true,
           format(atom(MESSAGE), "Stairs\' depth: ~w should be a number",[C2]));
    ERR=false.
checkStairs(_DATA,STAIRS,ERR,MESSAGE):-
    ERR = true,
    format(atom(MESSAGE), "Stairs definition: ~w is illegal",[STAIRS]).

stairsKind(diff(_ROOM)).  %stairs that just balance level difference
stairsKind(floor(_-_)).   %stairs that lead from one floor to other

element(inDoor,DATA,stairs(BUILD/FLOOR/ID,KIND,DIFF,STAIRS),
    [(ground(ID),
        atomic(BUILD),
       (atomic(FLOOR)->once(integer(FLOOR)), -100<FLOOR, FLOOR<100;
       FLOOR=FL1-FL2, integer(FL1), integer(FL2),
                      -100<FL1,FL1<100,-100<FL2,FL2<100)
       ),
     once(stairsKind(KIND)),
       (once(integer(DIFF))-> -500=<DIFF, DIFF=<500;
       DIFF=D1-D2, -500=<D1,D1=<500, -500=<D2,D2=<500),
     checkStairs(DATA,STAIRS)
    ],
    circularWalls(STAIRS)).


wallKind(line).
wallKind(polyline).
wallKind(polygon).

element(inDoor,DATA,wall(ID,KIND,POINTS,SECTIONS),
        [ground(ID),
        once(wallKind(KIND)),
        forall(member(PID,POINTS),checkCorner(DATA,PID,KIND))],
        checkWall4(DATA,KIND,POINTS,SECTIONS)).
element(inDoor,DATA,wall(ID,KIND,LINES),
        [ground(ID),
        once(wallKind(KIND)),
        (is_list(LINES), length(LINES,NR), NR>1)],
        checkWall3(DATA,LINES)).

element(inDoor,DATA,blockage(ID,LINE),
        [ground(ID),
        (is_list(LINE),checkCorners(DATA, LINE))],true).

checkCall(CALL,ERR,MESSAGE):-
    predicate_property(CALL,built_in) ->
      (\+ CALL->ERR=true,
         format(atom(MESSAGE),'Test ~w failed!',[CALL]);
      true);
    call(CALL,ERR,MESSAGE).


checkDataCall(DATA,CALL,ERR,MESSAGE):-
    predicate_property(CALL,built_in) ->
      (\+ CALL->ERR=true,
         format(atom(MESSAGE),'Test ~w failed!',[CALL]);
      true);
    %arg(1,CALL,DATA),
    call(CALL,ERR,MESSAGE).

%checkIDAmbigous(+MOD,+PATTERN,-ID) is nondet.
%checks, if parm1 in PATTERN is ambigous
%if yes, double ID-s are returned nondeterministically
%checkIDAmbigous(MOD,PATTERN,IDS):-
%    setof(I,(arg(1,PATTERN,I),call(MOD:PATTERN)),IDSET),
%    setof(I,(member(I,IDSET),findall(I1,call(MOD:PATTERN),I1S),
%             I1S=[_|_]),IDS).


%eg. corner/2:southwest - only checking calls are given
%+DATA: data environment (a Prolog module)
%+NAMEID:        name or ID of the unit being searched and checked
%[:]PARMCALLS:   a list of calls to check the params of corner/2
%:STRUCTCALLS: one call that may check the entire structure
checkElementInstance(DATA,NAMEID,PARMCALLS,STRUCTCALL)-->
    {nth1(NTH,PARMCALLS,CALL),
       checkCall(CALL,ERR,PARMSG),ERR==true} ->
    {format(atom(MESSAGE),'~w Param-test ~w failed! ~w',[NAMEID,NTH,PARMSG])},       [MESSAGE];
    {checkDataCall(DATA,STRUCTCALL,ERR,MESSAGE), ERR==true} ->
    {format(atom(ERRSTRUCT),'~w Struct-test failed!',[NAMEID])},
      [ERRSTRUCT,MESSAGE];
    [] .


%checkElement(+MOD,+DATA,+NAME/+ARITY,?ID,-ERRORS,-LAST)
%check element DATA:NAME/ARITY (eg. data:corner/2),
%+DATA: data environment (a Prolog module)
%ID: can be given as bound or can be a variable
%returns the list of error messages, if any/an empty list otherwise
checkElement(MOD,DATA,NAME/ARITY,ID)-->
    {functor(PATTERN,NAME,ARITY), arg(1,PATTERN,ID),
    element(MOD,DATA,PATTERN,PARMCALLS,STRUCTCALL),
    findall([NAME:ID,PARMCALLS,STRUCTCALL],DATA:PATTERN,MATRIX),
    transpose(MATRIX,[NAMEIDS,PARMCALLSS,STRUCTCALLS])},
    foldl(checkElementInstance(DATA),NAMEIDS,PARMCALLSS,STRUCTCALLS).


%all elements, matching DATA:NAME/ARITY-ID are checked
%doCheckElement(+CTX,+DATA,?NAME/ARITY,?ID)
%errors are displayed
%doCheckElement(inDoor,ptettk,wall/4,103)
%doCheckElement(inDoor,ptettk,room/4,103).
doCheckElement(CTX,DATA,NAME/ARITY,ID):-
    element(CTX,DATA,PATTERN,_,_), functor(PATTERN,NAME,ARITY),
    upcase_atom(NAME,UPNAME),
    writef('----------> %ws are checked.\n',[UPNAME]),
    checkElement(CTX,DATA,NAME/ARITY,ID,ERRORS,[]),
    ignore(forall(member(LINE,ERRORS),writeln(LINE))),
    writef('----------> %ws have been checked\n',[UPNAME]).

%+PROBLEM:  problem/context identifier
%+DATA:     data context module
doCheckAll(PROBLEM,DATA):-
    \+ currentContext(DATA)-> existence_error(context_module,DATA);
    %doCheckElement(MOD,NAME/ARITY,_),
    element(PROBLEM,DATA,PATTERN,_,_), functor(PATTERN,NAME,ARITY),
    doCheckElement(PROBLEM,DATA,NAME/ARITY,_), fail; true.

doCheckAllIndoor(DATA):- doCheckAll(inDoor,DATA).

doCheckAllIndoor:- doCheckAll(inDoor,data).

count(DATA,PATTERN,NR):-
    current_module(DATA), current_predicate(_,DATA:PATTERN),
    findall(_P,DATA:PATTERN,PL), length(PL,NR).

doClearElement(CTX,DATA,NAME/ARITY):-
    element(CTX,DATA,PATTERN,_,_), functor(PATTERN,NAME,ARITY),
    upcase_atom(NAME,UPNAME),
    writef('----------> %ws are cleared.\n',[UPNAME]),
    count(DATA,PATTERN,NR),
    %retractall(DATA:PATTERN),
    abolish(DATA:NAME/ARITY),
    writef('----------> %w %ws have been cleared\n',[NR,UPNAME]).


%+CTX:  problem/context identifier
%+DATA: data module
doClearAll(CTX,DATA):-
    element(CTX,DATA,PATTERN,_,_), functor(PATTERN,NAME,ARITY),
    doClearElement(CTX,DATA,NAME/ARITY), fail;
    true. %, clearModule(DATA).

doClearAllIndoor(DATA):-
    doClearAll(inDoor,DATA).

doClearAllIndoor:-
    doClearAll(inDoor,data).


% Application contexts: databases, usually for different applications
% There can be several contexts loaded in the same time,
% as there might be several applications served
%
%subdirectory to contain building models (contexts)
%(later it could be passed as a program parameter)
contextDir("buildings").

buildDatafile(DATA,DATAFILE):-
  contextDir(DIR), atomic_list_concat([DIR,DATA],"/",DATAFILE).

%+DATA: context is a proper module that can be loaded by consulting it
%       It is also possible to load a context several times...
%       Fails if the file doesn't exist
loadContext(DATA):-
  buildDatafile(DATA,DATAFILE),
  catch(consult(DATAFILE),
       error(existence_error(source_sink,_),_),fail).

%?DATA: context is a proper module that is (or not) loaded
%@tbd: basic data structure should be checked (model driven way)
currentContext(DATA):-
  current_module(DATA).


%+DATA: context is a proper module that can be loaded by consulting it
%       It is also possible to clear not-loaded contexts,
%       or to clear contexts several times
%       throws exception if the file doesnt exist
clearContext(DATA):-
  \+ currentContext(DATA)-> existence_error(context_module,DATA);
  doClearAllIndoor(DATA), abolish(DATA:corridorLength/2).

