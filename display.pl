:- module(display, [clearXPCEPicture/1,closePicture/1, closePicture/2
                    ,flushNaviPicture/2,
                    initParameters/2
                 ,createNaviPicture/2, htmlNaviPicture//2
                 ,closeNaviPicture/0, closeNaviPicture/1
                 ,clearNaviPicture/0, clearNaviPicture/1
                 ,drawNaviPath/3, htmlNaviPath//3
                 ,walkingPathDraw/7 %draws the walking path to the picture
]).

%@tbd stairs check stepping period (ttk/'F')
%@tbd a wall to polygon(s) algorithm - generally
%@tbd client/server collect drawing in an attribute-not in dynamic
%
:- use_module(perspective).


:- use_module(geometry).
:- use_module(xpceFace).
%:- use_module(data). data is loaded dynamically
:- use_module(listext).

:- use_module(library('http/html_write')).
:- use_module(library(dcg/high_order)).

:- use_module(planar).

:- op(1,fx,@).
:- op(500,yfx,?).


margin(1.2).

%myPerspective(FRUSTUM,TRANS,ANGLE,SCALE,XYDIFF).
%FRUSTUM:
%TRANS: TX/TY/TZ - translation
%ANGLE: AX/AY/AZ - rotation angle (degrees)
%SCALE: AX*AY*AZ - scale factor
%XYDIFF: XDIFF/YDIFF - display pixel coordinate difference
myPerspective(FRUSTUM,TRANS,ANGLE,SCALE,XYDIFF):-
  myCamera(TRANS,ANGLE), myScaling(SCALE),
  myFrustum(FRUSTUM), myXYDiff(XYDIFF).

myScaling(15*10*20).

myFrustum([-200 - 200,-200- 200,1500-1000]).

% myCamera(60000/30000/100000, 215/0/90). was OK, 15000 slightly better
myCamera(60000/30000/130000, 225/0/90).

%coordinate difference to display canvas in pixels
myXYDiff(800/500).

%frame(FRAME,XYDIFF,XYSCALE,XYCHANGE).
%FRAME:   frame ID
%XYDIFF:  XDIFF/YDIFF a difference of 0/0 point in machine coordinates
%XYSCALE: XSCALE/YSCALE scale factor betw. machine/abstract coordinates
%XYCHANGE: true;false if both of coordinates are to be exchanged or not
% One window may contain several "frames" with separate
% turtles In case of a single turtle FRAME may be left variable

%:- dynamic frame/4.

%the dynamic is transformed to a (global) variable
%because that is thread-specific, that is different for clients
%value(frame:DICT)
%DICT: {FRAMEX:[XYDIFF,XYSCALE,XYCHANGE]}

frame(FRAME,XYDIFF,XYSCALE,XYCHANGE):-
  nb_getval(frames,FRAMES),
  [XYDIFF,XYSCALE,XYCHANGE] = FRAMES.FRAME.

%translate point
pointTrans(X0/Y0,XD/YD,X/Y):-
  X is X0+XD, Y is Y0+YD.

% a generalized procedure to covert a 3D (2.5D) point to 2D
% FRAME: - for floor plan elevations FRAMEs are floor IDs
%        and Z (height) coordinates are irrelevant
%        - for perspectivic display floor IDs are irrelevant
point2Scale(FRAME,X/Y/Z,XX/YY):- !,
  (Z > -inf -> myPerspective(FR,TR,ANG,SC,XYDIFF),
    setPerspective(FR,TR,ANG,SC),
    project2D(X/Y/Z,XY1), pointTrans(XY1,XYDIFF,XX/YY);
  frame(FRAME,DIFFX/DIFFY,SCALEX*SCALEY,XYCHANGE),
  (XYCHANGE->
    YY is X*SCALEX+DIFFX, XX is Y*SCALEY+DIFFY;
  XX is X*SCALEX+DIFFX, YY is Y*SCALEY+DIFFY)).
point2Scale(FRAME,X/Y,XX/YY):-
  frame(FRAME,DIFFX/DIFFY,SCALEX*SCALEY,XYCHANGE)->
  (XYCHANGE->
    YY is X*SCALEX+DIFFX, XX is Y*SCALEY+DIFFY;
  XX is X*SCALEX+DIFFX, YY is Y*SCALEY+DIFFY).

sizes2Scale(FRAME,H0*W0,H*W):-
  frame(FRAME,_DIFFXY,SCX*SCY,XYCHANGE)->
  (XYCHANGE-> W is H0*abs(SCX), H is W0*abs(SCY);
  H is H0*abs(SCX), W is W0*abs(SCY)).

size2Scale(FRAME,SIZE,S):-
  frame(FRAME,_DIFFXY,SCX*SCY,_)->
  SCALE is (abs(SCX)+abs(SCY))/2, S is SIZE*SCALE.

rect2Scale(FRAME,XY0-H0*W0,XY-H*W):-
  !, point2Scale(FRAME,XY0,XY), sizes2Scale(FRAME,H0*W0,H*W).
rect2Scale(FRAME,XY1-XY2,XXY1-XXY2):-
  point2Scale(FRAME,XY1,XXY1), point2Scale(FRAME,XY2,XXY2).


mPoint2Scale(FRAME,LEVEL,XY0,X/Y):-
  LEVEL > -inf -> myPerspective(FR,TR,ANG,SC,XYDIFF),
    setPerspective(FR,TR,ANG,SC),
    project2D(XY0/LEVEL,XY1), pointTrans(XY1,XYDIFF,X/Y);
  point2Scale(FRAME,XY0, X/Y).

mRect2Scale(FRAME,LEVEL,XY0-XY1,XY2-XY3):-
  LEVEL > inf ->  myPerspective(FR,TR,ANG,SC,XYDIFF),
    setPerspective(FR,TR,ANG,SC),
    project2D(XY0/LEVEL,XY00), pointTrans(XY00,XYDIFF,XY2),
    project2D(XY1/LEVEL,XY11), pointTrans(XY11,XYDIFF,XY3);
  point2Scale(FRAME,XY0,XY2), point2Scale(FRAME,XY1,XY3).

poly2Scale(FRAME,LEVEL,POLYGON,POLY):-
  maplist(mPoint2Scale(FRAME,LEVEL),POLYGON,POLY).

pointRound(X/Y,XI/YI):- XI is round(X), YI is round(Y).


%
% mPoly2Scale(MFRPOLY,POLY2GON)
% takes a multiframe polygon (a list of polygons w frame notes)
% and scales it to machine level (xpce or html)
mPoly2Scale([],[]):- !.
mPoly2Scale([FRAME-LEVEL-POLY|MFRPOLY],[POLYGON|MFRPOLYGON]):-
  poly2Scale(FRAME,LEVEL,POLY,POLYGON),
    mPoly2Scale(MFRPOLY,MFRPOLYGON).


%
% draw2XPCE(+PICTURE,+DRAW)
% takes a single inDoor graphic instruction and draws it to XPCE
draw2XPCE(PICTURE,line(X1/Y1,X2/Y2,COLOR,TH)):-
    !, line(PICTURE,X1/Y1,X2/Y2,COLOR-TH-none).
draw2XPCE(PICTURE,poly(POLY,none,COLOR,TH)):-
          poly(PICTURE,POLY,COLOR-TH-none).

% draw2XPCE(+PICTURE,+DRAW)
% takes a list of inDoor graphic instructions and draws them to XPCE
draws2XPCE(PICTURE,DRAW):- is_list(DRAW),
  maplist(draw2XPCE(PICTURE),DRAW).



% transforms the polygons point list to a single string as HTML demands
poly2HTML([],LEFT,LEFT):- ! .
poly2HTML([X/Y|POLY],LEFT0,HTML):-
  poly2HTML(POLY,LEFT0,LEFT),
  concat_atom([X,',',Y,' '],XY), concat(LEFT,XY,HTML).

%mapDiffList(GOAL,LIST0,LIST,END)
%+GOAL:  goal(...,X,Y) to apply for each elements of LIST0 and LIST
%+LIST0: input list
%+LIST:  result list
%?END:   end of result list

mapDiffList(GOAL,LIST0,LIST,END):- mapDiffList_(GOAL,LIST0,LIST,END).

mapDiffList_(_,[],END,END).
mapDiffList_(GOAL,[ELEM1|TAIL1],[ELEM2|TAIL2],END):-
  call(GOAL,ELEM1,ELEM2), mapDiffList_(GOAL,TAIL1,TAIL2,END).


% DCG:draws2HTML(+DRAW)
% draws2HTML(+DRAW,-HTMLSVG)
% takes a list of inDoor graphic instructions
% and converts them to HTML/SVG

draw2HTML(line(X1/Y1,X2/Y2,COLOR,TH),
          line([x1=XA,y1=YA,x2=XXA,y2=YYA,
               stroke=COLOR,'stroke-width'=THA],[])):-
  XI is round(X1), string_to_atom(XA,XI),
    YI is round(Y1), string_to_atom(YA,YI),
    XXI is round(X2), string_to_atom(XXA,XXI),
    YYI is round(Y2), string_to_atom(YYA,YYI), string_to_atom(THA,TH).
draw2HTML(poly(POLY,none,COLOR,TH),
          polyline([points=POLYHTML,
                    fill="none", stroke=COLORA,'stroke-width'=THA],[])):-
  poly2HTML(POLY,"",POLYHTML), string_to_atom(COLORA,COLOR),
  string_to_atom(THA,TH).

draws2HTML(DRAWS,HTMLS,END):-
  mapDiffList(draw2HTML,DRAWS,HTMLS,END)
  %,debug(display,"HTML:~q\n",[HTMLS])
  .

draws2HTML(DRAWS,HTMLS):-
  maplist(draw2HTML,DRAWS,HTMLS).

%line(X0/Y0,X/Y,COLOR,THICK)
wallSectionDrawing(FRAME,LEVEL,XY0,XY,THICK,COLOR)-->
  {mPoint2Scale(FRAME,LEVEL,XY0,X1/Y1),
   mPoint2Scale(FRAME,LEVEL,XY,XX1/YY1),
   (once(frame(FRAME,_,_,_)) -> size2Scale(FRAME,THICK,TH);
    projectSizeCubeDiag(XY0/LEVEL,THICK,TH)),
      XI is round(X1), YI is round(Y1),
      XXI is round(XX1), YYI is round(YY1)},
   [line(XI/YI,XXI/YYI,COLOR,TH)].

%wallLineDrawing(DATA,FRAME,LEVEL,POINTS,SECTIONS,COLOR).
%+DATA:      data context (a Prolog module)
%+FRAME:     picture identifier
%+LEVEL:     level of the wall LEVEL= -inf ==> 2D else 3D
%+POINTS
%+SECTIONS
%+COLOR
wallLineDrawing(DATA,FRAME,LEVEL,[P0|POINTS],SECTIONS,COLOR)-->
  {last(POINTS,Z0)},
  foreach(planar:nextCornerLineSection(DATA,P0,Z0,POINTS,SECTIONS,
                                       XY0,XY,_P,THICK,_KIND),
          wallSectionDrawing(FRAME,LEVEL,XY0,XY,THICK,COLOR)).

%wallPolygonDrawing(DATA,FRAME,LEVEL,LINES,COLOR).
%+DATA:      data context (a Prolog module)
%+FRAME:     Frame ID inside the window
%+LEVEL:     Floor level (LEVEL= -inf ==> 2D; LEVEL> -inf ==> 3D)
%+LINES:     list of line sections composing the poly
%+COLOR:
%
%polygons and polylines are handled as a list of straight lines
%
wallPolygonDrawing(_,_,_,[],_)--> [], !.
wallPolygonDrawing(DATA,FRAME,LEVEL,POINTS-SECTIONS,COLOR)-->
  !, wallLineDrawing(DATA,FRAME,LEVEL,POINTS,SECTIONS,COLOR).
wallPolygonDrawing(DATA,FRAME,LEVEL,
                   [line(POINTS,SECTIONS)|LINES],COLOR)-->
  wallLineDrawing(DATA,FRAME,LEVEL,POINTS,SECTIONS,COLOR),
  wallPolygonDrawing(DATA,FRAME,LEVEL,LINES,COLOR).


composeBypass(FROM,TO,[CUT1,CUT2],POLYGON,BYPASSED):-
  shiftPolygon(POLYGON,[XY1,XY2|POLY]),
  (CUT1=XY1,CUT2=XY2,
    (isNearer(FROM,XY1,XY2), append([FROM,XY1|POLY],[XY2,TO],BYPASSED), !;
     append([FROM,XY2|POLY],[XY1,TO],BYPASSED), ! );
  CUT1=XY2,CUT2=XY1,
    (isNearer(FROM,XY1,XY2), reverse(POLY,YLOP),
     append([FROM,XY1|YLOP],[XY2,TO],BYPASSED), !;
     append([FROM,XY2|POLY],[XY1,TO],BYPASSED), ! )).

%drawing a line from point to point with bypassing/avoiding a
%blockage - (at the moment only floor to floor stairs)
% +DATA:   data context (a Prolog module)
% +BFLID = BUILDING/FLOOR/ROOMID: room identifier structure
% +ROOMID: room identifier, stairs are located in
% +XYFR:   door (middle) geometrical point
% +XYTO:   door (middle) geometrical point
%
% drawLineBlockage(DATA,'F'/0/ID,mainCorridor,600/777,600/3000,red-20)
% is semidet.
% Fails if there is no blockage in the given corridor
%
% intersectSurface(sect(780/1113,705/1903),polygon([580/1321,580/2003,830/2003,830/1321]),
% (intersectSurface(line(43/77-78/257),polygon([58/132,58/201,83/201,83/132])))
% display:drawLineBlockage(data,'F'/1/R,
%                          1043.0/3288.5,780.0/3494.5,50,P,[])
%
drawLineBlockage(DATA,B/FL/ROOMID0,XYFR,XYTO,CDIST)-->
  {DATA:blockage(B/FL/ROOMID0,CORNERS),
  planar:room2Polygon(DATA,B/FL/ROOMID0,ROOMPOLY),
  maplist(DATA:corner,CORNERS,BLOCKAGEPOLY),
  inflatePolyline(BLOCKAGEPOLY,CDIST,polygon(POLYLINE)),
  intersectSurface(sect(XYFR,XYTO),polygon(POLYLINE),
                   polygon(POLY1),polygon(POLY2),CUTPOINTS)}->
    ({isInside(polygon(POLY1),polygon(ROOMPOLY)),
      composeBypass(XYFR,XYTO,CUTPOINTS,POLY1,BYPOLY)},
      BYPOLY, ! ;
    {isInside(polygon(POLY2),polygon(ROOMPOLY)),
      composeBypass(XYFR,XYTO,CUTPOINTS,POLY2,BYPOLY)},
      BYPOLY, ! ;
    [XYFR,XYTO]).
drawLineBlockage(DATA,B/FL/ROOMID0,XYFR,XYTO,CDIST)-->
  {DATA:stairs(B/(FL- _FL)/_IDST,floor(ROOMID0- _ID),_,CORNERS),
  planar:room2Polygon(DATA,B/FL/ROOMID0,ROOMPOLY),
  maplist(DATA:corner,CORNERS,BLOCKAGEPOLY),
  inflatePolygon(polygon(BLOCKAGEPOLY),CDIST,polygon(POLYLINE)),
  intersectSurface(sect(XYFR,XYTO),polygon(POLYLINE),
                   polygon(POLY1),polygon(POLY2),CUTPOINTS)}->
    ({isInside(polygon(POLY1),polygon(ROOMPOLY)),
      composeBypass(XYFR,XYTO,CUTPOINTS,POLY1,BYPOLY)},
      BYPOLY, ! ;
    {isInside(polygon(POLY2),polygon(ROOMPOLY)),
      composeBypass(XYFR,XYTO,CUTPOINTS,POLY2,BYPOLY)},
      BYPOLY, ! ;
    [XYFR,XYTO]).


% draws the blockage circumvention
% or simply draws a straight line
drawBlockageOrLine(DATA,B/FL/ROOMID0,XYFR,XYTO,CDIST)-->
    drawLineBlockage(DATA,B/FL/ROOMID0,XYFR,XYTO,CDIST), !.
drawBlockageOrLine(_,_,XYFR,XYTO,_)--> [XYFR,XYTO].

% draws a path step (from door/stair to door/stair)
%
% pathStep(DATA,FLOOR,FROMOBJ,TOOBJ,CDIST)
% pathStep(DATA,FLOOR,door(FROM,XYFR),door(TO,XYTO),CDIST)
% +DATA:  data context (Prolog module)
% +FLOOR: floor/frame identifier structure
% +FROM:  door identifier "from"
% +XYFR:  door (middle) geometrical point
% +TO:    door identifier "to"
% +XYTO:  door (middle) geometrical point
%
pathStep(DATA,FL,door(FL/DOORFROM,XYFR),
                          door(FL/DOORTO,XYTO),CDIST)-->
  %when the path step is along the same side of a corridor
  {once(planar:sameWallDoors(DATA,_B1/FL/_R1,corridor,
                             DOORFROM,DOORTO,PLUSMINUS))} -> !,
    %%% calculating corridor pathway CDIST far from the wall
    {(PLUSMINUS<0 -> orthogonalShift(XYTO-XYFR,CDIST,XY90TO-XY90FR);
      orthogonalShift(XYFR-XYTO,CDIST,XY90FR-XY90TO))},
    [XYFR,XY90FR],
    drawBlockageOrLine(DATA,_B2/FL/_R2,XY90FR,XY90TO,CDIST),
    [XY90TO,XYTO];
  %@tbd the common room/building/floor should be calculated
  drawLineBlockage(DATA,_B3/FL/_R3,XYFR,XYTO,CDIST).
pathStep(DATA,FL,door(FL/_DOOR,XYFR),stair(FL/STAIR,XYTO),CDIST)-->
  %when the step is from a door to a stairstep
  {DATA:stairs(B/(FL-_)/STAIR,floor(R-_),_,_)}, !,
  drawLineBlockage(DATA,B/FL/R,XYFR,XYTO,CDIST).
pathStep(DATA,FL,stair(FL/STAIR,XYFR),door(FL/_DOOR,XYTO),CDIST)-->
  %when the step is from a door to a stairstep
  {DATA:stairs(B/(FL-_)/STAIR,floor(R-_),_,_)}, !,
  drawLineBlockage(DATA,B/FL/R,XYFR,XYTO,CDIST).
pathStep(_DATA,_FL,FROM,_TO,_CDIST)-->
   {arg(2,FROM,XYFROM)}, [XYFROM].


walkingPathRestFloor(_,[],[],_,_)--> [], !.
%the last section is a stair leading to a new floor
%the stair should be displayed on the new floor too
walkingPathRestFloor(_DATA,[FROM,TO|PATH],[FROM,TO|PATH],
                     FLOOR,_CDIST)-->
  {FROM = stair(FLOOR/ST,XYFR), TO = stair(NEXTFLOOR/ST,XYTO),
  FLOOR \= NEXTFLOOR}, !, [XYFR,XYTO].
%no more steps if new floor comes - the last point is appended
walkingPathRestFloor(_DATA,[POINT|PATH],[POINT|PATH],FLOOR,_)-->
  {\+ arg(1,POINT,FLOOR/_), arg(2,POINT,XY)}, [XY], !.
walkingPathRestFloor(DATA,[FROM,TO|PATH],REMPATH,FLOOR,CDIST)-->
  pathStep(DATA,FLOOR,FROM,TO,CDIST), !,
    %if the doors are on the same side of a corridor
    walkingPathRestFloor(DATA,[TO|PATH],REMPATH,FLOOR,CDIST).
walkingPathRestFloor(DATA,[POINT|PATH],REMPATH,FLOOR,CDIST)-->
  {arg(2,POINT,XY)}, [XY],
    walkingPathRestFloor(DATA,PATH,REMPATH,FLOOR,CDIST).


walkingPathFloor(DATA,[FROM,TO|PATH],REMPATH,
                     FLOOR,CDIST)-->
  {FROM = stair(PREVFLOOR/ST,_), TO = stair(FLOOR/ST,_),
  FLOOR \= PREVFLOOR}, !,
  pathStep(DATA,FLOOR,FROM,TO,CDIST),
  walkingPathRestFloor(DATA,[TO|PATH],REMPATH,FLOOR,CDIST).
walkingPathFloor(DATA,[POINT|PATH],REMPATH,FLOOR,CDIST)-->
  {arg(1,POINT,FLOOR/_)},
  walkingPathRestFloor(DATA,[POINT|PATH],REMPATH,FLOOR,CDIST).


walkingPath(_,[],_,_,[]):- !.
walkingPath(DATA,[FROM|PATH],FLH,CDIST,[FLOOR-LEVEL-FLPATH|FLPATHS]):-
    walkingPathFloor(DATA,[FROM|PATH],REMPATH,FLOOR,CDIST,FLPATH,[]),
    (FLH>=0 ->LEVEL is FLOOR*FLH;LEVEL is -inf),
    walkingPath(DATA,REMPATH,FLH,CDIST,FLPATHS).


%walkingPathDraw(+DATA,+PATH,+HEIGHT,+CDIST,+COLTH)
%+DATA:    data context (Prolog module)
%+PICTURE: picture/window ID: []-->HTML output
%+PATH:    a list of nodes, one node can be
%          door(FL/ID,XY) ID: identifier
%                         XY: middle point
%+HEIGHT:  floor height (level difference)
%+CDIST:   corridor going distance (from walls, 50cm)
%+COLTH:   path color-thickness (default: red-4)
walkingPathDraw(DATA,PATH,HEIGHT,CDIST,COLOR-TH)-->
  {walkingPath(DATA,PATH,HEIGHT,CDIST,POINT2S), mPoly2Scale(POINT2S,POLY2LINE)},
  foreach((member(POLYLINE,POLY2LINE),
           maplist(pointRound,POLYLINE,POLYLINEI)),
          [poly(POLYLINEI,none,COLOR,TH)]).


%poly(POINTLIST,FILL,COLOR,THICK)
%FILL=none
stairsRectangles(_,_,_,0,_,_)--> [], !.
stairsRectangles(FRAME,LEVEL,XY0-XY1,NRSTEPS,WIDTH,COLOR-TH)-->
    {orthogonalShift(XY0-XY1,WIDTH,XY00-XY10),
    poly2Scale(FRAME,LEVEL,[XY0,XY1,XY10,XY00,XY0],POLYLINE),
    maplist(pointRound,POLYLINE,POLYLINEI),NR is NRSTEPS-1},
    [poly(POLYLINEI,none,COLOR,TH)],
    stairsRectangles(FRAME,LEVEL,XY00-XY10,NR,WIDTH,COLOR-TH).


%+ X0/Y0-X/Y: stairs baseline
%+ HEIGHT: height difference stairs bridge over
%+ DEPTH: total length/(orthogonal depth) of stairs
%16: one stair height (vertical difference of stairs)
%it is constant now, later it can be taken from somewhere
stairsDrawing(FRAME,LEVEL,stairs(_ROOMID,XY0,XY1,DEPTH,HEIGHT),TH)-->
    {NRSTEPS is div(abs(HEIGHT),16), STAIRDEPTH is DEPTH/NRSTEPS},
    stairsRectangles(FRAME,LEVEL,XY0-XY1,NRSTEPS,
                     STAIRDEPTH,black-TH).


drawBuildingFloor(DATA,FLOOR,LEVEL,STAIRSTH,_W^FLOOR^WALL^MOD:CALLWALL,
            _S^FLOOR^STAIRS^MOD:CALLSTAIRS)-->
  foreach(MOD:CALLWALL,
          wallPolygonDrawing(DATA,FLOOR,LEVEL,WALL,black)),
  foreach(MOD:CALLSTAIRS,
          stairsDrawing(FLOOR,LEVEL,STAIRS,STAIRSTH)).

drawBuilding(DATA,FRAMES,FLH,STAIRSTH,W^FLOOR^WALL^MOD:CALLWALL,
            S^FLOOR^STAIRS^MOD:CALLSTAIRS)-->
  foreach((member(FLOOR,FRAMES),
          (FLH>= 0->LEVEL is FLOOR*FLH;LEVEL is -inf)),
          drawBuildingFloor(DATA,FLOOR,LEVEL,STAIRSTH,
                           W^FLOOR^WALL^MOD:CALLWALL,
                           S^FLOOR^STAIRS^MOD:CALLSTAIRS)).


%+DATA:       data context (a Prolog module)
%+FRAMES:     list of frame IDs within picture, if any
%+FLH:        floor height (if they are uniform)
%+STAIRSTH:   thickness to draw stairs
%+CALLVARS:   callback to return wall sections in a backtracking fashion
%             ID^WALL^MOD:CALLBACK
%             ID:   identifier of the wall
%             WALL: wall structure
%             MOD:  module of definition
%             CALLBACK: structure to call (contains ID, WALL)
createBuildingPicture(DATA,FRAMES,FLH,STAIRSTH,
              CALLVARS,CALLSTAIRS)-->
  {debug(display,'createBuildingPicture(FRAMES:~w,FLH:~w,\c
                  STAIRSTH:~w,...,...',
        [FRAMES,FLH,STAIRSTH])},
    drawBuilding(DATA,FRAMES,FLH,STAIRSTH,CALLVARS,CALLSTAIRS).

createBuildingNaviPictures(DATA,BUILDING,FLOORS,STAIRSTH,FLH)-->
    createBuildingPicture(DATA,FLOORS,FLH,STAIRSTH,
        WID^FLOOR^WALL^planar:wall34(DATA,BUILDING/FLOOR/WID,FLOOR,WALL),
        SID^FLOOR^STAIRS^planar:stairs(DATA,BUILDING/FLOOR/SID,FLOOR,STAIRS)).


%clears each graphic object attached to the XPCE picture
clearXPCEPicture(PICTURE):-
  send(@PICTURE?graphicals,for_all,message(@arg1,free)),
    flush(PICTURE).

closePicture(PICTURE):-
  closePicture(PICTURE,[xpce(true)]).

closePicture(PICTURE,OPTIONS):-
  (option(xpce(true),OPTIONS)->PICTUREID = PICTURE;PICTUREID=[]),
  clearXPCEPicture(PICTUREID), free(@PICTUREID).

% Decision if the picture already exists
% get(?(@(display),contains),find_all,message(@(arg1),instance_of,frame),WS),
% chain_list(WS,LS), writeln(LS).

windowID(BUILDING,FLOOR,IDPREFIX,WID):-
  var(BUILDING) -> WID=IDPREFIX;
  var(FLOOR) -> atomic_list_concat([IDPREFIX,'-',BUILDING],WID);
  atomic_list_concat([IDPREFIX,'-',BUILDING,'/',FLOOR],WID).

composeWindowTitle(SHORTPREFIX,LONGPREFIX,BUILDING,FLOOR,WID,TITLE):-
    windowID(BUILDING,FLOOR,SHORTPREFIX,WID),
    atomic_list_concat([LONGPREFIX,':',WID],TITLE).

%xpce(true)->an explicite picture must be opened
%xpce(false)->HTML picture is opened by the framework
%floorheight(FL)>=-inf -> 3D perspectivic visualisation
%                         with floor height distortion
%floorheight(FL)=-inf ->  2D visualisation, floors are drawn
%                         with 2D offset
composeWindow(DATA,BUILDING,OPTIONS,PICTURE):-
  option(scale(SCH*SCW),OPTIONS,0.15*0.15),
  option(floorheight(FH),OPTIONS,800),
  DATA:signature(LONG,SHORT),
  floors(DATA,BUILDING,FLOORS), margin(MRATE),
  composeWindowTitle(SHORT,LONG,BUILDING,_,ID,TITLE),
  option(framedist(DXY),OPTIONS,0/300),
  wallsMinMax(DATA,BUILDING,FLOORS,[outsideWall],display:point2Scale,
              DXY/FH,XMIN/YMIN-XMAX/YMAX),
  H is abs((YMAX-YMIN)*MRATE), W is abs((XMAX-XMIN)*MRATE),
  debug(display,'composeWindow->~w',[YMIN/XMIN-H*W]),
  (option(xpce(true),OPTIONS)->PICTURE = ID,
    createPicture(PICTURE,TITLE,YMIN/XMIN-H*W);
  PICTURE=[]).

%prepares the frame (floor) differences for 2D display
%framesCorners(FLOORS,XYSCALE,XYCHANGE,DXY,XY0,DICT0,DICT):-
%+FLOORS:   list of floors
%+XYSCALE:  XSCALE*YSCALE scaling factor
%+XYCHANGE: boolean, change of x and y axes are needed
%+DXY:      difference of frames
%+XY0:      X/Y - coordinate of the first corner
framesCorners([],_XYSCALE,_XYCHANGE,_DIFFXY,_XY,DICT,DICT):- !.
framesCorners([FLOOR|FLOORS],XYSCALE,XYCHANGE,DX/DY,X0/Y0,DICT0,DICT):-
  DICT1 = DICT0.put(FLOOR,[X0/Y0,XYSCALE,XYCHANGE]),
  X is X0+DX, Y is Y0+DY,
  framesCorners(FLOORS,XYSCALE,XYCHANGE,DX/DY,X/Y,DICT1,DICT).


:- predicate_options(initParameters/2,3,[scale(false),
                                      diff(false),
                                      framedist(integer),
                                      stairsthick(integer),
                                      xychange(false),
                                      xpce(boolean)]).
initParameters(DATA,OPTIONS):-
  debug(display,'initParameters..(~w,~w)\n',[DATA,OPTIONS]),
  option(scale(SCALE),OPTIONS,0.15*0.15),
  option(xychange(XYCH),OPTIONS,true),
  option(framedist(DXY),OPTIONS,300/0),
  option(diff(XY0),OPTIONS,0/0),
  once(floors(DATA,_BUILDING,FLOORS)),
  framesCorners(FLOORS,SCALE,XYCH,DXY,XY0,frames{},FRAMES),
  debug(display,'framesCorners..(~w)\n',FRAMES),
  nb_setval(frames,FRAMES).


%at the moment we have a single building for a context
:-predicate_options(createNaviPicture/2,2,
                   [scale(false),
                    diff(false),
                    framedist(false),
                    floorheight(integer),
                    stairsthick(integer),
                    xychange(true),
                    xpce(boolean)]).
createNaviPicture(DATA,OPTIONS):-
  option(floorheight(FLH),OPTIONS,-inf),
  option(stairsthick(STAIRSTH),OPTIONS,1),
  floors(DATA,BUILDING,FLOORS),
  composeWindow(DATA,BUILDING,OPTIONS,PICTURE),
  createBuildingNaviPictures(DATA,BUILDING,FLOORS,STAIRSTH,FLH,DRAW,[]),
  draws2XPCE(PICTURE,DRAW), flush(PICTURE).

%Generates the static picture of the building
%(at the moment we have only a single building)
%the predicate generates a diff list of SVG instructions
%(HTML tag), without SVG parentheses
:-predicate_options(htmlNaviPicture//2,2,
                   [scale(false),
                    diff(false),
                    framedist(false),
                    floorheight(integer),
                    stairsthick(integer),
                    xychange(true),
                    xpce(boolean)]).
htmlNaviPicture(DATA,OPTIONS)-->
    {option(floorheight(FLH),OPTIONS,-inf),
    option(stairsthick(STAIRSTH),OPTIONS,1)},
    {floors(DATA,BUILDING,FLOORS)},
    {createBuildingNaviPictures(DATA,BUILDING,FLOORS,STAIRSTH,FLH,DRAW,[])},
    draws2HTML(DRAW).


flushNaviPicture(PICTURE,DRAW):-
  debug(display,'flushNaviPicture(~w,~w)',[PICTURE,DRAW]),
  (PICTURE=[] -> true;
  draws2XPCE(PICTURE,DRAW)).

%Generates the route path, a diff list of SVG instructions
%(HTML tag), without SVG parentheses
:-predicate_options(htmlNaviPath//3,3,
                   [xpce(boolean),
                    bypassDistance(positive_integer)]).
htmlNaviPath(DATA,PATH,OPTIONS)-->
  {debug(display,'htmlNaviPath(~w,~w,~w)',[DATA,PATH,OPTIONS])},
  {option(bypassDistance(CDIST),OPTIONS,50),
    option(paththickcol(PTHICKCOL),OPTIONS,red-4),
    option(floorheight(HEIGHT),OPTIONS,-inf)},
    {walkingPathDraw(DATA,PATH,HEIGHT,CDIST,PTHICKCOL,DRAW,[])},
    draws2HTML(DRAW).

:-predicate_options(drawNaviPath/3,3,
                   [xpce(boolean),
                    bypassDistance(positive_integer)]).
%both for XPCE and HTML
drawNaviPath(DATA,PATH,OPTIONS):-
  (option(xpce(true),OPTIONS,true)->
    DATA:signature(LONG,SHORT), once(floors(DATA,B,_)),
    composeWindowTitle(SHORT,LONG,B,_,PICTURE,_); PICTURE=[]),
    option(paththickcol(PTHICKCOL),OPTIONS,red-4),
    option(floorheight(HEIGHT),OPTIONS,-inf),
    option(bypassDistance(CDIST),OPTIONS,50),
    walkingPathDraw(DATA,PATH,HEIGHT,CDIST,PTHICKCOL,DRAW,[]),
    flushNaviPicture(PICTURE,DRAW).

%%!! only for XPCE(true)
clearNaviPicture(DATA):-
    DATA:signature(LONG,SHORT), once(floors(DATA,BUILDING,_)),
    composeWindowTitle(SHORT,LONG,BUILDING,_,PICTURE,_),
    clearXPCEPicture(PICTURE).

clearNaviPicture:-
    clearNaviPicture(ptettkmii).

%%!! only for XPCE(true)
closeNaviPicture(DATA):-
    DATA:signature(LONG,SHORT), floors(DATA,BUILDING,_),
    composeWindowTitle(SHORT,LONG,BUILDING,_,PICTURE,_),
    closePicture(PICTURE).

closeNaviPicture:-
    closeNaviPicture(ptettkmii).













