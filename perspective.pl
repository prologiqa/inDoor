:-module(perspective,[
             setPerspective/4,
             project2D/2, project2D/3,
             projectSizeCubeDiag/3]).
%
% perspective projection of 3D geometrical structures to a 2D plane
% https://skannai.medium.com/projecting-3d-points-into-a-2d-screen-58db65609f24
%
:- use_module(listext).
%
%:-pack_install('https://github.com/friguzzi/matrix.git').
% https://friguzzi.github.io/matrix/
% Matrix: list of matrix lines
% Matrix multiplication: X(NxP),Y(PxM),M(NxM)
:- use_module(pack(matrix/prolog/matrix)).

% coordinate system:
% X - horizontal axis
% Y - vertical axis
% Z - depth (horizontal distance from the camera)

unitPerspective([FR,TR,A,SC]):-
  unitCamera(TR,A,SC), myFrustum(FR).

myPerspective([FRUSTUM,TRANS,ANGLE,SCALE]):-
  myCamera(TRANS,ANGLE), myScaling(SCALE),
  myFrustum(FRUSTUM).

myScaling(500*500*500).

myFrustum([-80 - 80,-80- 80,20-500]).

unitCamera(0/0/0,0/0/0).

unitScale(1*1*1).

unitCamera(TRANS,ANGLE,SCALE):- unitCamera(TRANS,ANGLE), unitScale(SCALE).

myCamera(TRANS,ANGLE):- unitCamera(TRANS,ANGLE).

% Camera:
% - Frustum - understood in locale/camera coordinates
% - POSITION: X/Y/Z in world coordinate system
% - ANGLE: ANGLEX/ANGLEY/ANGLEZ

%          ANLGEX: pitch, ANGLEY:yaw, ANGLEZ: roll
%

%project2D(PLANE,....,POINT3D,PROJ2D)
%+PLANE:    a struct of v(A,B,C,D), describing the plane
%+RXYZ
%+POINT3D:  X0/Y0/Z0: 3D point to project
%-PROJ2D:   X/Y: a 2D point, the result of projection
%
%X/Y/Z = MVP x MPER x MCAM x X0/Y0/Z0/1
%
%MCAM: camera matrix [[UX,UY,UZ,0][VX,VY,VZ,0]
%                     [WX,WY,WZ,0][0,0,0,1]]
%                  x [[1,0,0,-EX][0,1,0,-EY]
%                     [0,0,1][0,0,0,1]]
% the first matrix describes the rotation
% the second matrix describes the translation
%
%Perspective projection matrix
%MPER = MORTH x MP20

perspective2Projection([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],
          TX/TY/TZ,PITCH/YAW/ROLL,SCX*SCY*SCZ,MPROJECTION):- !,
  mCamera(TX/TY/TZ-PITCH/YAW/ROLL-SCX*SCY*SCZ,MCAMERA),
  mPersProjection([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],MPERSPECTIVE),
  matrix_multiply(MPERSPECTIVE,MCAMERA,MPROJECTION).

%setPerspective(ORIGO,ANGLE,SCALE).
%+ORIGO:     TX/TY/TZ
%+ANGLE:     PITCH/YAW/ROLL
%+SCALE:     SCX*SCY*SCZ):-
setCamera(ORIGO,ANGLE,SCALE):- !,
  mCamera(ORIGO-ANGLE-SCALE,MCAMERA),
  nb_setval(camera,MCAMERA).


%setPerspective(FRUSTUM,CAMORIGO,CAMANGLE,SCALE).
%+FRUSTUM:   [LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR]
%+CAMORIGO:  TX/TY/TZ
%+CAMANGLEG: PITCH/YAW/ROLL
%+SCALE:     SCX*SCY*SCZ):-
setPerspective([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],
          TX/TY/TZ,PITCH/YAW/ROLL,SCX*SCY*SCZ):- !,
  setCamera(TX/TY/TZ,PITCH/YAW/ROLL,SCX*SCY*SCZ),
  nb_setval(frustum,[LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR]).

setPerspective([FRUSTUM,TRANS,ANGLE,SCALE]):-
  setPerspective(FRUSTUM,TRANS,ANGLE,SCALE).


mRotation(PITCHGR/YAWGR/ROLLGR,ROTZYX):-
  PITCH is PITCHGR*pi/180,  COSPITCH is cos(PITCH), SINPITCH is sin(PITCH),
  ROTX =  [[1,0,0,0],
          [0,COSPITCH,-SINPITCH,0],
          [0,SINPITCH,COSPITCH,0],
          [0,0,0,1]],
  YAW is YAWGR*pi/180, COSYAW is cos(YAW), SINYAW is sin(YAW),
  ROTY = [[COSYAW,0,SINYAW,0],
          [0,1,0,0],
          [-SINYAW,0,COSYAW,0],
          [0,0,0,1]],
  ROLL is ROLLGR*pi/180, COSROLL is cos(ROLL), SINROLL is sin(ROLL),
  ROTZ = [[COSROLL,-SINROLL,0,0],
          [SINROLL,COSROLL,0,0],
          [0,0,1,0],
          [0,0,0,1]],
  matrix_multiply(ROTY,ROTX,ROTYX),
  matrix_multiply(ROTYX,ROTZ,ROTZYX).

myTranScaling([[1,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]]).

myTranScaling(0/0/0-1*1*1).


mTranslation(TX/TY/TZ,TRANSMX):-
  TRANSMX = [[1,0,0,TX],[0,1,0,TY],[0,0,1,TZ],[0,0,0,1]].

mTranScaling(TX/TY/TZ,SX*SY*SZ,TRANSCALMX):-
  TRANSCALMX = [[SX,0,0,TX],[0,SY,0,TY],[0,0,SZ,TZ],[0,0,0,1]].

mCamera(TX/TY/TZ-PITCH/YAW/ROLL-SX*SY*SZ,MCAM):-
    mTranScaling(TX/TY/TZ,SX*SY*SZ,MTRANS),
    mRotation(PITCH/YAW/ROLL,MROT),
    matrix_multiply(MTRANS,MROT,MCAM).


mOrthographic([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],
             [[2/(RIGHT-LEFT),0,0,-(RIGHT+LEFT)/(RIGHT-LEFT)],
             [0,2/(TOP-BOTTOM),0,-(TOP+BOTTOM)/(TOP-BOTTOM)],
             [0,0,-2/(FAR-NEAR),-(FAR+NEAR)/(FAR-NEAR)],
             [0,0,0,1]]).


%creates the perspective matrix from the clipping boundaries
mPersProjection([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],
             [[2*NEAR/(RIGHT-LEFT),0,(RIGHT+LEFT)/(RIGHT-LEFT),0],
             [0,2*NEAR/(TOP-BOTTOM),(TOP+BOTTOM)/(TOP-BOTTOM),0],
             [0,0,-(FAR+NEAR)/(FAR-NEAR),-2*FAR*NEAR/(FAR-NEAR)],
             [0,0,-1,0]]):- ! .

%creates the perspective matrix
%+FOVY:   vertical viewing angle of the camera
%         (Field Of View Y vertical angle)
%         measured in degrees, usually 30=<FOVY<=90
%+ASPECT: aspect ratio of viewing window (WIDTH/HEIGHT)
%+NEAR:   the viewing plane (the near end of frustum)
%+FAR:    a clipping distance (the far end of frustum)
mPersProjection([FOVY,ASPECT,NEAR-FAR],MATRIX):-
  TANHALFFOVY is tan(FOVY/2 * pi / 180),
  HHEIGHT is NEAR*TANHALFFOVY, HWIDTH is HHEIGHT*ASPECT,
  mPersProjection([-HWIDTH-HWIDTH,-HHEIGHT-HHEIGHT,NEAR-FAR],MATRIX).


project2DL(CAMERA,NEAR,[X0,Y0,Z0],[X,Y]):-
  number(NEAR), !,
     matrix_multiply(CAMERA,[[X0],[Y0],[Z0],[1]],[[X1],[Y1],[Z1],_]),
     Z1>NEAR,
     X is -NEAR*X1/Z1, Y is -NEAR*Y1/Z1.

project2DL(CAMERA,[LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],
           [X0,Y0,Z0],[X,Y]):- !,
  matrix_multiply(CAMERA,[[X0],[Y0],[Z0],[1]],[[X1],[Y1],[Z1],[W1]]),
  mPersProjection([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],MFRUSTUM),
  matrix_multiply(MFRUSTUM,[[X1],[Y1],[Z1],[W1]],[[X2],[Y2],[_],[W2]]),
  X is X2/W2, Y is Y2/W2.

project2DL([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR]
          -TX/TY/TZ-PITCH/YAW/ROLL-SCX*SCY*SCZ,
          [X0,Y0,Z0],[X,Y]):- !,
    mCamera(TX/TY/TZ-PITCH/YAW/ROLL-SCX*SCY*SCZ,MCAMERA),
    project2DL([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR],MCAMERA,
          [X0,Y0,Z0],[X,Y]).


:- discontiguous project2D/3.


project2D([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR]
          -TX/TY/TZ-PITCH/YAW/ROLL-SCX*SCY*SCZ,X0/Y0/Z0,X/Y):- !,
  project2DL([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR]
          -TX/TY/TZ-PITCH/YAW/ROLL-SCX*SCY*SCZ,
            [X0,Y0,Z0,1],[X,Y]).
project2D([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR]
          -TX/TY/TZ-PITCH/YAW/ROLL-SCX*SCY*SCZ,POINTS3D,POINTS2D):- !,
  maplist(project2DL([LEFT-RIGHT,BOTTOM-TOP,NEAR-FAR]
          -TX/TY/TZ-PITCH/YAW/ROLL-SCX*SCY*SCZ),
          POINTS3D,POINTS2D).

%
% last step: perspective projection
% X0/Y0/Z0 --> X,Y
% X = - NEAR*X0/Z0
% Y = - NEAR*Y0/Z0
%
project2D(NEAR,X0/Y0/Z0,X/Y):-
  number(NEAR), !, nb_getval(camera,CAMERA),
  project2DL(CAMERA,NEAR,[X0,Y0,Z0],[X,Y]).
project2D(NEAR,POINTS3D,POINTS2D):-
  number(NEAR), !, nb_getval(camera,CAMERA),
  maplist(project2DL(CAMERA,NEAR),POINTS3D,POINTS2D).

project2D(CAMERA,X0/Y0/Z0,X/Y):- !,
  nb_getval(frustum,[_,_,NEAR-_]),
  project2DL(CAMERA,NEAR,[X0,Y0,Z0],[X,Y]).
project2D(CAMERA,POINTS3D,POINTS2D):-
  is_list(POINTS3D),
    maplist(project2D(CAMERA),
            POINTS3D,POINTS2D).

project2D(XYZ0,XY):- !,
  nb_getval(camera,CAMERA), project2D(CAMERA,XYZ0,XY).


projectSizeCubeDiag(NEAR,CAMERA,
                X0/Y0/Z0,SIZE0,SIZE):-
  SIZ is SIZE0/sqrt(3), %SIZ is SIZE0*sqrt(3),
  project2DL(CAMERA,NEAR,[X0,Y0,Z0],[XX0,YY0]),
  project2DL(CAMERA,NEAR,[X0+SIZ,Y0+SIZ,Z0+SIZ],[XX1,YY1]),
  SIZE is sqrt((XX1-XX0)*(XX1-XX0)+(YY1-YY0)*(YY1-YY0)).



projectSizeCubeDiag(X0/Y0/Z0,SIZE0,SIZE):-
  nb_getval(camera,CAMERA), nb_getval(frustum,[_,_,NEAR-_]),
  projectSizeCubeDiag(NEAR,CAMERA,X0/Y0/Z0,SIZE0,SIZE).


projectF2D(FRUSTUM,X0/Y0/Z0,X/Y):-
  nb_getval(camera,CAMERA),
  project2DL(CAMERA,FRUSTUM,[X0,Y0,Z0],[X,Y]).


translate(X0/Y0/Z0,TX/TY/TZ,XYZ):-
  mTranslation(TX/TY/TZ,MTRANS),
  matrix_multiply(MTRANS,[[X0],[Y0],[Z0],[1]],XYZ).

rotate(X0/Y0/Z0,PITCH/YAW/ROLL,X/Y/Z):-
  mRotation(PITCH/YAW/ROLL,MROT),
  matrix_multiply(MROT,[[X0],[Y0],[Z0],[1]],[[X],[Y],[Z],_]).

cubeVertices([-100/ -100/ -100,
              100/ -100/ -100,
               100/100/ -100,
              -100/100/ -100,

              -100/100/100,
              100/100/100,
              100/ -100/100,
              -100/ -100/100
             ]).
cubeEdges([3-0,1-6,2-5,4-7]).

%xpceface import is necessary only for tests
:- use_module(xpceface).

setRoll([FRUSTUM,ORIGO,PITCH/YAW/_,SCALE],ROLL,
        [FRUSTUM,ORIGO,PITCH/YAW/ROLL,SCALE]).
setYaw([FRUSTUM,ORIGO,PITCH/_/ROLL,SCALE],YAW,
        [FRUSTUM,ORIGO,PITCH/YAW/ROLL,SCALE]).
setPitch([FRUSTUM,ORIGO,_/YAW/ROLL,SCALE],PITCH,
        [FRUSTUM,ORIGO,PITCH/YAW/ROLL,SCALE]).
setScale([FRUSTUM,ORIGO,PITCH/YAW/ROLL,_],SC,
        [FRUSTUM,ORIGO,PITCH/YAW/ROLL,SC*SC*SC]).
setCameraZ([FRUSTUM,X0/Y0/_,PITCH/YAW/ROLL,SCALE],Z0,
        [FRUSTUM,X0/Y0/Z0,PITCH/YAW/ROLL,SCALE]).

drawCube:-
  PICTURE='PTE-TTK-F',
   cubeVertices(POINTS3D),
   forall(circNeighbour2(FROM3,TO3,POINTS3D),
          (project2D(FROM3,FROM2), project2D(TO3,TO2)->
         line(PICTURE,FROM2,TO2,red-4-none);true)),
   cubeEdges(EDGES),
   forall(member(FX-TX,EDGES),
          (nth0(FX,POINTS3D,FROM3),nth0(TX,POINTS3D,TO3),
           project2D(FROM3,FROM2), project2D(TO3,TO2)->
           line(PICTURE,FROM2,TO2,red-4-none);true)),
   xpceFace:flush(PICTURE).

grow:-
  unitPerspective(P00), setCameraZ(P00,5000,P0),
  forall(between(1,20,SC),
         (setScale(P0,SC*3,P), setPerspective(P),
          drawCube, sleep(1)
         ,display:clearPicture('PTE-TTK-F')
         )
        ).

pitch:-
  unitPerspective(P00), setCameraZ(P00,170,P1),
  setScale(P1,1,P0),
  forall(between(1,18,PITCH),
         (setPitch(P0,PITCH*5,P),
           setPerspective(P), drawCube, sleep(1),
           display:clearPicture('PTE-TTK-F'))
         ).


roll:-
  unitPerspective(P00), setCameraZ(P00,130,P1),
  setScale(P1,1,P0),
  forall(between(1,18,ROLL),
         (setRoll(P0,ROLL*10,P),
           setPerspective(P), drawCube, sleep(1),
           display:clearPicture('PTE-TTK-F'))
         ).

yaw:-
  unitPerspective(P00), setCameraZ(P00,170,P0),
  forall(between(0,18,YAW),
         (setYaw(P0,YAW*10,P),
           setPerspective(P), drawCube, sleep(1),
           display:clearPicture('PTE-TTK-F'))
         ).

pitchyaw:-
  unitPerspective(P00), setCameraZ(P00,180,P0),
  %setScale(P1,50,P0),
  forall(between(1,18,YP),
         (setYaw(P0,YP*10,P2), setPitch(P2,YP*9,P),
           setPerspective(P), drawCube, sleep(1),
           display:clearPicture('PTE-TTK-F'))
         ).


distancing:-
  unitPerspective(P00),
  forall(between(1,20,D),
         (setCameraZ(P00,D*5+100,P),
           setPerspective(P), drawCube, sleep(1),
           display:clearPicture('PTE-TTK-F'))
         ).

cube:-
  PICTURE='PTE-TTK-F', display:clearPicture(PICTURE), flush(PICTURE)
  , unitPerspective(P00), setCameraZ(P00,130,P1),
  setScale(P1,1,P0),
  setPerspective(P0),
    drawCube.

distproj:-
  PICTURE='PTE-TTK-F',
  unitPerspective(P00),
  setScale(P00,100,P0),
  setPerspective(P0),
  P1=1000/0/Z, P2= -1000/0/Z,
  forall(between(1,20,Z0),
         (Z is Z0*20,
          %NEAR = 10,project2D(NEAR,P1,FXY),project2D(NEAR,P2,TXY),
          myFrustum(FRUSTUM),
          projectF2D(FRUSTUM,P1,FXY),
          projectF2D(FRUSTUM,P2,TXY),
          %writeln(FXY-TXY-Z),
          line(PICTURE,FXY,TXY,red-4-none), flush(PICTURE),
          fail;
          sleep(1)
          ,display:clearPicture(PICTURE)
         )
         ).


:- begin_tests(perspective).

test(rotation,true((abs(X-1.0)=<epsilon,
                   abs(Y-1.0)=<epsilon,
                   abs(Z-1.0)=<epsilon))):-
  rotate(1/1/1,0/0/0,X/Y/Z).

test(rotation,true((abs(X- -1.0)=<epsilon,
                   abs(Y- -1.0)=<epsilon,
                   abs(Z- -1.0)=<epsilon))):-
  rotate( -1/ -1/ -1,0/0/0,X/Y/Z).

test(rotation,true((abs(X-1.0)=<10*epsilon,
                   abs(Y-1.0)=<10*epsilon,
                   abs(Z-1.0)=<10*epsilon))):-
  rotate(1/1/1,0/360/720,X/Y/Z).

test(rotation,true((abs(X- -1.0)=<10*epsilon,
                   abs(Y- -1.0)=<10*epsilon,
                   abs(Z-1.0)=<10*epsilon))):-
  rotate(1/1/1,0/0/180,X/Y/Z).

test(rotation,true((abs(X- -1.0)=<10*epsilon,
                   abs(Y-1.0)=<10*epsilon,
                   abs(Z- -1.0)=<10*epsilon))):-
  rotate(1/1/1,0/180/0,X/Y/Z).

test(rotation,true((abs(X- 1.0)=<10*epsilon,
                   abs(Y- -1.0)=<10*epsilon,
                   abs(Z- -1.0)=<10*epsilon))):-
  rotate(1/1/1,180/0/0,X/Y/Z).

test(translation,true(XYZ=([[10], [10], [10],[1]]))):-
  translate(0/0/0,10/10/10,XYZ).

:- end_tests(perspective).










