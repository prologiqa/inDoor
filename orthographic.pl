:-module(orthographic,[project2D/3
                      ,transProject2D/4]).
%
% perspective projection of 3D geometrical structures to a 2D plane
% https://www.baeldung.com/cs/3d-point-2d-plane
%
%
%:-pack_install('https://github.com/friguzzi/matrix.git').
% https://friguzzi.github.io/matrix/
:-use_module(pack(matrix/prolog/matrix)).

isUnit([X,Y]):-
    !, U is X*X+Y*Y, U >= 1, U=<nexttoward(1.0,2.0).
isUnit([X,Y,Z]):-
    !, U is X*X+Y*Y+Z*Z, U >= 1, Z=<nexttoward(1.0,2.0).

isUnit(V1,V2):-
    isUnit(V1), isUnit(V2).

isUnit(V1,V2,V3):-
    isUnit(V1), isUnit(V2), isUnit(V3).

isOrthogonal(V1,V2):-
  dot_product(V1,V2,PROD), PROD=:=0.

isOrthogonal(V1,V2,V3):-
  isOrthogonal(V1,V2), isOrthogonal(V1,V3), isOrthogonal(V2,V3).

isOrthonormal(E1,E2):-
    isUnit(E1,E2),
    isOrthogonal(E1,E2).

isOrthonormal(E1,E2,E3):-
    isUnit(E1,E2,E3),
    isOrthogonal(E1,E2,E3).

project2D(v(A,B,C,D),X0/Y0/Z0,X/Y/Z):- !,
  K is (D-A*X0-B*Y0-C*Z0)/(A*A+B*B+C*C),
  X is X0 - K*A, Y is Y0 - K * B, Z is Z0+K*C.
project2D(v(A,B,C),XYZ0,XYZ):- !,
  project2D(v(A,B,C,0),XYZ0,XYZ).
project2D(NORMAL,POLYLINE,POLY):- is_list(POLYLINE),
    maplist(project2D(NORMAL),POLYLINE,POLY).

project2D(v(A,B,C),RX/RY/RZ,X0/Y0/Z0,X1/Y1/Z1):-
  D is A*RX+B*RY+C*RZ,
  project2D(v3(A,B,C,D),X0/Y0/Z0,X1/Y1/Z1).


transform32D(v(E1,E2),X0/Y0/Z0,RESULT):-
  assertion(isOrthonormal(E1,E2)),
  matrix_multiply([E1,E2],[[X0],[Y0],[Z0]],RESULT).

transProject2D(NORMAL,ORTHO,PO/IN/T3D,X/Y):- !,
  project2D(NORMAL,PO/IN/T3D,P3D),
  transform32D(ORTHO,P3D,[[X],[Y]]).
transProject2D(NORMAL,ORTHO,POLYLINE,POLY):-
  is_list(POLYLINE),
  maplist(transProject2D(NORMAL,ORTHO),POLYLINE,POLY).

:- begin_tests(orthographic).
test(orthonormal,true):-
    isOrthonormal([0,sqrt(2)/2,sqrt(2)/2],[0,-sqrt(2)/2,sqrt(2)/2]).

test(project,true(X = 0.7071067811865477/0.03721614637823939)):-
     transProject2D(v(1,3,3),
              v([0,sqrt(2)/2,sqrt(2)/2],[0,-sqrt(2)/2,sqrt(2)/2]),
                   0/0/1,X).



test(project,true(Projected=
                 [0.7071067811865477/ -1.6499158227686108,
                  0.0/ -1.8856180831641267,
                  0.7071067811865475/ -3.064129385141706,
                  1.4142135623730954/ -2.8284271247461903])):-
    Polyline = [4/0/1,4/0/0,4/1/0,4/1/1],
    ProjectionPlane = v(5,5,5,0),
    Orthonormal = v([0,sqrt(2)/2,sqrt(2)/2],[0,-sqrt(2)/2,sqrt(2)/2]),
    transProject2D(ProjectionPlane, Orthonormal, Polyline, Projected).


:-end_tests(orthographic).






