:- module(xpceFace,[createPicture/3
                   ,flush/1,line/4,poly/3]).

/*
   Interface for SWI-Prolog and XPCE
   Author: Kili�n Imre, 18-Jan-2021.

   Picture: a drawing port - an XPCE window
*/

:- op(1,fx,@).


embedPoint(X/Y,point(X,Y)).
%poly(PICTURE,POINTS,CTA).
%draw polyline (that could also be polygon)
%+PICTURE:  picture ID
%+POINTS:   list of nodes (at least 2 nodes!)
%+CTA:      color-thickness-arrow structure
poly(PICTURE,POINTS,COL-THICK-_):-
  POINTS = [_|_],
  maplist(embedPoint,POINTS,EMPOINTS),
  chain_list(CHAINPOINTS,EMPOINTS),
  new(POLYGON,path(poly,0,CHAINPOINTS)),
  send(POLYGON,colour(COL)),
  send(POLYGON,pen(THICK)),
  send(@PICTURE,display,POLYGON).

%draw line primitive operation on xpce
line(PICTURE,X0/Y0,X/Y,COLOR-THICK-ARROW):-
  send(@PICTURE,display,new(LINE,line(X0,Y0,X,Y,ARROW))),
  send(LINE,pen(THICK)), send(LINE,colour(COLOR)).

flush(PICTURE):-
  send(@PICTURE,flush).


% PICTURE:   picture/window ID
% TOP/LEFT-HEIGHT*WIDTH abstract coordinates of window
% T/L-H*W:   Top Left coordinates, Heigth*Width of window (device
%            coordinates)
% SCALE-DIFF: SCALEH/SCALEW-DIFFH/DIFFV horizontal/vertical
%            scale/difference abstract coordinates! (no XY coordinate
%            change in this case!)
createPicture(PICTURE,TITLE,TOP/LEFT-HEIGHT*WIDTH):-
    debug(xpceFace,"createPicture(~w,~w)",[PICTURE,TOP/LEFT-HEIGHT*WIDTH]),
    (object(@PICTURE)-> true;
     ignore(new(@PICTURE,picture(TITLE)))),
    send(@PICTURE,set(height:=HEIGHT,width:=WIDTH)),
    send(@PICTURE,set(x:= 0, y:= 0)),
    send(@PICTURE,set(x:= TOP+HEIGHT/2, y:= LEFT+WIDTH/2)),
    %send(@PICTURE,center,point(TOP+HEIGHT/2,LEFT+WIDTH/2)),
    send(@PICTURE,open).


/*
poly(PIC):-
   new(@PIC,picture('Polygon Example')),
   send(@PIC,size,size(200,200)),
   POINTS=[50/50, 100/50, 100/100, 75/125, 50/100],
   send(@PIC,open),
   xpcePrimitives:poly(PIC,POINTS,red-4-none),
   xpcePrimitives:flush(PIC).
*/




















