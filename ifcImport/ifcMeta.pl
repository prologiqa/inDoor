:- module(ifcMeta,  [primitive/1, type/3, enum/2, class/7,
                     op(500, fx,#), op(500, xfx,#),
                     op(990,xfx,(<=>))
                    ]).

:- use_module(ifc2inDoor).


:- discontiguous type/3.
:- discontiguous enum/2.
:- discontiguous class/7.

primitive(void).
primitive(number).
primitive(integer).
primitive(float).
primitive(boolean).
primitive(string).
primitive(string(_)).
primitive(atom).
primitive(expr).
primitive(expr(_)).

type(ifcBinary,[ifcBlobTexture,ifcPixelTexture],true).
type(ifcLogical,boolean,X^(once((X==true;X==false)))).
type(ifcBoolean,boolean,X^(once((X==true;X==false)))).
type(ifcInteger,integer,X^integer(X)).
type(ifcReal,float,X^float(X)).
type(ifcLabel,string(255),STR^(atom(STR),string_length(STR,L),L=<255)).
type(ifcIdentifier,string(255),STR^(atom(STR),string_length(STR,L),L=<255)).
type(ifcText,string,STR^atom(STR)).
type(ifcValue,expr,true).
type(ifcGloballyuniqueid,string(22),STR^(atom(STR),string_length(STR,L),L=<30)).
type(ifcTimestamp,integer,X^integer(X)).
type(ifcLengthMeasure,float,X^float(X)).
type(ifcPositiveLengthMeasure,float,X^(float(X),X>0)).
type(ifcNonNegativeLengthMeasure,float,X^(float(X),X>=0)).
type(ifcDate,string,STR^atom(STR)).
type(ifcCompoundplaneanglemeasure,expr(deg(_,_,_)),true).
type(ifcDimensioncount,integer,X^integer(X)).
type(ifcURIReference,string,STR^atom(STR)).



enum(ifcStateEnum,[readwrite,readonly,locked,
                   readwritelocked,readonlylocked]).
enum(ifcTypeActionEnum,[nochange,modified,added,deleted,notdefined]).

enum(ifcRoleEnum,[supplier,manufacturer,contractor,subcontractor,
                 architect,structuralEngineer,costEngineer,client,
                 buildingOwner,buildingOperator,mechanicalEngineer,
                 electricalEngineer,projectManager,facitiliesManager,
                 civilEngineer,commissioningEngineer,engineer,owner,
                 consultant,constructionManager,fieldConstructionManager,
                 reseller,userDefined]).

enum(ifcAddressTypeEnum,[office,site,home,distributionPoint,userDefined]).

enum(ifcChangeActionEnum,[noChange,modified,added,deleted,notDefined]).


class(ifcObjectdefinition/4,abstract,ifcRoot,[],[],[],true).

class(ifcActorrole/3,class,'',[role,userDefinedRole,description],
                               [ifcRoleEnum,ifcLabel,ifcText],[],true).

class(ifcAddress/3,class,'',
       [purpose,description,userDefinedPurpose],
       [ifcAddressTypeEnum,ifcText,ifcLabel],[],true).

class(ifcPostaladdress/10,class,ifcAddress,
       [internalLocation,addressLines,postalBox,town,region,postalCode,country],
       [ifcLabel,[ifcLabel],ifcLabel,ifcLabel,ifcLabel,ifcLabel,ifcLabel],
       [],true).

class(ifcPerson/8,class,'',
       [identification,familyName,givenName,middleNames,
        prefixTitles,suffixTitles,roles,addresses],
       [ifcIdentifier,ifcLabel,ifcLabel,[ifcLabel],
       [ifcLabel],[ifcLabel],[ifcActorrole],[ifcAddress]],[],true).

class(ifcOrganization/5,class,'',
       [identification,name,description,roles,addresses],
       [ifcIdentifier,ifcLabel,ifcText,[ifcActorrole],[ifcAddress]],[],true).

class(ifcPersonandorganization/3,class,'',
       [thePerson,theOrganization,roles],
       [ifcPerson,ifcOrganization,[ifcActorrole]],[],true).

class(ifcApplication/4,class,'',
       [applicationDeveloper,version,applicationFullName,applicationIdentifier],
       [ifcOrganization,ifcLabel,ifcLabel,ifcIdentifier],[],true).

class(ifcOwnerhistory/8,class,'',
       [owningUser,owningApplication,state,changeAction,
       lastModifiedDate,lastModifyingUser,lastModifyingApplication,
       creationDate],
       [ifcPersonandorganization,ifcApplication,ifcStateEnum,
       ifcChangeActionEnum,ifcTimestamp,ifcPersonandorganization,
       ifcApplication,ifcTimestamp],[],true).

class(ifcRoot/4,root,'',
       [globalID,ownerHistory,name,description],
       [ifcGloballyuniqueid,ifcOwnerhistory,ifcLabel,ifcText],[],true).

class(ifcRelationship/4,class,ifcRoot,
       [],[],[],true).

class(ifcRelconnects/4,class,ifcRelationship,
       [],[],[],true).

class(ifcRelcontainedinspatialstructure/6,class,ifcRelconnects,
       [relatedElements,relatingStructure],
       [ifcRelcontainedinspatialstructure,ifcRelcontainedinspatialstructure],
       [],true).

class(ifcPresentationitem/0,abstract,'',
       [],[],[],true).

class(ifcRatiomeasure/0,abstract,'',
       [],[],[],true).

class(ifcNormalizedratiomeasure/0,abstract,ifcRatiomeasure,
       [],[],[],true).

class(ifcColourspecification/1,class,ifcPresentationitem,
       [name],[ifcLabel],[],true).

class(ifcPredefinedcolour/1,class,ifcPredefineditem,
      [],[],[],true).

class([ifcColourspecification/1,ifcPredefinedcolour/1],select,ifcColour,
      [],[],[],true).

class(ifcColourrgb/4,class,ifcColourspecification,
       [red,green,blue],[ifcNormalizedratiomeasure,
                         ifcNormalizedratiomeasure,ifcNormalizedratiomeasure],
       [],true).

% A representation item is an element of product data that participates
% in one or more representations or contributes to the definition of
% another representation item. A representation item contributes to the
% definition of another representation item when it is referenced by that
% representation item.
class(ifcRepresentationitem/0,abstract,'',
       [], [], [],true).

% An geometric representation item is a representation item that has
% the additional meaning of having geometric position or orientation or
% both. This meaning is present by virtue of:
% - being a Cartesian point or a direction
% - referencing directly a Cartesian point or direction
% - referencing indirectly a Cartesian point or direction
class(ifcGeometricrepresentationitem/0,class,ifcRepresentationitem,
       [], [],
       [],true).

% A point is a location in some real Cartesian coordinate space Rm,
% for m = 1, 2 or 3.
class(ifcPoint/0,class,ifcGeometricrepresentationitem,
       [], [], [],true).

% A point defined by its coordinates in a two or three dimensional
% rectangular Cartesian coordinate system, or in a two dimensional
% parameter space. The class is defined in a two or three dimensional
% space.
class(ifcCartesianpoint/1,class,ifcPoint,
       [coordinates],[[ifcLengthMeasure]],
       [],true).

class([ifcNamedunit/2,ifcDerivedunit/3,ifcMonetaryunit/1],select,ifcUnit,
      [],[],[],true).

class(ifcUnitassignment/1,class,'',
       [units],[[ifcUnit]],[],true).

class(ifcRepresentationcontext/2,class,'',
       [contextIdentifier,contextType],[ifcLabel,ifcLabel],[],true).

class(ifcContext/9,abstract,ifcObjectdefinition,
                         [objectType,longName,phase,
                          representationContexts,unitsInContext],
                         [ifcLabel,ifcLabel,ifcLabel,
                         [ifcRepresentationcontext],ifcUnitassignment],[],true).

% The root of all objects inside an IFC model. Under the IfcProject,
% there is usually at least one IfcSite, that can contain one or more
% IfcBuildings, which in turn are divided in IfcBuildingStoreys. These
% can be further divided into IfcZones and IfcSpaces.
class(ifcProject/9,class,ifcContext,[],[],[],true).

class(ifcPresentationlayerassignment/4,class,'',
       [name,description,assignedItems,identifier],
       [ifcLabel,ifcText,[ifcLayereditem],ifcIdentifier],[],true).

class(ifcLayereditem/1,class,'',[ifcPresentationlayerassignment],
       [class],[],true).

class([ifcPresentationstyle/1,ifcPresentationstyleassignment/1],
       select,ifcStyleassignmentselect,
       [],[],[],true).

class(ifcStyleditem/3,class,'',
       [item,styles,name],
       [ifcRepresentationitem,[ifcStyleassignmentselect],ifcLabel],
      [],true).

class(ifcStylemodel/4,class,ifcRepresentation,
       [],[],[],true).

class(ifcStyledrepresentation/4,class,ifcStylemodel,
       [],[],[],true).


class(ifcMeasurewithunit/2,class,'',
       [valueComponent,unitComponent],
       [ifcValue,ifcUnit],
      [],true).

%------------------------------------------------------

class(ifcRepresentation/4,class,'',
       [contextOfItems,representationIdentifier,representationType,items],
       [ifcRepresentationcontext,ifcLabel,ifcLabel,[ifcRepresentationitem]],
      [],true).

% The IfcProductRepresentation defines a representation of a product,
% including its (geometric or topological) representation. A product can
% have zero, one or many geometric representations, and a single
% geometric representation can be shared among various products using
% mapped representations.
class(ifcProductrepresentation/3,class,'',
       [name,description,representation],
       [ifcLabel,ifcText,[ifcRepresentation]], [],true).

class(ifcObjectplacement/0,class,'',
       [], [], [],true).

class(ifcObject/5,class,ifcRoot,
       [objectType],[ifcLabel],
      [],true).

% The IfcProduct is an abstract representation of any object that
% relates to a geometric or spatial context. An IfcProduct occurs at a
% specific location in space if it has a geometric representation
% assigned. It can be placed relatively to other products, but
% ultimately relative to the project coordinate system.
class(ifcProduct/7,class,ifcObject,
       [objectPlacement,representation],
       [ifcObjectplacement,ifcProductrepresentation],
      [],true).

class(ifcSpatialelement/8,class,ifcProduct,
       [longName], [ifcLabel],
      [],true).

class(ifcSpatialstructureelement/9,class,ifcSpatialelement,
       [compositionType], [ifcElementCompositionEnum],
      [],true).

class(ifcSite/14,class,ifcSpatialstructureelement,
       [refLatitude,refLongitude,refElevation,landTitleNumber,siteAddress],
       [ifcCompoundplaneanglemeasure,ifcCompoundplaneanglemeasure,
       ifcLengthMeasure,ifcLabel,ifcPostaladdress],
       [],true).

% A product definition shape identifies a productís shape as the
% conceptual idea of the form of a product.
class(ifcProductdefinitionshape/3,class,ifcProductrepresentation,
       [],[],[],true).


%------------------------------------------------------------
class(ifcDirection/1,class,ifcGeometricrepresentationitem,
       [directionRatios],[[ifcReal]],[],true).

class(ifcPlacement/1,class,ifcGeometricrepresentationitem,
       [location],[ifcCartesianpoint],[],true).

class(ifcAxis2placement3d/3,class,ifcPlacement,
       [axis,refDirection], [ifcDirection,ifcDirection], [],true).

class(ifcAxis2placement2d/2,class,ifcPlacement,
       [refDirection], [ifcDirection], [],true).

class([ifcAxis2placement2d/2,ifcAxis2placement3d/3],select,ifcAxis2placement,
       [],[],[],true).

class(ifcGeometricrepresentationcontext/6,class,ifcRepresentationcontext,
       [coordinateSpaceDimension,precision,worldCoordinateSystem,trueNorth],
       [ifcDimensioncount,ifcReal,ifcAxis2placement,ifcDirection],
      [],true).

%------------------------------------------------------------
class(ifcPresentationstyle/1,class,'',
       [name],[ifcLabel],[],true).

enum(ifcSurfaceSide,[positive,negative,both]).

class([ifcSurfacestyleshading/2
       %????,ifcExternallydefinedSurfacestyle/3
       %????,ifcSurfacestylelighting/4
       %????,ifcSurfacestylerefraction/2
       %????,ifcSurfacestylewithtextures/2
       ],select,ifcSurfacestyleelementselect,
       [],[],[],true).

type(ifcNormalisedRatioMeasure,ifcRatiomeasure,true).

class(ifcSurfacestyleshading/2,class,ifcPresentationitem,
       [surfaceColour,transparency],
       [ifcColourrgb,ifcNormalisedRatioMeasure],
       [],true).

class(ifcSurfacestyle/3,class,ifcPresentationstyle,
       [side,styles],
       [ifcSurfaceSide,[ifcSurfacestyleelementselect]],[],true).

%------------------------------------------------------------

%The predefined type container that collects all possible road
%facility types together into the implemented enumeration.
enum(ifcRoadTypeEnum,[userdefined,notdefined]).

%This enumeration indicates the composition of a spatial structure element
%or proxy.
enum(ifcElementCompositionEnum,[complex,element,partial]).

% A Facility (derived from IfcSpatialStructureElement) may be an
% IfcBuilding, an IfcBridge, an IfcRailway, an IfcRoad, an
% IfcMarineFacility (or any other type of built facility defined in the
% future, such as IfcTunnel).
class(ifcFacility/10,class,ifcSpatialstructureelement,
       [compositionType],[ifcElementCompositionEnum],[],true).


%A route built on land to allow travel from one location to another,
%including highways, streets, cycle and foot paths,
%but excluding railways.
%As a type of Facility, Road provides the basic element in the
%project structure hierarchy for the components of a road project (i.e.
%any undertaking such as design, construction or maintenance).
class(ifcRoad/11,class,ifcFacility,
       [predefinedType],[ifcRoadTypeEnum],[],true).

%------------------------------------------------------------

% Represents the concept of a particular geometric and/or
% topological representation of a product's shape or a product
% component's shape within a representation context. This
% representation context has to be a geometric representation context
% (with the exception of topology representations without associated
% geometry)
class(ifcShapemodel/4,class,ifcRepresentation,
       [],[],[],true).


class(ifcShaperepresentation/4,class,ifcShapemodel,
       [],[],[],true).

%-----------------------------------------------------------

class(ifcTopologicalrepresentationitem/0,abstract,ifcRepresentationitem,
       [],[],[],true).

class(ifcFacebound/2,class,ifcTopologicalrepresentationitem,
       [bound,orientation],[ifcLoop,ifcBoolean],[],true).

% A face is a topological entity of dimensionality 2 corresponding to
% the intuitive notion of a piece of surface bounded by loops.
class(ifcFace/1,class,ifcTopologicalrepresentationitem,
       [bounds],[[ifcFacebound]],[],true).

class(ifcLoop/0,class,ifcTopologicalrepresentationitem,
       [],[],[],true).

% The IfcPolyLoop is always closed and the last segment is from the last
% IfcCartesianPoint in the list of Polygon's to the first
% IfcCartesianPoint. Therefore the first point shall not be repeated at
% the end of the list, neither by referencing the same instance, nor by
% using an additional instance of IfcCartesianPoint having the
% coordinates as the first point.
class(ifcPolyloop/1,class,ifcLoop,
       [polygon],[[ifcCartesianpoint]],[],true).

% The IfcPolyline is a bounded curve with only linear segments defined by
% a list of Cartesian points. If the first and the last Cartesian point
% in the list are identical, then the polyline is a closed curve,
% otherwise it is an open curve.
class(ifcPolyline/1,class,ifcBoundedcurve,
       [points],[[ifcCartesianpoint]],[],true).

class(ifcNamedunit/2,class,'',
       [dimensions,unitType],[ifcDimensionalexponents,ifcUnitEnum],[],true).

enum(ifcSIPrefix,[exa,peta,tera,giga,mega,kilo,hecto,deca,
                 deci,centi,milli,micro,nano,pico,femto,atto]).

class(ifcDimensionalexponents/7,class,'',
       [lengthExponent,massExponent,timeExponent,electricCurrentExponent,
       thermodynamicTemperatureExponent,amountOfSubstanceExponent,
       luminousIntensityExponent],
       [ifcInteger,ifcInteger,ifcInteger,ifcInteger,
        ifcInteger,ifcInteger,ifcInteger],[],true).

enum(ifcUnitEnum,[absorbeddoseunit,amountofsubstanceunit,areaunit,
                 doseequivalentunit,electricalcapacitanceunit,
                 electricalchargeunit,electricalconductanceunit,
                 electricalcurrentunit,electricalresistanceunit,
                 electricalvoltageunit,energyunit,forceunit,
                 frequencyunit,illuminanceunit,inductanceunit,
                 lengthunit,luminousfluxunit,luminousintensityunit,
                 magneticfluxdensityunit,magneticfluxunit,
                 massunit,planeangleunit,powerunit,pressureunit,
                 radioactivityunit,solidangleunit,
                 thermodynamictemperatureunit,timeunit,volumeunit,
                 userdefined]).

enum(ifcSIUnitName,[ampere,becquerel,candela,coulomb,cubic_metre,
                   degree_celsius,farad,gram,gray,henry,hertz,joule,
                   kelvin,lumen,lux,metre,mole,newton,ohm,pascal,radian,
                   second,siemens,sievert,square_metre,steradian,tesla,
                   volt,watt,weber]).

class(ifcSiunit/4,class,ifcNamedunit,
       [prefix,name],[ifcSIPrefix,ifcSIUnitName],[],true).


%------------------------------------------------------------

enum(ifcRoadPartTypeEnum,[roadsidepart,bus_stop,hardshoulder,
                         intersection,passingbay,roadwayplateau,
                         roadside,refugeisland,tollplaza,centralreserve,
                         sidewalk,parkingbay,railwaycrossing,
                         pedestrian_crossing,softshoulder,bicyclecrossing,
                         centralisland,shoulder,trafficlane,roadsegment,
                         roundabout,layby,carriageway,trafficisland,
                         userdefined,notdefined]).

%class(ifcFacilityparttypeselect/0,class,ifcRoadPartTypeEnum,
%       [],[],[],true).

enum(ifcFacilityUsageEnum,[lateral,longitudinal,region,vertical,
                           userdefined,notdefined]).

class(ifcFacilitypart/11,class,ifcSpatialstructureelement,
       [predefinedType,usageType],
       [%ifcFacilityparttypeselect,
           ifcRoadPartTypeEnum,
        ifcFacilityUsageEnum],[],true).

%------------------------------------------------------------

class(ifcReldecomposes/4,class,ifcRelationship,
       [],[],[],true).

class(ifcRelaggregates/6,class,ifcReldecomposes,
       [relatingObject,relatedObjects],
       [ifcObjectdefinition,[ifcObjectdefinition]],[],true).


%------------------------------------------------------------


class(ifcPresentationstyleassignment/1,class,'',
       [styles],[[ifcSurfacestyle]],[],true).


%------------------------------------------------------------

% Generalization of all components that make up an AEC product. Those
% elements can be logically contained by a spatial structure element that
% constitutes a certain level within a project structure hierarchy (e.g.,
% site, building, storey or space). This is done by using the
% IfcRelContainedInSpatialStructure relationship.
class(ifcElement/8,class,ifcProduct,
       [tag],[ifcIdentifier],[],true).

class(ifcConnectiongeometry/0,class,'',
       [],[],[],true).

class(ifcRelconnectselements/7,class,ifcRelconnects,
       [connectionGeometry,relatingElement,relatedElements],
       [ifcConnectiongeometry,ifcElement,ifcElement],[],true).

%------------------------------------------------------------

% The built element comprises all elements that are primarily part of
% the construction of a built facility, i.e., its structural and space
% separating system. Built elements are all physically existent and
% tangible things.
class(ifcBuiltelement/8,class,ifcElement,
       [],[],
       [],true).

enum(ifcKerbTypeEnum,[userdefined,notdefined]).

% A border of stone, concrete or other rigid material
% formed at the edge of the carriageway or footway.
class(ifcKerb/9,class,ifcBuiltelement,
       [predefinedType],[ifcKerbTypeEnum],[],true).


%------------------------------------------------------------

type(ifcSpecularroughness,float,X^float(X)).
type(ifcSpecularexponent,float,X^float(X)).

class([ifcSpecularexponent/1,ifcSpecukarroughness/1],select,
       ifcSpecularhighlightselect,[],[],[],true).

class(ifcColourorfactor/4,class,ifcColourrgb,
       [],[],[],true).

enum(ifcReflectanceMethodEnum,[blinn,flat,glass,matt,metal,mirror,
                              phong,plastic,strauss,notdefined]).

class(ifcSurfacestylerendering/9,class,ifcSurfacestyleshading,
       [diffuseColour,transmissionColour,diffuseTransmissionColour,
        reflectionColour,specularColour,
        specularHighlight,reflectanceMethond],
       [ifcColourorfactor,ifcColourorfactor,ifcColourorfactor,
       ifcColourorfactor,ifcColourorfactor,
       ifcSpecularhighlightselect,ifcReflectanceMethodEnum],[],true).

%------------------------------------------------------------

type(ifcCountmeasure,number,X^number(X)).

class(ifcConnectedfaceset/1,class,ifcTopologicalrepresentationitem,
       [cfsFaces],[[ifcFace]],[],true).

class(ifcClosedshell/1,class,ifcConnectedfaceset,
       [],[],[],true).

% An open shell is a shell of the dimensionality 2. Its domain, if
% present, is a finite, connected, oriented, 2-manifold with boundary,
% but is not a closed surface. It can be thought of as a closed shell
% with one or more holes punched in it.
class(ifcOpenshell/1,class,ifcConnectedfaceset,
       [],[],[],true).

class([ifcClosedshell/1,ifcOpenshell/1],select,ifcShell,[],[],[],true).

class(ifcShellbasedsurfacemodel/1,class,ifcGeometricrepresentationitem,
       [sbsmBoundary],[ifcShell],[],true).


class(ifcPhysicalquantity/2,class,'',
       [name,description],[ifcLabel,ifcText],[],true).

class(ifcPhysicalsimplequantity/3,class,ifcPhysicalquantity,
       [unit],[ifcNamedunit],[],true).

class(ifcQuantitycount/5,class,ifcPhysicalsimplequantity,
       [countValue,formula],[ifcCountmeasure,ifcLabel],[],true).

type(ifcAreameasure,float,X^float(X)).

class(ifcQuantityarea/5,class,ifcPhysicalsimplequantity,
       [areaValue,formula],[ifcAreameasure,ifcLabel],[],true).

type(ifcVolumemeasure,float,X^float(X)).

class(ifcQuantityvolume/5,class,ifcPhysicalsimplequantity,
       [volumeValue,formula],[ifcVolumemeasure,ifcLabel],[],true).

%------------------------------------------------------------

class([ifcObjectdefinition/4,ifcPropertydefinition/4],select,
      ifcDefinitionselect,[],[],[],true).

class(ifcExternalinformation/0,abstract,'',[],[],[],true).

class(ifcRelassociates/5,class,ifcRelationship,
       [relatedObjects],[[ifcDefinitionselect]],[],true).

class(ifcClassification/7,class,ifcExternalinformation,
       [source,edition,editionDate,name,description,
       location,referenceTokens],
       [ifcLabel,ifcLabel,ifcDate,ifcLabel,ifcText,ifcURIReference,
       [ifcIdentifier]],[],true).

class([ifcClassification/7,ifcClassificationreference/6],
       select,ifcClassificationreferenceselect,[],[],[],true).

class([ifcClassification/7,ifcClassificationreference/6],
       select,ifcClassificationselect,[],[],[],true).

class(ifcClassificationreference/6,class,ifcExternalreference,
       [referencedSource,description,sort],
       [ifcClassificationreferenceselect,ifcText,ifcIdentifier],[],true).

class(ifcRelassociatesclassification/6,class,ifcRelassociates,
       [relatingClassification],[ifcClassificationselect],[],true).

class(ifcMaterialdefinition/0,abstract,'',
       [],[],[],true).

class(ifcMaterialproperties/4,class,ifcExtendedproperties,
       [material],[ifcMaterialdefinition],[],true).

class(ifcMaterial/3,class,'',
       [associatedTo,hasExternalReferences,hasProperties],
       [ifcRelassociatesmaterial,ifcExternalreferencerelationship,
       ifcMaterialproperties],[],true).

class(ifcMateriallayer/7,class,ifcMaterialdefinition,
       [material,layerThickness,isVentilated,
        name,description,category,priority],
       [ifcMaterial,ifcNonNegativeLengthMeasure,ifcLogical,
       ifcLabel,ifcText,ifcLabel,ifcInteger],[],true).

class(ifcMaterialdefinitionrepresentation/4,class,ifcProductrepresentation,
       [representedMaterial],[ifcMaterial],[],true).

class([ifcMateriallist/1,ifcMaterialusagedefinition/1,ifcMaterialdefinition/3],
      select,ifcMaterialselect,[],[],[],true).

class(ifcPredefineditem/1,class,ifcPresentationitem,
       [name],[ifcLabel],[],true).

class(ifcPredefinedcurvefont/1,class,ifcPredefineditem,
       [],[],[],true).

class(ifcDraughtingpredefinedcurvefont/1,class,ifcPredefinedcurvefont,
       [],[],[],true).

% An IfcLocalPlacement defines the relative placement of a product in
% relation to the placement of another product or the absolute placement
% of a product within the geometric representation context of the
% project.
class(ifcLocalplacement/2,class,ifcObjectplacement,
       [placementRelTo,relativePlacement],
       [ifcObjectplacement,ifcAxis2placement],[],true).

class(ifcDraughtingpredefinedcolour/1,class,ifcPredefinedcolour,
       [],[],[],true).

%------------------------------------------------------------

% This enumeration defines the different types of walls that can
% further specify an IfcWall or IfcWallType.
enum(ifcWallTypeEnum,[movable,parapet,partitioning,plumbingwall,
                      shear,solidwall,standard,polygonial,
                     elementedwall]).

class(ifcWalltype/10,class,ifcBuildingelementtype,
       [predefinedType],[ifcWallTypeEnum],[],true).

% A building represents a structure that provides shelter for its
% occupants or contents and stands in one place. The building is also
% used to provide a basic element within the spatial structure hierarchy
% for the components of a building project (together with site, storey,
% and space).
% A building may span over several connected or disconnected buildings.
% Therefore building complex provides for a collection of buildings
% included in a site. A building can also be decomposed in parts, where
% each part defines a building section. This is defined by the
% composition type attribute of the supertype IfcSpatialStructureElement
% which is interpreted as follow:
% COMPLEX: building complex
% ELEMENT: building
% PARTIAL: building section
% +globalID,+history,+name,+description,+objType,+objPlacement
% +repr,+longName,+compKind, +elevRefHeight,+elevTerrain,+address
class(ifcBuilding/12,class,ifcSpatialstructureelement,
       [elevationOfRefHeight, %the elevation above sea of 0.0 level
        elevationOfTerrain, %
        buildingAddress],
       [ifcLengthMeasure,ifcLengthMeasure,ifcPostaladdress],[],true).


% The building element comprises all elements that are primarily part of
% the construction of a building, i.e., its structural and space
% separating system.
class(ifcBuildingelement/8,class,ifcElement,
       [],[],[],true).


% Vertical construction usually in masonry or in concrete which bounds
% or subdivides a construction works and fulfills a load bearing or
% retaining function.
%
class(ifcWall/9,class,ifcBuildingelement,
       [predefinedType],[ifcWallTypeEnum],[],true).

% meta(ac20,ifcWall(#31190,GID,HIST,NAME,DESCR,TYPE,PLACE,REPR,TAG,PRETYPE).
% The standard wall (IfcWallStandardCase) defines a wall with certain
% constraints for the provision of parameters and with certain
% constraints for the geometric representation. The IfcWallStandardCase
% handles all cases of walls, that are extruded vertically, and have
% parallel sides (thickness is fixed) +globalID, +ownerHistory, +name,
% +description, +objectType, +objectPlacement, +representation, +tag,
% +predefinedType
class(ifcWallstandardcase/9,class,ifcWall,
       [],[],[],true).

%------------------------------------------------------------

class(ifcPropertydefinition/4,class,ifcRoot,
       [],[],[],true).

class(ifcPropertysetdefinition/4,class,ifcPropertydefinition,
       [],[],[],true).

class(ifcPropertyabstraction/0,class,'',
       [],[],[],true).

class(ifcProperty/2,class,ifcPropertyabstraction,
       [name,description],[ifcIdentifier,ifcLabel],[],true).

class(ifcPropertyset/5,class,ifcPropertysetdefinition,
       [hasProperties],[[ifcProperty]],[],true).

class(ifcExtendedproperties/3,class,ifcPropertyabstraction,
       [name,description,properties],
       [ifcIdentifier,ifcText,[ifcProperty]],[],true).

%------------------------------------------------------------

class(ifcTypeobject/6,class,ifcRoot,
       [applicableOccurrence,hasPropertysets],
       [ifcIdentifier,[ifcPropertysetdefinition]],[],true).

class(ifcRepresentationmap/2,class,'',
       [mappingOrigin,mappedRepresentation],
       [ifcAxis2placement,ifcRepresentation],[],true).

class(ifcMappeditem/2,class,ifcRepresentationitem,
       [mappingSource,mappingTarget],
       [ifcRepresentationmap,ifcCartesiantransformationoperator],[],true).

class(ifcTypeproduct/8,class,ifcTypeobject,
       [representationMaps,tag],
       [[ifcRepresentationmap],ifcLabel],[],true).

class(ifcElementtype/9,class,ifcTypeproduct,
       [elementType],[ifcLabel],[],true).

class(ifcBuildingelementtype/9,class,ifcElementtype,
       [],[],[],true).

enum(ifcWindowTypePartitioningEnum,
     [single_panel,double_panel_vertical,
     double_panel_horizontal,triple_panel_bottom,
     triple_panel_top,triple_panel_left,triple_panel_right,
     triple_panel_horizontal,userdefined,notdefined]).

enum(ifcWindowTypeEnum,[window,skylight,lightdome,userdefined,notdefined]).

class(ifcWindowtype/13,class,ifcBuildingelementtype,
       [predefinedType,partitioningType,
        parameterTakesPrecedence,userDefinedPartitioningType],
       [ifcWindowTypeEnum,ifcWindowTypePartitioningEnum,
        ifcBoolean,ifcLabel],[],true).


%------------------------------------------------------------


class(ifcSurface/0,class,ifcGeometricrepresentationitem,
       [],[],[],true).

class(ifcBoundedsurface/0,abstract,ifcSurface,
       [],[],[],true).

class(ifcElementarysurface/1,class,ifcSurface,
       [position],[ifcAxis2placement3d],[],true).

class(ifcPlane/1,class,ifcElementarysurface,
       [],[],[],true).


class(ifcCurve/0,class,ifcGeometricrepresentationitem,
       [],[],[],true).

class([ifcCurve/0,ifcPoint/0,ifcSurface/0],
       select,ifcGeometricsetselect,[],[],[],true).

class(ifcGeometricset/1,class,ifcGeometricrepresentationitem,
       [elements],[ifcGeometricsetselect],[],true).

class(ifcGeometriccurveset/1,class,ifcGeometricset,
       [],[],[],true).

class(ifcCurveboundedplane/3,class,ifcBoundedsurface,
       [basisSurface,outerBoundary,innerBoundaries],
       [ifcPlane,ifcCurve,[ifcCurve]],[],true).

%------------------------------------------------------------


class(ifcQuantitylength/5,class,ifcPhysicalsimplequantity,
       [lengthValue,formula],[ifcLengthMeasure,ifcLabel],[],true).

% This enumeration defines the basic ways to describe how doors operate,
% as shown in Figure 194. It combines the partitioning of the door into a
% single or multiple door panels and the operation types of that panels.
enum(ifcDoorTypeOperationEnum,
     [single_swing_left,single_swing_right,
      double_door_single_swing,
      double_door_single_swing_opposite_left,
      double_door_single_swing_opposite_right,
      double_swing_left,double_swing_right,
      double_door_double_swing,
      sliding_to_left,sliding_to_right,double_door_sliding,
      folding_to_left,folding_to_right,double_door_folding,
      revolving,rollingup,swing_fixed_left,swing_fixed_right,
      userdefined,notdefined]).

% This enumeration defines the different predefined types of an IfcDoor
% or IfcDoorType object.
enum(ifcDoorTypeEnum,[door,gate,trapdoor,userdefined,notdefined]).

% The door is a building element that is predominately used to provide
% controlled access for people and goods. It includes constructions with
% hinged, pivoted, sliding, and additionally revolving and folding
% operations. A door consists of a lining and one or several panels.
class(ifcDoor/13,class,ifcBuildingelement,
       [overallHeight,overallWidth,
        predefinedType,operationType,userDefinedOperationType],
       [ifcPositiveLengthMeasure,ifcPositiveLengthMeasure,
       ifcDoorTypeEnum,ifcDoorTypeOperationEnum,ifcLabel],[],true).

class(ifcDoortype/13,class,ifcBuildingelementtype,
       [predefinedType,operationType,parameterTakesPrecedence,
        userDefinedOperationType],
       [ifcDoorTypeEnum,ifcDoorTypeOperationEnum,ifcBoolean,ifcLabel],[],true).

%------------------------------------------------------------

enum(ifcBooleanOperator,[union,intersection,difference]).

class(ifcBooleanoperand/3,class,ifcBooleanresult,
       [],[],[],true).

class(ifcBooleanresult/3,class,ifcGeometricrepresentationitem,
       [operator,firstOperand,secondOperand],
       [ifcBooleanOperator,ifcBooleanoperand,ifcBooleanoperand],[],true).

class(ifcBooleanclippingresult/3,class,ifcBooleanresult,
       [],[],[],true).

class(ifcSolidmodel/0,class,ifcGeometricrepresentationitem,
       [],[],[],true).

enum(ifcProfileTypeEnum,[curve,area]).

class(ifcProfiledef/2,class,'',
       [profileType,profileName],[ifcProfileTypeEnum,ifcLabel],[],true).

% It is defined by sweeping a bounded planar surface. The direction of
% the extrusion is given by the ExtrudedDirection attribute and the
% length of the extrusion is given by the Depth attribute. If the planar
% area has inner boundaries, i.e. holes defined, then those holes shall
% be swept into holes of the solid.
class(ifcSweptareasolid/2,class,ifcSolidmodel,
       [sweptArea,position],[ifcProfiledef,ifcAxis2placement3d],
       [],true).

class(ifcExtrudedareasolid/4,class,ifcSweptareasolid,
       [extrudedDirection,depth],[ifcDirection,ifcPositiveLengthMeasure],
       [],true).

%------------------------------------------------------------

enum(ifcTransitionCode,[discontinuous,continuous,
                        contsamegradient,contsamegradientsamecurvature]).

class(ifcCompositecurvesegment/3,class,ifcGeometricrepresentationitem,
       [transition,sameSense,parentCurve],
       [ifcTransitionCode,ifcBoolean,ifcCurve],[],true).

class(ifcCompositecurve/2,class,ifcBoundedcurve,
       [segments,selfIntersect],
       [[ifcCompositecurvesegment],ifcLogical],[],true).

class(ifcBoundedcurve/0,class,ifcCurve,
       [],[],[],true).

enum(ifcRailingTypeEnum,[handrail,guardrail,balustrade,
                         userdefined,notdefined]).

class(ifcRailing/9,class,ifcBuildingelement,
       [predefinedType],[ifcRailingTypeEnum],[],true).

class(ifcRailingtype/10,class,ifcBuildingelementtype,
       [predefinedType],[ifcRailingTypeEnum],[],true).

class(ifcQuantityset/4,class,ifcPropertysetdefinition,
       [],[],[],true).

class(ifcElementquantity/6,class,ifcQuantityset,
       [methodOfMeasurement,quantities],[ifcLabel,ifcPhysicalquantity],[],true).

% It defines an arbitrary two-dimensional profile for the use within the
% swept surface geometry, the swept area solid or a sectioned spine. It
% is given by an outer boundary from which the surface or solid can be
% constructed.
class(ifcArbitraryclosedprofiledef/3,class,ifcProfiledef,
       [outerCurve],[ifcCurve],[],true).

class(ifcCircleprofiledef/4,class,ifcParametrizedprofiledef,
       [radius],[ifcPositiveLengthMeasure],[],true).

class(ifcParametrizedprofiledef/3,class,ifcProfiledef,
       [position],[ifcAxis2placement2d],[],true).


%------------------------------------------------------------

class(ifcMateriallayerset/3,class,ifcMaterialdefinition,
       [materialLayers,layerSetName,description],
       [[ifcMateriallayer],ifcLabel,ifcText],[],true).

enum(ifcSlabTypeEnum,[floor,roof,landing,baseslab,userdefined,notdefined]).

class(ifcSlabtype/10,class,ifcBuildingelementtype,
       [predefinedType],[ifcSlabTypeEnum],[],true).

% A slab is a component of the construction that may enclose a space
% vertically. The slab may provide the lower support (floor) or upper
% construction (roof slab) in any space in a building.
class(ifcSlab/9,class,ifcBuildingelement,
       [predefinedType],
       [ifcSlabTypeEnum],[],true).

class(ifcHalfspacesolid/2,class,ifcGeometricrepresentationitem,
       [baseSurface,agreementFlag],[ifcSurface,ifcBoolean],
       [],true).

class(ifcPolygonalboundedhalfspace/4,class,ifcHalfspacesolid,
       [position,polygonialBoundary],[ifcAxis2placement3d,ifcBoundedcurve],
       [],true).


class(ifcSimpleproperty/2,class,ifcProperty,
       [],[],[],true).


class(ifcPropertysinglevalue/4,class,ifcSimpleproperty,
       [nominalValue,unit],[ifcValue,ifcUnit],
       [],true).

class(ifcPositiveratiomeasure/0,class,ifcRatiomeasure,
       [],[],[],true).

enum(ifcGeometricProjectionEnum,[graph_view,sketch_view,model_view,
                                plan_view,reflected_plan_view,
                                section_view,elevation_view,
                                userdefined,notdefined]).

class(ifcGeometricrepresentationsubcontext/10,class,
       ifcGeometricrepresentationcontext,
       [parentContext,targetScale,targetView,userDefinedTargetView],
       [ifcGeometricrepresentationcontext,ifcPositiveratiomeasure,
       ifcGeometricProjectionEnum,ifcLabel],[],true).

enum(ifcConnectionTypeEnum,[atpath,atstart,atend,notdefined]).

class(ifcRelconnectspathelements/11,class,ifcRelconnectselements,
       [relatingPriorities,relatedPriorities,
        relatedConnectionType,relatingConnectionType],
       [[ifcInteger],[ifcInteger],
        ifcConnectionTypeEnum,ifcConnectionTypeEnum],[],true).

class(ifcDerivedunitelement/2,class,'',
       [unit,exponent],[ifcNamedunit,ifcInteger],[],true).

class(ifcMateriallist/1,class,'',
       [materials],[ifcMaterial],[],true).

%------------------------------------------------------------

class(ifcPredefinedpropertyset/4,class,ifcPropertysetdefinition,
       [],[],[],true).

enum(ifcDoorPanelOperationEnum,[swinging,double_acting,sliding,folding,
                               revolving,rollingup,fixedpanel,
                                userdefined,notdefined]).

enum(ifcDoorPanelPositionEnum,[left,right,middle,notdefined]).

class([ifcRepresentationmap/2,ifcProductdefinitionshape/3],
       select,ifcProductRepresentationselect,
       [],[],[],true).

class(ifcShapeaspect/5,class,'',
       [shapeRepresentation,name,description,productDefinitional,
       partOfProductDefinitionShape],
       [[ifcShapemodel],ifcLabel,ifcText,ifcLogical,
        ifcProductRepresentationselect],[],true).

class(ifcDoorpanelproperties/9,class,ifcPredefinedpropertyset,
       [panelDepth,panelOperation,panel_width,
        panelPosition,shapedAspectStyle],
       [ifcPositiveLengthMeasure,ifcDoorPanelOperationEnum,
       ifcNormalisedRatioMeasure,ifcDoorPanelPositionEnum,
       ifcShapeaspect],[],true).


class(ifcWindowliningproperties/16,class,ifcPredefinedpropertyset,
       [liningDepth,liningThickness,transomThickness,mullionThickness,
       firstTransomThickness,secondTransomThickness,
       firstMullionOffset,secondMullionOffset,shapeAspectStyle,
       liningOffset,liningToPanelOffsetX,liningToPanelOffsetY],
       [ifcPositiveLengthMeasure,ifcNonNegativeLengthMeasure,
       ifcNonNegativeLengthMeasure,ifcNonNegativeLengthMeasure,
       ifcNormalisedRatioMeasure,
       ifcNormalisedRatioMeasure,ifcNormalisedRatioMeasure,
       ifcNormalisedRatioMeasure,ifcShapeaspect,ifcLengthMeasure,
       ifcLengthMeasure,ifcLengthMeasure],[],true).

enum(ifcColumnTypeEnum,[column,pilaster,userdefined,notdefined]).

% Vertical structural or architectural member which often is aligned
% with a structural grid intersection
class(ifcColumn/9,class,ifcBuildingelement,
       [predefinedType],[ifcColumnTypeEnum],[],true).

class(ifcColumntype/10,class,ifcBuildingelementtype,
       [predefinedType],[ifcColumnTypeEnum],[],true).


class(ifcMaterialusagedefinition/0,class,'',
       [],[],[],true).

enum(ifcLayerSetDirectionEnum,[axis1,axis2,axis3]).

enum(ifcDirectionSenseEnum,[positive,negative]).

class(ifcMateriallayersetusage/5,class,ifcMaterialusagedefinition,
       [forLayerSet,layerSetDirection,directionSense,
       offsetFromReferenceLine,referenceExtent],
       [ifcMateriallayerset,ifcLayerSetDirectionEnum,
       ifcDirectionSenseEnum,ifcLengthMeasure,ifcPositiveLengthMeasure],[],true).

class(ifcClosedShell/2,class,ifcConnectedfaceset,
       [cfsFaces],[[ifcFace]],[],true).

class(ifcManifoldsolidbrep/1,class,ifcSolidmodel,
       [outer],[ifcClosedShell],[],true).

class(ifcFacetedbrep/1,class,ifcManifoldsolidbrep,
       [],[],[],true).

class(ifcPlanarextent/2,class,ifcGeometricrepresentationitem,
       [sizeInX,sizeInY],[ifcLengthMeasure,ifcLengthMeasure],[],true).

%------------------------------------------------------------

type(ifcDescriptiveMeasure,string,STR^atom(STR)).

class([ifcDescriptiveMeasure/1,ifcLengthMeasure/1,ifcNormalisedRatioMeasure/1,
        ifcPositiveLengthMeasure/1,ifcPositiveratiomeasure/1]
      ,select,ifcSizeselect,[],[],[],true).

class(ifcCurvestylefontpattern/2,class,ifcPresentationitem,
       [visibleSegmentLength,invisibleSegmentLength],
       [ifcLengthMeasure,ifcPositiveLengthMeasure],[],true).

class(ifcCurvestylefont/2,class,ifcPresentationitem,
       [name,patternList],[ifcLabel,ifcCurvestylefontpattern],[],true).

class([ifcCurvestylefont/2,ifcPredefinedcurvefont/1],select,
      ifcCurvestylefontselect,[],[],[],true).

class(ifcCurvestylefontandscaling/3,class,ifcPresentationitem,
       [name,curveFont,curveFontScaling],
       [ifcLabel,ifcCurvestylefontselect,ifcPositiveratiomeasure],[],true).

class([ifcCurvestylefontandscaling/3,ifcCurvestylefontselect/2],
      select,ifcCurvefontorscaledcurvefontselect,[],[],[],true).

class(ifcCurvestyle/5,class,ifcPresentationstyle,
       [curveFont,curveWidth,curveColour,modelOrDraughting],
       [ifcCurvefontorscaledcurvefontselect,ifcSizeselect,
       ifcColour,ifcBoolean],[],true).

class(ifcFaceouterbound/2,class,ifcFacebound,
       [],[],[],true).

%------------------------------------------------------------

enum(ifcSpaceTypeEnum,[space,parking,gfa,
                       internal,external,userdefined,notdefined]).

% A space represents an area or volume bounded actually or theoretically.
% Spaces are areas or volumes that provide for certain functions within a
% building.
% +globalID,+ownerHistory,+name,+descr
% +objectType,+objectPlacement,+representation,+longName,
% +compositionType,+predefinedType,+elevationWithFlooring
class(ifcSpace/11,class,ifcSpatialstructureelement,
       [predefinedType,elevationWithFlooring],
       [ifcSpaceTypeEnum,ifcLengthMeasure],[],true).

class(ifcSpatialelementtype/9,class,ifcTypeproduct,
       [elementType],[ifcLabel],[],true).

class(ifcSpatialstructureelementtype/9,class,ifcSpatialelementtype,
       [],[],[],true).

class(ifcSpacetype/11,class,ifcSpatialstructureelementtype,
       [predefinedType,longName],
       [ifcSpaceTypeEnum,ifcLabel],[],true).

class(ifcRelassociatesmaterial/6,class,ifcRelassociates,
       [relatingMaterial],
       [ifcMaterialselect],
       [],true).

class(ifcReldefines/4,class,ifcRoot,
       [],[],[],true).


class(ifcReldefinesbyproperties/6,class,ifcReldefines,
       [relatedObjects,relatingPropertyDefinition],
       [[ifcObjectdefinition],[ifcPropertysetdefinition]],
       [],true).

class(ifcReldefinesbytype/6,class,ifcReldefines,
       [relatedObjects,relatingType],
       [[ifcObject],ifcTypeobject],
       [],true).

% The window is a building element that is predominately used to provide
% natural light and fresh air. It includes vertical opening but also
% horizontal opening such as skylights or light domes. It includes
% constructions with swinging, pivoting, sliding, or revolving panels and
% fixed panels. A window consists of a lining and one or several panels.
class(ifcWindow/13,class,ifcBuildingelement,
       [overallHeight,overallWidth,
        predefinedType,partitioningType,userDefinedPartitioningType],
       [ifcPositiveLengthMeasure,ifcPositiveLengthMeasure,
        ifcWindowTypeEnum,ifcWindowTypePartitioningEnum,ifcLabel],[],true).

class(ifcConversionbasedunit/4,class,ifcNamedunit,
       [name,conversionFactor],
       [ifcLabel,ifcMeasurewithunit],
       [],true).

class(ifcFeatureelement/8,class,ifcElement,
       [],[],[],true).

class(ifcFeatureelementsubtraction/8,class,ifcFeatureelement,
       [],[],[],true).

enum(ifcOpeningElementTypeEnum,[opening,recess,userdefined,notdefined]).

class(ifcOpeningelement/9,class,ifcFeatureelementsubtraction,
       [predefinedType],[ifcOpeningElementTypeEnum],
       [],true).

class(ifcRelfillselement/6,class,ifcRelconnects,
       [relatingOpeningElement,relatedBuildingElement],
       [ifcOpeningelement,ifcElement],
       [],true).

enum(ifcExternalSpatialElementTypeEnum,[external,external_earth,
                                       external_water,external_fire,
                                       userdefined,notdefined]).

class(ifcExternalspatialstructureelement/8,class,ifcSpatialelement,
       [],[],[],true).

class(ifcExternalspatialelement/9,class,ifcExternalspatialstructureelement,
       [predefinedType],
       [ifcExternalSpatialElementTypeEnum],[],true).

class([ifcExternalspatialelement/9,ifcSpace/11],class,
      ifcSpaceboundaryselect,[],[],[],true).

enum(ifcPhysicalOrVirtualEnum,[physical,virtual,notdefined]).

enum(ifcInternalOrExternalEnum,[internal,external,
                                external_earth,external_water,
                                external_fire,notdefined]).

class(ifcRelspaceboundary/9,class,ifcRelconnects,
       [relatingSpace,relatedBuildingElement,connectionGeometry,
       physicalOrVirtualBoundary,internalOrExternalBoundary],
       [ifcSpaceboundaryselect,ifcElement,ifcConnectiongeometry,
       ifcPhysicalOrVirtualEnum,ifcInternalOrExternalEnum],
       [],true).

%------------------------------------------------------------

type(ifcPresentableText,string,STR^atom(STR)).

type(ifcTextFontName,string,STR^atom(STR)).

enum(ifcTextPath,[left,right,up,down]).

class(ifcTextliteral/3,class,ifcGeometricrepresentationitem,
       [literal,placement,path],
       [ifcPresentableText,ifcAxis2placement,ifcTextPath],[],true).

enum(ifcBoxalignment,[top_left,top_middle,top_right,
                      middle_left,center,middle_right,
                     bottom_left,bottom_middle,bottom_right]).

enum(ifcFontStyle,[normal,italic,oblique]).

enum(ifcFontVariant,[normal,small-caps]).

enum(ifcFontWeight,[normal,small-caps,100,200,300,400,500,600,700,800,900]).

class(ifcTextliteralwithextent/5,class,ifcTextliteral,
       [extent,boxAlignment],[ifcPlanarextent,ifcBoxalignment],[],true).

class(ifcPredefinedtextfont/1,class,ifcPredefineditem,
       [],[],[],true).

class(ifcTextstylefordefinedfont/2,class,ifcPresentationitem,
       [colour,backgroundColour],
       [ifcColour,ifcColour],[],true).

class(ifcTextstylefontmodel/6,class,ifcPredefinedtextfont,
       [fontFamily,fontStyle,fontVariant,fontWeight,fontSize],
       [[ifcTextFontName],ifcFontStyle,ifcFontVariant,
       ifcFontWeight,ifcSizeselect],[],true).

enum(ifcTextAlignment,[left,right,center,justify]).

enum(ifcTextDecoration,[none,underline,overline,line_through,blink]).

enum(ifcTextTransformation,[capitalize,uppercase,lowercase,none]).

class(ifcTextstyletextmodel/7,class,ifcPresentationitem,
       [textIndent,textAlign,textDecoration,
       letterSpacing,wordSpacing,textTransform,lineHeight],
       [ifcSizeselect,ifcTextAlignment,ifcTextDecoration,
       ifcSizeselect,ifcSizeselect,ifcTextTransformation,ifcSizeselect],[],true).

class(ifcExternalreference/3,class,'',
       [location,identification,name],
       [ifcURIReference,ifcIdentifier,ifcLabel],[],true).

class(ifcResourcelevelrelationship/2,class,'',
       [name,description], [ifcLabel,[ifcText]],[],true).

class([ifcActorrole/3,
       %ifcAppliedvalue,
       %ifcApproval,
       %ifcConstraint,
       %ifcContextdependentunit,
        ifcConversionbasedunit/4,
       %ifcExternalinformation,
        ifcExternalreference/3,
        ifcMaterialdefinition/3,
        ifcOrganization/5,
        ifcPerson/8,
        ifcPersonandorganization/3,
        ifcPhysicalquantity/2,
        ifcProfiledef/2,
        ifcPropertyabstraction/0
       %,ifcTimeseries
       ],
      select,ifcResourceobjectselect,[],[],[],true).

class(ifcExternalreferencerelationship/4,class,ifcResourcelevelrelationship,
       [relatingReference,relatedResourceObjects],
       [ifcExternalreference,[ifcResourceobjectselect]],[],true).

class(ifcExternallydefinedfont/3,class,ifcExternalreference,
       [],[],[],true).

class([ifcExternallydefinedfont/3,ifcPredefinedtextfont/1],select,
      ifcTextfontselect,[],[],[],true).

class(ifcTextstyle/5,class,ifcPresentationstyle,
       [textCharacterAppearance,textStyle,textFontStyle,modelOrDraughting],
       [ifcTextstylefordefinedfont,ifcTextstyletextmodel,
       ifcTextfontselect,ifcBoolean],[],true).

%------------------------------------------------------------

class(ifcRectangleprofiledef/5,class,ifcParametrizedprofiledef,
       [xDim,yDim],[ifcPositiveLengthMeasure,ifcPositiveLengthMeasure],[],true).

class(ifcRelvoidselement/6,class,ifcReldecomposes,
       [relatingBuildingElement,relatedOpeningElement],
       [ifcElement,ifcFeatureelementsubtraction],[],true).

enum(ifcWindowPanelOperationEnum,[sidehungrighthand,sidehunglefthand,
                                  tiltandturnrighthand,tiltandturnlefthand,
                                  tophung,bottomhung,
                                  pivothorizontal,pivotvertical,
                                  slidinghorizontal,slidingvertical,
                                  removablecasement,fixedcasement,
                                  otheroperation,notdefined]).

enum(ifcWindowPanelPositionEnum,[left,middle,right,bottom,top,notdefined]).

class(ifcWindowpanelproperties/9,class,ifcPredefinedpropertyset,
       [operationType,panelPosition,
        frameDepth,frameThickness,shapeAspectStyle],
       [ifcWindowPanelOperationEnum,ifcWindowPanelPositionEnum,
       ifcPositiveLengthMeasure,ifcPositiveLengthMeasure,
       ifcShapeaspect],[],true).

%------------------------------------------------------------

class(ifcFacesurface/4,class,ifcFace,
       [bounds,faceSurface,sameSense],
       [[ifcFacebound],ifcSurface,ifcBoolean],[],true).

class(ifcFacebasedsurfacemodel/1,class,ifcGeometricrepresentationitem,
       [fbsmFaces],[[ifcConnectedfaceset]],[],true).

class([ifcFacebasedsurfacemodel/1,ifcFacesurface/3,ifcSurface/0],select,
      ifcSurfaceorfacesurface,[],[],[],true).

class(ifcConnectionsurfacegeometry/2,class,ifcConnectiongeometry,
       [surfaceOnRelatingElement,surfaceOnRelatedElement],
       [ifcSurfaceorfacesurface,ifcSurfaceorfacesurface],[],true).

%------------------------------------------------------------

enum(ifcDerivedUnitEnum,[angularvelocityunit,areadensityunit,
                        compoundplaneangleunit,dynamicviscosityunit,
                        heatfluxdensityunit,integercountrateunit,
                        isothermalmoisturecapacityunit,
                        kimenaticviscosityunit,linearvelocityunit,
                        massdensityunit,massflowrateunit,
                        moisturediffusivityunit,molecularweightunit,
                        specificheatcapacityunit,thermaladmittanceunit,
                        thermalconductanceunit,thermalresistanceunit,
                        thermaltransmittanceunit,vaporpermeabilityunit,
                        volumetricflowrateunit,rotationalfrequencyunit,
                        torqueunit,momentofinertiaunit,linearmomentunit,
                        linearforceunit,planarforceunit,
                        modulusofelasticityunit,shearmodulusunit,
                        linearstiffnessunit,rotationalstiffnessunit,
                        modulusofsubgradereactionunit,accelerationunit,
                        curvatureunit,heatingvalueunit,ionconcentrationunit,
                        luminousintensitydistributionunit,massperlengthunit,
                        modulusoflinearsubgradereactionunit,
                        modulusofrotationalsubgradereactionunit,
                        phunit,rotationalmassunit,sectionareaintegralunit,
                        sectionmodulusunit,soundpowerlevelunit,
                        soundpowerunit,soundpressurelevelunit,
                        soundpressureunit,temperaturegradientunit,
                        temperaturerateofchangeunit,
                        thermalexpansioncoefficientunit,warpingconstantunit,
                        warpingmomentunit,userdefined]).

class(ifcDerivedunit/3,class,'',
       [elements,unitType,userDefinedType],
       [[ifcDerivedunitelement],ifcDerivedUnitEnum,ifcLabel],[],true).

class(ifcFurnishingelementtype/9,class,ifcElementtype,
       [],[],[],true).

class(ifcFurnishingelement/8,class,ifcElement,
       [],[],[],true).

class(ifcBoundingbox/4,class,ifcGeometricrepresentationitem,
       [corner,xDim,yDim,zDim],
       [ifcCartesianpoint,ifcPositiveLengthMeasure,
       ifcPositiveLengthMeasure,ifcPositiveLengthMeasure],[],true).

% The building storey has an elevation and typically represents a
% (nearly) horizontal aggregation of spaces that are vertically bound.
class(ifcBuildingstorey/10,class,ifcSpatialstructureelement,
       [elevation],
       [ifcLengthMeasure],[],true).

%------------------------------------------------------------

class(ifcCartesiantransformationoperator/4,abstract,
       ifcGeometricrepresentationitem,
       [axis1,axis2,localOrigin,scale],
       [ifcDirection,ifcDirection,ifcCartesianpoint,
       ifcLengthMeasure],[],true).

class(ifcCartesiantransformationoperator3d/5,class,
       ifcCartesiantransformationoperator,
       [axis3],[ifcDirection],[],true).

class(ifcDoorliningproperties/17,class,ifcPredefinedpropertyset,
       [liningDepth,liningThickness,thresholdDepth,thresholdThickness,
       transomThickness,transomOffset,liningOffset,thresholdOffset,
       casingThickness,casingDepth,shapeAspectStyle,
       liningToPanelOffsetX,liningToPanelOffsetY],
       [ifcPositiveLengthMeasure,ifcNonNegativeLengthMeasure,
       ifcPositiveLengthMeasure,ifcNonNegativeLengthMeasure,
       ifcNonNegativeLengthMeasure,ifcLengthMeasure,ifcLengthMeasure,
       ifcLengthMeasure,ifcPositiveLengthMeasure,ifcPositiveLengthMeasure,
       ifcShapeaspect,ifcPositiveLengthMeasure,
       ifcPositiveLengthMeasure],[],true).

%------------------------------------------------------------
enum(ifcStairTypeEnum,[straight_run_stair,two_straigth_run_stair,
                      quarter_winding_stair,quarter_turn_stair,
                      half_winding_stair,half_turn_stair,
                      two_quarter_winding_stair,two_quarter_turn_stair,
                      three_quarter_winding_stair,three_quarter_turn_stair,
                      spiral_stair,double_return_stair,curved_run_stair,
                      two_curved_run_stair,userdefined,notdefined]).

% A stair is a vertical passageway allowing occupants to walk (step)
% from one floor level to another floor level at a different elevation.
% It may include a landing as an intermediate floor slab.
class(ifcStair/9,class,ifcBuildingelement,
       [predefinedType],
       [ifcStairTypeEnum],[],true).

class(ifcAnnotation/7,class,ifcProduct,
       [],[],[],true).

%------------------------------------------------------------

class(ifcMonetaryunit/1,class,'',[currency],[ifcLabel],[],true).

%============================================================
%  transformation rules
%  from BIM/IFC to inDoor/Prolog
%============================================================
%
%ifcCartesianpoint()  --> corner
%ifcPolyline --> wall line/side
%
