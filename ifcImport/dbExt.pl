:- module(dbExt,  [clauses/3, clauses/4,
                   delete_module/1,
                  predicate_length/2, nth1_clause/3,
                  is_unique/2,
                  module_statistics/2]).

%the predicate doesn't delete the parameter predicate itself
%...only its clauses (with retractall)
%Unfortunately in swi-prolog it is impossible
%...to delete a module or a predicate either...
delete_module(MOD):-
  forall(current_predicate(_NAME,MOD:HEAD),
        retractall(MOD:HEAD)).


clausess(MOD:NAME/ARITY,CLAUSES):-
  !, functor(HEAD,NAME,ARITY), clausess(MOD:HEAD,CLAUSES).
clausess(MOD:HEAD,CLAUSES):-
  findall(MOD:HEAD,clause(MOD:HEAD,_),CLAUSES).

predicate_length(MOD:PREDICATE,LENGTH):-
  clausess(MOD:PREDICATE,CLAUSES), length(CLAUSES,LENGTH).

nth1_clause(MOD:NAME/ARITY,NTH,CLAUSE):-
  clausess(MOD:NAME/ARITY,CLAUSES), nth1(NTH,CLAUSES,CLAUSE).


module_statistics(MOD,STATISTICS):-
  findall(NAME/ARITY-LN,(current_predicate(_,MOD:HEAD),
                      predicate_length(MOD:HEAD,LN),
                      functor(HEAD,NAME,ARITY)),STATISTICS).


matching_clauses(MOD:HEAD,ARG,VALUE,CLAUSES):-
  arg(ARG,HEAD,VALUE),
  findall(MOD:HEAD,clause(MOD:HEAD,_),CLAUSES).

matching_length(MOD:NAME/ARITY-ARG=VALUE,LENGTH):-
  functor(HEAD,NAME,ARITY), matching_clauses(MOD:HEAD,ARG,VALUE,CLAUSES),
  length(CLAUSES,LENGTH).


%Produces a list of list of clauses with same argument constraint
%
%clauses(+MOD:+HEAD,?ARGS,-CLAUSESS):-
%+MOD:+HEAD  - module:clause head
%+ARGS       - list of arguments or unbound var
%-CLAUSESS   - list of (ARGS-list of clauses
%              with common constrained value)
clauses(MOD:HEAD,ARGS,CLAUSESS):-
  bagof(ARGS-CLAUSES,
        bagof((HEAD:-B),
              B^(HEAD =.. [_|ARGS],clause(MOD:HEAD,B)),
              CLAUSES),CLAUSESS).

%clauses(+MOD:+NTH,?VALUE,-CLAUSESS):-
%+MOD:+HEAD  - module:clause head
%+NTH        - index number of argument constraint
%+VALUE      - value constraint of the argument, specified above
%-CLAUSESS   - list of (ARGS-list of clauses
%              with common constrained value)
clauses(MOD:HEAD,NTH,VALUE,CLAUSESS):-
  integer(NTH), !,
  functor(HEAD,_NAME,ARITY), length(ARGS,ARITY),
  nth1(NTH,ARGS,VALUE),
  clauses(MOD:HEAD,ARGS,CLAUSESS).


is_unique(MOD:NAME/ARITY-NTH,VALUE):-
  functor(HEAD,NAME,ARITY),
  clauses(MOD:HEAD,NTH,VALUE,CLAUSESS),
  maplist(=(_-[]),CLAUSESS).















