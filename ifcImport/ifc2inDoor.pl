:-module(ifc2inDoor,[op(990,xfx,(<=>)),
                     (<=>) /2]).

:-op(990,xfy,<=>).

:- discontiguous <=> / 2 .


list2Point([X,Y,Z],X/Y/Z).
list2Point([X,Y],X/Y).

ifcCartesianpoint(#_ID,[X,Y,Z]) <=> X/Y/Z.
ifcCartesianpoint(#_ID,[X,Y]) <=> X/Y.

%meta(ac20,ifcLocalplacement(#31139,X,Y)<=>STRUCT).
ifcLocalplacement(#ID,#RELTO,#REL)<=> [REST|POINT] :-
    var(POINT), var(REST), nonvar(RELTO),
    ifcLocalplacement(#RELTO,#RELTO1,#REL1) <=> POINT,
    ifcAxis2placement3d(#REL,LOC,DIR1,DIR2) <=> REST.
ifcLocalplacement(#ID,#RELTO,#REL)<=> [] :-
    var(RELTO).

%room(BUILD/FLOOR/ID,KIND,LEVEL,SIDES)
% ID:    ground, the name of the room
% BUILD: building ID
% FLOOR: floor number (0: ground floor
% KIND:  kind of room (office;lecture,common,storage;lab,corridor;
%                     elevator;bath;outside)
% LEVEL: floor height relative to the level height
% SIDES: list of wall sections: [side(WALL,CORNER1,CORNER2]
ifcSpace(GID,History,Name,Descr,Type,Placement,Repr,
         LongName,CompType,PredefType,Elevation) <=> room(Name,Elevation):-
    ifcObjectplacement()<=> PLACEMENT.

ifcAxis2placement3d(#ID,#POI,#DIR1,#DIR2) <=> POINT:-
    ifcCartesianpoint(#POI,_PSTR) <=> POINT.


ifcPolyloop(#ID,REFLIST)<=>polygon(LIST):-
    maplist(ac20:ifcCartesianpoint,REFLIST,LIST3LIST),
    maplist(ifc2inDoor:list2Point,LIST3LIST,LIST).

ifcPolyline(#ID,REFLIST)<=>polyline(LIST):-
    maplist(ac20:ifcCartesianpoint,REFLIST,LIST3LIST),
    maplist(ifc2inDoor:list2Point,LIST3LIST,LIST).

ifcOrganization(REF,ID,NAME,DESCRIPTION,ROLES,ADDRESSES) <=> NAME.
  %findall(ROLE,ac20:ifcActorrole(ROLE,_USERROLE,DESCR),ROLES),
  %findall(KIND-DESCR,ac20:ifcAddress(KIND,DESCR,_PURPOSE),ADDRESSES).

ifcApplication(REF,REFORG,Version,FullName,Identifier) <=> NAME :-
    ifcOrganization(REFORG,ORGID,NAME,ORGDESCR,RS,AS) <=> NAME.

ifcOwnerhistory(REF,User,REFAPP,State,ChangeAction,
       Modified,ModUser,ModAppl,Created) <=> APPORG :-
    ifcApplication(REFAPP,_,_,_,_)<=>APPORG.

ifcBuilding(REF,GID,History,Name,Descr,Type,Placement,
            Repr,LongName,CompType,ElevRefHeight,ElevTerrain,Address) <=>
    signature(COMPANY,Name):-
    ifcOwnerhistory(History,_,_,_,_,_,_,_,_) <=> COMPANY.



ifcLoop() <=> true.


ifcFacebound(REF,LREF,ORIENTATION) <=> LOOP :-
    ifcLoop(LREF) <=> LOOP.


ifcFace(REF,BOUNDS) <=> true.


ifcRepresentation(CONTEXT,ID,TYPE,ITEMS) <=> ITEMS:-
    findall(ITEM,ifcRepresentationitem(),ITEMS).

%+CONTEXT:ifcRepresentationcontext, +ID:String, +TYPE:String
%+items: [ifcRepresentationitem]
ifcShaperepresentation(REF,CONTEXT,ID,TYPE,ITEMS) <=> ITEMS:-
    true.

ifcProductrepresentation(NAME,DESCR,REPRS) <=> REPRESENTATIONS:-
    [R:REPRS,REPR:REPRESENTATIONS]^ifcRepresentation(R)<=>REPR.
    %findall(R,(member(REPR,REPRS),
    %           ac20:ifcRepresentation(REPR,CONTEXT,ID,ITEMS) <=> R),7
    %        REPRESENTATIONS).


%ifcProductdefinitionshape(#31185,N,D,RS).
%The definition contains all shape relevant information of a product
%+NAME:String, +DESCR:String, +REPRS:[ifcRepresentation]
ifcProductdefinitionshape(REF,_SNAME,_SDESCR,REPRS) <=> REPRS:-
  [R:REPRS,RE:REPRESENTATIONS]^(ifcShaperepresentation(R,_RN,_RD,RE)<=>
    REPRESENTATIONS).


ifcWall(REF,GID, History, NAME, Descr, Type,
                    Placement, ReprID, Tag, PreType) <=>
    wall(BUILDING/STAIR/NAME,polygon,OBJPLACEMENT):-
    %ifcObjectplacement
    ifcLocalplacement(Placement,ObjPlace,Axis2Place) <=> OBJPLACEMENT,
    ifcProductdefinitionshape(ReprID,_NAME,_DESCR,REPRS).
    %ifcProducrepresentation(Repr) <=> REPR.

% meta(ac20,ifcWall(#31190,GID,HIST,NAME,DESCR,TYPE,PLACE,REPR,TAG,PRETYPE)<=>X).
%
ifcWallstandardcase(REF,GID, History, NAME, Descr, Type,
                    Placement, Repr, Tag, PreType) <=> STRUCT :-
    ifcWall(REF,GID, History, NAME, Descr, Type,
                    Placement, Repr, Tag, PreType) <=>  STRUCT.















