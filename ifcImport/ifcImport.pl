:- module(ifcImport,  [loadIFC/3]).

:- op(500, fx,user:(#)).
:- op(500, xfx,user:(#)).
:- op(990,xfx,user:(<=>)).



:-use_module(library(dcg/basics)).
:-use_module(library(dcg/high_order)).

:- use_module(lazytokens).

:- use_module(metadriven).

:- use_module(ifcMeta).

:- use_module(dbExt).


%initRegexp:-      lazytokens:set_regexp("\\d+\\.\\d*|\\d+|\c
%	   [\\w|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�]+'?").
% the following definition recognizes also scientific fractional numbers
initRegexp:- lazytokens:set_regexp(
              "\\d+\\.(\\d*([Ee][-+]\\d+)?)?|\\d+|\c
	      [\\w|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�]+'?").


assertStatement(MOD,STMT):- assert(MOD,STMT).

myNumberString(NUM,STR):- number_string(NUM,STR), !.
myNumberString(NUM,STR):- string_concat(NUMSTR,".",STR),
    number_string(NUM,NUMSTR).


ifcUnsigned(UNS)--> [UNSSTR], {myNumberString(UNS,UNSSTR)}.

ifcNumber(NUM)--> ["-"], !, ifcUnsigned(UNS), {NUM is 0-UNS};
                   ifcUnsigned(NUM).

ifcInteger(INT)--> ifcNumber(INT), {integer(INT)}.

ifcRef(#(INT))--> ["#"], ifcNumber(INT).

ifcUnit({UNIT})--> ["."], ifcAtom(UNIT), ["."].


ifcAtom(ATOM)-->[ATOMUP], {ATOMUP \= "(", ATOMUP \= ")",
                    string_lower(ATOMUP,ATOMLOW),
                          atom_string(ATOM,ATOMLOW)}.

ifcAtomic(REF)--> ifcRef(REF), !.
ifcAtomic(INT)--> ifcNumber(INT), !.
ifcAtomic(ATOM)--> ifcAtom(ATOM).

ifcTerm(QATOM)-->[QUOTED],
    {string(QUOTED), sub_string(QUOTED,0,1,_,'\''), sub_string(QUOTED,_,1,0,'\''),
    sub_string(QUOTED,1,_,1,QATOM)}, !.
ifcTerm(_)--> ["$"], !.
ifcTerm(REF)--> ifcRef(REF), !.
ifcTerm(UNIT)--> ifcUnit(UNIT), !.
ifcTerm(TERM)-->
    ifcAtom(ATOM),
    sequence(["("], ifcTerm, [","], [")"], LIST), {TERM =..[ATOM|LIST]}, !.
ifcTerm(LIST)-->
    sequence(["("], ifcTerm, [","], [")"], LIST), !.
ifcTerm(ATOMIC)-->
    ifcAtomic(ATOMIC).

ifcCloseNL--> [";","\n"].

nls--> ["\n"], !, nls.
nls--> [].

while(END,LIST)--> END -> !, {LIST=[]} ;
    [TOKEN], while(END,TAIL), {LIST=[TOKEN|TAIL]}.



name2IFC(IFCNAME,NAME):- atom_chars(IFCNAME,[i,f,c,C|REST]), !,
    upcase_atom(C,CUP), atom_chars(NAME,[i,f,c,CUP|REST]).
name2IFC(IFCNAME,IFCNAME).

%we create diagnostic output at each 1000th line
ifcStatement(_NN,comment(COMMENT))--> ["/","*"],
    while(["*","/"],COMMENT), !, nls.
ifcStatement(NN,STATEMENT)--> ifcRef(REF), ["="], ifcTerm(TERM),
    [";"], !, nls,
    {TERM =.. [IFCNAME|ARGS], name2IFC(IFCNAME,NAME),
     STATEMENT =.. [NAME,REF|ARGS]},
    {REF= #(N), N mod NN=:= 0 ->
       debug(ifcImport,"ifcStatement(~w)\n",[REF=STATEMENT]);true}.

ifcISO(N1-N2)--> ["ISO","-"], ifcInteger(N1), ["-"], ifcInteger(N2), [";"], nls.
ifcDescription(LIST)-->
    ifcTerm(TERM), {TERM=..['file_description'|LIST]}, [";"], nls.
ifcFile(LIST)-->  ifcTerm(TERM), {TERM=..['file_name'|LIST]}, [";"], nls.
ifcSchema(SCHEMA)--> ifcTerm(TERM), {TERM=..['file_schema',SCHEMA]},[";"], nls.


ifcHeader(header(ISO,DESCRIPTION,FLIST,SCHEMA))-->
    ifcISO(ISO),nls,
    ["HEADER",";"],nls,
    ifcDescription(DESCRIPTION),
    ifcFile(FLIST),
    ifcSchema(SCHEMA),
    ["ENDSEC",";"], nls,
    ["DATA",";"], nls.

ifcFooter(ISO)-->
    nls, ["ENDSEC"], ifcCloseNL, nls,
    ["END","-"], ifcISO(ISO).

ifcBody(N,STATEMENTS)-->
    sequence(ifcStatement(N),STATEMENTS).

ifcProgram(N,HEADER-STATEMENTS)-->
    ifcHeader(HEADER), nls, {HEADER=header(ISO,_,_,_)},
    ifcBody(N,STATEMENTS),
    nls, ifcFooter(ISO), nls.

ifcStatementAdd(N,MOD,STMT)-->
    ifcStatement(N,STMT), !, {assert(MOD:STMT)}.

addStatements(N,MOD)-->
    ifcStatement(N,STMT), !, {assert(MOD:STMT)},
    addStatements(N,MOD).
addStatements(_,_)--> [] .


loadIFC(FILE,MOD,N):-
    initRegexp,
    exists_file(FILE), open(FILE,read,IN),
    stream_to_lazy_tokens(IN,TOKENS),
    once(phrase(ifcHeader(header(ISO,_,_,_)),TOKENS,STATEMENTS)),
    %once(phrase(sequence(ifcStatementAdd(N,MOD),_),STATEMENTS,FOOTER)),
    addStatements(N,MOD,STATEMENTS,FOOTER),
    once(phrase(ifcFooter(ISO),FOOTER)),
    close(IN).

loadIFCLines(FILE):-
    initRegexp,
    exists_file(FILE), open(FILE,read,IN),
    stream_to_lazy_tokens(IN,TOKENS),
    phrase(sequence((while(["\n"],LINETOKENS),
                     {writeln(LINETOKENS)}),FLINE),TOKENS,[]),
    close(IN).


loadIFCTokens(FILE):-
    initRegexp,
    exists_file(FILE), open(FILE,read,IN),
    stream_to_lazy_tokens(IN,TOKENS),
    (member(TOKEN,TOKENS), write(TOKEN), fail;
    close(IN)).

testSentence(FILE,N):- integer(N), exists_file(FILE), !,
    testSentence(FILE,ifcProgram(N,_PROG)).
testSentence(FILE,CALL):- exists_file(FILE), !,
    open(FILE,read,In),
    stream_to_lazy_tokens(In,Tokens), phrase(CALL,Tokens,[]), close(In).

testSentence(LINE,CALL,X):- string(LINE), !, string_to_tokens(LINE,TOKENS),
    phrase(CALL,TOKENS), writeln(X).

testLines(FILE,N):- exists_file(FILE), !,
    open(FILE,read,In),
    stream_to_lazy_lines(In,Lines),
    forall((nth1(NTH,Lines,Line),(NTH<N->true;!)), write(Line)), close(In).



testSentence(FILE):- exists_file(FILE), !,
    testSentence(1000,FILE).
testSentence(LINE):- string(LINE), !,
    testSentence(LINE,ifcStatement(1,STMT),STMT).


testTokens(FILE,N):- exists_file(FILE), !,
    initRegexp,
    open(FILE,read,In),
    stream_to_lazy_tokens(In,Tokens),
    forall((nth1(NTH,Tokens,Token),(NTH<N->true;!)),
           writef('%w ',[Token])), close(In).
testTokens(STRING):- string(STRING), !,
    initRegexp,
    string_to_tokens(STRING,Tokens),
    forall(member(Token,Tokens), writef('%w ',[Token])).

:- writeln(":-loadIFC('AC20-Institute-Var-2.ifc',ac20,100).").





















