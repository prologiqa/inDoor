:- module(metadriven,[metaClassAttributes/7,
                      subMetaClass/3,
                      contains/4, contained/4,
                      containsTree/4,
                      displayContainsTree/1,
                      meta/2,
              op(990,xfy,<=>),
              op(500, fx,#),
              op(500, xfx,#)
          ]).


metaClassKind(root).
metaClassKind(abstract).
metaClassKind(class).
metaClassKind(enum).
metaClassKind(select).

metaTypeDefined(META,[NAME]/ARITY):- !, metaTypeDefined(META,NAME/ARITY).
metaTypeDefined(META,NAME/ARITY):- META:class(NAME/ARITY,_,_,_,_,_,_).
metaTypeDefined(META,NAME/ARITY):- META:class([_/ARITY|_],_,NAME,_,_,_,_).
metaTypeDefined(META,NAME/_ARITY):- META:type(NAME,_,_).
metaTypeDefined(META,NAME/ARITY):- META:primitive(NAME/ARITY).
metaTypeDefined(META,NAME/_ARITY):- META:enum(NAME,_).


metaClassChecks(META,NAME/ARITY,KIND,SUPER,LABELS,TYPES,ALLCALLS,CALL):-
  META:class(NAME/ARITY,KIND,SUPER,LABELS,TYPES,CALLS,CALL),
  SUPER \== NAME, KIND \== select,
   (CALLS == [] -> length(TYPES,AR),length(ALLCALLS,AR),
                  maplist(=(true),ALLCALLS);
   ALLCALLS = CALLS).
metaClassChecks(META,NAME0/ARITY,KIND,SUPER,LABELS,TYPES,CALLS,CALL):-
  META:class(SUBCLASSES,select,NAME0,_,_,_,_),
    member(NAME/ARITY,SUBCLASSES),
    metaClassChecks(META,NAME/ARITY,KIND,SUPER,LABELS,TYPES,CALLS,CALL).


% metaClassAttributes(META,NAME/ARITY,LABELS,TYPES,CALLS,CALL,LN)
% is semidet.
% retrieves the attribute list from the given metaclass
% definition... For enums it returns empty list
% +META:       metamodel-module
% +NAME/ARITY: metaclass designator
% -LABELS:     attribute label list
% -TYPES:      attribute type list
% -CALLS:      attribute constraint checks -CALL:
% -LN:         length of LABELS/TYPES/CALLS should be LN=ARITY
% structure constraint length(LABELS)=length(TYPES)=length(CALLS)
metaClassAttributes(META,NAME/ARITY,LABELS,TYPES,CALLS,CALL,LN):-
  metaClassChecks(META,NAME/ARITY,_,ANCESTOR,OWNLABELS,OWNTYPES,OWNCALLS,CALL),
  (ANCESTOR == '' -> LABELS=OWNLABELS, TYPES=OWNTYPES, CALLS=OWNCALLS,
   length(LABELS,LN);
   ANCESTOR=[ANC|_] ->
  metaClassAttributes(META,ANC/_AARITY,ANCLABELS,ANCTYPES,ANCCALLS,CALL,LN0),
    append(ANCLABELS,OWNLABELS,LABELS), append(ANCTYPES,OWNTYPES,TYPES),
    append(ANCCALLS,OWNCALLS,CALLS), length(LABELS,L), LN is LN0+L;
  metaClassAttributes(META,ANCESTOR/_AARITY,
                      ANCLABELS,ANCTYPES,ANCCALLS,CALL,LN0),
    append(ANCLABELS,OWNLABELS,LABELS), append(ANCTYPES,OWNTYPES,TYPES),
    append(ANCCALLS,OWNCALLS,CALLS),length(OWNLABELS,L), LN is LN0+L).

metaTypeAttributes(META,ENUM/0,[],[],[],_):-
  META:enum(ENUM,_).
metaTypeAttributes(META,TYPE/0,[],[],[],_):-
  META:type(TYPE,_,_).
metaTypeAttributes(META,NAME/ARITY,LABELS,TYPES,CALLS,CALL):-
  metaClassAttributes(META,NAME/ARITY,LABELS,TYPES,CALLS,CALL,_).


% supMetaClassAttributes(META,NAME0/ARITY0,NAME/ARITY,
% LABELS,TYPES,CALLS,CALL) is nondet.
%
% retrieves the attribute list from the given metaclass definition
% and for its superclasses in a backtracking manner...starts from root
%
%+META:         metamodel-module
%+NAME0/ARITY0: metaclass designator reference
%-NAME/ARITY:   metaclass designator
%-LABELS:       attribute label list
%-TYPES:        attribute type list
%-CALLS:        attribute constraint checks
%-CALL:         structure constraint
%length(LABELS)=length(TYPES)=length(CALLS)

supMetaClassAttributes(META,NAME0/ARITY0,NAME/ARITY,LABELS,TYPES,CALLS,CALL):-
  findall(N/A,supMetaClass(META,NAME0/ARITY0,N/A),NALIST),
  collectInheritedAttributes(META,NALIST,NAME/ARITY,LABELS,TYPES,CALLS,CALL).

collectInheritedAttributes(META,[NAME/ARITY|_],
                           NAME/ARITY,OWNLABELS,OWNTYPES,OWNCALLS,CALL):-
  metaClassChecks(META,NAME/ARITY,_,_,OWNLABELS,OWNTYPES,OWNCALLS,CALL).
collectInheritedAttributes(META,[NAME0/ARITY0|NALIST],NA,
                           LABELS,TYPES,CALLS,CALL):-
  metaClassChecks(META,NAME0/ARITY0,_,_,OWNLABELS,OWNTYPES,OWNCALLS,_),
    collectInheritedAttributes(META,NALIST,NA,ANCLABELS,ANCTYPES,ANCCALLS,CALL),
    append(ANCLABELS,OWNLABELS,LABELS), append(ANCTYPES,OWNTYPES,TYPES),
    append(ANCCALLS,OWNCALLS,CALLS).


memberOrSelf(THIS,THESE):- is_list(THESE), !, member(THIS,THESE).
memberOrSelf(THIS,THIS).

%subMetaClass(META,SUBCLASS/SUBARITY,CLASS/ARITY)
%Transitive closure of the sub-superclass relation
%
%+META:       metamodel, as a Prolog module
%?SUBCLASS:   submetaclass name
%?SUBARITY:   arity of submetaclass
%?CLASS:      super metaclass name
%?ARITY:      super metaclass arity
%
%SUBCLASS/SUBARITY=CLASS/ARITY is the first solution!
subMetaClass(META,CLASS/ARITY,CLASS/ARITY):-
    META:class(CLASS/ARITY,_KIND,_SUPER,_,_,_,_).
%searching upwards (more general entities)
subMetaClass(META,SUBCLASS/SUBARITY,CLASS/ARITY):-
  ground(SUBCLASS), !,
    META:class(SUBCLASS/SUBARITY,_KIND,SUPERS,_,_,_,_),
    memberOrSelf(SUPER,SUPERS),
    subMetaClass(META,SUPER/_,CLASS/ARITY).
%searching downwards (more special entities)
subMetaClass(META,SUBCLASS/SUBARITY,CLASS/_ARITY):-
  ground(CLASS),
    META:class(SUBS,_KIND,CLASS,_,_,_,_),
    (is_list(SUBS)->
      member(SUBENT/SUBAR,SUBS),
      subMetaClass(META,SUBCLASS/SUBARITY,SUBENT/SUBAR);
    SUBS = SUBENT/SUBAR,
      subMetaClass(META,SUBCLASS/SUBARITY,SUBENT/SUBAR)).


%supMetaClass(META,CLASS0/ARITY0,CLASS/ARITY)
%Transitive closure of the sub-super-metaclass relation
%Strategy: returns the most general metaclass, then the
%one level concreter etc.
%The last result is the starting metaclass itself
%
%+META:       metamodel, as a Prolog module
%+CLASS0:     sub-metaclass name
%+ARITY0:     arity of sub-metaclass
%?CLASS:      super metaclass name
%?ARITY:      super metaclass arity
%
%CLASS0/ARITY0=CLASS/ARITY is the last solution!
supMetaClass(META,CLASS0/ARITY0,CLASS/ARITY):-
  META:class(CLASS0/ARITY0,_KIND,ENT,_,_,_,_), ENT \== '',
  supMetaClass(META,ENT/_AR,CLASS/ARITY).
supMetaClass(META,CLASS/ARITY,CLASS/ARITY):-
  META:class(CLASS/ARITY,_,_,_,_,_,_).
%superEnt(ifcMeta,ifcAxis2placement2d/A0,SUPER/ARITY),

remref(#(REF),REF).

remRefOrLeave(#(REF),REF):- ! .
remRefOrLeave(REF,REF).


decompObject(META,_MOD,CLASS,EMPTY,CLASS,AR,EMPTY):- var(EMPTY), !,
    META:class(CLASS/AR,_,_,_,_,_,_).
%***check if REF#CLASS exists - or if its subtype object exists
decompObject(META,MOD,CLASS0,#REF,CLASS,AR,REF):-
  subMetaClass(META,CLASS/AR,CLASS0/_ARITY),
    META:class(CLASS/AR,_,_,_,_,_,_),
    length(ARGS,AR), CALL=..[CLASS,#REF|ARGS],
    current_predicate(CLASS,MOD:CALL), call(MOD:CALL), !.
decompObject(META,MOD,[CLASS],REFSTR,ENT,AR,REF):-
  decompObject(META,MOD,CLASS,REFSTR,ENT,AR,_),
  maplist(remref,REFSTR,REF).


decompValue(CLASS,$,CLASS,_).
decompValue([CLASS],VALUES,CLASS,PREPVALUES):- is_list(VALUES), !,
  maplist(remRefOrLeave,VALUES,PREPVALUES).
decompValue(CLASS,{VALUE},CLASS,VALUE).
decompValue(CLASS,VALUE,CLASS,VALUE):- atomic(VALUE).


%contains(META,MOD,REF0#CLASS0/ARITY0-ARG,REF#CLASS/ARITY) is nondet.
%Finds the directly contained object references
%of a container object. Part objects don't necessarily exist
% - maybe a subtype of them extists
%
%+META:       metamodel, as a Prolog module
%+MOD:        model, as a Prolog module
%+REF0:       container instance ID
%+CLASS0:     container class name
%?ARG:        argument number of the class
%?REF:        contained instance ID
%?CLASS:      contained class name
%?ARITY_      contained class arity
contains(META,MOD,REF0#CLASS0,ARITY0,NTH,TYPES,REF#CLASS,ARITY):-
    length(ARGS,ARITY0), CALL =.. [CLASS0,#REF0|ARGS],
    current_predicate(CLASS0,MOD:CALL),
    call(MOD:CALL), nth1(NTH,TYPES,ENTITIES), nth1(NTH,ARGS,REFSTR),
    (decompObject(META,MOD,ENTITIES,REFSTR,CLASS,ARITY,REF)->true;
    once((decompValue(ENTITIES,REFSTR,CLASS,REF),
         metaTypeDefined(META,CLASS/ARITY)))).

contains(META,MOD,REF0#CLASS0/ARITY0-LABEL,REF#CLASS/ARITY):-
    atom(LABEL), !,
      metaTypeAttributes(META,CLASS0/ARITY0,LABELS,TYPES,_CALLS,_CALL),
      nth1(NTH,LABELS,LABEL),
      contains(META,MOD,REF0#CLASS0,ARITY0,NTH,TYPES,REF#CLASS,ARITY).
contains(META,MOD,REF0#CLASS0/ARITY0-NTH,REF#CLASS/ARITY):-
    metaTypeAttributes(META,CLASS0/ARITY0,_LABELS,TYPES,_CALLS,_CALL),
      contains(META,MOD,REF0#CLASS0,ARITY0,NTH,TYPES,REF#CLASS,ARITY).

%subContains(META,MOD,REF0#CLASS00/ARITY00-ARG,CLASS#REF) is nondet.
%Finds the directly contained objects of a container object
%the container object may be of a concreter class
%
%+META:       metamodel, as a Prolog module
%+MOD:        model, as a Prolog module
%+REF0:       container instance ID
%+CLASS0:     container class name
%?ARG:        argument number of the class
%?REF:        contained instance ID
%?CLASS:      contained class name
subContains(META,MOD,REF0#CLASS00/ARITY00-NTH,REF#CLASS/ARITY):-
  anyInstance(META,MOD,CLASS00,REF0#CLASS0/ARITY0),
    contains(META,MOD,REF0#CLASS0/ARITY0-NTH,REF#CLASS/ARITY).


%metaClassContainsType(META,CLASS/ARITY,NTH,TYPE0,TYPE):-
%Checks, if just by the metamodel, a metaclass contains
%(refers to) a given metaclass type
%
%+META:         metamodel, as a Prolog modul
%?CLASS/ARITY:  container class with arity
%?NTH:          ordinal number of attribute in container
%+TYPE0:        the concrete type contained by container
%-TYPE:         the actual attribute type specification in CLASS
%               (can be the same or ancestor of TYPE0)
%
metaClassRefersToType(META,CLASS/ARITY,NTH,TYPE):-
  metaClassAttributes(META,CLASS/ARITY,_LABELS,TYPES,_CALLS,_CALL,_), ARITY>0,
  nth1(NTH,TYPES,TYP), (TYP=TYPE;TYP=[TYPE]).

metaClassContainsType(META,CLASS/ARITY,NTH,TYPE0,TYPE):-
  subMetaClass(META,TYPE0/_,TYPE/_),
    metaClassRefersToType(META,CLASS/ARITY,NTH,TYPE).

%metaClassContainsType(ifcMeta,E/A,NTH,ifcBuilding,T)

%contained(META,MOD,REF0#CLASS0-ARG,REF#CLASS)
%+META:       metamodel, as a Prolog module
%+MOD:        model, as a Prolog module
%?REF0:       container instance ID
%?CLASS0:     container class name
%?ARG:        argument number of the class
%+REF:        contained instance ID
%+CLASS:      contained class name
contained(META,MOD,REF0#ENT-NTH,REF#CLASS):-
  metaClassContainsType(META,CLASSS/ARITY,NTH,CLASS,_),
  subMetaClass(META,ENT/AR,CLASSS/ARITY),
  callArg(MOD,ENT/AR#REF0,NTH,#REF).


%containing(META,MOD,REF#CLASS,REF0#CLASS0-NTH):-
%  contained(META,MOD,REF0#CLASS0-NTH,REF#CLASS).
containing(META,MOD,REF#CLASS,REF0#CLASS0):-
  contained(META,MOD,REF0#CLASS0-_,REF#CLASS).


containingList(META,MOD,REF#CLASS,REFLIST,END):-
%  findall(REF0#CLASS0-NTH,
  findall(REF0#CLASS0,
          containing(META,MOD,REF#CLASS,REF0#CLASS0),REFLIST,END).


containingTree(META,MOD,REF#CLASS,REF#CLASS-REFSTRUCT):-
  containingList(META,MOD,REF#CLASS,REFLIST,[]),
  maplist(containingTree(META,MOD),REFLIST,REFSTRUCT).


callArg(MOD,ENT/AR#REF,NTH,VALUE):-
  length(ARGS,AR), CALL =.. [ENT,#REF|ARGS],
  current_predicate(ENT,MOD:CALL), call(MOD:CALL),
  nth1(NTH,ARGS,ARG),
  (is_list(ARG)->once(member(VALUE,ARG)); VALUE=ARG).

%contained(ifcMeta,ac20,X-NTH,#(1329,ifcLocalplacement)).
%contained(ifcMeta,ac20,(REF#ENT)-NTH,2261#ifcExtrudedareasolid).
%contained(ifcMeta,ac20,(REF#ENT)-NTH,22622#ifcProductiondefinition).

%containsTree(META,MOD,REF0#CLASS0,REF0#STRUCT)
%collects a substructure of an instance, using the metamodel
%
%+META:       metamodel, as a Prolog module
%+MOD:        model, as a Prolog module
%+REF0:       instance ID
%+CLASS0:     class (class) name
%-REF0#STRUCT:root instance ID with substructure
%
%with >0 multiplicity ([CLASS], is_list(REF0))
%
%containsTree(ifcMeta,ac20,3#ifcOrganization,3#X),
%displayContainsTree(3#X)
%containsTree(ifcMeta,ac20,49#ifcUnitassignment,49#X)
%containsTree(ifcMeta,ac20,1261#ifcRepresentation,X).

containsTree(_META,_MOD,EMPTY#_CLASS0,EMPTY):-
  EMPTY == [], !; var(EMPTY), !.
containsTree(META,MOD,[REF0|REFS]#CLASS0,[REFSTRUCT|REFSTRUCTS]):-
  !, containsTree(META,MOD,REF0#CLASS0,REFSTRUCT),
    containsTree(META,MOD,REFS#CLASS0,REFSTRUCTS).
containsTree(META,_,REF0#TYPE,REF0):- META:type(TYPE,_,_), !.
containsTree(META,_,MEMBER#ENUM,MEMBER):- META:enum(ENUM,MEMBERS),
  member(MEMBER,MEMBERS), !.
containsTree(META,MOD,REF0#CLASS0,REF0#STRUCT):-
  integer(REF0),
  findall(REFSTRUCT,
            (contains(META,MOD,REF0#CLASS0/_AR-_,REF#CLASS/_),
             once(anyInstance(META,MOD,CLASS,REF#CLASS1/AR)),
             containsTree(META,MOD,REF#CLASS1,REFSTRUCT)),
        STRUCTS),
  STRUCTS \== [], !, STRUCT =.. [CLASS0|STRUCTS] .
containsTree(META,MOD,REF0#CLASS0,REF0#STRUCT):-
  anyInstance(META,MOD,CLASS0,REF0#CLASS), !,
  containsTree(META,MOD,REF0#CLASS,REF0#STRUCT).


writeTab(N):-
  sub_atom("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t",
                       1,N,_,TAB), write(TAB).

displayMetaClassRef(INDENT,REF0#CLASS):-
  atom(CLASS), !, writeTab(INDENT), write(REF0#CLASS).

notcompound(TERM):- \+ compound(TERM).

displayContainsTree(INDENT,REF#CLASS):- atom(CLASS), !,
  displayMetaClassRef(INDENT,REF#CLASS).
displayContainsTree(INDENT,VAR):-
  var(VAR), !, writeTab(INDENT), writeln('_').
displayContainsTree(INDENT,ATOMIC):-
  atomic(ATOMIC), !, writeTab(INDENT), writeln(ATOMIC).
displayContainsTree(INDENT,REF#STRUCT):-
  compound(STRUCT), !, STRUCT =.. [CLASS|ARGS],
  displayMetaClassRef(INDENT,REF#CLASS),
  (maplist(notcompound,ARGS) -> writeln(ARGS);
  maplist(maplist(atomic),ARGS) -> maplist(writeln,ARGS);
  nl, IN is INDENT+1, displayContainsTree(IN,ARGS)).
displayContainsTree(INDENT,STRUCTS):-
  is_list(STRUCTS), !,
    maplist(displayContainsTree(INDENT),STRUCTS).

displayContainsTree(REF0#STRUCT):- displayContainsTree(0,REF0#STRUCT).

:- if(false).
partRoot(META,NAME/ARITY):-
    META:class(NAME/ARITY,_,_,_,_,_,_),
    \+ (META:class(_,_,_,_,TYPES,_,_), member(TYPE,TYPES),
        subMetaClass(META,TYPE/_,NAME/_)).
:- endif.

%erroneousEnum(+META,+ENUM,+ENUMLIST,-ERR,-ERRMSG).
%+META:       module of the the metamodel
%+ENUM:       name of the enum
%+ENUMLIST:   definition of the type
%true, ERR=true ==> string(ERRMSG) ==> error was detected
%false ==> no error was detected
erroneousEnum(META,ENUM,ENUMLIST,ERR,ERRMSG):-
    is_list(ENUMLIST), !, \+ forall(member(ITEM,ENUMLIST),atom(ITEM)),
      ERR=true, format(atom(ERRMSG),
                "Enum ~w has an erroneous definition: ~w",[META:ENUM,ENUMLIST]).

erroneousEnumInstance(NAME,VALUE,LIST,ERR,MESSAGE):-
    \+ member(VALUE,LIST)->ERR=true,
       format(atom(MESSAGE), "~w: ~w should be in ~w",[NAME,VALUE,LIST]).


%erroneousType(+META,+TYPE,+DEFINITION,-ERR,-ERRMSG).
%+META:       module of the the metamodel
%+TYPE:       name of the type
%+DEFINITION: definition of the type
%true, ERR=true ==> string(ERRMSG) ==> error was detected
%false ==> no error was detected
erroneousType(META,TYPE,DEF,ERR,ERRMSG):-
    atom(TYPE), atom(DEF), !, \+ META:primitive(DEF),
      ERR=true, format(atom(ERRMSG),
                       "Type ~w definition ~w is illegal",[META:TYPE,DEF]).
erroneousType(META,TYPE,{DEF},ERR,ERRMSG):-
    atom(TYPE), atom(DEF), !, \+ META:primitive(DEF),
      ERR=true, format(atom(ERRMSG),
                       "Type ~w definition ~w is illegal",[META:TYPE,{DEF}]).
erroneousType(META,TYPE,DEF,ERR,ERRMSG):-
    atom(TYPE), DEF=..[MAIN,_SUBTYPE],
    \+ (META:primitive(MAIN), true),%integer(SUBTYPE)),
      ERR=true, format(atom(ERRMSG),
                       "Type ~w definition ~w is illegal",[META:TYPE,DEF]).


erroneousAtom(NAME,ERR,MESSAGE):-
    \+ atom(NAME)->ERR=true,
       format(atom(MESSAGE),"~w is not a proper metaclass ID",[NAME]).

unknownMetaClass(META,CONTEXT,CLASS,ERR,ERRMSG):-
    CLASS \= '', \+ META:class(CLASS/_,_,_,_,_,_,_)->ERR=true,
       format(atom(ERRMSG),"~w: Metaclass ~w is undefined",[CONTEXT,CLASS]).


unknownType(META,NAME/ARITY,[TYPE],ERR,ERRMSG):-
    !, unknownType(META,NAME/ARITY,TYPE,ERR,ERRMSG).
unknownType(META,NAME/ARITY,TYPES,ERR,ERRMSG):-
    is_list(TYPES), !, member(TYPE,TYPES),
    unknownType(META,NAME/ARITY,TYPE,ERR,ERRMSG).
unknownType(META,NAME/ARITY,TYPE,ERR,ERRMSG):-
    \+ metaTypeDefined(META,TYPE/_)->ERR=true,
       format(atom(ERRMSG),"~w: Metatype ~w is undefined",
              [META:NAME/ARITY,TYPE]).


checkCall(X^CALL,X):- !, CALL.
checkCall(CALL,_):- CALL.


erroneousAttributes(META,NAME/ARITY,ATTLABELS,ATTYPES,ERR,ERRMSG):-
    \+ is_list(ATTLABELS) -> ERR=true,
       format(atom(ERRMSG),"~w: Attr labels ~w must be a list!",
              [NAME/ARITY,ATTLABELS]);
    \+ is_list(ATTYPES) -> ERR=true,
       format(atom(ERRMSG),"~w: Attr types ~w must be a list!",
              [NAME/ARITY,ATTLABELS]);
    member(ATTYPE,ATTYPES), unknownType(META,NAME/ARITY,ATTYPE,ERR,ERRMSG), !;
    (metaClassAttributes(META,NAME/ARITY,LABELS,TYPES,_,_,_LN)->
      length(TYPES,LNTYPES), length(LABELS,LNLABELS),
      (LNTYPES=\=LNLABELS -> ERR=true,
         format(atom(ERRMSG),
         "~w: Attrtypes and labels must be equal long: ~w<>~w",
              [NAME/ARITY,LNLABELS,LNTYPES]);
      LNTYPES=\=ARITY -> ERR=true,
        format(atom(ERRMSG),
        "~w: Arity error - collected: ~w",
              [NAME/ARITY,LNTYPES]));
    ERR=true,format(atom(ERRMSG),
          "~w: attribute inheritance problem (ancestors?)",[NAME/ARITY])).

checkMetaClass(META,NAME/ARITY):-
    META:class(NAME/ARITY,KIND,SUPER,LABELS,TYPES,_CONDLIST,_COND),
    %check if the definition is unique
    findall(K,metaClassKind(K),LIST),
    (findall(NAME/ARITY,META:class(NAME/ARITY,_,_,_,_,_,_),EL),
      EL=[_,_|_]->format("~w: ambiguous definition!\n",[NAME/ARITY]);
    erroneousAtom(NAME,ERR,ERRMSG), ERR==true->writeln(ERRMSG);
    erroneousAtom(SUPER,ERR,ERRMSG), ERR==true->writeln(ERRMSG);
    erroneousEnumInstance(NAME,KIND,LIST,ERR,ERRMSG),
      ERR==true->writeln(ERRMSG);
    metaClassAttributes(META,NAME/ARITY,LS,_TS,_,_,LN),
      (length(LS,LLS), LLS =\= LN ->
      format("~w: Different calculated and collected property length ~w ~w",
             [NAME/ARITY,LLS,LN]);
      ARITY =\= LN ->
      format("~w: Different arity and collected property length: ~w",
             [NAME/ARITY,LN]));
    SUPER \= '', unknownType(META,NAME/ARITY,SUPER,ERR,ERRMSG), ERR==true ->
      writeln(ERRMSG);
    NAME==SUPER ->
      format("~w: cyclic inheritance from ~w",[NAME/ARITY,SUPER]);
    erroneousAttributes(META,NAME/ARITY,LABELS,TYPES,ERR,ERRMSG), ERR==true
      -> writeln(ERRMSG);
    true).

checkMetaClass(META,FUNCTORS):- is_list(FUNCTORS), !,
    maplist(checkMetaClass(META),FUNCTORS).


anyInstance(_META,_MOD,CLASS,EMPTY#CLASS/_AR):- var(EMPTY), !.
anyInstance(META,MOD,CLASS,REFLIST#TYP/AR):- is_list(REFLIST), !,
  forall(member(REF,REFLIST),anyInstance(META,MOD,CLASS,REF#TYP/AR)),
  once(member(REF,REFLIST)),anyInstance(META,MOD,CLASS,REF#TYP/AR).
anyInstance(META,_,TYPE,_VALUE#TYPE/0):- META:type(TYPE,_,_), !.
anyInstance(META,_,ENUM,VALUE#ENUM/0):- META:enum(ENUM,LIST), !,
  member(VALUE,LIST).
anyInstance(META,MOD,CLASS,REF#TYP/AR):-
  subMetaClass(META,TYP/AR,CLASS/_ARITY),
    metaClassAttributes(META,TYP/AR,_L,_T,_CALLS,_CALL,_),
    length(PARAMS,AR), HEAD =.. [TYP,#REF|PARAMS],
    current_predicate(TYP,MOD:HEAD),
    call(MOD:HEAD), !.

erroneousAttribute(META,_MOD,NAME/ARITY,ARG,LABEL,
                  TYPE,_CALL,ERR,MESSAGE):-
  META:type(TYPE,_,TYPECALL), !,
    \+ checkCall(TYPECALL,ARG) -> ERR=true,
       format(atom(MESSAGE),"~w: Metaclass attribute ~w check ~w fails.",
       [NAME/ARITY-LABEL,TYPE:ARG,TYPECALL]).
erroneousAttribute(META,_MOD,NAME/ARITY,ARG,LABEL,
                  ENUM,_CALL,ERR,MESSAGE):-
  META:enum(ENUM,ENUMLIST), !,
    \+ member(ARG,ENUMLIST) -> ERR=true,
       format(atom(MESSAGE),"~w: Enumeration member ~w missing from ~w .",
       [NAME/ARITY-LABEL,ARG,ENUM:ENUMLIST]).
erroneousAttribute(META,MOD,NAME/ARITY,ARG,LABEL,
                  TYPE,CALL,ERR,MESSAGE):-
  \+ checkCall(CALL,ARG) -> ERR=true,
       format(atom(MESSAGE),"~w: Improper attribute ~w:~w",
              [NAME/ARITY-LABEL,TYPE,ARG]);
  ARG = (#REF), \+ anyInstance(META,MOD,TYPE,REF#_TYP/_AR) -> ERR=true,
       format(atom(MESSAGE),
              "~w: Instance ~w not defined or unknown.",[TYPE,ARG]).

erroneousAttributes(META,MOD,NAME/ARITY,[ARG|ARGS],[LABEL|LABELS],
                  [TYPE|TYPES],[CALL|CALLS],ERR,MESSAGES):-
  erroneousAttribute(META,MOD,NAME/ARITY,ARG,LABEL,TYPE,CALL,ERR,MSG)
    ->
    erroneousAttributes(META,MOD,NAME/ARITY,ARGS,LABELS,
                  TYPES,CALLS,ERR,MSGS), MESSAGES = [MSG|MSGS];
  erroneousAttributes(META,MOD,NAME/ARITY,ARGS,LABELS,
                  TYPES,CALLS,ERR,MESSAGES).


erroneousInstance(META,MOD,REF#(CLASS/ARITY),ARGS,ERR,MESSAGE):-
  length(ARGS,ARITY), HEAD=..[CLASS,#REF|ARGS],
  ( \+ current_predicate(CLASS,MOD:HEAD)-> ERR=true,
       format(atom(MESSAGE),'~w unknown metaclass!',[CLASS/ARITY]);
    \+ call(MOD:HEAD) -> ERR=true,
       format(atom(MESSAGE),'~w#~w unknown instance!',[REF,CLASS/ARITY]);
  call(MOD:HEAD),
  metaClassAttributes(META,CLASS/ARITY,LABELS,TYPES,CALLS,CALL,_),
    (erroneousAttributes(META,MOD,CLASS/ARITY,
                         ARGS,LABELS,TYPES,CALLS,ERR,MESSAGE)->true;
     \+ checkCall(CALL,HEAD),ERR = true,
    format("~w#~w Structure test failed",[CLASS/ARITY,REF]))).


%+MOD:        module name
%?NAME/ARITY: metaclass in target format
%?REF:        instance ID
%?ARGS:       list of object arguments
forEachClause(MOD,NAME/ARITY,REF,ARGS):-
 current_module(MOD), current_predicate(_,MOD:HEAD), clause(MOD:HEAD,_BODY),
 functor(HEAD,NAME,A), ARITY is A-1, HEAD =.. [NAME,REF|ARGS].

forEachPredicate(MOD,NAME/ARITY):-
 current_module(MOD), current_predicate(_,MOD:HEAD),
 functor(HEAD,NAME,A), ARITY is A-1.

% each clause that is defined as an class/7
forEachClassClause(META:MOD,NAME/ARITY,CLAUSE):-
    current_module(MOD), META:class(NAME/ARITY,_,_,_,_,_,_),
    ARITY1 is ARITY+1, functor(HEAD,NAME,ARITY1),
    clause(MOD:HEAD,BODY),
    (BODY == true -> CLAUSE=HEAD; CLAUSE=(HEAD:-BODY)).


%
% The predicate checks the metamodel against the model
% that is given in the parameter
% if there exist a metadefintion for each model clauses
%checkModule(+META,+MOD)
% +META: the metamodel as a Prolog module
% +MOD: the model, as a Prolog module
%
checkModuleMeta(META,MOD):-
    forEachPredicate(MOD,NAME/ARITY),
    %maybe it is better so? forEachClause(MOD,NAME/ARITY,_,_),
    (META:class(NAME/ARITY,_,_,_,_,_,_)->
      format("METACLASS ~w ...\n",[NAME/ARITY]),false;
    format("**** ~w: missing metaclass description\n",
           [META:NAME/ARITY]), false);
    true.


%
%The predicate checks the model that is given in the parameter
% - using the checking rules, given in the metamodel
%checkModule(+META,+MOD)
%+META:   the metamodel as a Prolog module
%+MOD:    the model, as a Prolog module
%
checkModule(META,MOD):-
    forEachClause(MOD,NAME/ARITY,#REF,ARGS),
    (META:class(NAME/ARITY,_,_,_,_,_,_)->
      format("METACLASS ~w#~w is checked...\n",[REF,NAME/ARITY]),
      erroneousInstance(META,MOD,REF#NAME/ARITY,ARGS,ERR,MESSAGE),
      (ERR=true->writeln(MESSAGE),false);
    format("**** ~w: missing metaclass description\n",
           [META:NAME/ARITY]), false);
    true.

:-writeln(":-checkModule(ifcMeta,ac20).").

%
%Checks the metamodel that is given in the parameter
%checkMeta(+META)
%+META:  metamodel as Prolog module
%
checkMeta(META):-
  META:type(TYPE,DEF,_CALL), format("METATYPE ~w is checked.\n",[META:TYPE]),
    erroneousType(META,TYPE,DEF,ERR,MSG),
    ERR==true, format("TYPE ~w: ~w\n",[META:TYPE,MSG]), fail;
  META:enum(ENUM,_), format("METAENUM ~w is checked.\n",[META:ENUM]),
    erroneousEnum(META,ENUM,_LIST,ERR,MSG),
    ERR==true, format("ENUM ~w: ~w\n",[META:ENUM,MSG]), fail;
  META:class(N/A,_,_,_,_,_,_), format("METACLASS ~w is checked.\n",[META:N/A]),
  checkMetaClass(META,N/A), fail; true.

:-writeln(":-checkMeta(ifcMeta).").


bind(MOD,STRUCT):- nonvar(STRUCT), functor(STRUCT,NAME,_AR),
  current_predicate(NAME,MOD:STRUCT),
    \+ predicate_property(MOD:STRUCT,imported_from(_)),
    call(MOD:STRUCT).

%meta(ac20,31139#ifcLocalplacement<=>STRUCT).
%meta(ac20,31139#ifcLocalplacement(X,Y)<=>STRUCT).
%meta(ac20,104#ifcLocalplacement(X,Y)<=>STRUCT).
meta(MOD,REF#STRUCT<=>RIGHT):- !,
  STRUCT =.. [CLASS|ARGS], LEFT =.. [CLASS,#REF|ARGS],
  meta(MOD,LEFT<=>RIGHT).
meta(MOD,[X1:LIST1,X2:LIST2]^CALL):-
  is_list(LIST1), !, findall(X2,(member(X1,LIST1),
                              meta(MOD,CALL)),LIST2).
meta(MOD,[X1:LIST1,X2:LIST2]^CALL):-
  is_list(LIST2), !, findall(X1,(member(X2,LIST2),
                              meta(MOD,CALL)),LIST1).
meta(MOD,LEFT <=> RIGHT):- !, clause(ifcMeta:(LEFT<=>RIGHT),BODY),
  ignore(bind(MOD,LEFT)), ignore(bind(MOD,RIGHT)), meta(MOD,BODY).
meta(MOD,(CALL,BODY)):- !, meta(MOD,CALL), meta(MOD,BODY).
meta(_MOD,CALL):- call(CALL).

%transMeta(+META,+MOD0,+MOD)
transMeta(META,MOD0,MOD):-
  true.

:-begin_tests(metadriven,[setup(loadAC20)]).

loadAC20:-
  current_module(ac20)->true;
  writeln("Loading the IFC file! it is not a quick business!"),
  loadIFC('AC20-Institute-Var-2.ifc',ac20,100).

test(subMetaClass,set(SUB=[ifcSolidmodel/0
                        ,ifcSweptareasolid/2
                        ,ifcExtrudedareasolid/4
                        ,ifcManifoldsolidbrep/1
                        ,ifcFacetedbrep/1
                       ])):-
  subMetaClass(ifcMeta,SUB,ifcSolidmodel/0).

test(subMetaClass,set(SUB=[ifcPlacement/1,
                        ifcAxis2placement2d/2,ifcAxis2placement3d/3])):-
  subMetaClass(ifcMeta,SUB,ifcPlacement/_).

test(supMetaClass,all(SUPER=[ifcRepresentationitem/0
                           ,ifcGeometricrepresentationitem/0
                           ,ifcPlacement/1
                           ,ifcAxis2placement2d/2])):-
  supMetaClass(ifcMeta,ifcAxis2placement2d/2,SUPER).

test(metaClassAttributes,[nondet,true((A=3,
                       LS=[name,description,representation],
                       TS=[ifcLabel,ifcText,[ifcRepresentation]],
                       C=true))]):-
  metaClassAttributes(ifcMeta,ifcProductrepresentation/A,LS,TS,_CS,C,_).

test(metaClassAttributes,[nondet,true((A=12,
                       LS=[globalID,ownerHistory,name,description,objectType,
                          objectPlacement,representation,longName,
                          compositionType,elevationOfRefHeight,
                          elevationOfTerrain,buildingAddress],
                       TS=[ifcGloballyuniqueid, ifcOwnerhistory, ifcLabel,
                          ifcText,ifcLabel,ifcObjectplacement,
                          ifcProductrepresentation,ifcLabel,
                          ifcElementCompositionEnum,ifcLengthMeasure,
                          ifcLengthMeasure,ifcPostaladdress],
                       C=true))]):-
  metaClassAttributes(ifcMeta,ifcBuilding/A,LS,TS,_CS,C,_).



test(contains,[nondet,X-NTH=[18106]#ifcFace/1-1]):-
  contains(ifcMeta,ac20,18108#ifcOpenshell/_A-NTH,X).

test(contains,all(NTH-X=[1-(104#ifcLocalplacement/2),
                         2-(1328#ifcAxis2placement3d/3)])):-
   contains(ifcMeta,ac20,1329#ifcLocalplacement/_A-NTH,X).

test(contained,set(X-NTH=[22611#ifcExtrudedareasolid-1])):-
   contained(ifcMeta,ac20,X-NTH,22599#ifcArbitraryclosedprofiledef).

test(contained,all(X-NTH=[1306#ifcRelaggregates-5,
                         93#ifcReldefinesbyproperties-5])):-
   contained(ifcMeta,ac20,X-NTH,66#ifcProject).


test(containsTree,X=ifcOrganization(_,"Nicht definiert",_,_,_)):-
   containsTree(ifcMeta,ac20,3#ifcOrganization,3#X).

test(containsTree,X=added):-
   containsTree(ifcMeta,ac20,added#ifcChangeActionEnum,X).

test(containsTree,X=12#ifcOwnerhistory(_,_,_,_,_,_,_,_)):-
     containsTree(ifcMeta,ac20,12#ifcOwnerhistory,X).

test(containsTree,X=101#ifcCartesianpoint([0,0,0])):-
  containsTree(ifcMeta,ac20,101#ifcCartesianpoint,X).

test(containsTree,X=103#ifcAxis2placement3d(_,_,_)):-
  containsTree(ifcMeta,ac20,103#ifcAxis2placement3d,X).

test(containsTree,X=99#ifcDirection([0,0,1])):-
  containsTree(ifcMeta,ac20,99#ifcDirection,X).

test(containsTree,X=1331#ifcBuilding(_,_,_,_,_,_,_,_,_,_,_,_)):-
  containsTree(ifcMeta,ac20,1331#ifcBuilding,X).

test(containsTree,X=1289#ifcProductdefinitionshape(_,_,_)):-
  containsTree(ifcMeta,ac20,1289#ifcProductdefinitionshape,X).

test(containsTree,X=117#ifcFaceouterbound(_,_)):-
  containsTree(ifcMeta,ac20,117#ifcFaceouterbound,X).

%containsTree(ifcMeta,ac20,17058#ifcOpenshell,[......]#17058).

test(supMetaClassAttributes,all(NAME/ARITY-LABELS-TYPES-CALLS=[
                               ifcRepresentationitem/0-[]-[]-[]
                               ,ifcGeometricrepresentationitem/0-[]-[]-[]
                               ,ifcPlacement/1-[location]-
                                  [ifcCartesianpoint]-[true]
                               ,ifcAxis2placement3d/3-
                                  [axis,refDirection,location]-
                                  [ifcDirection,ifcDirection,ifcCartesianpoint]
                                  -[true,true,true]
                                                       ])):-
  supMetaClassAttributes(ifcMeta,ifcAxis2placement3d/_,
                        NAME/ARITY,LABELS,TYPES,CALLS,_CALL).

test(anyInstance,T/A=ifcOwnerhistory/8):-
  anyInstance(ifcMeta,ac20,ifcOwnerhistory,12#T/A).

test(anyInstance,T/A==ifcGeometricrepresentationcontext/6):-
  anyInstance(ifcMeta,ac20,ifcGeometricrepresentationcontext,62#T/A).

test(anyInstance,T/A==ifcGeometricrepresentationcontext/6):-
  anyInstance(ifcMeta,ac20,ifcRepresentationcontext,62#T/A).

test(containingTree,ST=66#ifcProject-[1306#ifcRelaggregates-[],
                                      93#ifcReldefinesbyproperties-[]]):-
     containingTree(ifcMeta,ac20,66#ifcProject,ST).


test(polyloop,STRUCT=polygon([50/19/ -0.15, -8/27/ -0.15, -8/19/ -0.15])):-
  meta(ac20,ifcPolyloop(#141,_Y)<=>STRUCT).

test(polyloop,STRUCT=polygon([50/27/ -0.15,-8/27/ -0.15, 50/19/ -0.15])):-
  meta(ac20,ifcPolyloop(#115,_X)<=>STRUCT).


test(polyline,STRUCT=polyline([0/0,42/0,42/12,24/12,
                               24/14,18/14,18/12,0/12,0/0])):-
  meta(ac20,ifcPolyline(#22597,_X)<=>STRUCT).

test(organization,(X="GRAPHISOFT")):-
  meta(ac20,ifcOrganization(#10,ID,N,D,RS,AS)<=>X).

test(application,(X-FN-R-V = "GRAPHISOFT"-"ARCHICAD-64"- (#10)-"20.0.0")):-
  meta(ac20,ifcApplication(#11,R,V,FN,_ID)<=>X).


test(ownerHistory,(R-APP-ST-CH-X=
                  (#7)-(#11)-{added}-1484145520-"GRAPHISOFT")):-
     meta(ac20,ifcOwnerhistory(#12,R,APP,ST,CH,MOD,MU,MA,CR)<=>X).


test(building,(X=signature("GRAPHISOFT","Buerogebaeude"))):-
  meta(ac20,ifcBuilding(#1331,ID,H,N,D,T,PL,R,LO,CT,ELR,ELT,ADDR)<=>X).

:-end_tests(metadriven).























