﻿:- module(lazytokens,
	  [    init_regexp/0
	      ,set_regexp/1, set_regexp/2
	      ,stream_to_lazy_lines/2
	      ,stream_to_lazy_tokens/2
	      ,string_to_tokens/2
	  ]).

/** <module> Creates a lazy list from a file stream
 The module resembles to various lazy list implementations.
 The input stream is handled as a lazy list of tokens,
 whose end is a delayed condition to fill it up from a
 given file (of course only until there are still
 some tokens in the file).
 For token recognition the swi-prolog regular expression library
 is used (library(pcre))
 Default token definitions can be modified if necessary.

 @author Kilián Imre 2018.
 */

:- use_module(library(pcre)).

%! set_regexp(+PATTERN:string, +WHITESPACE:string) is det.
%! set_regexp(+PATTERN:string) is det.
%  Resets the token (and the whitespace) definition
%  as a regexp pattern. The definition must conform
%  the library(pcre)  /Perl regular expression/ syntax
%
% @arg PATTERN:string - regular expression to describe tokens
% @arg WHITESPACE:string - regular expression of whitespaces
% WHITESPACES should delimit PATTERNS, therefore they should
% exclude each other.
% On the other hand PATTERN should describe valued/significant tokens
% If something doesn't match neither WHITESPACE nor PATTERN
% then they are split character by character.
% That is special characters are not necessarily to be defined
% in PATTERN, unless there are agglutinant specials...
%
set_regexp(PATTERN,WHITESPACE):-
	 re_compile(PATTERN,TBLOB,[anchored(true)]),
	 nb_setval(regexp,TBLOB),
	 re_compile(WHITESPACE,WBLOB,[anchored(true)]),
	 nb_setval(whitespace,WBLOB).

%set_regexp(PATTERN):- set_regexp(PATTERN,"[\\n\\t\\r\\f ]+").
set_regexp(PATTERN):- set_regexp(PATTERN,"[\\t\\f ]+").

% decimal numbers with decimal point (floats)|
% decimal numbers without decimal point |
% visible letters at least one, plus ', plus 's' |
% visible letters or Hungarian/German accented letter at least one
init_regexp:-
%set_regexp("\\d+\\.\\d*|\\d+|\c
%           \\w+'s?|\c
%	   [\\w|á|Á|é|É|í|Í|ó|Ó|ö|Ö|ő|Ő|ú|Ú|ü|Ü|ű|Ű]+").
%set_regexp("\\d+\\.\\d*|\\d+|\c
%           \\w+'?|\c
%	   [\\w|ä|Ä|á|Á|é|É|í|Í|ó|Ó|ö|Ö|ő|Ő|ú|Ú|ü|Ü|ű|Ű]+").
set_regexp("\\d+\\.\\d*|\\d+|\c
	   [\\w|ß|ä|Ä|á|Á|é|É|í|Í|ó|Ó|ö|Ö|ő|Ő|ú|Ú|ü|Ü|ű|Ű]+'?").
%+++ recognizes accented characters and scientific fractional numbers
%set_regexp("\\d+\\.\\d*([Ee][-]\\d+)?|\\d+|\c
%	   [\\w|ß|ä|Ä|á|Á|é|É|í|Í|ó|Ó|ö|Ö|ő|Ő|ú|Ú|ü|Ü|ű|Ű]+'?").

optional_init_regexp:-
  nb_current(regexp, _), nb_current(whitespace,_)-> true;
  init_regexp.


:- initialization init_regexp.
:- thread_initialization init_regexp.


%succeeds if STREAM is at end of line, and also reads it
end_of_line(STREAM):- at_end_of_stream(STREAM), !.
end_of_line(STREAM):- peek_char(STREAM,'\n'), get_char(STREAM,'\n'), !.
end_of_line(STREAM):- peek_char(STREAM,'\r'), get_char(STREAM,'\r'),
	 ignore((peek_char(STREAM,'\n'), get_char(STREAM,'\n'))).

readNextLine(STREAM,[EOL]):- end_of_line(STREAM), char_code('\n',EOL), !.
readNextLine(STREAM,[CODE|LINE]):-
  get_code(STREAM,CODE),
  readNextLine(STREAM,LINE).

%
%LINES: list of strings, being read from Stream
readToEol(STREAM,POS,LINES):-
  set_stream_position(STREAM,POS),
  (at_end_of_stream(STREAM)->LINES=[];
   readNextLine(STREAM,CHARS),string_chars(LINE,CHARS),
   LINES=[LINE|TAIL], stream_to_lazy_lines(STREAM,TAIL)).

%! stream_to_lazy_lines(+STREAM:expr, -LINES:list) is det.
%
% Creates a lazy list representing the lines in Stream.
% Lines is a list that ends in a delayed goal. Lines can be
% unified completely transparently to a (partial) list and processed
% transparently using DCGs, but please be aware that a lazy list
% is not the same as a materialized list in all respects.
%
%@arg STREAM stream object to read tokens from...
%@arg LINES the lazy list of strings, representing the lines...
%
stream_to_lazy_lines(STREAM,LINES):-
  stream_property(STREAM,position(POS)),
  freeze(LINES,readToEol(STREAM,POS,LINES)).



%! string_to_tokens(+STRING:string, -TOKENS:list) is det.
% Creates a list representing the tokens (words) in STRING.
% TOKENS is a complete list. The operation is otherwise
% the same as stream_to_lazy_tokens/2
string_to_tokens(STRING,TOKENS):-
     readTokens([STRING],TOKENS).

%! stream_to_lazy_tokens(+STREAM:expr, -TOKENS:list) is det.
%
% Creates a lazy list representing the tokens (words) in
% STREAM. TOKENS is a
% list that ends in a delayed goal. Tokens can be unified
% completely transparent to a (partial) list and processed
% transparently using DCGs, but please be aware that a lazy list
% is not the same as a materialized list in all respects.
%
% Typically, this predicate is used as a building block for more
% high level safe predicates such as phrase_from_file/2.
%
% The exact syntax of tokens and whitespaces can be defined by
% set_regexp/1 or set_regexp/2 predicates.
% Default:
% TOKEN=~"\\d+\\.\\d+|\\d+|[\\w|á|Á|é|É|í|Í|ó|Ó|ö|Ö|ő|Ő|ú|Ú|ü|Ü|ű|Ű]+").
% WHITESPACE=~"[\\n\\t\\r\\f ]+"
% (TOKEN conforms more or less to the Hungarian letter set)
%
%@arg STREAM stream object to read tokens from...
%@arg TOKENS the lazy list, representing the tokens...
%

stream_to_lazy_tokens(CH,TOKENS):-
  stream_to_lazy_lines(CH,LINES),
  freeze(TOKENS,readTokens(LINES,TOKENS)).

readTokens(LINES,TOKENS):-
  LINES=[]->TOKENS=[];
  LINES=[""|REST]->readTokens(REST,TOKENS);
  LINES=[LINE0|REST], nextToken(TOKEN,LINE0,LINE),
    TOKENS=[TOKEN|RESTTOKENS], skipWhitespace(LINE,LIN),
    freeze(RESTTOKENS,readTokens([LIN|REST],RESTTOKENS)).

skipWhitespace(LINE0,LINE):-
   nb_getval(whitespace,WHITE),
   (re_matchsub(WHITE,LINE0,DICT,[anchored(true),bol(true)])->W=DICT.0,
      string_concat(W,LINE,LINE0);
    LINE=LINE0).

%stringAtomParen('\'').	using this, (dog's) possessives dont work
stringParen("\"").
stringParen("\'").

%reads the next token from
%LINE: string, the actual line content
%TOKEN: string, the token read
%REST: string, the remaining line content
nextToken(TOKEN,LINE,REST):-
   skipWhitespace(LINE,LINE0), sub_string(LINE0,0,1,_,CHS),
   (stringParen(CHS),
     split_string(LINE0,CHS,"",["",ATOM|_])->
     atomics_to_string([CHS,ATOM,CHS],TOKEN), string_concat(TOKEN,REST,LINE0);
   nb_getval(regexp,REGEXP),
   re_matchsub(REGEXP,LINE0,DICT,[anchored(true),bol(true)]),
     SYMBOL=DICT.0, string_concat(SYMBOL,REST,LINE0),
       string_chars(ATOM,SYMBOL), ATOM=TOKEN, !;
      TOKEN=CHS, string_concat(CHS,REST,LINE0)).


testTokens(FILE):- exists_file(FILE), !,
        open(FILE,read,In),
	stream_to_lazy_tokens(In,Tokens),
	forall(member(Token,Tokens), writef('%w ',[Token])), close(In).
testTokens(STRING):-   string(STRING), !,
	string_to_tokens(STRING,Tokens),
	forall(member(Token,Tokens), writef('%w ',[Token])).

testTokens:- testTokens("a 12.34 mesterlövész? \t\n\c
                        ...és a 40 män's rabló Ali-baba!!").

testLines(FILE):- exists_file(FILE), !,
        open(FILE,read,In),
	stream_to_lazy_lines(In,Lines),
	forall(member(Line,Lines), write(Line)), close(In).


















